package com.xbdl.flalexaclock

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import java.text.DateFormat

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ExampleUnitTest {
    @Test
    @Throws(Exception::class)
    fun addition_isCorrect() {
        val list = listOf(1, 2, 3, 4, 5)
        val avg: Double = list.sum().toDouble() / list.count()
        assertTrue(list.any { it % 2 == 0 })
        assertTrue(list.any { it < 10 })
        assertEquals(list.size, list.count())
        assertEquals(15 + 1, list.fold(1) { total, next -> total + next })
        assertEquals(5, list.max())
        assertEquals(1, list.min())
        assertTrue(avg == list.average())
        assertEquals(1, list.maxBy { -it })
        assertEquals(5, list.minBy { -it })
        assertEquals(true, list.none { it > 7 })
        assertEquals(15, list.reduceRight { total, next -> total + next })

        assertEquals(6, list.sumBy { it / 2 })

        println(Pair(listOf(5, 6), listOf(7, 8)))
        println(listOf(Pair(1, 7), Pair(4, 8)))
        println(list.zip(listOf(7, 9, 4, 5, 6)))

        DateFormat.MEDIUM

        val a: Int? = 2
        println(a?.toString() ?: "1")

        assertEquals(4, (2 + 2).toLong())
    }



}