package com.xbdl.flalexaclock

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    @Throws(Exception::class)
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.xbdl.flalexaclock", appContext.packageName)
    }

    @Test
    @Throws(Exception::class)
    fun testDayLight() {
        val timeZoneList = TimeZoneTableUtils.codeTable

        timeZoneList.forEach {
            val tz = TimeZone.getTimeZone(it.timeZoneId)

            // 当前系统默认时区的时间：
            val calendar = GregorianCalendar()
            calendar.timeZone = tz

            if (tz.inDaylightTime(calendar.time)) {
                println(" ${it.cityName}  --> $tz --> ${tz.inDaylightTime(calendar.time)} ")
            }
        }

        assertEquals(1, 1 - 0)
    }

    @Test
    @Throws(Exception::class)
    fun printTimeZoneIds() {
//        val zoneIds = ZoneId.getAvailableZoneIds()
        val zoneIds = TimeZone.getAvailableIDs()
        println("avaliableTimeZone: " + zoneIds.joinToString("\n"))
    }
}
