package com.xbdl.flalexaclock.loginterminator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.xbdl.flalexaclock.R;
import com.xbdl.flalexaclock.base.GizBaseActivity;
import com.xbdl.flalexaclock.ui.user.LoginActivity;
import com.xbdl.flalexaclock.utils.ToastUtil;

/**
 * google账号登录
 * Created by xtagwgj on 2017/9/13.
 * http://blog.csdn.net/robin_java/article/details/52540711
 */

public class GooglePlusHelper {

    private static final String TAG = "GooglePlusHelper";

    private static GizBaseActivity loginActivity;
    private static GoogleApiClient mGoogleApiClient;

    public static void initGooglePlus(Activity activity, SignInButton sign_in_button, View clickView) {
        loginActivity = (GizBaseActivity) activity;
        GoogleSignInOptions gso = null;

        if (mGoogleApiClient == null) {
            gso = new GoogleSignInOptions
                    .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(activity.getString(R.string.google_server_client_id))
                    .requestEmail()
                    .requestId()
//                    .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                    .requestServerAuthCode(activity.getString(R.string.google_server_client_id))
                    .build();

            mGoogleApiClient = new GoogleApiClient
                    .Builder(activity)
                    .addConnectionCallbacks(new MyGoogleConnectionCallbacks())
                    .addOnConnectionFailedListener(new MyGoogleConnectionFaliedListeners())
                    .enableAutoManage((FragmentActivity) activity, new MyGoogleConnectionFaliedListeners())/* FragmentActivity *//* OnConnectionFailedListener */
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();

        }

        if (sign_in_button != null) {
            sign_in_button.setSize(SignInButton.SIZE_STANDARD);
            sign_in_button.setScopes(gso.getScopeArray());
            sign_in_button.setOnClickListener(new MyClickListener());
        }

        if (clickView != null) {
            clickView.setOnClickListener(new MyClickListener());
        }
    }

    private static class MyGoogleConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnected(@Nullable Bundle bundle) {
            Log.i(TAG, "google登录-->onConnected,bundle==" + bundle);
        }

        @Override
        public void onConnectionSuspended(int i) {
            Log.i(TAG, "google登录-->onConnectionSuspended,i==" + i);
        }
    }

    private static class MyGoogleConnectionFaliedListeners implements GoogleApiClient.OnConnectionFailedListener {

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.i(TAG, "google登录-->onConnectionFailed,connectionResult==" + connectionResult);
        }
    }

    private static class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Log.i(TAG, "点击了登录按钮");
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            loginActivity.startActivityForResult(signInIntent, LoginActivity.Companion.getREQUEST_CODE_GOOGLE_PLUS());
        }
    }

    public static void handleSignInResult(Intent data) {
        Log.i(TAG, "handleSignInResult执行了");
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result == null) {
            return;
        }
        if (result.isSuccess()) {
            Log.i(TAG, "handleSignInResult-->success");
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {
                /**click this link to see the detail doc of information
                 * <p href="https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInAccount</p>
                 */
                Log.i(TAG, "用户名是:" + acct.getDisplayName() + ",用户email是:" + acct.getEmail() + ",用户头像是:" + acct.getPhotoUrl() +
                        ",用户Id是:" + acct.getId() + ",用户Token是" + acct.getIdToken() + ",ServerAuthCode是" + acct.getServerAuthCode());


            }
        } else {
            ToastUtil.showToast(loginActivity, "failed,result is :" + result.getStatus());
        }

    }

    public static void initGooglePlusOnstart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public static void initGooglePlusOnstop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    public static void signOutGoogle(ResultCallback<Status> resultCallback) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(resultCallback);
        }
    }
}
