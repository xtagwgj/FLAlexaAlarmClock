package com.xbdl.flalexaclock.base

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Build
import android.view.KeyEvent
import android.view.View
import com.androidnetworking.error.ANError
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.google.gson.Gson
import com.jakewharton.rxbinding2.view.RxView
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.entity.GizBaseResponse
import com.xbdl.flalexaclock.ui.MainActivity
import com.xbdl.flalexaclock.ui.user.LoginActivity
import com.xbdl.flalexaclock.utils.StatusLightUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base._BaseActivity
import com.xtagwgj.baseproject.utils.AndroidDeviceUtil
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.view.loadingdialog.view.LoadingDialog
import com.xtagwgj.baseproject.widget.StatusBarUtil
import io.reactivex.functions.Consumer
import java.util.concurrent.TimeUnit


/**
 * 机智云的基类activity
 * Created by xtagwgj on 2017/6/16.
 */
abstract class GizBaseActivity : _BaseActivity() {

    private var loadingDialog: LoadingDialog? = null

    companion object {
        fun startByAnim(mContext: Activity, intent: Intent, requestCode: Int = -1, finishOld: Boolean = false) {
            if (requestCode != -1)
                mContext.startActivityForResult(intent, requestCode)
            else
                mContext.startActivity(intent)

            if (AndroidDeviceUtil.getSDKVersion() < Build.VERSION_CODES.M) {
                startAnim(mContext)
            }


            if (finishOld)
                AppManager.getAppManager().finishActivity(mContext)

        }

        fun destroyByAnim(mContext: Activity, requestCode: Int = -1) {
            if (requestCode != -1)
                mContext.finishActivity(requestCode)
            else
                mContext.finish()


            AppManager.getAppManager().finishActivity(mContext)

            if (AndroidDeviceUtil.getSDKVersion() < Build.VERSION_CODES.M) {
                stopAnim(mContext)
            }
        }

        private fun startAnim(mContext: Activity) {
            mContext.overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left)
        }

        private fun stopAnim(mContext: Activity) {
            mContext.overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right)
        }
    }

    override fun initStatusBar() {
//        StatusBarUtil.setTranslucent(this,122)
        if (!StatusLightUtils.tryLightStatus(this)) {
            StatusBarUtil.setColor(this, resources.getColor(R.color.black), 0)
        }
    }

    /**
     * 服务是否在运行
     */
    private fun isServiceRunning(serviceName: String): Boolean {
        if (serviceName.isNullOrBlank())
            return false

        val atyManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        val service = atyManager
                .getRunningServices(50)
                .firstOrNull { it.service.className == serviceName }

        return service != null
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            closeLoadingDialog()
            return isConsumeBackKey()
        }
        return super.onKeyDown(keyCode, event)
    }

    private var exitTime: Long = 0

    private fun isConsumeBackKey(): Boolean {
        val activity = AppManager.getAppManager().currentActivity()
        if (activity is MainActivity) {
            if (System.currentTimeMillis() - exitTime > 2000) {//未处理监听事件，请求后续监听
                ToastUtil.showToast(this, "再按一次退出程序")
                exitTime = System.currentTimeMillis()
            } else {
                stopService(App.instance.serviceIntent)
                AppManager.getAppManager().AppExit(this, false)
            }

        } else {
            exitTime = 0
            AppManager.getAppManager().finishActivity()
            if (AppManager.getAppManager().activityStackSize() < 1) {
                stopService(App.instance.serviceIntent)
                AppManager.getAppManager().AppExit(this, false)

            }
        }
        return true
    }

    protected fun clickEventWhenOk(view: View, condition: () -> Boolean, task: () -> Unit) {
        RxView.clicks(view)
                .throttleFirst(TIME_THROTTLE, TimeUnit.MILLISECONDS)
                .compose(bindToLifecycle())
                .filter { condition() }
                .subscribe({
                    task()
                }, {
                    LogUtils.e("BaseFragment", it.message)
                })
    }

    interface IKeyBoardVisibleListener {
        fun onSoftKeyBoardVisible(visible: Boolean, windowBottom: Int)
    }

    var isVisiableForLast = false
    fun addOnSoftKeyBoardVisibleListener(listener: IKeyBoardVisibleListener) {

        val decorView = window.decorView
        decorView.viewTreeObserver.addOnGlobalLayoutListener {
            val rect = Rect()
            decorView.getWindowVisibleDisplayFrame(rect)
            //计算出可见屏幕的高度
            val displayHight = rect.bottom - rect.top
            //获得屏幕整体的高度
            val hight = decorView.height
            //获得键盘高度
            val keyboardHeight = hight - displayHight
            val visible = displayHight.toDouble() / hight < 0.8
            if (visible != isVisiableForLast) {
                listener.onSoftKeyBoardVisible(visible, keyboardHeight)
            }
            isVisiableForLast = visible
        }

    }

    protected fun getStatusBarHeight(): Int {
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        return resources.getDimensionPixelSize(resourceId)
    }

    /**
     * 请求机智云出错的回调
     */
    fun getErrorConsumer(flag: Boolean = true): Consumer<Throwable> {
        return Consumer {
            it.printStackTrace()
            closeLoadingDialog()

            if (it is ANError) {
                val errorBody = it.errorBody
                val errorResponse = Gson().fromJson(errorBody, GizBaseResponse::class.java)

                if (errorResponse != null) {
                    when (errorResponse.error_code) {
                        GizWifiErrorCode.GIZ_OPENAPI_TOKEN_INVALID.result -> {
                            ToastUtil.showToast(this, GizErrorToast.parseExceptionCode(GizWifiErrorCode.GIZ_OPENAPI_TOKEN_INVALID))
                            if (flag)
                                LoginActivity.doAction(this, true)
                        }

                        else -> ToastUtil.showToast(this, errorResponse.error_message)
                    }
                }

            }
        }
    }
}