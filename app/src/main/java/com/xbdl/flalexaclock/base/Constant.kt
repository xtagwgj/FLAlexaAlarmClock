package com.xbdl.flalexaclock.base

/**
 * 常量类
 * Created by xtagwgj on 2017/6/29.
 */

//val TEMP_PHONE = "hbpeng@gizwits.com"
val TEMP_PHONE = "1611805812@qq.com"
val TEMP_PWD = "123456"
//val TEMP_PHONE = "wuwei@homtime.com"
//val TEMP_PWD = "jordan"

val FEEDBACK_EMAIL_DEBUG = "xtagwgj@163.com"
val FEEDBACK_EMAIL_RELEASE = "info@homtime.com"

//登录保存的数据
val SP_UID = "sp_uid"
val SP_TOKEN = "sp_token"
val SP_ACCOUNT = "sp_account"
val SP_EXPIRE_AT = "sp_expire_at"
val SP_VERSION = "sp_version"
val SP_IS_THIRD_LOGIN = "sp_is_third_login"
val SP_THIRD_EMAIL = "sp_third_email"
val SP_THIRD_NAME = "sp_third_name"
//是否存在默认的分组，没有的话就查询，查询再没有就创建
val SP_DEFAULT_GROUP = "sp_default_group"
val SP_USER_PHOTO = "sp_user_photo"
val SP_LOGIN_TYPE = "sp_login_type"
val SP_GENDER = "sp_gender"

val TYPE_NORMAL = "normal"
val TYPE_FACEBOOK = "facebook"
val TYPE_AMAZON = "amazon"
val TYPE_GOOGLE = "google"

val PIC_CHANGE = "pic_change"

val DEFAULT_GROUP_NAME = ""
//最少的密码位数
val MIN_LENGTH_LOGIN_PWD = 4
//验证码的位数
val LENGTH_CODE_NUMBER = 6
//再次获取验证码的时间
val GET_CODE_COUNT_DOWN_SECONDS = 60
//防抖动的时间
val TIME_THROTTLE = 600L

val SN_ADD_MODIFY_ALARM = 101
val SN_CLOSE_ALARM = 102
val SN_DELETE_ALARM = 103

val REQUEST_ADD_ALARM = 104
val REQUEST_MODIFY_ALARM = 105

val MODIFY_ALARM_CALI = 106
val MODIFY_TIMEZONE = 107

val FRAGMENT_CHANGED = "changed_fragment"

val MAIN_NO_SWIPE = true

val SERVER_CN = "https://api.gizwits.com"
val SERVER_US = "https://usapi.gizwits.com"

//闹铃字节从左开始算
val PARSE_FROM_LEFT = true

val REFRESH_DEVICE_LIST = "refresh_device_list"
val REFRESH_ALARM_LIST = "refresh_alarm_list"
val REFRESH_ALARM_SINGLE = "refresh_alarm_single"
val REFRESH_GROUP_LIST = "refresh_group_list"
val REFRESH_GROUP = "refresh_group"
val REFRESH_DEVICE = "refresh_device"
val REFRESH_EMAIL = "refresh_email"
val CHECK_UPDATE = "CHECK_UPDATE"
//绑定时间的格式，最前面必须有一个下划线
val BIND_TIME_FORMAT = "_yyyy-MM-dd"

val PRODUCT_1 = "my iC1mini"

val PROVICE_URL = "http://www.homtime.com/privacy.html"