package com.xbdl.flalexaclock.base

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.xbdl.flalexaclock.BuildConfig
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class ActivityCallBack : Application.ActivityLifecycleCallbacks {
    private var lastTime = 0L

    /**
     * 5分钟后重新
     */
    private val fiveTimes = 300000L

    override fun onActivityPaused(activity: Activity?) {
    }

    override fun onActivityResumed(activity: Activity?) {
    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
        lastTime = Date().time
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        if (lastTime != 0L && Date().time - lastTime > fiveTimes) {
            activity?.let {
                val map = ConcurrentHashMap<String, String>()
                map["openAPIInfo"] = "usapi.gizwits.com"
                map["siteInfo"] = "ussite.gizwits.com"
                map["pushInfo"] = "us.push.gizwitsapi.com"

                GizWifiSDK.sharedInstance().startWithAppID(
                        it,
                        BuildConfig.APP_ID,
                        BuildConfig.APP_SECRET,
                        arrayListOf(BuildConfig.PRODUCT_KEY),
                        map,
                        true)
            }

        }
    }
}