package com.xbdl.flalexaclock.base

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.support.multidex.MultiDex
import com.facebook.FacebookSdk
import com.facebook.drawee.backends.pipeline.Fresco
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.pgyersdk.crash.PgyCrashManager
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xtagwgj.baseproject.base._MyApplication
import com.xtagwgj.baseproject.utils.LogUtils
import java.util.concurrent.ConcurrentHashMap

/**
 * 应用
 * Created by xtagwgj on 2017/6/16.
 */

class App : _MyApplication() {

    init {
        instance = this
    }

    companion object {
        lateinit var instance: App
    }

    lateinit var serviceIntent: Intent

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)

//        registerReceiver(mReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))

//        //注册机智云服务
        val map = ConcurrentHashMap<String, String>()
        map["openAPIInfo"] = "usapi.gizwits.com"
        map["siteInfo"] = "ussite.gizwits.com"
        map["pushInfo"] = "us.push.gizwitsapi.com"
//
        GizWifiSDK.sharedInstance().startWithAppID(
                this,
                BuildConfig.APP_ID,
                BuildConfig.APP_SECRET,
                arrayListOf(BuildConfig.PRODUCT_KEY),
                map,
                true)

        Fresco.initialize(this)

        FacebookSdk.sdkInitialize(this)
        PgyCrashManager.register(this)

        registerActivityLifecycleCallbacks(ActivityCallBack())
    }

    //通过IntentService进行初始化
    override fun initService() {
        serviceIntent = Intent(this, ParseDataService::class.java)
        //启动机智云的数据解析服务
        try {
            LogUtils.e("app", "onCreate ${App.instance.serviceIntent} _ ${ParseDataService::class.java} _$this")

            startService(serviceIntent)
        } catch (e: Exception) {
            LogUtils.e("app", e.message)
        }


        InitService.start(this)
    }



    /**
     * 服务是否在运行
     */
    private fun isServiceRunning(serviceName: String): Boolean {
        if (serviceName.isBlank())
            return false

        val atyManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        val service = atyManager
                .getRunningServices(100)
                .firstOrNull { it.service.className == serviceName }

        return service != null
    }

}