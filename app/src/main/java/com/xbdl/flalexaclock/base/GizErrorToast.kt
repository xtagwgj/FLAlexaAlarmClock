package com.xbdl.flalexaclock.base

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.xbdl.flalexaclock.R


/**
 * 获取错误信息
 * Created by xtagwgj on 2017/7/21.
 */
object GizErrorToast {
    fun parseExceptionCode(errorCode: GizWifiErrorCode): String {
        return toastError(errorCode)
    }

    private fun getStringFromResId(resId: Int): String {
        return if (resId == 0) {
            ""
        } else {
            App.instance.resources.getString(resId)
        }
    }

    private fun toastError(errorCode: GizWifiErrorCode): String {
        val errorString: String
        when (errorCode) {
            GizWifiErrorCode.GIZ_SDK_PARAM_FORM_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_PARAM_FORM_INVALID)
            GizWifiErrorCode.GIZ_SDK_CLIENT_NOT_AUTHEN -> errorString = getStringFromResId(R.string.GIZ_SDK_CLIENT_NOT_AUTHEN)
            GizWifiErrorCode.GIZ_SDK_CLIENT_VERSION_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_CLIENT_VERSION_INVALID)
            GizWifiErrorCode.GIZ_SDK_UDP_PORT_BIND_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_UDP_PORT_BIND_FAILED)
            GizWifiErrorCode.GIZ_SDK_DAEMON_EXCEPTION -> errorString = getStringFromResId(R.string.GIZ_SDK_DAEMON_EXCEPTION)
            GizWifiErrorCode.GIZ_SDK_PARAM_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_PARAM_INVALID)
            GizWifiErrorCode.GIZ_SDK_APPID_LENGTH_ERROR -> errorString = getStringFromResId(R.string.GIZ_SDK_APPID_LENGTH_ERROR)
            GizWifiErrorCode.GIZ_SDK_LOG_PATH_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_LOG_PATH_INVALID)
            GizWifiErrorCode.GIZ_SDK_LOG_LEVEL_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_LOG_LEVEL_INVALID)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONFIG_SEND_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONFIG_SEND_FAILED)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONFIG_IS_RUNNING -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONFIG_IS_RUNNING)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONFIG_TIMEOUT -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONFIG_TIMEOUT)
            GizWifiErrorCode.GIZ_SDK_DEVICE_DID_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_DID_INVALID)
            GizWifiErrorCode.GIZ_SDK_DEVICE_MAC_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_MAC_INVALID)
            GizWifiErrorCode.GIZ_SDK_SUBDEVICE_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_SUBDEVICE_DID_INVALID)
            GizWifiErrorCode.GIZ_SDK_DEVICE_PASSCODE_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_PASSCODE_INVALID)
            GizWifiErrorCode.GIZ_SDK_DEVICE_NOT_SUBSCRIBED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_NOT_SUBSCRIBED)
            GizWifiErrorCode.GIZ_SDK_DEVICE_NO_RESPONSE -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_NO_RESPONSE)
            GizWifiErrorCode.GIZ_SDK_DEVICE_NOT_READY -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_NOT_READY)
            GizWifiErrorCode.GIZ_SDK_DEVICE_NOT_BINDED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_NOT_BINDED)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONTROL_WITH_INVALID_COMMAND -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONTROL_WITH_INVALID_COMMAND)
        // case GIZ_SDK_DEVICE_CONTROL_FAILED:
        // errorString= (String)
        // getText(R.string.GIZ_SDK_DEVICE_CONTROL_FAILED);
        // break;
            GizWifiErrorCode.GIZ_SDK_DEVICE_GET_STATUS_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_GET_STATUS_FAILED)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONTROL_VALUE_TYPE_ERROR -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONTROL_VALUE_TYPE_ERROR)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONTROL_VALUE_OUT_OF_RANGE -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONTROL_VALUE_OUT_OF_RANGE)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONTROL_NOT_WRITABLE_COMMAND -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONTROL_NOT_WRITABLE_COMMAND)
            GizWifiErrorCode.GIZ_SDK_BIND_DEVICE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_BIND_DEVICE_FAILED)
            GizWifiErrorCode.GIZ_SDK_UNBIND_DEVICE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_UNBIND_DEVICE_FAILED)
            GizWifiErrorCode.GIZ_SDK_DNS_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DNS_FAILED)
            GizWifiErrorCode.GIZ_SDK_M2M_CONNECTION_SUCCESS -> errorString = getStringFromResId(R.string.GIZ_SDK_M2M_CONNECTION_SUCCESS)
            GizWifiErrorCode.GIZ_SDK_SET_SOCKET_NON_BLOCK_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_SET_SOCKET_NON_BLOCK_FAILED)
            GizWifiErrorCode.GIZ_SDK_CONNECTION_TIMEOUT -> errorString = getStringFromResId(R.string.GIZ_SDK_CONNECTION_TIMEOUT)
            GizWifiErrorCode.GIZ_SDK_CONNECTION_REFUSED -> errorString = getStringFromResId(R.string.GIZ_SDK_CONNECTION_REFUSED)
            GizWifiErrorCode.GIZ_SDK_CONNECTION_ERROR -> errorString = getStringFromResId(R.string.GIZ_SDK_CONNECTION_ERROR)
            GizWifiErrorCode.GIZ_SDK_CONNECTION_CLOSED -> errorString = getStringFromResId(R.string.GIZ_SDK_CONNECTION_CLOSED)
            GizWifiErrorCode.GIZ_SDK_SSL_HANDSHAKE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_SSL_HANDSHAKE_FAILED)
            GizWifiErrorCode.GIZ_SDK_DEVICE_LOGIN_VERIFY_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_LOGIN_VERIFY_FAILED)
            GizWifiErrorCode.GIZ_SDK_INTERNET_NOT_REACHABLE -> errorString = getStringFromResId(R.string.GIZ_SDK_INTERNET_NOT_REACHABLE)
            GizWifiErrorCode.GIZ_SDK_HTTP_ANSWER_FORMAT_ERROR -> errorString = getStringFromResId(R.string.GIZ_SDK_HTTP_ANSWER_FORMAT_ERROR)
            GizWifiErrorCode.GIZ_SDK_HTTP_ANSWER_PARAM_ERROR -> errorString = getStringFromResId(R.string.GIZ_SDK_HTTP_ANSWER_PARAM_ERROR)
            GizWifiErrorCode.GIZ_SDK_HTTP_SERVER_NO_ANSWER -> errorString = getStringFromResId(R.string.GIZ_SDK_HTTP_SERVER_NO_ANSWER)
            GizWifiErrorCode.GIZ_SDK_HTTP_REQUEST_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_HTTP_REQUEST_FAILED)
            GizWifiErrorCode.GIZ_SDK_OTHERWISE -> errorString = getStringFromResId(R.string.GIZ_SDK_OTHERWISE)
            GizWifiErrorCode.GIZ_SDK_MEMORY_MALLOC_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_MEMORY_MALLOC_FAILED)
            GizWifiErrorCode.GIZ_SDK_THREAD_CREATE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_THREAD_CREATE_FAILED)
            GizWifiErrorCode.GIZ_SDK_USER_ID_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_USER_ID_INVALID)
            GizWifiErrorCode.GIZ_SDK_TOKEN_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_TOKEN_INVALID)
            GizWifiErrorCode.GIZ_SDK_GROUP_ID_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUP_ID_INVALID)
            GizWifiErrorCode.GIZ_SDK_GROUPNAME_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUPNAME_INVALID)
            GizWifiErrorCode.GIZ_SDK_GROUP_PRODUCTKEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUP_PRODUCTKEY_INVALID)
            GizWifiErrorCode.GIZ_SDK_GROUP_DELETE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUP_FAILED_DELETE_DEVICE)
            GizWifiErrorCode.GIZ_SDK_GROUP_EDIT_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUP_FAILED_ADD_DEVICE)
            GizWifiErrorCode.GIZ_SDK_GROUP_LIST_UPDATE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_GROUP_GET_DEVICE_FAILED)
            GizWifiErrorCode.GIZ_SDK_DATAPOINT_NOT_DOWNLOAD -> errorString = getStringFromResId(R.string.GIZ_SDK_DATAPOINT_NOT_DOWNLOAD)
            GizWifiErrorCode.GIZ_SDK_DATAPOINT_SERVICE_UNAVAILABLE -> errorString = getStringFromResId(R.string.GIZ_SDK_DATAPOINT_SERVICE_UNAVAILABLE)
            GizWifiErrorCode.GIZ_SDK_DATAPOINT_PARSE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DATAPOINT_PARSE_FAILED)
        // case GIZ_SDK_NOT_INITIALIZED:
        // errorString= getStringFromResId(R.string.GIZ_SDK_SDK_NOT_INITIALIZED);
        // break;
            GizWifiErrorCode.GIZ_SDK_APK_CONTEXT_IS_NULL -> errorString = getStringFromResId(R.string.GIZ_SDK_APK_CONTEXT_IS_NULL)
            GizWifiErrorCode.GIZ_SDK_APK_PERMISSION_NOT_SET -> errorString = getStringFromResId(R.string.GIZ_SDK_APK_PERMISSION_NOT_SET)
            GizWifiErrorCode.GIZ_SDK_CHMOD_DAEMON_REFUSED -> errorString = getStringFromResId(R.string.GIZ_SDK_CHMOD_DAEMON_REFUSED)
            GizWifiErrorCode.GIZ_SDK_EXEC_DAEMON_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_EXEC_DAEMON_FAILED)
            GizWifiErrorCode.GIZ_SDK_EXEC_CATCH_EXCEPTION -> errorString = getStringFromResId(R.string.GIZ_SDK_EXEC_CATCH_EXCEPTION)
            GizWifiErrorCode.GIZ_SDK_APPID_IS_EMPTY -> errorString = getStringFromResId(R.string.GIZ_SDK_APPID_IS_EMPTY)
            GizWifiErrorCode.GIZ_SDK_UNSUPPORTED_API -> errorString = getStringFromResId(R.string.GIZ_SDK_UNSUPPORTED_API)
            GizWifiErrorCode.GIZ_SDK_REQUEST_TIMEOUT -> errorString = getStringFromResId(R.string.GIZ_SDK_REQUEST_TIMEOUT)
            GizWifiErrorCode.GIZ_SDK_DAEMON_VERSION_INVALID -> errorString = getStringFromResId(R.string.GIZ_SDK_DAEMON_VERSION_INVALID)
            GizWifiErrorCode.GIZ_SDK_PHONE_NOT_CONNECT_TO_SOFTAP_SSID -> errorString = getStringFromResId(R.string.GIZ_SDK_PHONE_NOT_CONNECT_TO_SOFTAP_SSID)
            GizWifiErrorCode.GIZ_SDK_DEVICE_CONFIG_SSID_NOT_MATCHED -> errorString = getStringFromResId(R.string.GIZ_SDK_DEVICE_CONFIG_SSID_NOT_MATCHED)
            GizWifiErrorCode.GIZ_SDK_NOT_IN_SOFTAPMODE -> errorString = getStringFromResId(R.string.GIZ_SDK_NOT_IN_SOFTAPMODE)
        // case GIZ_SDK_PHONE_WIFI_IS_UNAVAILABLE:
        // errorString= (String)
        // getText(R.string.GIZ_SDK_PHONE_WIFI_IS_UNAVAILABLE);
        // break;
            GizWifiErrorCode.GIZ_SDK_RAW_DATA_TRANSMIT -> errorString = getStringFromResId(R.string.GIZ_SDK_RAW_DATA_TRANSMIT)
            GizWifiErrorCode.GIZ_SDK_PRODUCT_IS_DOWNLOADING -> errorString = getStringFromResId(R.string.GIZ_SDK_PRODUCT_IS_DOWNLOADING)
            GizWifiErrorCode.GIZ_SDK_START_SUCCESS -> errorString = getStringFromResId(R.string.GIZ_SDK_START_SUCCESS)
            GizWifiErrorCode.GIZ_SITE_PRODUCTKEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_SITE_PRODUCTKEY_INVALID)
            GizWifiErrorCode.GIZ_SITE_DATAPOINTS_NOT_DEFINED -> errorString = getStringFromResId(R.string.GIZ_SITE_DATAPOINTS_NOT_DEFINED)
            GizWifiErrorCode.GIZ_SITE_DATAPOINTS_NOT_MALFORME -> errorString = getStringFromResId(R.string.GIZ_SITE_DATAPOINTS_NOT_MALFORME)
            GizWifiErrorCode.GIZ_OPENAPI_MAC_ALREADY_REGISTERED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_MAC_ALREADY_REGISTERED)
            GizWifiErrorCode.GIZ_OPENAPI_PRODUCT_KEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_PRODUCT_KEY_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_APPID_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_APPID_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_TOKEN_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_TOKEN_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_USER_NOT_EXIST -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_USER_NOT_EXIST)
            GizWifiErrorCode.GIZ_OPENAPI_TOKEN_EXPIRED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_TOKEN_EXPIRED)
            GizWifiErrorCode.GIZ_OPENAPI_M2M_ID_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_M2M_ID_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_SERVER_ERROR -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SERVER_ERROR)
            GizWifiErrorCode.GIZ_OPENAPI_CODE_EXPIRED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_CODE_EXPIRED)
            GizWifiErrorCode.GIZ_OPENAPI_CODE_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_CODE_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_SANDBOX_SCALE_QUOTA_EXHAUSTED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SANDBOX_SCALE_QUOTA_EXHAUSTED)
            GizWifiErrorCode.GIZ_OPENAPI_PRODUCTION_SCALE_QUOTA_EXHAUSTED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_PRODUCTION_SCALE_QUOTA_EXHAUSTED)
            GizWifiErrorCode.GIZ_OPENAPI_PRODUCT_HAS_NO_REQUEST_SCALE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_PRODUCT_HAS_NO_REQUEST_SCALE)
            GizWifiErrorCode.GIZ_OPENAPI_DEVICE_NOT_FOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DEVICE_NOT_FOUND)
            GizWifiErrorCode.GIZ_OPENAPI_FORM_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_FORM_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_DID_PASSCODE_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DID_PASSCODE_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_DEVICE_NOT_BOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DEVICE_NOT_BOUND)
            GizWifiErrorCode.GIZ_OPENAPI_PHONE_UNAVALIABLE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_PHONE_UNAVALIABLE)
            GizWifiErrorCode.GIZ_OPENAPI_USERNAME_UNAVALIABLE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_USERNAME_UNAVALIABLE)
            GizWifiErrorCode.GIZ_OPENAPI_USERNAME_PASSWORD_ERROR -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_USERNAME_PASSWORD_ERROR)
            GizWifiErrorCode.GIZ_OPENAPI_SEND_COMMAND_FAILED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SEND_COMMAND_FAILED)
            GizWifiErrorCode.GIZ_OPENAPI_EMAIL_UNAVALIABLE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_EMAIL_UNAVALIABLE)
            GizWifiErrorCode.GIZ_OPENAPI_DEVICE_DISABLED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DEVICE_DISABLED)
            GizWifiErrorCode.GIZ_OPENAPI_FAILED_NOTIFY_M2M -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_FAILED_NOTIFY_M2M)
            GizWifiErrorCode.GIZ_OPENAPI_ATTR_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ATTR_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_USER_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_USER_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_FIRMWARE_NOT_FOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_FIRMWARE_NOT_FOUND)
            GizWifiErrorCode.GIZ_OPENAPI_JD_PRODUCT_NOT_FOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_JD_PRODUCT_NOT_FOUND)
            GizWifiErrorCode.GIZ_OPENAPI_DATAPOINT_DATA_NOT_FOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DATAPOINT_DATA_NOT_FOUND)
            GizWifiErrorCode.GIZ_OPENAPI_SCHEDULER_NOT_FOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SCHEDULER_NOT_FOUND)
            GizWifiErrorCode.GIZ_OPENAPI_QQ_OAUTH_KEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_QQ_OAUTH_KEY_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_OTA_SERVICE_OK_BUT_IN_IDLE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_OTA_SERVICE_OK_BUT_IN_IDLE)
            GizWifiErrorCode.GIZ_OPENAPI_BT_FIRMWARE_UNVERIFIED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_BT_FIRMWARE_UNVERIFIED)
            GizWifiErrorCode.GIZ_OPENAPI_BT_FIRMWARE_NOTHING_TO_UPGRADE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SAVE_KAIROSDB_ERROR)
            GizWifiErrorCode.GIZ_OPENAPI_SAVE_KAIROSDB_ERROR -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SAVE_KAIROSDB_ERROR)
            GizWifiErrorCode.GIZ_OPENAPI_EVENT_NOT_DEFINED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_EVENT_NOT_DEFINED)
            GizWifiErrorCode.GIZ_OPENAPI_SEND_SMS_FAILED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SEND_SMS_FAILED)
        // case GIZ_OPENAPI_APPLICATION_AUTH_INVALID:
        // errorString= (String)
        // getText(R.string.GIZ_OPENAPI_APPLICATION_AUTH_INVALID);
        // break;
            GizWifiErrorCode.GIZ_OPENAPI_NOT_ALLOWED_CALL_API -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_NOT_ALLOWED_CALL_API)
            GizWifiErrorCode.GIZ_OPENAPI_BAD_QRCODE_CONTENT -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_BAD_QRCODE_CONTENT)
            GizWifiErrorCode.GIZ_OPENAPI_REQUEST_THROTTLED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_REQUEST_THROTTLED)
            GizWifiErrorCode.GIZ_OPENAPI_DEVICE_OFFLINE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
            GizWifiErrorCode.GIZ_OPENAPI_TIMESTAMP_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_TIMESTAMP_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_SIGNATURE_INVALID -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SIGNATURE_INVALID)
            GizWifiErrorCode.GIZ_OPENAPI_DEPRECATED_API -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_DEPRECATED_API)
            GizWifiErrorCode.GIZ_OPENAPI_RESERVED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_RESERVED)
            GizWifiErrorCode.GIZ_PUSHAPI_BODY_JSON_INVALID -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_BODY_JSON_INVALID)
            GizWifiErrorCode.GIZ_PUSHAPI_DATA_NOT_EXIST -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_DATA_NOT_EXIST)
            GizWifiErrorCode.GIZ_PUSHAPI_NO_CLIENT_CONFIG -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_NO_CLIENT_CONFIG)
            GizWifiErrorCode.GIZ_PUSHAPI_NO_SERVER_DATA -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_NO_SERVER_DATA)
            GizWifiErrorCode.GIZ_PUSHAPI_GIZWITS_APPID_EXIST -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_GIZWITS_APPID_EXIST)
            GizWifiErrorCode.GIZ_PUSHAPI_PARAM_ERROR -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_PARAM_ERROR)
            GizWifiErrorCode.GIZ_PUSHAPI_AUTH_KEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_AUTH_KEY_INVALID)
            GizWifiErrorCode.GIZ_PUSHAPI_APPID_OR_TOKEN_ERROR -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_APPID_OR_TOKEN_ERROR)
            GizWifiErrorCode.GIZ_PUSHAPI_TYPE_PARAM_ERROR -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_TYPE_PARAM_ERROR)
            GizWifiErrorCode.GIZ_PUSHAPI_ID_PARAM_ERROR -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_ID_PARAM_ERROR)
            GizWifiErrorCode.GIZ_PUSHAPI_APPKEY_SECRETKEY_INVALID -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_APPKEY_SECRETKEY_INVALID)
            GizWifiErrorCode.GIZ_PUSHAPI_CHANNELID_ERROR_INVALID -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_CHANNELID_ERROR_INVALID)
            GizWifiErrorCode.GIZ_PUSHAPI_PUSH_ERROR -> errorString = getStringFromResId(R.string.GIZ_PUSHAPI_PUSH_ERROR)

            GizWifiErrorCode.GIZ_OPENAPI_REGISTER_IS_BUSY -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_REGISTER_IS_BUSY)

            GizWifiErrorCode.GIZ_OPENAPI_CANNOT_SHARE_TO_SELF -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_CANNOT_SHARE_TO_SELF)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_OWNER_CAN_SHARE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_OWNER_CAN_SHARE)

            GizWifiErrorCode.GIZ_OPENAPI_NOT_FOUND_GUEST -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_NOT_FOUND_GUEST)

            GizWifiErrorCode.GIZ_OPENAPI_GUEST_ALREADY_BOUND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_GUEST_ALREADY_BOUND)

            GizWifiErrorCode.GIZ_OPENAPI_NOT_FOUND_SHARING_INFO -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_NOT_FOUND_SHARING_INFO)

            GizWifiErrorCode.GIZ_OPENAPI_NOT_FOUND_THE_MESSAGE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_NOT_FOUND_THE_MESSAGE)

            GizWifiErrorCode.GIZ_OPENAPI_SHARING_IS_WAITING_FOR_ACCEPT -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SHARING_IS_WAITING_FOR_ACCEPT)

            GizWifiErrorCode.GIZ_OPENAPI_SHARING_IS_COMPLETED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SHARING_IS_COMPLETED)

            GizWifiErrorCode.GIZ_OPENAPI_INVALID_SHARING_BECAUSE_UNBINDING -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_INVALID_SHARING_BECAUSE_UNBINDING)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_OWNER_CAN_BIND -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_OWNER_CAN_BIND)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_OWNER_CAN_OPERATE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_OWNER_CAN_OPERATE)

            GizWifiErrorCode.GIZ_OPENAPI_SHARING_ALREADY_CANCELLED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SHARING_ALREADY_CANCELLED)

            GizWifiErrorCode.GIZ_OPENAPI_OWNER_CANNOT_UNBIND_SELF -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_OWNER_CANNOT_UNBIND_SELF)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_GUEST_CAN_CHECK_QRCODE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_GUEST_CAN_CHECK_QRCODE)

            GizWifiErrorCode.GIZ_OPENAPI_MESSAGE_ALREADY_DELETED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_MESSAGE_ALREADY_DELETED)

            GizWifiErrorCode.GIZ_OPENAPI_BINDING_NOTIFY_FAILED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_BINDING_NOTIFY_FAILED)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_SELF_CAN_MODIFY_ALIAS -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_SELF_CAN_MODIFY_ALIAS)

            GizWifiErrorCode.GIZ_OPENAPI_ONLY_RECEIVER_CAN_MARK_MESSAGE -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_ONLY_RECEIVER_CAN_MARK_MESSAGE)

            GizWifiErrorCode.GIZ_OPENAPI_SHARING_IS_EXPIRED -> errorString = getStringFromResId(R.string.GIZ_OPENAPI_SHARING_IS_EXPIRED)

            GizWifiErrorCode.GIZ_SDK_NO_AVAILABLE_DEVICE -> errorString = getStringFromResId(R.string.GIZ_SDK_NO_AVAILABLE_DEVICE)

            GizWifiErrorCode.GIZ_SDK_HTTP_SERVER_NOT_SUPPORT_API -> errorString = getStringFromResId(R.string.GIZ_SDK_HTTP_SERVER_NOT_SUPPORT_API)

            GizWifiErrorCode.GIZ_SDK_SUBDEVICE_ADD_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_ADD_SUBDEVICE_FAILED)

            GizWifiErrorCode.GIZ_SDK_SUBDEVICE_DELETE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_DELETE_SUBDEVICE_FAILED)

            GizWifiErrorCode.GIZ_SDK_SUBDEVICE_LIST_UPDATE_FAILED -> errorString = getStringFromResId(R.string.GIZ_SDK_GET_SUBDEVICES_FAILED)
            else -> errorString = getStringFromResId(R.string.UNKNOWN_ERROR)
        }
        return errorString
    }

}