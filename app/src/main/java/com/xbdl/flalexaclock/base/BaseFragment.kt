package com.xbdl.flalexaclock.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.view.RxView
import com.trello.rxlifecycle2.components.support.RxFragment
import com.xbdl.flalexaclock.R
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.view.loadingdialog.view.LoadingDialog
import io.reactivex.functions.Consumer
import java.util.concurrent.TimeUnit

/**
 * 基本的fragment
 * Created by xtagwgj on 2017/7/4.
 */
abstract class BaseFragment : RxFragment() {

    private var loadingDialog: LoadingDialog? = null

    protected var isFirstLoad: Boolean = true

    protected var rootView: View? = null

    /**
     * 是否第一次对用户可见A
     */
    protected var isFirstVisible: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (this.rootView == null) {
            this.rootView = this.initContentView(this.getLayoutId(), inflater, container)
        }

        return this.rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.initView(savedInstanceState)
        this.initEventListener()
    }

    protected fun initContentView(@LayoutRes layoutRes: Int, inflater: LayoutInflater, container: ViewGroup?): View {
        return inflater.inflate(layoutRes, container, false)
    }

    protected abstract fun getLayoutId(): Int

    protected abstract fun initView(var1: Bundle?)

    protected abstract fun initEventListener()


    protected fun hideInput() {
        val inputMethodManager = this.activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.activity.window.decorView.windowToken, 0)

    }

    protected fun clickEvent(view: View, consumer: Consumer<Any>) {
        RxView.clicks(view)
                .throttleFirst(TIME_THROTTLE, TimeUnit.MILLISECONDS)
                .compose(bindToLifecycle())
                .subscribe(consumer)
    }

    protected fun clickEventWhenOk(view: View, condition: () -> Boolean, task: () -> Unit) {
        RxView.clicks(view)
                .throttleFirst(TIME_THROTTLE, TimeUnit.MILLISECONDS)
                .compose(bindToLifecycle())
                .filter { condition() }
                .subscribe({
                    task()
                }, {
                    LogUtils.e("BaseFragment", it.message)
                })
    }

    protected fun showLoadingDialog(@StringRes loadRes: Int, @StringRes successRes: Int) {
        if (loadingDialog == null) {
            loadingDialog = LoadingDialog(activity)
        }

        loadingDialog?.setLoadingText(activity.getString(loadRes))
                ?.setSuccessText(activity.getString(successRes))
                ?.setInterceptBack(false)
                ?.closeSuccessAnim()
                ?.show()
    }

    protected fun showLoadingDialog(@StringRes loadRes: Int) {
        showLoadingDialog(loadRes, R.string.app_name)
    }

    protected fun closeLoadingDialog() {
        loadingDialog?.close()
        loadingDialog = null
    }

    override fun onDestroy() {
        super.onDestroy()
        closeLoadingDialog()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (rootView == null)
            return

        if (isVisibleToUser && isFirstVisible) {
            isFirstVisible = false
            onFirstLoad()
        }
    }

    abstract fun onFirstLoad()
}