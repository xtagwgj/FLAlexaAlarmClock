package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.xtagwgj.baseproject.view.swipe.SwipeMenuRecyclerView;

/**
 * 可空的SwipeMenuRecyclerView
 * Created by xtagwgj on 2017/9/11.
 */

public class EmptySwipeRecyclerView extends SwipeMenuRecyclerView {

    private View emptyView;


    public EmptySwipeRecyclerView(Context context) {
        super(context);
    }

    public EmptySwipeRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptySwipeRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    final private AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    private void checkIfEmpty() {
        if (emptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible =
                    getAdapter().getItemCount() == 0;
            emptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
            setVisibility(emptyViewVisible ? GONE : VISIBLE);
        }
    }

    @Override
    public void setAdapter(Adapter adapter) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }

        checkIfEmpty();
    }

    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
        checkIfEmpty();
    }
}
