package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xbdl.flalexaclock.R;

/**
 * 列表
 * Created by xtagwgj on 2017/9/11.
 */

public class ItemView extends RelativeLayout {
    public ItemView(Context context) {
        this(context, null);
    }

    public ItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        View view = View.inflate(context, R.layout.view_item, this);
        TextView textView = (TextView) view.findViewById(R.id.leftText);
        View arrowView = view.findViewById(R.id.arrow);

        TypedArray typedArray;
        if (attrs != null) {
            typedArray = context.obtainStyledAttributes(attrs, R.styleable.ItemView);
            textView.setText(typedArray.getString(R.styleable.ItemView_leftTextRes));

            Drawable leftDrawable = typedArray.getDrawable(R.styleable.ItemView_leftIconRes);
            if (leftDrawable != null) {
                leftDrawable.setBounds(0, 0, leftDrawable.getMinimumWidth(), leftDrawable.getMinimumHeight());
                textView.setCompoundDrawables(leftDrawable, null, null, null);
            }

            arrowView.setVisibility(
                    typedArray.getBoolean(R.styleable.ItemView_showRightArrow, false) ? VISIBLE : GONE);

            boolean flag = typedArray.getBoolean(R.styleable.ItemView_showShortSplit, true);
            view.findViewById(R.id.viewSplitShort).setVisibility(flag ? VISIBLE : INVISIBLE);

            typedArray.recycle();
        }

        setEnabled(true);
        setClickable(true);

    }
}
