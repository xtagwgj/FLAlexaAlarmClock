package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.jakewharton.rxbinding2.view.RxView;
import com.xbdl.flalexaclock.R;

import java.util.concurrent.TimeUnit;

import io.reactivex.functions.Consumer;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.xtagwgj.baseproject.constant.BaseConstants.THROTTLE_TIME;

/**
 * Created by xtagwgj on 2017/8/7.
 */

public class MorePop extends PopupWindow {


    public MorePop(Context context, final PopClickListener popclickListener) {
        super(context);
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = (ViewGroup) mLayoutInflater.inflate(
                R.layout.view_pop, null, true);

        //更换设备名字
        clickEvent(rootView.findViewById(R.id.updateDeviceTextView), new Consumer() {
            @Override
            public void accept(Object o) throws Exception {
                if (popclickListener != null) {
                    popclickListener.updateDeviceName();
                }
                dismiss();
            }
        });

//        //更换设备封面图
//        clickEvent(rootView.findViewById(R.id.exchangePicTextView), new Consumer() {
//            @Override
//            public void accept(Object o) throws Exception {
//                if (popclickListener != null)
//                    popclickListener.exchangePic();
//                dismiss();
//            }
//        });

        //删除设备
        clickEvent(rootView.findViewById(R.id.deleteDeviceTextView), new Consumer() {
            @Override
            public void accept(Object o) throws Exception {
                if (popclickListener != null) {
                    popclickListener.deleteDevice();
                }
                dismiss();
            }
        });


        this.setContentView(rootView);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setFocusable(true);
        this.setBackgroundDrawable(new BitmapDrawable());
        this.setOutsideTouchable(true);
        this.setTouchable(true);

        this.setTouchInterceptor(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    dismiss();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * pop点击的接口
     */
    public interface PopClickListener {
        void updateDeviceName();

        void exchangePic();

        void deleteDevice();
    }

    private void clickEvent(View view, Consumer consumer) {
        if (view != null) {
            RxView.clicks(view)
                    .throttleFirst(THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .subscribe(consumer);
        }
    }

}
