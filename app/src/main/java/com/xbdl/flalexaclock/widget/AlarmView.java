package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.xbdl.flalexaclock.R;

/**
 * 闹钟视图
 * Created by xtagwgj on 2017/11/11.
 */

public class AlarmView extends FrameLayout {

    private boolean online = false;
    private TextClock textClock;
    private TextView tvOffline;

    public AlarmView(Context context) {
        this(context, null);
    }

    public AlarmView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AlarmView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AlarmView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        View view = View.inflate(context, R.layout.view_alarm, this);

        textClock = (TextClock) view.findViewById(R.id.tv_currTime);
        textClock.setTypeface(getTypeFace());

        tvOffline = (TextView) view.findViewById(R.id.tv_offline);
        tvOffline.setTypeface(getTypeFace());

        setTimeVisible(false);
    }

    public void setTime(String timeZoneId) {
        textClock.setTimeZone(timeZoneId);
        setTimeVisible(true);
    }

    public void setTimeVisible(boolean visible) {
        online = visible;

        textClock.setVisibility(visible ? View.VISIBLE : View.GONE);
        tvOffline.setVisibility(!visible ? View.VISIBLE : View.GONE);
    }

    private Typeface getTypeFace() {
        return Typeface.createFromAsset(getContext().getAssets(), "fonts/digitalRegular.TTF");
    }

}
