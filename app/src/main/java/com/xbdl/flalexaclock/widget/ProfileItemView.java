package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.xtagwgj.baseproject.utils.StringUtils;
import com.xbdl.flalexaclock.R;
import com.xbdl.flalexaclock.base.App;

/**
 * 个人信息的列表
 * Created by xtagwgj on 2017/9/11.
 */

public class ProfileItemView extends RelativeLayout {
    private TextView leftTextView, rightTextView, leftBottomTextView;
    private SimpleDraweeView rightImageView;
    private ImageView arrowView;

    public ProfileItemView(Context context) {
        this(context, null);
    }

    public ProfileItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProfileItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ProfileItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        View view = View.inflate(context, R.layout.view_profile_info, this);
        leftTextView = (TextView) view.findViewById(R.id.leftText);
        leftBottomTextView = (TextView) view.findViewById(R.id.leftBottomText);
        rightTextView = (TextView) view.findViewById(R.id.rightText);
        rightImageView = (SimpleDraweeView) view.findViewById(R.id.rightImageView);
        arrowView = (ImageView) view.findViewById(R.id.arrow);

        TypedArray typedArray;
        if (attrs != null) {

            //左边文字
            typedArray = context.obtainStyledAttributes(attrs, R.styleable.ProfileItemView);
            leftTextView.setText(typedArray.getString(R.styleable.ProfileItemView_piv_leftTextRes));

            //左边底部文字
            String leftBottomText = typedArray.getString(
                    R.styleable.ProfileItemView_piv_leftBottomTextRes
            );
            setLeftBottomText(leftBottomText);

            //右边箭头的显示与隐藏
            arrowView.setVisibility(
                    typedArray.getBoolean(R.styleable.ProfileItemView_piv_showRightArrow,
                            true) ? VISIBLE : INVISIBLE);

            //底部分割线的显示与隐藏
            boolean flag = typedArray.getBoolean(R.styleable.ProfileItemView_piv_showShortSplit,
                    true);
            view.findViewById(R.id.viewSplitShort).setVisibility(flag ? VISIBLE : INVISIBLE);

            boolean showRightImageView = typedArray.getBoolean(
                    R.styleable.ProfileItemView_piv_showRightIv, false);

            rightImageView.setVisibility(showRightImageView ? VISIBLE : GONE);
            rightTextView.setVisibility(!showRightImageView ? VISIBLE : GONE);

            int imgId = typedArray.getResourceId(R.styleable.ProfileItemView_piv_rightIvRes,
                    0);
            if (imgId != 0) {
                setRightImage(imgId);
            }

            typedArray.recycle();
        }

        setEnabled(true);
        setClickable(true);

    }

    public void setLeftText(String text) {
        if (leftTextView != null) {
            leftTextView.setText(text);
        }
    }

    public void setLeftBottomText(String text) {
        if (leftBottomTextView != null) {
            if (StringUtils.isEmpty(text)) {
                leftBottomTextView.setVisibility(GONE);
            } else {
                leftBottomTextView.setText(text);
                leftBottomTextView.setVisibility(VISIBLE);
            }
        }
    }

    public String getLeftBottomText() {
        if (leftBottomTextView == null || leftBottomTextView.getVisibility() != View.VISIBLE) {
            return "";
        } else {
            return leftBottomTextView.getText().toString();
        }
    }

    public void setRightText(String text) {
        if (rightTextView != null) {
            rightTextView.setText(text);
            rightTextView.setVisibility(VISIBLE);
            rightImageView.setVisibility(GONE);
        }
    }

    public void setRightText(String text, @ColorRes int textColorRes) {
        if (rightTextView != null) {
            rightTextView.setText(text);
            rightTextView.setTextColor(getContext().getResources().getColor(textColorRes));

            rightTextView.setVisibility(VISIBLE);
            rightImageView.setVisibility(GONE);
        }
    }

    public void setRightImage(String urls) {
        if (rightImageView != null) {
            rightImageView.setImageURI(Uri.parse(urls));
            rightTextView.setVisibility(GONE);
            rightImageView.setVisibility(VISIBLE);
        }
    }

    public void setRightImage(@DrawableRes int drawableRes) {
        if (rightImageView != null) {
            rightImageView.setImageURI(Uri.parse("res://" +
                    App.instance.getPackageName() + "/" + drawableRes));
            rightTextView.setVisibility(GONE);
            rightImageView.setVisibility(VISIBLE);
        }
    }

    public void showArrow(Boolean isShow) {
        if (arrowView != null) {
            arrowView.setVisibility(isShow ? VISIBLE : INVISIBLE);
        }
    }

    public String getRightText() {
        if (rightTextView != null) {
            return rightTextView.getText().toString().trim();
        } else {
            return "";
        }
    }
}
