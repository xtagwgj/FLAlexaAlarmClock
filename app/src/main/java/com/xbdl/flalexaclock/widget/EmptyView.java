package com.xbdl.flalexaclock.widget;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xbdl.flalexaclock.R;

/**
 * 空的View
 * Created by xtagwgj on 2017/9/11.
 */

public class EmptyView extends RelativeLayout {
    private ImageView imageView;
    private TextView textView;
    private Button button;

    public EmptyView(Context context) {
        this(context, null);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        View rootView = LayoutInflater.from(this.getContext()).inflate(R.layout.layout_empty_view, this);
        imageView = (ImageView) rootView.findViewById(R.id.imageView);
        textView = (TextView) rootView.findViewById(R.id.textView);
        button = (Button) rootView.findViewById(R.id.button);
    }

    public void initView(@DrawableRes int imgRes, @StringRes int textRes, @StringRes int buttonTextRes, OnClickListener onClickListener) {
        if (imageView != null) {
            imageView.setImageResource(imgRes);
        }

        if (textView != null) {
            textView.setText(textRes);
        }

        if (button != null) {
            button.setText(buttonTextRes);
            button.setOnClickListener(onClickListener);
        }
    }
}
