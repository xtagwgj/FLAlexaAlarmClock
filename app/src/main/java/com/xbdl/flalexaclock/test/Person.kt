package com.xbdl.flalexaclock.test

import kotlin.properties.Delegates

/**
 * Created by xtagwgj on 2017/6/19.
 */
class Person constructor(var name: String, var age: Int) {

    var firstName = name.toUpperCase()
        get() = "firstName is $field"
        set(value) {
            field = "set $value"
        }

    var address: String by Delegates.observable("<no name>") {
        prop, old, new ->
        println("$old -> $new")
    }

}