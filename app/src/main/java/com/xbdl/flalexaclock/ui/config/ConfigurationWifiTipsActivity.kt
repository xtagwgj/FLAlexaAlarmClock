package com.xbdl.flalexaclock.ui.config

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.FRAGMENT_CHANGED
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.SP_TOKEN
import com.xbdl.flalexaclock.base.SP_UID
import com.xbdl.flalexaclock.extensions.Preference
import com.xtagwgj.baseproject.base.RxBus
import kotlinx.android.synthetic.main.activity_configuration_wifi_tips.*

/**
 * 配网提示
 * Created by xtagwgj on 2017/7/21.
 */
class ConfigurationWifiTipsActivity : GizBaseActivity() {

    override fun getLayoutId(): Int = R.layout.activity_configuration_wifi_tips

    var uid: String by Preference(this, SP_UID, "")
    var token: String by Preference(this, SP_TOKEN, "")

    companion object {
        fun doAction(mContext: Activity) {
            RxBus.getInstance().post(FRAGMENT_CHANGED, 1)
            val intent = Intent(mContext, ConfigurationWifiTipsActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun initView(p0: Bundle?) {

    }

    override fun initEventListener() {
        clickEvent(nextButton, {
            ConfigurationWifiActivity.doAction(this)
        })

        clickEvent(cancelConfiguration, {
            finish()
        })

    }


}