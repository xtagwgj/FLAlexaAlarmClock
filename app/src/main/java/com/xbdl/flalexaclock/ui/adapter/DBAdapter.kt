package com.xbdl.flalexaclock.ui.adapter

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * DataBinding的基础类
 * Created by xtagwgj on 2017/8/31.
 */

abstract class DBAdapter<T, VDB : ViewDataBinding>(data: List<T>?,
                                                   private val mLayoutRes: Int)
    : RecyclerView.Adapter<DBViewHolder<VDB>>() {

    /**
     * 数据集合
     */
    var mDatas = mutableListOf<T>()

    lateinit var mContext: Context

    init {
        if (mLayoutRes == 0)
            throw NullPointerException("mLayoutRes must not be 0")

        if (data != null && data.isNotEmpty())
            mDatas.addAll(data)
    }

    /**
     * 添加数据
     *
     * @param addList 待添加的数据
     */
    fun addData(addList: List<T>?) {
        if (addList == null || addList.isEmpty())
            return

        mDatas.addAll(addList)
        notifyItemRangeInserted(mDatas.size - addList.size, addList.size)
    }

    fun addData(data: T) {
        mDatas.add(data)
        notifyItemInserted(mDatas.size - 1)
    }

    fun removeData(data: T) {
        val index = mDatas.indexOf(data)
        if (index != -1) {
            mDatas.remove(data)
            notifyItemRemoved(index)
        }
    }

    /**
     * 刷新数据
     *
     * @param refreshList 刷新获取的数据
     */
    open fun refreshData(refreshList: List<T>?) {
        mDatas.clear()

        if (refreshList != null && refreshList.isNotEmpty())
            mDatas.addAll(refreshList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DBViewHolder<VDB> {
        mContext = parent.context

        val binding = DataBindingUtil.inflate<VDB>(
                LayoutInflater.from(mContext), mLayoutRes, parent, false)

        return DBViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DBViewHolder<VDB>, position: Int) {
        onBindViewHolder(holder, position, mDatas[position])
        //立刻更新视图
        holder.binding.executePendingBindings()
    }

    abstract fun onBindViewHolder(holder: DBViewHolder<VDB>, position: Int, item: T)

    override fun getItemCount() = mDatas.size

}