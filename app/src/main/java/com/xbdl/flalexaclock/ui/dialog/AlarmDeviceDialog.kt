package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.ui.adapter.AlarmDeviceAdapter
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find

/**
 * 选择设备的对话框
 * Created by xtagwgj on 2017/9/13.
 */
class AlarmDeviceDialog : RxDialogFragment() {

    var singleChooseConsumer: Consumer<ClockEntity>? = null
    var multiChooseConsumer: Consumer<List<ClockEntity>>? = null
    var cancelConsumer = Consumer<Any> {
    }
    lateinit var inflate: View

    private var adapter: AlarmDeviceAdapter? = null

    var clockEntityList = mutableListOf<ClockEntity>()

    /**
     *  选择的设备的did
     */
    var selectedDeviceDid: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_alarm_device, null)

        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()

        if (singleChooseConsumer != null) {
            /**
             *  设备列表
             */
            loadClockInfo()
        } else if (clockEntityList.isNotEmpty()) {
            adapter?.refreshData(clockEntityList)
        }
    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {
        dialogWindow.setGravity(Gravity.CENTER)
        dialogWindow.decorView.setPadding(0, 0, 0, 0)
        val lp = dialogWindow.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }


    fun initView() {

        adapter = AlarmDeviceAdapter(
                emptyList(),
                {
                    //单选的话，选择一个就结束
                    if (singleChooseConsumer != null) {
                        singleChooseConsumer?.accept(it)
                        dismiss()
                    }
                })

        selectedDeviceDid?.let {
            adapter?.setSelectedDid(selectedDeviceDid!!)
        }

        inflate.find<RecyclerView>(R.id.recyclerDevice).apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(SimpleDividerDecoration(activity,
                    dividerColorInt = activity.resources.getColor(R.color.colorBg)))
        }.adapter = adapter

        inflate.find<View>(R.id.bt_cancel).setOnClickListener {
            cancelConsumer.accept(null)
            dismiss()
        }

        inflate.find<View>(R.id.bt_confirm).apply {
            visibility = if (multiChooseConsumer == null) View.GONE else View.VISIBLE

            setOnClickListener {
                if (adapter?.getSelectedDevice() == null || adapter?.getSelectedDevice()!!.isEmpty()) {
                    ToastUtil.showToast(context,R.string.text_no_choose_device)
                } else {
                    multiChooseConsumer?.accept(adapter?.getSelectedDevice() ?: emptyList())
                    dismiss()
                }
            }
        }

    }


    /**
     * 根据获取到的设备信息，去拉取设备最新的数据点信息进行解析 然后显示
     */
    private fun loadClockInfo() {

        adapter?.refreshData(ClockUtils.clockAlarmMap.values.toList())

//        Observable.fromIterable(deviceList)
//                .compose(bindToLifecycle())
//                .flatMap { HttpRequest.getLatestDevicePoint(it.did) }
//                .filter { it.error_code == 0 }//正确的获取了数据
//                .map { entity ->
//                    LogUtils.e("clockFragment next:", entity)
//
//                    val clock = ClockEntity()
//                    clock.device = deviceList.first { it.did == entity.did }
//                    clock.timeZone = entity.attr.time_zone
//                    clock.isClockSwitch = entity.attr.alarm_state == 1
//
//                    //主闹钟
//                    clock.mainAlarm = entity.attr.getMainAlarm(null)
//                    val subList = (1..12).map {
//                        entity.attr.getSubAlarm(it, null)
//                    }
//                    //子闹钟 只取没有删除的闹钟
//                    clock.alarmList = subList.filter { it.state != 3 }
//                    clock
//                }//解析成了需要的闹钟的实体
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .toList()
//                .toObservable()
//                .subscribe({
//                    adapter?.refreshData(it)
//                }, {
//                    it.printStackTrace()
//                }, {
//
//                })

    }

}