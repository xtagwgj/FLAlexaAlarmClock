package com.xbdl.flalexaclock.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.ui.user.AboutUsActivity
import com.xbdl.flalexaclock.ui.user.FeedbackActivity
import com.xbdl.flalexaclock.ui.user.ProfileActivity
import com.xbdl.flalexaclock.ui.user.ServiceHelpActivity
import com.xtagwgj.baseproject.base.RxBus
import kotlinx.android.synthetic.main.fragment_account.*

/**
 * 个人账户页面
 */
class AccountFragment : BaseFragment(), View.OnClickListener {

    lateinit var uid: String
    lateinit var token: String
    lateinit var account: String
    lateinit var photo: String

    companion object {
        fun newInstance(uid: String, token: String, account: String, photo: String): AccountFragment {
            val fragment = AccountFragment()
            val args = Bundle()
            args.putString(SP_UID, uid)
            args.putString(SP_TOKEN, token)
            args.putString(SP_ACCOUNT, account)
            args.putString(SP_USER_PHOTO, photo)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            uid = arguments.getString(SP_UID)
            token = arguments.getString(SP_TOKEN)
            account = arguments.getString(SP_ACCOUNT)
            photo = arguments.getString(SP_USER_PHOTO)
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_account

    @SuppressLint("SetTextI18n")
    override fun initView(var1: Bundle?) {
        tv_account.text = account
        headImageView.setImageURI(photo)
        tv_version.text = "v ${BuildConfig.VERSION_NAME}"

        //更新头像
        RxBus.getInstance().register<String>(PIC_CHANGE)
                .compose(bindToLifecycle())
                .subscribe({
                    photo = it
                    headImageView.setImageURI(it)
                }, {
                    it.printStackTrace()
                })

        //更新帐号
        RxBus.getInstance().register<String>(REFRESH_EMAIL)
                .compose(bindToLifecycle())
                .subscribe({
                    account = it
                    tv_account?.text = it
                }, {
                    it.printStackTrace()
                })
    }

    override fun initEventListener() {
        tv_EditProfile.setOnClickListener(this)
        item_feedback.setOnClickListener(this)
        item_service.setOnClickListener(this)
        item_about.setOnClickListener(this)
        item_privacy.setOnClickListener(this)
        item_version.setOnClickListener(this)
    }

    override fun onFirstLoad() {

    }


    override fun onClick(v: View) {
        when (v.id) {
        //编辑个人信息
            tv_EditProfile.id -> ProfileActivity.doAction(activity)

        //反馈
            item_feedback.id -> FeedbackActivity.doAction(activity)

        //服务与帮助
            item_service.id -> ServiceHelpActivity.doAction(activity)

        //关于
            item_about.id -> AboutUsActivity.doAction(activity)

        //隐私协议
            item_privacy.id -> {
                WebViewActivity.doAction(activity)
            }

        //版本检查
            item_version.id -> {
                RxBus.getInstance().post(CHECK_UPDATE, true)
            }
            else -> {
            }
        }

    }
}