package com.xbdl.flalexaclock.ui.adapter

import android.databinding.ViewDataBinding
import android.support.annotation.IdRes
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import org.jetbrains.anko.find

/**
 * DataBinding 的 ViewHolder类
 * Created by xtagwgj on 2017/9/8.
 */
class DBViewHolder<VDB : ViewDataBinding>(var binding: VDB) : RecyclerView.ViewHolder(binding.root) {

    fun getImageViewById(@IdRes imgId: Int): ImageView {
        return binding.root.find(imgId)
    }
}

