package com.xbdl.flalexaclock.ui.dialog

import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_no_device.*

/**
 * 无设备提示的对话框
 * Created by xtagwgj on 2017/9/11.
 */

class NoDeviceDialog : BaseRxDialog() {

    var okConsumer: Consumer<Any>? = null

    override fun getLayoutId(): Int {
        return R.layout.dialog_no_device
    }

    override fun initView() {

        clickEvent(bt_cancel, {
            dismiss()
        })

        if (okConsumer!=null)
            clickEvent(bt_ok, okConsumer)

    }
}
