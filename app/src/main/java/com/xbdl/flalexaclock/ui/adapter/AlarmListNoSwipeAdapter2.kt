package com.xbdl.flalexaclock.ui.adapter

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.daimajia.swipe.SwipeLayout
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.SN_CLOSE_ALARM
import com.xbdl.flalexaclock.databinding.ItemAlarmBinding
import com.xbdl.flalexaclock.databinding.ItemHeaderAlarmDetailsBinding
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.extensions.*
import com.xbdl.flalexaclock.ui.AddAlarmActivity
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.find
import java.util.concurrent.ConcurrentHashMap

/**
 * 闹钟的列表
 * Created by xtagwgj on 2017/9/11.
 */
class AlarmListNoSwipeAdapter2(private var isDeviceOnline: Boolean = true,
                               private val onItemClick: (AlarmDetailsEntity) -> Unit,
                               private val onItemDelete: (GizWifiDevice, String) -> Unit,
                               private val onSwitchChecked: (AlarmDetailsEntity, Int) -> Unit)
    : DBAdapter<AlarmDetailsEntity, ViewDataBinding>(null, -1) {

    var isEdit: Boolean = false
        set(value) {
            field = value

            if (openSwipeOnlyIndex != -1) {
                openSwipeOnlyIndex = -1
            }
            notifyDataSetChanged()
        }

    //当前打开的位置
    var openSwipeOnlyIndex: Int = -1

    override fun onBindViewHolder(holder: DBViewHolder<ViewDataBinding>, position: Int, item: AlarmDetailsEntity) {
    }

    private val VIEW_HEADER = 0
    private val VIEW_CONTENT = 1

    private var clockEntity: ClockEntity? = null
    private var autoCalibration = false

    fun setAutoCalibration(flag: Boolean) {
        this.autoCalibration = flag
    }

    fun refreshData(clockEntity: ClockEntity) {

        //头部更新
        this.clockEntity = clockEntity

        val alarmList = clockEntity.alarmList.filter { it.state == 1 || it.state == 2 }

        val sortedByList = alarmList.sortedBy {
            val hour = if (it.hour == 0)
                24 else it.hour

            hour * 60 + it.minute
        }

        //内容的列表
        refreshData(sortedByList)
    }

    private fun onBindViewHeader(holder: DBViewHolder<ItemHeaderAlarmDetailsBinding>) {

        val tv_currTime = holder.binding.root.find<TextClock>(R.id.tv_currTime)
        val tv_timeUnit = holder.binding.root.find<TextClock>(R.id.tv_timeUnit).apply {
            visibility = if (DateFormat.is24HourFormat(mContext)) View.GONE else View.VISIBLE
        }

        val textView2 = holder.binding.root.find<TextView>(R.id.textView2)
        val tv_timeZone = holder.binding.root.find<TextView>(R.id.tv_timeZone)

        val ll_alarmView = holder.binding.root.find<View>(R.id.ll_alarmView)
        val rb_alarmOn = holder.binding.root.find<CheckBox>(R.id.rb_alarmOn)

        //校准是否打开
        textView2.text = mContext.getString(if (autoCalibration) {
            R.string.text_auto_group
        } else {
            R.string.text_auto_group_off
        })

        holder.binding.root.find<TextView>(R.id.tv_edit).apply {
            text = if (isEdit) "Done" else "Edit"
            textColor = mContext.resources.getColor(if (isEdit) R.color.colorAccent else R.color.black)
        }.setOnClickListener {
            isEdit = !isEdit
//            notifyDataSetChanged()
        }


        //当前选择的时区
        val calTimeZone = TimeZoneTableUtils.getByTimeZone(clockEntity?.timeZone ?: 256)


        //初始化显示时间
//        tv_currTime.timeZone = TimeZoneTableUtils.getCurrTimeZoneId(calTimeZone).id
        tv_currTime.timeZone = calTimeZone.timeZoneId
        tv_timeUnit.timeZone = tv_currTime.timeZone

        //时区
        tv_timeZone.text = calTimeZone
                .getShowBySys()


        ll_alarmView.setBackgroundColor(
                mContext.resources.getColor(
//                        if (clockEntity?.mainAlarm?.state == 1 && clockEntity?.mainAlarm?.isGroupAlarm == true) {
                        if (clockEntity?.mainAlarm?.state == 1) {
                            //闹铃打开
                            R.color.colorItemBgOpen
                        } else {
                            R.color.colorItemBgClose
                        })
        )

        if (clockEntity == null || clockEntity?.mainAlarm == null) {
            ll_alarmView.visibility = View.GONE
        } else
            ll_alarmView.visibility = View.VISIBLE

        //主开关
        rb_alarmOn.apply {
            isChecked = clockEntity?.isClockSwitch == true

            setOnClickListener {
                //就算点击了也保持原来的状态，等到下发数据了再改变
                isChecked = clockEntity?.isClockSwitch == true

                val commandMap = ConcurrentHashMap<String, Any>()
                commandMap.put(GizDataPointKey.KEY_ALARM_STATE, !isChecked)
//                commandMap.put(GizDataPointKey.KEY_MAIN_ALARM_SET, if (isChecked) 2 else 1)


                //当前显示的是打开状态，其实是要进行关闭的操作
                if (isChecked) {
                    commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)

                    //关闭所有打开的闹铃
//                    mContext.getStateList(mDatas, 1).forEach {
//                        commandMap.put(it, 2)
//                    }
                } else {
                    //打开所有关闭的闹钟
//                    mContext.getStateList(mDatas, 2).forEach {
//                        commandMap.put(it, 1)
//                    }
                }

                mContext.sendCommand(clockEntity?.device, commandMap, SN_CLOSE_ALARM)
            }
        }

        val tv_time = holder.binding.root.find<TextView>(R.id.tv_time)
        val tv_ap = holder.binding.root.find<TextView>(R.id.tv_ap)
        val tv_day = holder.binding.root.find<TextView>(R.id.tv_day)
        val tv_nextTimeAlarm = holder.binding.root.find<TextView>(R.id.tv_nextTimeAlarm)

        val rb_open = holder.binding.root.find<CheckBox>(R.id.rb_open)

        holder.binding.root.find<TextView>(R.id.tv_time).setTextColor(
                if (clockEntity?.mainAlarm?.state != 1)
                    mContext.resources.getColor(R.color.colorTextClose)
                else
                    mContext.resources.getColor(R.color.colorTextOpen)
        )

        //主闹钟
        clockEntity?.mainAlarm.also { item ->

//            rb_open.isChecked = item?.state == 1 && item.isGroupAlarm
            rb_open.isChecked = item?.state == 1

            tv_time.text = item?.showTime ?: "00:00"
            tv_ap.text = item?.timeUnit ?: "AM"

            tv_day.text = item?.retryWeek
            tv_nextTimeAlarm.text = item?.nextRingTime
            rb_open.setOnClickListener {
                rb_open.isChecked = item?.state == 1

                //朱闹铃是关闭的，所以点击只是单纯的弹出提示信息
                if (item?.isGroupAlarm == false) {
                    ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                    return@setOnClickListener
                }
                //开启的话就关闭，关闭的话就开启
                val commandMap = ConcurrentHashMap<String, Any>()
                commandMap.put(GizDataPointKey.KEY_MAIN_ALARM_SET, if (item?.state == 1) 2 else 1)

                if (item?.state == 1)
                    commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)

                mContext.sendCommand(clockEntity?.device, commandMap)
            }

            ll_alarmView.apply {
                setOnClickListener {

                    //朱闹铃是关闭的，所以点击只是单纯的弹出提示信息
                    if (item?.isGroupAlarm == false) {
                        ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                        return@setOnClickListener
                    }

                    if (clockEntity?.device != null && mContext.deviceIsController(clockEntity?.device)) {
                        AddAlarmActivity.doAction(mContext as GizBaseActivity, clockEntity?.device, item)
                    } else {
                        ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                    }
                }
                visibility = View.VISIBLE
            }

        }

    }

    private fun onBindViewContent(holder: DBViewHolder<ItemAlarmBinding>, item: AlarmDetailsEntity, position: Int) {
        holder.binding.item = item
        holder.binding.mContext = mContext
        holder.binding.showDeviceName = false

        holder.binding.root.find<View>(R.id.tv_alarmInfo).visibility = View.GONE

        //可以滑动的布局
        val swipeLayout = holder.binding.root.find<SwipeLayout>(R.id.swipeLayout).apply {
            isSwipeEnabled = false
        }

        //删除点击
        holder.binding.root.find<Button>(R.id.bottom_wrapper).setOnClickListener {
            //不可控制，就不能进入详情
            if (!mContext.deviceIsController(item.currDevice)) {
                ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                return@setOnClickListener
            }

            //删除设备
            mContext.getSwitchSetKeyAndStateKey(item.pos, { _, stateKey ->
                //fixme:提示删除的信息
//                mContext.sendCommand(item.currDevice, stateKey, 3, SN_DELETE_ALARM)

                onItemDelete(item.currDevice, stateKey)

                swipeLayout.close()
            })

        }

        if (!isEdit)
            swipeLayout.close()
        else if (openSwipeOnlyIndex != position && swipeLayout.openStatus == SwipeLayout.Status.Open)
            swipeLayout.close()

        holder.binding.root.find<RelativeLayout>(R.id.ll_alarmView).apply {
            setBackgroundColor(
                    mContext.resources.getColor(
                            if (item.isGroupAlarm && item.state == 1
                                    && item.nextRingTime.contains("Alarm in", true))
                                R.color.colorItemBgOpen
                            else
                                R.color.colorItemBgClose)
            )
            setOnClickListener {

                if (isEdit) {//在编辑状态点击关闭swipeLayout
                    swipeLayout.close()
                } else {//进入该闹钟的详情

                    if (!item.isGroupAlarm) {
                        ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                        return@setOnClickListener
                    }

                    //不可控制，就不能进入详情
                    if (!isDeviceOnline) {
//                        ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                        ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                    } else {
                        onItemClick(item)
                    }
                }
            }
        }

        holder.binding.root.find<TextView>(R.id.tv_time).setTextColor(
                if (item.state != 1 || !item.isGroupAlarm)
                    mContext.resources.getColor(R.color.colorTextClose)
                else
                    mContext.resources.getColor(R.color.colorTextOpen)
        )


        //CheckBox
        holder.binding.root.find<CheckBox>(R.id.rb_open).apply {
            //开关的显示与隐藏
            visibility = if (isEdit) View.GONE else View.VISIBLE

            //设备离线的时候不给操作
            isEnabled = isDeviceOnline
//            isChecked = item.state == 1 && item.isGroupAlarm

            setOnClickListener {
                if (it is CheckBox) {
                    //保持checkbox的状态不变，等数据下发才改变
                    it.isChecked = item.showButton()

                    if (!item.isGroupAlarm) {
                        ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                        return@setOnClickListener
                    }

                    //开关 下发的命令
                    onSwitchChecked(item, if (item.state == 1) 2 else 1)
                    AnkoLogger<AlarmListNoSwipeAdapter2>().debug { "$it.isChecked" }
                }
            }
        }

        //最左边的减号的点击事件
        holder.binding.root.find<ImageView>(R.id.iv_del).apply {
            visibility = if (!item.isMainAlarm && isEdit)
                View.VISIBLE
            else
                View.GONE
        }.setOnClickListener({

            if (swipeLayout.openStatus == SwipeLayout.Status.Open) {
                swipeLayout.close()
                openSwipeOnlyIndex = -1
            } else {
                swipeLayout.open()
                if (position != openSwipeOnlyIndex) {
                    notifyItemChanged(openSwipeOnlyIndex)
                    openSwipeOnlyIndex = position
                }
            }

        })
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0)
            VIEW_HEADER
        else
            VIEW_CONTENT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DBViewHolder<ViewDataBinding> {
        mContext = parent.context

        return if (viewType == VIEW_HEADER) {
            val binding = DataBindingUtil.inflate<ItemHeaderAlarmDetailsBinding>(
                    LayoutInflater.from(mContext), R.layout.item_header_alarm_details, parent, false)

            DBViewHolder(binding)

        } else {
            val binding = DataBindingUtil.inflate<ItemAlarmBinding>(
                    LayoutInflater.from(mContext), R.layout.item_alarm, parent, false)

            DBViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: DBViewHolder<ViewDataBinding>, position: Int) {

        if (getItemViewType(position) == VIEW_HEADER)
            onBindViewHeader(holder as DBViewHolder<ItemHeaderAlarmDetailsBinding>)
        else
            onBindViewContent(holder as DBViewHolder<ItemAlarmBinding>, mDatas[position - 1], position)

        //立刻更新视图
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int = mDatas.size + 1

}