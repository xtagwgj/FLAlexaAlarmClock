package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import android.widget.*
import com.daimajia.swipe.SwipeLayout
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.SN_CLOSE_ALARM
import com.xbdl.flalexaclock.databinding.ItemAlarmBinding
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.extensions.deviceIsController
import com.xbdl.flalexaclock.extensions.getSwitchSetKeyAndStateKey
import com.xbdl.flalexaclock.extensions.sendCommand
import com.xbdl.flalexaclock.utils.ToastUtil
import org.jetbrains.anko.find
import java.util.concurrent.ConcurrentHashMap

/**
 * 闹钟的列表
 * Created by xtagwgj on 2017/9/11.
 */
class AlarmListAdapter(private val onItemClick: (AlarmDetailsEntity) -> Unit,
                       private val onItemDelete: (GizWifiDevice, String) -> Unit) :
        DBAdapter<AlarmDetailsEntity, ItemAlarmBinding>(null, R.layout.item_alarm) {

    var isEdit: Boolean = false
        set(value) {
            field = value

            if (openSwipeOnlyIndex != -1) {
                openSwipeOnlyIndex = -1
            }
            notifyDataSetChanged()
        }

    //当前打开的位置
    var openSwipeOnlyIndex: Int = -1

    //替换数据
    fun replaceAlarm(alarmList: MutableList<AlarmDetailsEntity>) {

        mDatas.removeAll {
            it.did == alarmList.first().did
        }

        mDatas.addAll(alarmList)

        mDatas.sortBy {
            val hour = if (it.hour == 0)
                24 else it.hour

            hour * 60 + it.minute
        }

        notifyDataSetChanged()
    }

    fun refresh(refreshList: List<AlarmDetailsEntity>) {
        val sortedBy = refreshList.sortedBy {
            val hour = if (it.hour == 0)
                24 else it.hour

            hour * 60 + it.minute
        }
        refreshData(sortedBy)
    }

    override fun onBindViewHolder(holder: DBViewHolder<ItemAlarmBinding>, position: Int, item: AlarmDetailsEntity) {
        holder.binding.isEdit = isEdit
        holder.binding.item = item
        holder.binding.showDeviceName = true

        //可以滑动的布局
        val swipeLayout = holder.binding.root.find<SwipeLayout>(R.id.swipeLayout).apply {
            isSwipeEnabled = false
        }

        //删除点击
        holder.binding.root.find<Button>(R.id.bottom_wrapper).setOnClickListener {
            //不可控制，就不能进入详情
            if (!mContext.deviceIsController(item.currDevice)) {
                ToastUtil.showToast(mContext, R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                return@setOnClickListener
            }

            //删除设备
            mContext.getSwitchSetKeyAndStateKey(item.pos, { _, stateKey ->
                //fixme:提示删除的信息
//                mContext.sendCommand(item.currDevice, stateKey, 3, SN_DELETE_ALARM)

                onItemDelete(item.currDevice, stateKey)

                swipeLayout.close()
            })

        }

        if (!isEdit)
            swipeLayout.close()
        else if (openSwipeOnlyIndex != position && swipeLayout.openStatus == SwipeLayout.Status.Open)
            swipeLayout.close()

        holder.binding.root.find<RelativeLayout>(R.id.ll_alarmView).apply {

            // ALARM IN 说明闹钟会响
            setBackgroundColor(

                    if (item.isMainAlarm) {
                        mContext.resources.getColor(
                                if (item.state == 1)
                                    R.color.colorItemBgOpen
                                else
                                    R.color.colorItemBgClose)
                    } else {
                        mContext.resources.getColor(
                                if (item.isGroupAlarm && item.state == 1
                                        && item.nextRingTime.contains("Alarm in", true))
                                    R.color.colorItemBgOpen
                                else
                                    R.color.colorItemBgClose)
                    }
            )

            setOnClickListener {
                if (isEdit) {//在编辑状态点击关闭swipeLayout
                    swipeLayout.close()
                } else {//进入该闹钟的详情

                    //朱闹铃是关闭的，所以点击只是单纯的弹出提示信息
                    if (!item.isGroupAlarm) {
                        ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                        return@setOnClickListener
                    }

                    //不可控制，就不能进入详情
                    if (!mContext.deviceIsController(item.currDevice)) {
                        ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                        return@setOnClickListener
                    }

                    onItemClick(item)
                }
            }
        }

        holder.binding.root.find<TextView>(R.id.tv_time).setTextColor(
                if (item.state != 1 || (!item.isGroupAlarm && !item.isMainAlarm))
                    mContext.resources.getColor(R.color.colorTextClose)
                else
                    mContext.resources.getColor(R.color.colorTextOpen)
        )

        //CheckBox
        holder.binding.root.find<CheckBox>(R.id.rb_open).apply {
            //设备离线的时候不给操作
//            isEnabled = mContext.deviceIsController(item.currDevice)

            setOnClickListener {
                if (it is CheckBox) {
                    //保持checkbox的状态不变，等数据下发才改变
                    it.isChecked = item.showButton()

                    //朱闹铃是关闭的，所以点击只是单纯的弹出提示信息
                    if (!item.isGroupAlarm) {
                        ToastUtil.showToast(mContext,R.string.text_open_mainclock_tips)
                        return@setOnClickListener
                    }

                    //不可控制，就不能进入详情
                    if (!mContext.deviceIsController(item.currDevice)) {
                        ToastUtil.showToast(mContext,R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                        return@setOnClickListener
                    }

                    mContext.getSwitchSetKeyAndStateKey(item.pos, { _, stateKey ->

                        //开关闹钟
                        val commandMap = ConcurrentHashMap<String, Any>()
                        commandMap.put(stateKey, if (item.state == 1) 2 else 1)
                        if (item.state == 1) {
                            commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)
                        }

                        mContext.sendCommand(item.currDevice, commandMap, SN_CLOSE_ALARM)

                    })
                }
            }
        }

        //最左边的减号的点击事件
        holder.binding.root.find<ImageView>(R.id.iv_del).apply {
            visibility = if (!item.isMainAlarm && isEdit)
                View.VISIBLE
            else
                View.GONE
        }.setOnClickListener({

            if (swipeLayout.openStatus == SwipeLayout.Status.Open) {
                swipeLayout.close()
                openSwipeOnlyIndex = -1
            } else {
                swipeLayout.open()
                if (position != openSwipeOnlyIndex) {
                    notifyItemChanged(openSwipeOnlyIndex)
                    openSwipeOnlyIndex = position
                }
            }

        })

    }
}