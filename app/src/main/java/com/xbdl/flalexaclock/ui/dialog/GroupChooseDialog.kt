package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.entity.GroupEntity
import com.xbdl.flalexaclock.ui.adapter.GroupNameAdapter
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find

/**
 * 选择设备的对话框
 * Created by xtagwgj on 2017/9/13.
 */
class GroupChooseDialog : RxDialogFragment() {

    lateinit var inflate: View

    lateinit var adapter: GroupNameAdapter

    //原来的分组id
    var defaultCheckId = ""

    var chooseConsumer: Consumer<GroupEntity>? = null

    lateinit var entityList: List<GroupEntity>

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_group_list, null)

        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()

    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {
        dialogWindow.setGravity(Gravity.CENTER)
        dialogWindow.decorView.setPadding(0, 0, 0, 0)
        val lp = dialogWindow.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }


    fun initView() {

        inflate.find<View>(R.id.bt_cancel).setOnClickListener { dismiss() }

        adapter = GroupNameAdapter(entityList, defaultCheckId, {
            if (it.id != defaultCheckId) {
                chooseConsumer?.accept(it)
            }
            dismiss()
        })

        inflate.find<RecyclerView>(R.id.recyclerGroup).apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(SimpleDividerDecoration(activity,
                    dividerColorInt = activity.resources.getColor(R.color.colorBg)))
        }.adapter = adapter
    }


}