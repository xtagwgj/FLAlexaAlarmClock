package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizUserAccountType
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.GizErrorToast
import com.xbdl.flalexaclock.entity.giz.GizBaseResponse
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.RegexUtil
import kotlinx.android.synthetic.main.activity_forget_password.*
import org.jetbrains.anko.toast

/**
 * 忘记密码
 * Created by xtagwgj on 2017/9/9.
 */
class ForgetPasswordActivity : GizBaseActivity() {

    companion object {

        /**
         * 启动页面
         * @param mContext activity
         * @param requestCode 请求码(可不填),默认为注册用户
         */
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, ForgetPasswordActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun initEventListener() {
        etEmail.setText("")
    }

    override fun initView(p0: Bundle?) {

        clickEvent(btn_getPassword, {
            if (!RegexUtil.checkEmail(etEmail.text.trim().toString())) {
                ToastUtil.showToast(this, R.string.text_hint_error_phone)
                return@clickEvent
            }


            GizWifiSDK.sharedInstance().resetPassword(
                    etEmail.text.trim().toString(),
                    null,
                    null,
                    GizUserAccountType.GizUserEmail)

        })

//        //调整底部提示文字的位置
//        addOnSoftKeyBoardVisibleListener(object : IKeyBoardVisibleListener {
//            override fun onSoftKeyBoardVisible(visible: Boolean, windowBottom: Int) {
//                view.setPadding(0, 0, 0, windowBottom - getStatusBarHeight())
//            }
//        })

        clickEvent(view, { hideInput() })

        //接收到修改用户密码结果的回调
        RxBus.getInstance()
                .register<GizBaseResponse>("GizBaseResponse")
                .compose(bindToLifecycle())
                .subscribe(
                        {
                            displayChangeUserPassword(it.result)

                        },
                        {

                        },
                        {
                            RxBus.getInstance().unRegister("GizBaseResponse")
                        })
    }

    override fun getLayoutId(): Int = R.layout.activity_forget_password

    /**
     * 修改密码
     */
    private fun displayChangeUserPassword(result: GizWifiErrorCode) {
        when (result) {
            GizWifiErrorCode.GIZ_SDK_SUCCESS -> {
                toast(R.string.text_success_send_email)
                destroyByAnim(ForgetPasswordActivity@ this)
            }
            else -> {
                val error = GizErrorToast.parseExceptionCode(result)
                toast(error ?: getString(R.string.text_error_send_email))
            }
        }
    }
}