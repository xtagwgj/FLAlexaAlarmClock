package com.xbdl.flalexaclock.ui.user

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.StringRes
import android.text.InputType
import android.util.Log
import android.view.View
import com.amazon.identity.auth.device.AuthError
import com.amazon.identity.auth.device.api.Listener
import com.amazon.identity.auth.device.api.authorization.AuthCancellation
import com.amazon.identity.auth.device.api.authorization.AuthorizationManager
import com.amazon.identity.auth.device.api.authorization.AuthorizeListener
import com.amazon.identity.auth.device.api.authorization.AuthorizeResult
import com.amazon.identity.auth.device.api.workflow.RequestContext
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizUserAccountType
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.firebase.auth.FirebaseAuth
import com.luck.picture.lib.PictureSelector
import com.luck.picture.lib.compress.Luban
import com.luck.picture.lib.config.PictureConfig
import com.luck.picture.lib.config.PictureMimeType
import com.tbruyelle.rxpermissions2.RxPermissions
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.GizUser
import com.xbdl.flalexaclock.extensions.Preference
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.loginterminator.FacebookHelper
import com.xbdl.flalexaclock.loginterminator.GooglePlusHelper
import com.xbdl.flalexaclock.ui.MainActivity
import com.xbdl.flalexaclock.ui.dialog.InputlMessageDialog
import com.xbdl.flalexaclock.ui.dialog.NormalMessageDialog
import com.xbdl.flalexaclock.ui.dialog.SexChooseDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.DbUtils
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.RegexUtil
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_profile.*
import org.jetbrains.anko.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * 个人信息修改页面
 * Created by xtagwgj on 2017/9/11.
 */
class ProfileActivity : GizBaseActivity(), View.OnClickListener {

    private val TAG = "self_ProfileActivity"

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")
    private var loginType: String by Preference(this, SP_LOGIN_TYPE, TYPE_NORMAL)

    private var account: String by Preference(this, SP_ACCOUNT, "")
    private var photo: String by Preference(this, SP_USER_PHOTO, "")

    private var userInfo: GizUser? = null

    private lateinit var mAuth: FirebaseAuth

    private val showSimpleFormat by lazy {
        SimpleDateFormat("MM/dd/yyyy", Locale.CHINA)
    }

    private val submitSimpleFormat by lazy {
        SimpleDateFormat("yyyy-MM-dd", Locale.CHINA)
    }


    companion object {
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, ProfileActivity::class.java)
            startByAnim(mContext, intent)
        }
    }


    override fun getLayoutId(): Int = R.layout.activity_profile

    private var facebookHelper: FacebookHelper? = null
    private var requestContext: RequestContext? = null

    override fun initView(p0: Bundle?) {
        displayLoginDialog(R.string.text_loading_profile, R.string.text_loading_profile_success)

        piv_head.isEnabled = false
        LogUtils.e(TAG, "initView = " + loginType)

        //设置头像
        headImageView.setImageURI(photo)

        when (loginType) {
            TYPE_FACEBOOK -> {
                piv_thirdAccount.setLeftText(getString(R.string.text_third_facebook))

                if (RegexUtil.checkEmail(account)) {
                    piv_thirdAccount.rightText = account
                    piv_email.rightText = account
                } else {
                    piv_thirdAccount.rightText = "Phone User"
                    piv_email.visibility = View.GONE
                }

                piv_email.visibility = View.GONE
                piv_reset_pwd.visibility = View.GONE

                if (facebookHelper == null)
                    facebookHelper = FacebookHelper()
            }

            TYPE_GOOGLE -> {
                piv_thirdAccount.rightText = account
                piv_thirdAccount.setLeftText(getString(R.string.text_third_google))
                piv_reset_pwd.visibility = View.GONE
                piv_email.visibility = View.GONE

                GooglePlusHelper.initGooglePlus(this, null, null)
            }

            TYPE_AMAZON -> {

                piv_thirdAccount.rightText = account
                piv_email.rightText = account

                piv_email.visibility = View.GONE
                piv_reset_pwd.visibility = View.GONE

                if (requestContext == null)
                    requestContext = RequestContext.create(this)

                requestContext?.registerListener(object : AuthorizeListener() {
                    override fun onSuccess(authorizeResult: AuthorizeResult?) {
                        LogUtils.e(TAG, "亚马逊登录授权成功：authorizeResult = $authorizeResult")
                    }

                    override fun onCancel(authCancellation: AuthCancellation) {
                        LogUtils.e(TAG, "亚马逊登录授权取消：authCancellation = $authCancellation")
                    }

                    override fun onError(authError: AuthError) {
                        LogUtils.e(TAG, "亚马逊登录授权失败：authError = $authError")
                    }
                })
            }

            else -> {
                piv_thirdAccount.visibility = View.GONE
            }
        }

        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
            mAuth = FirebaseAuth.getInstance()
            mAuth.signInAnonymously()
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "signInAnonymously:success")
                        } else {
                            Log.w(TAG, "signInAnonymously:failure", task.exception)
                        }

                        piv_head.isEnabled = task.isSuccessful
                        piv_head.showArrow(task.isSuccessful)
                    }
        }
    }

    override fun initEventListener() {
        piv_reset_pwd.setOnClickListener(this)
        btn_logOut.setOnClickListener(this)
        piv_birth.setOnClickListener(this)
        piv_head.setOnClickListener(this)
        piv_gender.setOnClickListener(this)

        if (loginType == TYPE_NORMAL) {
            piv_email.setOnClickListener(this)
        }
        piv_email.showArrow(loginType == TYPE_NORMAL)
    }

    override fun onResume() {
        super.onResume()

        requestUserInfo()
    }

    /**
     * 请求用户的个人信息
     */
    private fun requestUserInfo() {
        HttpRequest.getUserInfo({
            displayGetUserInfo(it)
        }, {
            setFailLoadingText(it.detail_message ?: it.error_message)
            failLoading()
        })
    }

    override fun onStart() {
        super.onStart()
        if (loginType == TYPE_GOOGLE) {
            GooglePlusHelper.initGooglePlusOnstart()
        }
        DbUtils.onStartUser(uid, {
            if (it == null) {//未注册到firebase
                DbUtils.updateUserIcon(uid, "", true)
            } else {
                LogUtils.e(TAG, it.headUrl)
                headImageView.setImageURI(it.headUrl)
            }
        })
    }

    override fun onStop() {
        super.onStop()
        if (loginType == TYPE_GOOGLE) {
            GooglePlusHelper.initGooglePlusOnstop()
        }
        DbUtils.onStopUser(uid)
    }

    override fun onClick(v: View) {
        when (v.id) {
        //修改登录邮箱
            piv_email.id -> {

                val inputEmailDialog = InputlMessageDialog()

                inputEmailDialog.titleTextRes = R.string.dialog_change_email
                inputEmailDialog.hintTextRes = R.string.hint_change_email
                inputEmailDialog.setInputInfoType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)

                inputEmailDialog.cancelConsumer = Consumer {
                    inputEmailDialog.dismiss()
                }

                inputEmailDialog.okConsumer = Consumer {

                    if (RegexUtil.checkEmail(it)) {
                        GizWifiSDK.sharedInstance()
                                .changeUserInfo(token, it, null, GizUserAccountType.GizUserEmail, null)

                        displayLoginDialog(R.string.text_modify_email, R.string.text_modify_email_success)

                        inputEmailDialog.dismiss()
                    } else {
                        toast(R.string.error_email_input)
                    }
                }
                inputEmailDialog.show(supportFragmentManager, "changeAccount")

            }

        //重置密码
            piv_reset_pwd.id -> ChangePwdActivity.doAction(this)

        //修改出生日期
            piv_birth.id -> modifyBirth()

        //修改头像
            piv_head.id -> {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
                    openGallery()
                else {
                    RxPermissions(this)
                            .request(Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .subscribe(
                                    {
                                        if (it) {
                                            openGallery()
                                        } else {
                                            toast(R.string.text_toast_permission_photo_refuse)
                                        }
                                    },
                                    { LogUtils.e(TAG, it.message) })

                }
            }

        //修改性别
            piv_gender.id -> modifySex()

        //退出登录
            btn_logOut.id -> {

                val logOutDialog = NormalMessageDialog()

                logOutDialog.textRes = R.string.text_logout_content
                logOutDialog.rightTextRes = R.string.text_dialog_confirm
                logOutDialog.okConsumer = Consumer {
                    displayLoginDialog(R.string.text_loading_logout, R.string.text_loading_logout_success)

                    when (loginType) {
                        TYPE_FACEBOOK -> {
                            FacebookHelper.signOut()
                            back2Login(logOutDialog)
                        }

                        TYPE_GOOGLE -> {
                            GooglePlusHelper.signOutGoogle {
                                if (it.isSuccess) {
                                    back2Login(logOutDialog)
                                } else {
                                    runOnUiThread {
                                        setFailLoadingText(getString(R.string.text_loading_logout_failure))
                                        failLoading()
                                    }
                                }
                            }

                        }

                        TYPE_AMAZON -> {

                            AuthorizationManager.signOut(this, object : Listener<Void, AuthError> {
                                override fun onSuccess(response: Void?) {
                                    LogUtils.e(TAG, "amazon onSuccess: ${response ?: ""}")
                                    back2Login(logOutDialog)

                                }

                                override fun onError(authError: AuthError?) {
                                    LogUtils.e(TAG, "amazon onError: ${authError ?: ""}")
                                    runOnUiThread {
                                        setFailLoadingText(getString(R.string.text_loading_logout_failure))
                                        failLoading()
                                    }
                                }
                            })
                        }

                        else -> {
                            back2Login(logOutDialog)
                        }
                    }
                }
                logOutDialog.isCancelable = false
                logOutDialog.show(ProfileActivity@ this.supportFragmentManager, "logOutDialog")
            }
        }

    }

    private fun displayLoginDialog(@StringRes loadingRes: Int = R.string.text_loading_logout,
                                   @StringRes successRes: Int = R.string.text_loading_logout_success) {
        showLoadingDialog(loadingRes, successRes)
    }

    private fun back2Login(logOutDialog: NormalMessageDialog) {
        runOnUiThread {
            ClockUtils.doReset()
            closeLoadingDialog()
        }

        AppManager.getAppManager().finishActivity(MainActivity::class.java)
        LoginActivity.doAction(ProfileActivity@ this, true)
        AppManager.getAppManager().finishActivity(ProfileActivity::class.java)

        logOutDialog.dismiss()
    }

    /**
     * 修改出生日期
     */
    private fun modifyBirth() {

        if (userInfo == null)
            return

        alert {
            val calendar = Calendar.getInstance()

            customView {
                val datePicker = datePicker().apply {

                    maxDate = calendar.time.time

                    if (!StringUtils.isTrimEmpty(userInfo?.birthday ?: "")) {
                        try {
                            val date = submitSimpleFormat.parse(userInfo!!.birthday!!)
                            calendar.time = date

                        } catch (e: Exception) {
                        }
                    }
                }

                datePicker.init(calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH])
                { _, year, monthOfYear, dayOfMonth ->
                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, monthOfYear)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                }
            }

            okButton {
                var modify = true
                try {
                    if (submitSimpleFormat.format(calendar.time) == userInfo?.birthday)
                        modify = false
                    else
                        userInfo?.birthday = submitSimpleFormat.format(calendar.time)

                } catch (e: Exception) {
                    modify = true
                }

                if (modify) {
                    updateUserInfo()
                }
            }

            cancelButton {

            }

        }.show()
    }

    /**
     * 更新用户个人信息
     */
    private fun updateUserInfo() {

        showLoadingDialog(R.string.text_modify_userinfo, R.string.text_modify_userinfo_success)

        HttpRequest.modifyUserInfo(token,
                userInfo?.gender ?: "",
                userInfo?.birthday ?: "", null, { isSuccess, errorMsg ->

            if (isSuccess) {
                requestUserInfo()

            } else {
                setFailLoadingText(errorMsg)
                failLoading()
            }

        })

    }

    /**
     * 修改性别
     */
    private fun modifySex() {
        if (userInfo == null)
            return

        val dialog = SexChooseDialog()
        dialog.sexStr = userInfo?.gender ?: ""
        dialog.okConsumer = Consumer {
            userInfo?.gender = if (it) "M" else "F"
            updateUserInfo()
        }
        dialog.show(supportFragmentManager, "sexDialog")
    }

    /**
     * 从相册获取图片
     */
    private fun openGallery() {
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofImage())
                .imageSpanCount(3)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .compressGrade(Luban.CUSTOM_GEAR)
                .isCamera(false)
                .enableCrop(false)
                .compress(true)
                .showCropFrame(false)
                .showCropGrid(false)
                .compressMode(PictureConfig.LUBAN_COMPRESS_MODE)
                .circleDimmedLayer(true)
                .forResult(PictureConfig.CHOOSE_REQUEST)
    }

    /**
     * 显示并且上传图片
     * @param picPath 图片地址
     * fixme:未上传
     */
    private fun showAndUploadPic(picPath: String) {

        picPath
                .filter { picPath.isNotEmpty() }
                .map { File(picPath) }.first { it.exists() }
                .also {
                    showLoadingDialog(getString(R.string.text_uploading_avatar), getString(R.string.text_upload_avatar_success))
                    DbUtils.uploadUserAvatar(uid, it, { isSuccess, info ->
                        if (isSuccess) {
                            photo = info
                            headImageView.setImageURI(photo)
                            RxBus.getInstance().post(PIC_CHANGE, photo)
                            successLoading()
                        } else {
                            setFailLoadingText(getString(R.string.error_upload_avatar))
                            failLoading()
                        }
                    })
                }

    }

    /**
     * 获取用户信息的回调
     */
    private fun displayGetUserInfo(user: GizUser) {
//        successLoading()
        closeLoadingDialog()

        this.userInfo = user
        LogUtils.e(TAG, "email=${userInfo?.email} gender=${userInfo?.gender} birthday=${userInfo?.birthday}")

        //显示的和获取的account都不为空，并且两者不相等，说明修改了
        if (!piv_email.rightText.isNullOrBlank()
                && !userInfo?.email.isNullOrBlank()
                && piv_email.rightText != userInfo?.email) {
            account = StringUtils.null2Length0(userInfo?.email)
            RxBus.getInstance().post(REFRESH_EMAIL, account)
            successLoading()
        }

        piv_email.rightText = StringUtils.null2Length0(userInfo?.email)

        piv_gender.rightText = StringUtils.null2Length0(

                when (userInfo?.gender) {
                    "F" -> "Female"
                    "M" -> "Male"
                    else -> ""
                }
        )

        if (!userInfo?.birthday.isNullOrEmpty()) {
            try {
                val parse = submitSimpleFormat.parse(userInfo?.birthday)
                piv_birth.rightText = StringUtils.null2Length0(showSimpleFormat.format(parse))
            } catch (e: Exception) {
                LogUtils.e(TAG, "日期解析出错 ${e.message}")
            }

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == PictureConfig.CHOOSE_REQUEST) {

            // 图片选择结果回调
            val selectList = PictureSelector.obtainMultipleResult(data)

            selectList
                    .firstOrNull()
                    ?.run {
                        showAndUploadPic(
                                when {
                                    isCompressed -> compressPath
                                    isCut -> cutPath
                                    else -> path
                                }
                        )
                    }
        }
    }
}