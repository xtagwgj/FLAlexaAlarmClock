package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.daimajia.swipe.SwipeLayout
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.databinding.ItemGroupBinding
import com.xbdl.flalexaclock.entity.GroupEntity
import com.xbdl.flalexaclock.ui.GroupSettingActivity
import org.jetbrains.anko.find

/**
 * 分组的列表
 * Created by xtagwgj on 2017/9/11.
 */
class GroupListAdapter(private val onDeleteListener: (GroupEntity) -> Unit)
    : DBAdapter<GroupEntity, ItemGroupBinding>(null, R.layout.item_group) {

    var isEdit: Boolean = false
        set(value) {
            field = value

            openSwipeOnlyIndex = -1
            notifyDataSetChanged()

        }

    //当前打开的位置
    private var openSwipeOnlyIndex: Int = -1

    override fun onBindViewHolder(holder: DBViewHolder<ItemGroupBinding>, position: Int, item: GroupEntity) {
        holder.binding.isEdit = isEdit
        holder.binding.item = item

        //可以滑动的布局
        val swipeLayout = holder.binding.root.find<SwipeLayout>(R.id.swipeLayout).apply {
            isSwipeEnabled = false
        }

        swipeLayout.close()

        holder.binding.root.find<View>(R.id.ll_groupView).apply {
            setOnClickListener {
                if (isEdit) {//在编辑状态点击关闭swipeLayout
                    swipeLayout.close()
                } else {//进入该组的详情
                    GroupSettingActivity.doAction(mContext as GizBaseActivity, item.id, item.group_name)
                }
            }
        }

        //最左边的减号的点击事件
        holder.binding.root.find<ImageView>(R.id.iv_del).setOnClickListener({
            if (swipeLayout.openStatus == SwipeLayout.Status.Open) {
                swipeLayout.close()
                openSwipeOnlyIndex = -1
            } else {
                swipeLayout.open()
//                val before = openSwipeOnlyIndex
//                openSwipeOnlyIndex = position
                if (position != openSwipeOnlyIndex) {
                    notifyItemChanged(openSwipeOnlyIndex)
                    openSwipeOnlyIndex = position
                }
            }

        })

        //删除按钮
        holder.binding.root.find<Button>(R.id.bottom_wrapper).apply {
            setOnClickListener {
                openSwipeOnlyIndex = -1
                onDeleteListener(item)
            }
        }

//        holder.binding.root.find<CheckBox>(R.id.rb_open).apply {
//            setOnClickListener({
//                isChecked = item.switchStatue
//                onButtonListener(item, !item.switchStatue)
//            })
//        }

    }

    fun refreshDataByTime(mDatas: List<GroupEntity>?) {
//        val sortedBy = mDatas?.sortedBy { it.created_at }
        refreshData(mDatas?.sortedBy { it.created_at })
    }
}