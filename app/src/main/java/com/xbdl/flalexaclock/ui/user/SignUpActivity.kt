package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizUserAccountType
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.jakewharton.rxbinding2.widget.RxTextView
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.GizErrorToast
import com.xbdl.flalexaclock.base.MIN_LENGTH_LOGIN_PWD
import com.xbdl.flalexaclock.entity.giz.RegisterUserResponse
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.RegexUtil
import kotlinx.android.synthetic.main.activity_sign_up.*


/**
 * 注册界面
 */
class SignUpActivity : GizBaseActivity() {

    private var passwordIsShow = false

    companion object {

        /**
         * 启动页面
         * @param mContext activity
         * @param requestCode 请求码(可不填),默认为注册用户
         */
        fun doAction(mContext: Activity, requestCode: Int = LoginActivity.REQUEST_SIGN_UP) {
            val intent = Intent(mContext, SignUpActivity::class.java)
            startByAnim(mContext, intent, requestCode)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_sign_up

    override fun initView(p0: Bundle?) {

    }

    override fun initEventListener() {

        clickEvent(outSizeView, { hideInput() })

        RxTextView.textChanges(passwordEditText)
                .compose(bindToLifecycle())
                .map { it.isNotEmpty() }
                .subscribe({
                    clearPwdButton.visibility = if (it) View.VISIBLE else View.INVISIBLE
                }, {})


        //密码的显示与隐藏
        clickEvent(hidePwdButton, {

            if (passwordIsShow) {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_close)
                //不可以看到密码
                passwordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_open)
                //不可以看到密码
                passwordEditText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
            passwordIsShow = !passwordIsShow

            //移动光标到最后的位置
            passwordEditText.setSelection(passwordEditText.text.toString().trim { it <= ' ' }.length)

        })


        //清除密码
        clickEvent(clearPwdButton, {
            passwordEditText.setText("")
            passwordEditText.requestFocus()
        })


        //去登录
        clickEvent(bottomAction, { finish() })

        //注册或修改密码按钮操作
        clickEvent(signUpButton, {

            if (!RegexUtil.checkEmail(emailEditText.text.toString())) {
                ToastUtil.showToast(this, R.string.text_hint_error_phone)
                return@clickEvent
            }

            if (passwordEditText.text.trim().toString().length < MIN_LENGTH_LOGIN_PWD) {
                ToastUtil.showToast(this, R.string.text_hint_error_password)
                return@clickEvent
            }

            if (!cb_agreeService.isChecked) {
                ToastUtil.showToast(this, R.string.text_hint_error)
                return@clickEvent
            }

            GizWifiSDK.sharedInstance().registerUser(
                    emailEditText.text.toString(),
                    passwordEditText.text.toString(),
                    null,
                    GizUserAccountType.GizUserEmail
            )

            showLoadingDialog(R.string.loading_register, R.string.success_giz_register)

        })


        //接收到注册用户结果的回调
        RxBus.getInstance()
                .register<RegisterUserResponse>("RegisterUserResponse")
                .compose(bindToLifecycle())
                .subscribe(
                        {
                            displayRegisterUser(it.result, it.uid, it.token)
                        },
                        {

                        },
                        {
                            RxBus.getInstance().unRegister("RegisterUserResponse")
                        })

//        emailEditText.setText("")
//        emailEditText.requestFocus()
//        val inputManager = emailEditText.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputManager.showSoftInput(emailEditText, 0)

        emailEditText.isFocusable = true
        emailEditText.requestFocus()
    }


    /**
     * 注册用户
     */
    private fun displayRegisterUser(result: GizWifiErrorCode, uid: String?, token: String?) {
        when (result) {
            GizWifiErrorCode.GIZ_SDK_SUCCESS -> {
                closeLoadingDialog()
                setResult(Activity.RESULT_OK, getResultIntent())
                destroyByAnim(SignUpActivity@ this, LoginActivity.REQUEST_SIGN_UP)
            }
            else -> {
                setFailLoadingText(GizErrorToast.parseExceptionCode(result))
                failLoading()
            }
        }
    }

    /**
     * 获取intent
     */
    private fun getResultIntent(): Intent {
        val intent = intent
        intent.putExtra(LoginActivity.ACCOUNT, emailEditText.text.toString().trim())
        intent.putExtra(LoginActivity.PASSWORD, passwordEditText.text.toString().trim())

        return intent
    }

}