package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemSearchClockBinding
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.widget.AlarmView
import org.jetbrains.anko.find

/**
 * 搜索结果的适配器
 * Created by xtagwgj on 2017/9/15.
 */
class SearchAdapter(private val onClickItem: (ClockEntity) -> Unit)
    : DBAdapter<ClockEntity, ItemSearchClockBinding>(null, R.layout.item_search_clock) {
    override fun onBindViewHolder(holder: DBViewHolder<ItemSearchClockBinding>, position: Int, item: ClockEntity) {
        holder.binding.item = mContext.getDeviceName(item.device)
        holder.binding.itemOtherInfo = item.alarmInfo
//
//        holder.binding.root.find<ImageView>(R.id.iv_group_pic).apply {
//            if (item.device.netStatus == GizWifiDeviceNetStatus.GizDeviceOffline
//                    || item.device.netStatus == GizWifiDeviceNetStatus.GizDeviceUnavailable) {
//                setImageResource(R.mipmap.pic_off_line)
//            } else {
//                setImageResource(R.mipmap.pic_product_set_alarm)
//            }
//        }

        holder.binding.root.find<View>(R.id.ll_search).setOnClickListener {
            onClickItem(item)
        }

        holder.binding.root.find<AlarmView>(R.id.iv_group_pic).apply {
            when (item.device.netStatus) {
                GizWifiDeviceNetStatus.GizDeviceControlled,
                GizWifiDeviceNetStatus.GizDeviceOnline -> {
                    val timeZone = TimeZoneTableUtils.getCurrTimeZone(item.timeZone)
                    setTime(timeZone.id)
                }

                else -> {
                    setTimeVisible(false)
                }
            }
        }

    }
}