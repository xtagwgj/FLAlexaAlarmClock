package com.xbdl.flalexaclock.ui.adapter

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import com.daimajia.swipe.SwipeLayout
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.databinding.ItemClockHeaderBinding
import com.xbdl.flalexaclock.databinding.ItemDeviceByGroupBinding
import com.xbdl.flalexaclock.entity.StickyItemBaseEntity
import com.xbdl.flalexaclock.entity.StickyItemEntity
import com.xbdl.flalexaclock.extensions.deviceIsOnline
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.ui.device.AlarmClockDetailsActivity
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.widget.AlarmView
import org.jetbrains.anko.find

/**
 * 设备按分组显示的列表
 * Created by xtagwgj on 2017/9/18.
 */
class DeviceByGroupAdapter(
        private val deleteDevice: (StickyItemEntity) -> Unit,
        private val controlSwitch: (StickyItemEntity, Boolean) -> Unit,
        private val reconnect: (GizWifiDevice) -> Unit)
    : DBAdapter<StickyItemBaseEntity, ViewDataBinding>(null, -1) {

    private val VIEW_TITLE = 0
    private val VIEW_CONTENT = 1

    var isEdit: Boolean = false
        set(value) {
            if (field != value) {
                field = value

                openSwipeOnlyIndex = -1
                notifyDataSetChanged()
            }
        }

    //当前打开的位置
    private var openSwipeOnlyIndex: Int = -1

    override fun onBindViewHolder(holder: DBViewHolder<ViewDataBinding>, position: Int, item: StickyItemBaseEntity) {
        if (getItemViewType(position) == VIEW_CONTENT) {
            bindDetails(holder as DBViewHolder<ItemDeviceByGroupBinding>, position, item as StickyItemEntity)
        } else {
            val spannableString = SpannableString(item.title)
            if (item.title.contains("(")) {
                val colorSpan = ForegroundColorSpan(mContext.resources.getColor(R.color.colorHeaderUTC))
                spannableString.setSpan(colorSpan, item.title.indexOf("("), spannableString.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
            }

            (holder.binding as ItemClockHeaderBinding).header = spannableString
        }
    }

    private fun bindDetails(holder: DBViewHolder<ItemDeviceByGroupBinding>, position: Int, item: StickyItemEntity) {
        holder.binding.isEdit = isEdit

        item.details.also {
            //            holder.binding.item = if (StringUtils.isEmpty(it.device.alias)) it.device.productName else it.device.alias
            holder.binding.item = mContext.getDeviceName(item.details.device)
            holder.binding.isOpen = it.isClockSwitch
            holder.binding.alarmInfo = it.alarmInfo
        }

        holder.binding.root.find<AlarmView>(R.id.iv_group_pic).apply {

            when (item.details.device.netStatus) {
                GizWifiDeviceNetStatus.GizDeviceControlled,
                GizWifiDeviceNetStatus.GizDeviceOnline -> {
//                    val timeZone = TimeZoneTableUtils.getCurrTimeZone(item.details.timeZone)
//                    setTime(timeZone.id)

                    setTime(TimeZoneTableUtils.getByTimeZone(item.details.timeZone).timeZoneId)
                }

                else -> {
                    setTimeVisible(false)
                }
            }
        }

        holder.binding.mContext = mContext

        //可以滑动的布局
        val swipeLayout = holder.binding.root.find<SwipeLayout>(R.id.swipeLayout).apply {
            isSwipeEnabled = false
            setBackgroundColor(mContext.resources.getColor(if (item.details.isClockSwitch) R.color.colorItemBgOpen else R.color.colorItemBgClose))
        }

        swipeLayout.close()

        holder.binding.root.find<RelativeLayout>(R.id.ll_groupView).apply {
            setOnClickListener {
                if (isEdit) {//在编辑状态点击关闭swipeLayout
                    swipeLayout.close()
                } else {//进入该设备的详情
                    AlarmClockDetailsActivity.doAction(mContext as GizBaseActivity, item.details.device)
                }
            }
        }

        //最左边的减号的点击事件
        holder.binding.root.find<ImageView>(R.id.iv_del).setOnClickListener {
            if (swipeLayout.openStatus == SwipeLayout.Status.Open) {
                swipeLayout.close()
                openSwipeOnlyIndex = -1
            } else {
                swipeLayout.open()
                if (position != openSwipeOnlyIndex) {
                    notifyItemChanged(openSwipeOnlyIndex)
                    openSwipeOnlyIndex = position
                }
            }

        }

        //删除按钮
        holder.binding.root.find<Button>(R.id.bottom_wrapper).setOnClickListener {
            deleteDevice(item)
        }

        //开关
//        holder.binding.root.find<CheckBox>(R.id.rb_open).apply {
//            visibility = if (!isEdit && mContext.deviceIsOnline(item.details.device)) View.VISIBLE else View.GONE
//            setOnClickListener {
//                isChecked = item.details.isClockSwitch
//                controlSwitch(item, !isChecked)
//            }
//        }


        //设备离线的操作
        holder.binding.root.find<ImageView>(R.id.iv_reConnect).apply {
            visibility = if (!isEdit && !mContext.deviceIsOnline(item.details.device)) View.VISIBLE else View.GONE
            setOnClickListener {
                reconnect(item.details.device)
            }
        }

        holder.binding.root.find<ImageView>(R.id.iv_more).apply {
            visibility = if (!isEdit && !mContext.deviceIsOnline(item.details.device)) View.GONE else View.VISIBLE
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (mDatas[position] is StickyItemEntity)
            VIEW_CONTENT
        else
            VIEW_TITLE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DBViewHolder<ViewDataBinding> {
        mContext = parent.context

        val binding = DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(mContext),

                if (viewType == VIEW_CONTENT)
                    R.layout.item_device_by_group
                else
                    R.layout.item_clock_header,

                parent, false)

        return DBViewHolder(binding)
    }

    /**
     * 获取时区，未找到返回-1
     */
    fun getTimeZone(did: String): Int {
        return mDatas
                .filterIsInstance<StickyItemEntity>()
                .firstOrNull { it.details.device.did == did }
                ?.let { it.details.timeZone }
                ?: -1
    }


    fun getSwitchStatus(did: String): Boolean {
        return mDatas
                .filterIsInstance<StickyItemEntity>()
                .firstOrNull { it.details.device.did == did }
                ?.let { it.details.isClockSwitch }
                ?: false
    }


    fun deleteItem(item: StickyItemEntity) {
        val pos = mDatas.indexOf(item)
        if (pos > 0) {
            mDatas.remove(item)
            notifyItemRemoved(pos)

            //判断是否需要删除头部信息
            if (getItemViewType(pos - 1) == VIEW_TITLE) {
            }
        }

    }

}