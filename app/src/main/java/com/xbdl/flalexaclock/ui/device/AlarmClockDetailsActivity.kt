package com.xbdl.flalexaclock.ui.device

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.entity.DeviceChangeEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.entity.GroupEntity
import com.xbdl.flalexaclock.extensions.Preference
import com.xbdl.flalexaclock.extensions.deviceIsController
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.extensions.sendCommand
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.AddAlarmActivity
import com.xbdl.flalexaclock.ui.adapter.AlarmListNoSwipeAdapter2
import com.xbdl.flalexaclock.ui.dialog.NormalMessageDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xbdl.flalexaclock.widget.ClockMorePop
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.MeasureUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_alarm_clock_details.*
import org.jetbrains.anko.toast
import java.util.concurrent.ConcurrentHashMap

/**
 * 闹钟详情页面
 * Created by xtagwgj on 2017/9/15.
 */
class AlarmClockDetailsActivity() : GizBaseActivity() {

    private val TAG = "self_AlarmClockDetailsActivity"

    private var morePop: ClockMorePop? = null
    private var adapter: AlarmListNoSwipeAdapter2? = null
    private lateinit var device: GizWifiDevice
    private var clockEntity: ClockEntity? = null

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")

    companion object {
        /**
         * @param judeDeviceMove 是否需要判断设备的移动
         */
        fun doAction(mContext: Activity, device: GizWifiDevice) {
            val intent = Intent(mContext, AlarmClockDetailsActivity::class.java)
            intent.putExtra("device", device)
            startByAnim(mContext, intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_alarm_clock_details

    override fun initView(p0: Bundle?) {

        //获取设备信息
        device = intent.getParcelableExtra("device")

        if (device == null)
            AppManager.getAppManager().finishActivity()

        initTitle(getDeviceName(device))

        adapter = AlarmListNoSwipeAdapter2(deviceIsController(device),
                {
                    AddAlarmActivity.doAction(this@AlarmClockDetailsActivity, device, it)
                },
                { currDevice, stateKey ->

                    //删除闹铃
                    showLoadingDialog(R.string.load_del_alarm, R.string.load_del_alarm_success)
                    sendCommand(currDevice, stateKey, 3, SN_DELETE_ALARM)
                },
                { item, state ->
                    //开启关闭闹铃

                    var key: String? = null

                    //子闹钟的开启关闭
                    when (item.pos) {
                        1 -> key = GizDataPointKey.KEY_ALARM1_STATE
                        2 -> key = GizDataPointKey.KEY_ALARM2_STATE
                        3 -> key = GizDataPointKey.KEY_ALARM3_STATE
                        4 -> key = GizDataPointKey.KEY_ALARM4_STATE
                        5 -> key = GizDataPointKey.KEY_ALARM5_STATE
                        6 -> key = GizDataPointKey.KEY_ALARM6_STATE
                        7 -> key = GizDataPointKey.KEY_ALARM7_STATE
                        8 -> key = GizDataPointKey.KEY_ALARM8_STATE
                        9 -> key = GizDataPointKey.KEY_ALARM9_STATE
                        10 -> key = GizDataPointKey.KEY_ALARM10_STATE
                        11 -> key = GizDataPointKey.KEY_ALARM11_STATE
                        12 -> key = GizDataPointKey.KEY_ALARM12_STATE
                        else -> {
                        }
                    }

                    if (key != null) {
                        val commandMap = ConcurrentHashMap<String, Any>()
                        commandMap.put(key, state)

                        if (state != 1)
                            commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)

                        sendCommand(device, commandMap)
                    }
                })

        recyclerClockList.apply {
            layoutManager = LinearLayoutManager(this@AlarmClockDetailsActivity)
            addItemDecoration(SimpleDividerDecoration(this@AlarmClockDetailsActivity,
                    dividerColorInt = resources.getColor(R.color.colorDevider),
                    leftPadding = MeasureUtil.dp2px(this@AlarmClockDetailsActivity, 16F)))
        }.adapter = adapter

        //添加新的闹铃
        clickEvent(bt_addNew, {
            if (clockEntity != null && clockEntity?.newAlarm == null) {
                ToastUtil.showToast(this, R.string.text_alarm_above_12)
            } else {
                AddAlarmActivity.doAction(this@AlarmClockDetailsActivity, device)
            }
        })


        //获取数据并且解析显示
        RxBus.getInstance()
                .register<DeviceChangeEntity>(REFRESH_DEVICE)
                .compose(bindToLifecycle())
                .filter {
                    LogUtils.d(TAG, "$it")
                    it.did == device.did
                }
                .map { ClockUtils.onlineDevicePoint[it.did] }
                .filter { it != null }
                .map { entity ->
                    LogUtils.e(TAG, entity)

                    adapter?.setAutoCalibration(entity!!.data_time_calibration)
                    val clock = ClockUtils.clockAlarmMap[device.did]
                    clock
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    closeLoadingDialog()
                    clockEntity = it
                    displayPageInfo(it!!)
                }, {
                    it.printStackTrace()
                })

//        //观察设备的状态
//        RxBus.getInstance()
//                .register<UpdateNetStatusResponse>("UpdateNetStatusResponse")
//                .compose(bindToLifecycle())
//                .filter { it.device?.did == device.did }
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    checkAndSubscribeDevice()
//                }, {
//                    it.printStackTrace()
//                })

        //解绑设备 是当前设备的解绑回调才执行
//        RxBus.getInstance()
//                .register<BindOrUnBindResponse>("BindOrUnBindResponse")
//                .compose(bindToLifecycle())
//                .filter { !it.isBind && it.did == device.did }//解绑，并且是当前的did
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    if (it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {//解绑成功
//                        closeLoadingDialog()
//                        AppManager.getAppManager().finishActivity()
//                    } else {//解绑失败
//                        setFailLoadingText(GizErrorToast.parseExceptionCode(it.result))
//                        failLoading()
//                    }
//                }, {
//                    it.printStackTrace()
//                })

        //时间制发生变化
        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_CHANGED)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    adapter?.notifyDataSetChanged()
                })

        displayPageInfo(ClockUtils.clockAlarmMap[device.did]!!)
    }

    override fun onResume() {
        super.onResume()
        initTitle(getDeviceName(device))
        if (adapter?.isEdit == true) {
            adapter?.isEdit = false
        }
    }

    /**
     * 检查并且订阅设备
     */
    private fun checkAndSubscribeDevice() {
        when (device.netStatus) {
            GizWifiDeviceNetStatus.GizDeviceOnline, GizWifiDeviceNetStatus.GizDeviceControlled -> {
                if (device.listener == null)
                    device.listener = ParseDataService.deviceListener

                if (!device.isSubscribed)
                    device.setSubscribe(BuildConfig.PRODUCT_SECRET, true)
            }
            GizWifiDeviceNetStatus.GizDeviceOffline -> {
                toast(R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
            }

            else -> {
                toast(R.string.text_device_unAvailable)
            }
        }
    }

    /**
     * 显示解析的数据
     */
    private fun displayPageInfo(clockEntity: ClockEntity) {
        //标题
        initTitle(
                getDeviceName(clockEntity.device)
        )


        //刷新子列表
        adapter?.refreshData(clockEntity)
    }


    private fun initTitle(title: String) {
        titleLayout.setTitleText(title)
    }

    override fun initEventListener() {

        iv_more.setOnClickListener {
            if (morePop == null) {
                morePop = ClockMorePop(this, object : ClockMorePop.PopClickListener {
                    override fun alarmSetting() {
                        AlarmClockSettingActivity.doAction(this@AlarmClockDetailsActivity, device)
                    }

                    override fun alarmUnbind() {

                        val unBindDialog = NormalMessageDialog()
                        unBindDialog.textRes = R.string.text_dialog_unbind
                        unBindDialog.okConsumer = Consumer {
                            showLoadingDialog(R.string.text_loading_unbind, R.string.text_loaing_unbind_success)
//                            GizWifiSDK.sharedInstance().unbindDevice(uid, token, device.did)

                            device.listener = null
                            HttpRequest.unBindDevice(device.did, token, { unBindResult, errorMsg ->
                                closeLoadingDialog()
//
//                                //解绑成功
//                                if (unBindResult) {
//                                    val showDeviceList = ClockUtils.getAllAlarmClock()
//                                    val index = showDeviceList.indexOfFirst { it.device.did == device.did }
//                                    if (index != -1)
//                                        showDeviceList.removeAt(index)
//
//                                    ClockUtils.deviceList.removeAll { it.did == device.did }
//
//                                    RxBus.getInstance().post(REFRESH_DEVICE_LIST)
//                                    RxBus.getInstance().post(REFRESH_ALARM_LIST)
//                                    AppManager.getAppManager().finishActivity()
//                                } else {
//                                    //解绑失败
//                                    toast(errorMsg ?: "UnBind Device fail")
//                                    device.listener = ParseDataService.deviceListener
//                                }

                                //解绑成功
                                if (unBindResult) {

                                    //删除分组中的信息
                                    removeDeviceFromGroup(device.did)

                                    ClockUtils.unbindDevice(device.did)

                                    //更新
                                    AppManager.getAppManager().finishActivity(AlarmClockDetailsActivity::class.java)

                                } else {
                                    //解绑失败
                                    toast(errorMsg ?: "UnBind Device fail")
                                    device.listener = ParseDataService.deviceListener
                                }


                            })

                        }

                        unBindDialog.show(this@AlarmClockDetailsActivity.supportFragmentManager, "unBindDialog")
                    }
                })
            }

            if (morePop!!.isShowing)
                morePop!!.dismiss()
            else
                morePop!!.showAsDropDown(iv_more)
        }

        //时间变化
        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_TICK)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //                    tv_nextTimeAlarm.text = clockEntity?.mainAlarm?.nextRingTime

                    if ((adapter?.itemCount ?: 0) > 0)
                        adapter?.notifyDataSetChanged()
                })

    }

    /**
     * 删除分组里面的数据
     */
    private fun removeDeviceFromGroup(deviceDid: String) {

        //查找设备所属的分组
        var groupEntity: GroupEntity? = null
        for ((group, didList) in ClockUtils.groupDeviceMap) {
            if (didList.contains(deviceDid)) {
                groupEntity = group
                break
            }
        }

        //没有分组，返回
        if (groupEntity == null)
            return

        //删除分组中的设备
        HttpRequest.removeDeviceFromGroup(groupEntity.id, listOf(deviceDid))
                .filter {
                    val deleteSuccess = it.success.isNotEmpty()

                    if (deleteSuccess) {//删除成功
                        groupEntity?.let {
                            ClockUtils.refreshGroupDeviceByGroup(it)
                        }

                    } else {
                        //删除失败
//                        activity.toast(R.string.fail_remove_device_from_group)
                    }
                    deleteSuccess
                }
                .subscribe({

                    //                    Log.e("从当前群组移除设备并添加到默认群组成功", it.toString())
                }, {
                    it.printStackTrace()
                })
    }
}