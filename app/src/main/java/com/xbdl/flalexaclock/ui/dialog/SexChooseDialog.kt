package com.xbdl.flalexaclock.ui.dialog

import android.view.View
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_choose_sex.*

/**
 * 性别选择框
 * Created by xtagwgj on 2017/9/12.
 */
class SexChooseDialog() : BaseRxDialog(), View.OnClickListener {

    lateinit var sexStr: String
    lateinit var okConsumer: Consumer<Boolean>


    override fun getLayoutId(): Int {
        return R.layout.dialog_choose_sex
    }


    override fun initView() {

        if (sexStr.toUpperCase().contains("F")) {
            rb_female.isChecked = true
        } else if (sexStr.toUpperCase().contains("M")) {
            rb_male.isChecked = true
        }

        clickEvent(bt_cancel, {
            dismiss()
        })


        tv_male.setOnClickListener(this)
        rb_male.setOnClickListener(this)
        tv_female.setOnClickListener(this)
        rb_male.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            tv_male.id, rb_male.id -> {
                okConsumer.accept(true)
                dismiss()
            }

            tv_female.id, rb_male.id -> {
                okConsumer.accept(false)
                dismiss()
            }

        }
    }
}