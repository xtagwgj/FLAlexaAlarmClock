package com.xbdl.flalexaclock.ui.config

import android.Manifest
import android.app.Activity
import android.content.*
import android.net.Uri
import android.net.wifi.WifiInfo
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizWifiConfigureMode
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.gizwits.gizwifisdk.enumration.GizWifiGAgentType
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tbruyelle.rxpermissions2.RxPermissions
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.giz.DeviceOnBoardingResposne
import com.xbdl.flalexaclock.extensions.Preference
import com.xbdl.flalexaclock.ui.device.AlarmClockSettingActivity
import com.xbdl.flalexaclock.ui.dialog.NormalMessageDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.NetWorkUtils
import com.xtagwgj.baseproject.utils.PhoneUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_configuration_wifi.*
import org.jetbrains.anko.doAsync
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * 配网、输入wifi的界面
 * Created by xtagwgj on 2017/7/21.
 */
class ConfigurationWifiActivity : GizBaseActivity() {

    private val TAG = "self_ConfigurationWifiActivity"

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")
    private var currShowPwd: Boolean = false

    //配网的设备did
    private var deviceDid: String? = null

    private var reConnectDid: String? = null

    /**
     * 是否已经显示了对话框，防止多次显示相同的对话框
     */
    private var isShowSettingDialog = false

    private var rxPermission: RxPermissions? = null


    //广播接收者，在这里接收一些需要使用到的系统广播
    private var mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {

            //网络变化
            if (intent?.action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                checkPermission()
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_configuration_wifi

    companion object {
        fun doAction(mContext: Activity, did: String? = null) {
            val intent = Intent(mContext, ConfigurationWifiActivity::class.java)
            intent.putExtra("did", did ?: "")
            startByAnim(mContext, intent)
        }
    }

    override fun initView(p0: Bundle?) {
        rxPermission = RxPermissions(this)

        //获取配网的did
        reConnectDid = intent.getStringExtra("did")

        hidePwdButton.setImageResource(R.mipmap.ic_eye_close)

        RxTextView.textChanges(wifiPasswordEditText)
                .compose(bindToLifecycle())
                .map { it.isNotEmpty() }
                .subscribe({
                    clearPwdButton.visibility = if (it) View.VISIBLE else View.GONE
                })

        registerReceiver(mReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun initEventListener() {

        Observable
                .combineLatest(
                        RxTextView.textChanges(wifiNameEditText)
                                .compose(this.bindToLifecycle<CharSequence>())
                                .map { charSequence -> charSequence.toString().trim().isNotEmpty() },
                        RxTextView.textChanges(wifiPasswordEditText)
                                .compose(this.bindToLifecycle<CharSequence>())
                                .map { charSequence -> charSequence.toString().trim().isNotEmpty() },
                        BiFunction<Boolean, Boolean, Boolean> { wifiNameValid, _ -> wifiNameValid!! }
                )
                .compose(this.bindToLifecycle<Boolean>())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { valueValid -> completeConfigButton.isEnabled = valueValid!! }

        clickEvent(view, { hideInput() })

        clickEvent(hidePwdButton, {
            if (currShowPwd) {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_close)
                //不可以看到密码
                wifiPasswordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_open)
                //可以看到密码
                wifiPasswordEditText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }

            currShowPwd = !currShowPwd
            //移动光标到最后的位置
            wifiPasswordEditText.setSelection(wifiPasswordEditText.text.toString().trim().length)
        })


        //清除密码
        clickEvent(clearPwdButton, {
            wifiPasswordEditText.setText("")
            wifiPasswordEditText.requestFocus()
        })

        //打开wifi设置页面
        clickEvent(rl_wifi, {
            val it = Intent().apply {
                val cn = ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings")
                component = cn
            }

            startActivity(it)
        })

        clickEvent(completeConfigButton, {

            showLoadingDialog(R.string.text_loading_config_wifi, R.string.app_name)

//            Observable.just(NetWorkUtils.isWifiConnected(ConfigurationWifiActivity@ this))
            Observable
                    .timer(1500, TimeUnit.MILLISECONDS)
                    .map { NetWorkUtils.isWifiConnected(ConfigurationWifiActivity@ this) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it) {
                            val types = ArrayList<GizWifiGAgentType>()
                            types.add(GizWifiGAgentType.GizGAgentESP)

                            //开始配网
                            GizWifiSDK.sharedInstance().setDeviceOnboarding(
                                    wifiNameEditText.text.toString().trim(),
                                    wifiPasswordEditText.text.toString().trim(),
                                    GizWifiConfigureMode.GizWifiAirLink, null, 60, types)

                        } else {

                            ToastUtil.showToast(ConfigurationWifiActivity@ this, "No Wi-Fi network selected")
                        }
                    }, {
                        it.printStackTrace()
                        closeLoadingDialog()
                    })

        })

        //配网回调的监听
        RxBus.getInstance()
                .register<DeviceOnBoardingResposne>("DeviceOnBoardingResposne")
                .subscribe({
                    LogUtils.e(TAG, it)
                    displaySetDeviceOnboarding(it.result, it.mac, it.did, it.productKey)
                }, {
                    it.printStackTrace()

                    setFailLoadingText(it.message)
                    failLoading()
                })

        //设备列表变化了
        RxBus.getInstance()
                .register<String>(REFRESH_DEVICE_LIST)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    LogUtils.e(TAG, "设备列表: ${ClockUtils.deviceList}")
                    showSetDialog()
                }, {
                    it.printStackTrace()
                    setFailLoadingText(it.message)
                    failLoading()
                })
    }


    /**
     * 检查是否有要进行引导设置的设备
     */
    private fun showSetDialog() {

        //已经在显示引导框 或者 配网的设备 did 为 null 就返回
        if (isShowSettingDialog || deviceDid.isNullOrEmpty()) {
            return
        }

        //配网的设备就是重连的设备，不需要显示配置的信息，使用旧的信息就可以
        if (deviceDid == reConnectDid) {
            LogUtils.e(TAG, "deviceDid == reConnectDid :" + (deviceDid == reConnectDid))
            back2Home()
            return
        }

        //在设备列表查找此时配置的设备
        val showDialogDevice: GizWifiDevice? = ClockUtils.deviceList.firstOrNull { it.did == deviceDid }
        if (showDialogDevice == null) {
            GizWifiSDK.sharedInstance().getBoundDevices(uid, token, listOf(BuildConfig.PRODUCT_KEY))
            return
        } else {
            showDialogDevice.listener = ParseDataService.deviceListener
            showDialogDevice.setSubscribe(BuildConfig.PRODUCT_SECRET, true)
            showDialogDevice.getDeviceStatus()
        }

        isShowSettingDialog = true

        closeLoadingDialog()

        val showSetting = NormalMessageDialog()
        showSetting.leftTextRes = R.string.text_wait_2_update
        showSetting.rightTextRes = R.string.text__update_now
        showSetting.textRes = R.string.bind_2_set_content
        showSetting.isCancelable = false

        showSetting.cancelConsumer = Consumer {
            showSetting.dismiss()

            // 配置成功
            back2Home()
        }
        showSetting.okConsumer = Consumer {
            showSetting.dismiss()

            AlarmClockSettingActivity.doAction(MainActivity@ this, showDialogDevice, true)
            // 配置成功
            back2Home()
        }
        showSetting.show(supportFragmentManager, "showSetting")
    }

    private fun back2Home() {
        // 配置成功
        AppManager.getAppManager().finishActivity(ConfigurationWifiTipsActivity::class.java)
        AppManager.getAppManager().finishActivity(ConfigurationWifiActivity::class.java)
    }

    override fun onStart() {
        super.onStart()
        checkPermission()
    }

    /**
     * 检查是否有权限获取 wifi
     */
    private fun checkPermission() {

        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {

            rxPermission?.request(Manifest.permission.ACCESS_FINE_LOCATION)
                    ?.subscribe { granted ->
                        //                        wifiNameEditText.isEnabled = granted == false
                        if (granted) {
                            getWifiInfo()
                        } else {
                            NormalMessageDialog().apply {
                                textRes = R.string.text_permission_loc_miss
                                cancelConsumer = Consumer {
                                    dismiss()
                                    finish()
                                }

                                okConsumer = Consumer {
                                    dismiss()
                                    goAppDetailsSettingIntent()
                                }

                                isCancelable = false

                            }.show(supportFragmentManager, "settingDialog")

                        }
                    }

        } else {
            getWifiInfo()
        }

    }

    /**
     * 获取wifi信息
     */
    private fun getWifiInfo() {
        Observable.just(PhoneUtils.getCurrWifi(applicationContext))
                .compose(this.bindToLifecycle<WifiInfo>())
                .filter { wifiInfo -> wifiInfo != null }
                .map { wifiInfo ->
                    val isWifi = NetWorkUtils.isWifiConnected(ConfigurationWifiActivity@ this)
                    if (!isWifi) {
                        getString(R.string.text_curr_wifi)
                    } else {
                        val wifiName = wifiInfo.ssid
                        if (wifiName.startsWith("\"") && wifiName.endsWith("\"")) {
                            wifiName.substring(1, wifiName.length - 1)
                        } else {
                            wifiName
                        }
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ wifiName ->

                    wifiNameEditText?.text = String.format(wifiName)
                    wifiPasswordEditText?.requestFocus()

                }) { throwable -> LogUtils.e(TAG, throwable.message) }
    }

    /**
     * 配网回调
     * 配网成功，再回到主页
     */
    private fun displaySetDeviceOnboarding(result: GizWifiErrorCode, mac: String?, did: String?, productKey: String?) {
        if (result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {

            //配网的设备did
            deviceDid = did

            //如果不为空，则远程绑定设备
            if (!mac.isNullOrEmpty()) {
                if (!ClockUtils.bindDeviceMacList.contains(mac!!)) {
                    ClockUtils.bindDeviceMacList.add(mac)
                }

                GizWifiSDK.sharedInstance().bindRemoteDevice(
                        uid,
                        token,
                        mac,
                        BuildConfig.PRODUCT_KEY,
                        BuildConfig.PRODUCT_SECRET
                )
            } else {
                setFailLoadingText(getString(R.string.error_get_mac))
                failLoading()
            }

        } else if (result == GizWifiErrorCode.GIZ_OPENAPI_SERVER_ERROR) {

            //配网失败3s后再检查一次
            doAsync {
                SystemClock.sleep(3000)
                val hasBind = ClockUtils.deviceList.firstOrNull { it.did == did ?: "" }
                if (hasBind != null) {
                    showSetDialog()
                } else {
                    setFailLoadingText(GizErrorToast.parseExceptionCode(result))
                    failLoading()
                }
            }

        } else if (result != GizWifiErrorCode.GIZ_SDK_DEVICE_CONFIG_IS_RUNNING) {
            // 配置失败
            setFailLoadingText(GizErrorToast.parseExceptionCode(result))
            failLoading()
        }
    }

    private fun goAppDetailsSettingIntent() {
        val localIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        localIntent.data = Uri.fromParts("package", packageName, null)
        startByAnim(this, localIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }
}