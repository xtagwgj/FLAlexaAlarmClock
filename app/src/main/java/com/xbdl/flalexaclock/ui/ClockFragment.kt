package com.xbdl.flalexaclock.ui

import android.os.Bundle
import android.support.annotation.IntRange
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.*
import com.xbdl.flalexaclock.entity.giz.BindOrUnBindResponse
import com.xbdl.flalexaclock.extensions.getDeviceBindTime
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.extensions.sendCommand
import com.xbdl.flalexaclock.extensions.textColor
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.adapter.DeviceByGroupAdapter
import com.xbdl.flalexaclock.ui.config.ConfigurationWifiTipsActivity
import com.xbdl.flalexaclock.ui.device.SearchActivity
import com.xbdl.flalexaclock.ui.dialog.ReconnectDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_clock.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * clock页面
 */
class ClockFragment : BaseFragment() {

    private val TAG = "self_ClockFragment"

    lateinit var uid: String
    lateinit var token: String
    lateinit var adapter: DeviceByGroupAdapter

    private var pagePos = 0

    companion object {
        fun newInstance(uid: String, token: String): ClockFragment {
            val fragment = ClockFragment()
            val args = Bundle()
            args.putString(SP_UID, uid)
            args.putString(SP_TOKEN, token)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            uid = arguments.getString(SP_UID)
            token = arguments.getString(SP_TOKEN)
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_clock

    override fun initView(var1: Bundle?) {

        titleLayout.setMoreClickListener {
            ConfigurationWifiTipsActivity.doAction(activity)
        }

        emptyView.initView(
                R.mipmap.icon_no_device,
                R.string.text_empty_device_list,
                R.string.text_device_add) {
                    ConfigurationWifiTipsActivity.doAction(activity)
                }

        if (recyclerClock.adapter == null) {
            adapter = DeviceByGroupAdapter({ item ->
                //删除提示
                showLoadingDialog(R.string.load_del_clock, R.string.load_del_clock_success)

                // 解绑设备
                unBindDevice(item)
            }, { item, isSwitchStatus ->
                //开关闹钟
                val commandMap = ConcurrentHashMap<String, Any>()
                commandMap.put(GizDataPointKey.KEY_ALARM_STATE, isSwitchStatus)
//                commandMap.put(GizDataPointKey.KEY_MAIN_ALARM_SET, if (isSwitchStatus) {
//                    1
//                } else {
//                    2
//                })

                if (!isSwitchStatus) {
                    commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)
                }

//                //操作子闹钟
//                if (isSwitchStatus) {
//                    commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)
//
//                    //打开所有关闭的闹钟
//                    activity.getStateList(item.details.alarmList, 2).forEach {
//                        commandMap.put(it, 1)
//                    }
//                } else {
//                    //关闭所有打开的闹铃
//                    activity.getStateList(item.details.alarmList, 1).forEach {
//                        commandMap.put(it, 2)
//                    }
//                }

                activity.sendCommand(item.details.device, commandMap, SN_CLOSE_ALARM)
            }, {
                //重新配网
                val dialog = ReconnectDialog()
                dialog.device = it
                dialog.show(activity.supportFragmentManager, "ReconnectDialog")

            })

            recyclerClock.apply {
                setEmptyView(emptyView)
                layoutManager = LinearLayoutManager(activity)
                addItemDecoration(SimpleDividerDecoration(activity,
                        dividerColorInt = activity.resources.getColor(R.color.colorDevider), leftPadding = 16))
            }.adapter = adapter

        }

    }

    override fun initEventListener() {

        clickEvent(iv_more, Consumer { ConfigurationWifiTipsActivity.doAction(activity) })

        clickEvent(iv_search, Consumer {
            SearchActivity.doAction(activity)
        })

        //左上角文字的点击事件
        clickEvent(tv_titleLeft, Consumer {
            when (tv_titleLeft.text.toString().toUpperCase()) {
                "EDIT" -> {

                    tv_titleLeft.apply {
                        text = "Done"
                        textColor = resources.getColor(R.color.colorAccent)
                    }

                    adapter.isEdit = true
                    titleLayout?.setMoreImageViewVisiable(false)
                }

                "DONE" -> {
                    tv_titleLeft.apply {
                        text = "Edit"
                        textColor = resources.getColor(R.color.colorEdit)
                    }

                    adapter.isEdit = false
                    titleLayout?.setMoreImageViewVisiable(true)
                }
            }
        })

        //按时区分类
        rb_timeZone.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                switchPage()
            }

            view_zone.visibility = if (isChecked) View.VISIBLE else View.INVISIBLE
        }

        //绑定时间
        rb_distribute.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                switchPage(1)
            }

            view_distribute.visibility = if (isChecked) View.VISIBLE else View.INVISIBLE
        }

        //按名字分类
        rb_name.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                switchPage(2)
            }

            view_name.visibility = if (isChecked) View.VISIBLE else View.INVISIBLE
        }

        /**
         * 设备数据点发生变化 --> 时区可能变
         */
        RxBus.getInstance()
                .register<DeviceChangeEntity>(REFRESH_DEVICE)
//                .compose(bindToLifecycle())
                .map { ClockUtils.onlineDevicePoint[it.did] }
                .filter { it != null }
                .filter { adapter.getTimeZone(it!!.did) != it.data_time_zone || it.data_alarm_state != adapter.getSwitchStatus(it.did) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    LogUtils.d(TAG, "设备时区或开关状态发生变化，刷新页面数据 " + ClockUtils.deviceList)
                    displayClockList(ClockUtils.deviceList)
                }, {
                    it.printStackTrace()
                })

        /**
         * 设备列表发生变化
         */
        RxBus.getInstance()
                .register<String>(REFRESH_DEVICE_LIST)
//                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    LogUtils.d(TAG, "设备列表发生变化，刷新页面数据 " + ClockUtils.deviceList)
                    displayClockList(ClockUtils.deviceList)
                }, {
                    it.printStackTrace()
                })

        /**
         * 解绑监听
         */
        RxBus.getInstance()
                .register<BindOrUnBindResponse>("BindOrUnBindResponse")
                .filter {
                    LogUtils.e(TAG, "解绑回调 $it")
                    !it.isBind
                }
                .subscribe({ response ->
                    closeLoadingDialog()
                    if (response.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {

                        ClockUtils.unbindDevice(response.did)

                        //解绑成功
//                        val showDeviceList = ClockUtils.getAllAlarmClock()
//                        showDeviceList.removeAt(showDeviceList.indexOfFirst { it.device.did == response.did })
                        switchPage(pagePos)


                    } else {
                        ToastUtil.showToast(activity, GizErrorToast.parseExceptionCode(response.result))
                    }

                }, {
                    it.printStackTrace()
                })

        //页面切换时重置编辑状态
        RxBus.getInstance().register<Int>(FRAGMENT_CHANGED)
                .filter { it == 1 && tv_titleLeft.text.toString().toUpperCase() == "DONE" }
                .subscribe({
                    tv_titleLeft.performClick()
                }, {
                    it.printStackTrace()
                })

        if (isFirstLoad) {
            if (userVisibleHint)
                showLoadingDialog(R.string.text_load_device)

            isFirstLoad = false
        }
    }

    override fun onFirstLoad() {
        displayClockList(ClockUtils.deviceList)
    }

    /**
     * 没有设备的话，隐藏 iv_search tv_titleLeft rl_range,否则显示
     */
    private fun displayClockList(deviceList: MutableList<GizWifiDevice>) {

        if (deviceList.isEmpty()) {
            closeLoadingDialog()
            tv_titleLeft?.visibility = View.GONE
            iv_search?.visibility = View.GONE
            rl_range?.visibility = View.GONE
        } else {
            tv_titleLeft?.visibility = View.VISIBLE
            iv_search?.visibility = View.VISIBLE
            rl_range?.visibility = View.VISIBLE
        }

        switchPage(pagePos)
    }

    /**
     * 处理数据
     */
    private fun dealData(clockList: MutableList<ClockEntity>) {
        if (clockList.isNotEmpty())
            Observable.just(clockList)
                    .compose(bindToLifecycle())
                    .map {
                        when (pagePos) {
                            0 -> //按时区降序排列
                            {
                                it.sortByDescending { curr ->
                                    TimeZoneTableUtils.codeTable.firstOrNull { it.timeZoneWithCity == curr.timeZone }?.timeZone
                                            ?: 0
                                }
                            }

                            1 -> //按绑定的时间排
                            {
                                val simpleData = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                                it.sortByDescending {
                                    val date = activity.getDeviceBindTime(it.device)
                                    simpleData.parse(date).time
                                }
//                                it.sortByDescending { it.device.macAddress }
                            }

                            else -> //按alias降序排列
                                it.sortByDescending {
                                    //                                    it.device.alias
                                    activity?.getDeviceName(it.device)
                                }
                        }

                        //升序排列
                        it.reverse()
                        it
                    }//先对数据进行排序
                    .map { entity ->
                        val datas = mutableListOf<StickyItemBaseEntity>()

                        entity.forEachIndexed { index, clockEntity ->
                            //当前的标题
                            val title = getHeaderByHeaderInfo(pagePos, when (pagePos) {
                                0 -> {
//                                    clockEntity.timeZone//具体的时区ID，不包括成功
                                    TimeZoneTableUtils.codeTable.firstOrNull { it.timeZoneWithCity == clockEntity.timeZone }?.timeZoneWithCity
                                            ?: 256
//                                    TimeZoneTableUtils.codeTable.firstOrNull { it.timeZoneWithCity == clockEntity.timeZone }?.cityName ?: "Lisbon"
                                }

                                1 -> {
                                    val date = activity.getDeviceBindTime(clockEntity.device)
                                    date.substring(0, date.length - 3)

//                                    clockEntity.device.macAddress

                                }
                                else -> {
//                                    clockEntity.device.alias
                                    activity.getDeviceName(clockEntity.device)
                                }
                            })

                            //判断与前一个的标题是否相同
                            if (index > 0 && datas.last().title == title) {
                                datas.add(StickyItemEntity(title, clockEntity))
                            } else {
                                datas.add(StickyItemBaseEntity(title))
                                datas.add(StickyItemEntity(title, clockEntity))
                            }
                        }

                        datas
                    }//将数据转换成需要的实体
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        closeLoadingDialog()
                        adapter.refreshData(it)
                    }, {
                        closeLoadingDialog()
                        it.printStackTrace()
                        LogUtils.e(TAG, it.message)
                    })
        else {
            adapter.refreshData(emptyList())

            if ("DONE" == tv_titleLeft.text.toString()) {
                tv_titleLeft.performClick()
            }
            tv_titleLeft.visibility = View.GONE
            closeLoadingDialog()
        }
    }

    /**
     * 根据标识获取头部信息
     * @param currPage 当前所处的页面
     * @param headerInfo 头部信息的标识
     */
    private fun getHeaderByHeaderInfo(@IntRange(from = 0, to = 2) currPage: Int, headerInfo: Any): String {
        val header = StringBuffer("")

        when (currPage) {
            0 -> {
                val timeZone = TimeZoneTableUtils.getByTimeZone(headerInfo as Int)
                return timeZone.getShowBySys()
//                headerInfo
            }

            1 -> {
                header.append("$headerInfo")
            }

            else -> {
                header.append(
                        if (headerInfo is String) {
                            if (StringUtils.isEmpty(headerInfo) || headerInfo == PRODUCT_1)
                                "Default"
                            else
                                headerInfo.toUpperCase().substring(0, 1)
                        } else {
                            "Default"
                        }
                )
            }
        }
        return header.toString()
    }

    override fun onResume() {
        super.onResume()
        displayClockList(ClockUtils.deviceList)
    }

    /**
     * 页面切换
     */
    private fun switchPage(@IntRange(from = 0, to = 2) pagePos: Int = 0) {
        this.pagePos = pagePos
        dealData(ClockUtils.getAllAlarmClock())
//        if (showDeviceList.isEmpty())
//            GizWifiSDK.sharedInstance().getBoundDevices(uid, token, listOf(BuildConfig.PRODUCT_KEY))
    }

    /**
     * 解绑设备
     * 如果在分组里面要在解除绑定后删除分组里面的数据
     */
    private fun unBindDevice(item: StickyItemEntity) {
        showLoadingDialog(R.string.text_loading_unbind, R.string.text_loaing_unbind_success)
        item.details.device.listener = null
//        GizWifiSDK.sharedInstance().unbindDevice(uid, token, item.details.device.did)

        HttpRequest.unBindDevice(item.details.device.did, token) { unBindResult, errorMsg ->
            closeLoadingDialog()

            //解绑成功
            if (unBindResult) {

                //删除分组中的信息
                removeDeviceFromGroup(item.details.device.did)

                ClockUtils.unbindDevice(item.details.device.did)

                //更新
                displayClockList(ClockUtils.deviceList)

            } else {
                //解绑失败
                ToastUtil.showToast(activity, errorMsg ?: "UnBind Device fail")
                item.details.device.listener = ParseDataService.deviceListener
            }

        }
    }

    /**
     * 删除分组里面的数据
     */
    private fun removeDeviceFromGroup(deviceDid: String) {

        //查找设备所属的分组
        var groupEntity: GroupEntity? = null
        for ((group, didList) in ClockUtils.groupDeviceMap) {
            if (didList.contains(deviceDid)) {
                groupEntity = group
                break
            }
        }

        //没有分组，返回
        if (groupEntity == null)
            return

        //删除分组中的设备
        HttpRequest.removeDeviceFromGroup(groupEntity.id, listOf(deviceDid))
                .filter {
                    val deleteSuccess = it.success.isNotEmpty()

                    if (deleteSuccess) {//删除成功
                        groupEntity?.let {
                            ClockUtils.refreshGroupDeviceByGroup(it)
                        }

                    } else {
                        //删除失败
//                        ToastUtil.showToast(activity,R.string.fail_remove_device_from_group)
                    }
                    deleteSuccess
                }
                .subscribe({

                    //                    Log.e("从当前群组移除设备并添加到默认群组成功", it.toString())
                }, {
                    it.printStackTrace()
                })
    }
}