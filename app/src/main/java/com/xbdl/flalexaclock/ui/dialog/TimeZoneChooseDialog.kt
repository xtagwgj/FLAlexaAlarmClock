package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.entity.CityCodeEntity
import com.xbdl.flalexaclock.ui.adapter.TimeZoneAdapter
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find

/**
 * 时区选择框
 * Created by xtagwgj on 2017/9/12.
 */
class TimeZoneChooseDialog() : RxDialogFragment() {

    lateinit var inflate: View

    lateinit var okConsumer: Consumer<CityCodeEntity>

    var defaultSelectTimeZone = 256

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_choose_timezone, null)

        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {

        val winManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val winWidth = winManager.defaultDisplay.width

        dialogWindow.setGravity(Gravity.CENTER)
        dialogWindow.decorView.setPadding(0, 0, 0, 0)
        val lp = dialogWindow.attributes
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.width = (winWidth * 0.9).toInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }


    fun initView() {

        //取消选择
        inflate.find<View>(R.id.bt_cancel).setOnClickListener {
            dismiss()
        }

        val zoneAdapter = TimeZoneAdapter(defaultSelectTimeZone,
                {
                    okConsumer.accept(it)
                    dismiss()
                }
        )

        inflate.find<RecyclerView>(R.id.recyclerTimeZone).apply {
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(SimpleDividerDecoration(
                    activity, dividerColorInt = activity.resources.getColor(R.color.colorBg)))

            adapter = zoneAdapter
            scrollToPosition(zoneAdapter.getCheckPosition())
        }

    }
}