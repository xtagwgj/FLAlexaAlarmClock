package com.xbdl.flalexaclock.ui.device

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import com.gizwits.gizwifisdk.api.GizDeviceScheduler
import com.gizwits.gizwifisdk.api.GizDeviceSchedulerCenter
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizScheduleWeekday
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.gizwits.gizwifisdk.listener.GizDeviceSchedulerCenterListener
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.DeviceChangeEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.entity.giz.GizCommandResponse
import com.xbdl.flalexaclock.entity.giz.SetCustomInfoResponse
import com.xbdl.flalexaclock.extensions.*
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.dialog.GroupChooseDialog
import com.xbdl.flalexaclock.ui.dialog.InputlDeviceNameDialog
import com.xbdl.flalexaclock.ui.dialog.SetCalibrationTimeDialog
import com.xbdl.flalexaclock.ui.dialog.TimeZoneChooseDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_alarm_clock_setting.*
import org.jetbrains.anko.toast
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit


/**
 * 闹钟设置页面
 * Created by xtagwgj on 2017/9/18.
 *
 * 点击取消要恢复原来的设置，这里只是预选
 */
class AlarmClockSettingActivity : GizBaseActivity() {

    private var device: GizWifiDevice? = null
    private var currTimeZone: Int = 256

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")

    //用来存放原来的组的id
    private var oriGroupId: String? = null

    //是否打开了自动校准
    private var autoCalibration = false

    /**
     * 当前的定时任务
     */
    private var currScheduler: GizDeviceScheduler? = null
    /**
     * 是否加载了定时任务，没加载的话提示用负
     */
    private var isloadScheduler = false

    /**
     * 创建或是更新
     */
    private var isCreateOrUpdate = false

    /**
     * 定时任务的监听
     */
    private val schedulerListener = object : GizDeviceSchedulerCenterListener() {
        override fun didUpdateSchedulers(result: GizWifiErrorCode, schedulerOwner: GizWifiDevice?, schedulerList: MutableList<GizDeviceScheduler>) {
            LogUtils.e("self_scheduler", "result = $result , schedulerOwner = $schedulerOwner , schedulerList = $schedulerList")
            if (result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {

                currScheduler = if (schedulerList.isNotEmpty()) {
                    schedulerList.last()
                } else {

//                    submitSchedulerTask(showDialog = false)

                    if (isCreateOrUpdate) {
                        //获取同步device的定时任务
                        GizDeviceSchedulerCenter.updateSchedulers(uid, token, device)
                    }

                    null
                }

                isCreateOrUpdate = false

            } else {
                setFailLoadingText(GizErrorToast.parseExceptionCode(result))
                failLoading()
            }

            showScheduler()
            isloadScheduler = true
        }

    }

    /**
     * 显示定时任务信息
     */
    private fun showScheduler() {

        if (currScheduler == null) {
            piv_calibrationTime.rightText = getString(R.string.no_set_calibration_time)
        } else {

            var hour: Int
            var minute: Int
            var isAm: Boolean

            try {
                val timeArray = currScheduler!!.time.split(":")
                hour = timeArray[0].toInt()
                minute = timeArray[1].toInt()
                isAm = hour in 1..12

            } catch (e: Exception) {
                hour = 12
                minute = 0
                isAm = true
            }

            val is24 = DateFormat.is24HourFormat(this)

            if (is24) {
                piv_calibrationTime.rightText = String.format("%1$02d:%2$02d daily",
                        hour % 24,
                        minute
                )
            } else {
                piv_calibrationTime.rightText = String.format("%1$02d:%2$02d ${if (isAm) "AM" else "PM"} daily",
                        when (hour) {
                            0 -> if (isAm) hour else 12
                            in 1..12 -> hour
                            else -> if (isAm) (hour - 12) else hour
                        },
                        minute
                )
            }


            //与设备的数据同步
            syncSetTime2Device()
        }

        closeLoadingDialog()
    }

    /**
     * 同步定时的功能与设备上设置的一致
     */
    private fun syncSetTime2Device() {
        if (deviceIsController(device)) {
            val showSetTime = ClockUtils.onlineDevicePoint[device?.did]?.data_time_calibration
            showSetTime?.let {
                if (currScheduler != null && showSetTime != currScheduler!!.enabled) {
                    currScheduler!!.enabled = autoCalibration
                    // 修改设备的定时任务，mDevice是设备列表中要创建定时任务的设备对象
                    GizDeviceSchedulerCenter.editScheduler(uid, token, device, currScheduler)
                }
            }
        }
    }

    companion object {
        fun doAction(mContext: Activity, device: GizWifiDevice, fromSet: Boolean = false) {
            val intent = Intent(mContext, AlarmClockSettingActivity::class.java)
            intent.putExtra("device", device)
            intent.putExtra("fromSet", fromSet)
            startByAnim(mContext, intent)
        }
    }

    override fun onResume() {
        super.onResume()

        LogUtils.e("setting", "device = $device ,listener = ${device?.listener}")

        if (device?.listener == null) {
            device?.listener = ParseDataService.deviceListener
            device?.setSubscribe(BuildConfig.PRODUCT_SECRET, true)
        } else {
            device?.getDeviceStatus(listOf(GizDataPointKey.KEY_TIME_ZONE, GizDataPointKey.KEY_TIME_CALIBRATION))
            device?.getHardwareInfo()
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_alarm_clock_setting


    override fun initView(p0: Bundle?) {
        //获取设备信息
        device = intent.getParcelableExtra("device")

        tv_alarmClockName.text = getDeviceName(device)

        piv_group.leftBottomText = device?.remark ?: "No Group"

        currTimeZone = ClockUtils.clockAlarmMap[device?.did]?.timeZone ?: 256
        val showSetTime = ClockUtils.onlineDevicePoint[device?.did]?.data_time_calibration ?: false
        piv_calibrationTime?.visibility = if (showSetTime) View.VISIBLE else View.GONE

        autoCalibration = ClockUtils.onlineDevicePoint[device?.did]?.data_time_calibration ?: false
        displayCalibration()

        //当前选择的时区
        displayTime(currTimeZone)

        //获取数据并且解析显示
        RxBus.getInstance()
                .register<DeviceChangeEntity>(REFRESH_DEVICE)
                .compose(bindToLifecycle())
                .filter { it.did == device?.did }
                .map { ClockUtils.onlineDevicePoint[it.did] }
                .filter { it != null }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    autoCalibration = it!!.data_time_calibration

                    currTimeZone = if (it.data_time_zone == 0) 256 else it.data_time_zone
                    displayCalibration()

                    //初始化显示时间
                    displayTime(currTimeZone)

                    //同步设置时间
                    syncSetTime2Device()

                    closeLoadingDialog()

                }, {
                    it.printStackTrace()
                })

        //设置设备的基本信息
        RxBus.getInstance()
                .register<SetCustomInfoResponse>("SetCustomInfoResponse")
                .compose(bindToLifecycle())
                .filter { it.device?.macAddress == device?.macAddress }
                .subscribe({

                    closeLoadingDialog()

                    if (it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {
                        //判断了年份的前两位
                        if (getDeviceName(it?.device).startsWith(tv_alarmClockName.text.toString())) {
                        }

                        if (piv_group.leftBottomText == it?.device?.remark ?: "") {
                            ClockUtils.refreshAllGroup()
                        }
                    } else {
                        setFailLoadingText(GizErrorToast.parseExceptionCode(it.result))
                        failLoading()
                    }
                }, {
                    it.printStackTrace()
                })

//        //获取硬件信息
//        RxBus.getInstance()
//                .register<GetHardwareInfoResponse>("GetHardwareInfoResponse")
//                .compose(bindToLifecycle())
//                .filter { it.device?.macAddress == device?.macAddress }
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    piv_version.visibility = it?.hardwareInfo?.get("mcuSoftVersion")?.let {
//                        piv_version.leftBottomText = it
//                        View.VISIBLE
//                    } ?: View.GONE
//                }, {
//                    it.printStackTrace()
//                })

        //时间制发生变化
        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_CHANGED)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    displayTime(currTimeZone)

                    //时区变化的话可以更新
                    showScheduler()

                }

        RxBus.getInstance()
                .register<GizCommandResponse>("GizCommandResponse")
                .filter {
                    it.device.macAddress == device?.macAddress
                }
                .compose(bindToLifecycle())
                .subscribe {
                    if (it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {
                        if (it.sn == MODIFY_ALARM_CALI) {
                            device?.getDeviceStatus(listOf(GizDataPointKey.KEY_TIME_CALIBRATION))
                        } else if (it.sn == MODIFY_TIMEZONE) {
                            device?.getDeviceStatus(listOf(GizDataPointKey.KEY_TIME_ZONE))
                        }
                    } else {
                        setFailLoadingText(GizErrorToast.parseExceptionCode(it.result))
                        failLoading()
                    }
                }

        //从配网页面过来的话自动更新时间
        if (intent.getBooleanExtra("fromSet", false)) {
            GizWifiSDK.sharedInstance().getBoundDevices(uid, token, mutableListOf(BuildConfig.PRODUCT_KEY))

            Observable.interval(1, 1L, TimeUnit.SECONDS)
                    .filter { device != null && deviceIsOnline(device) }
                    .take(1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        LogUtils.e("test", "从配网页面过来的话自动更新时间")
                        adjustTime(false, cb_automatic.isChecked)
                    }, {
                        it.printStackTrace()
                    })

        }
    }

    /**
     * 显示自动校准的设置
     */
    private fun displayCalibration() {
        cb_automatic.isChecked = autoCalibration
        piv_calibrationTime?.visibility = if (autoCalibration) View.VISIBLE else View.GONE
    }

    private fun displayTime(timeZoneWithCity: Int) {
        val timeZoneEntity = TimeZoneTableUtils.getByTimeZone(timeZoneWithCity)

        tv_currTime.timeZone = timeZoneEntity.timeZoneId

        tv_timeUnit.timeZone = timeZoneEntity.timeZoneId
        tv_timeUnit?.visibility = if (!DateFormat.is24HourFormat(this)) View.VISIBLE else View.GONE

        piv_timeZone.rightText = timeZoneEntity.getShowBySys()

        //时区
        tv_timeZone?.text = timeZoneEntity.getShowBySys()
    }

    override fun initEventListener() {

        //修改设备名称
        clickEvent(tv_edit) {
            val dialog = InputlDeviceNameDialog()
            dialog.okConsumer = Consumer {
                modifyDeviceName(it)
            }

            dialog.show(supportFragmentManager, "modifyDeviceName")
        }

        //时区选择
        clickEvent(piv_timeZone) {

            if (!deviceIsOnline(mDevice = device)) {
                ToastUtil.showToast(this, R.string.offline)
                return@clickEvent
            }

            val dialog = TimeZoneChooseDialog()
            dialog.defaultSelectTimeZone = currTimeZone
            dialog.okConsumer = Consumer {

                showLoadingDialog(R.string.text_set_timezone, R.string.app_name)

                val commandMap = ConcurrentHashMap<String, Any>()
                commandMap.put(GizDataPointKey.KEY_TIME_ZONE, it.timeZoneWithCity)
                commandMap.put(GizDataPointKey.KEY_TIME_CALIBRATION, true)

                //是否是夏令时
                val tz = TimeZone.getTimeZone(it.timeZoneId)
                val calendar = GregorianCalendar()
                calendar.timeZone = tz

                commandMap.put(GizDataPointKey.KEY_DST, tz.inDaylightTime(calendar.time))

                //发送命令修改时区 同时开启校准
                sendCommand(device, commandMap, MODIFY_TIMEZONE)
//                timeZoneChanged = true
            }
            dialog.show(supportFragmentManager, "TimeZoneChooseDialog")
        }

        //自动校准时间的功能
        cb_automatic.setOnClickListener {
            cb_automatic.isChecked = autoCalibration

            if (adjustTime(true, !autoCalibration).not()) {
                return@setOnClickListener
            }

            //同时开启或关闭校准功能
            if (currScheduler != null) {
                currScheduler!!.enabled = !autoCalibration
                // 修改设备的定时任务，mDevice是设备列表中要创建定时任务的设备对象
                GizDeviceSchedulerCenter.editScheduler(uid, token, device, currScheduler)
            }

        }

        //点击分组的列
        clickEvent(piv_group) {

            //所有的分组
            val groupList = ClockUtils.findAllGroup()

            if (groupList.isEmpty()) {
                toast(R.string.text_no_group)
            } else {
                val dialog = GroupChooseDialog()

                //原来组的id
                oriGroupId = groupList.firstOrNull { it.group_name == device?.remark }?.id

                //通过当前页面显示的名称获取组id
                dialog.defaultCheckId =
                        groupList.firstOrNull { it.group_name == piv_group.leftBottomText }?.id ?: ""

                dialog.entityList = groupList
                dialog.chooseConsumer = Consumer { currGroup ->

                    showLoadingDialog(R.string.text_dialog_group_change, R.string.app_name)

                    //选择后保存group的名称，按ok再提交
                    piv_group.leftBottomText = currGroup.group_name

                    showLoadingDialog(R.string.text_loading_save_setting, R.string.text_loading_setting_success)

                    if (StringUtils.isEmpty(oriGroupId)) {

                        HttpRequest.moveDevice2Group(currGroup.id, listOf(device?.did ?: ""))
                                .subscribe({
                                    closeLoadingDialog()
                                    //直接修改设备的备注信息
                                    device?.setCustomInfo(piv_group.leftBottomText, null)
                                    oriGroupId = currGroup.id
                                }, {
                                    closeLoadingDialog()
                                    it.printStackTrace()
                                })
                    } else {
                        removeDeviceFromCurrGroup(oriGroupId!!, currGroup.id)
                    }


                }
                dialog.show(supportFragmentManager, "dialog")
            }

        }

        //设置定时任务的校准时间
        clickEvent(piv_calibrationTime) {

            //还没有加载定时任务
            if (!isloadScheduler) {
                toast(R.string.loading_scheduler)
                return@clickEvent
            }

            val timeDialog = SetCalibrationTimeDialog()
            timeDialog.initTime(currScheduler?.time ?: "12:00")
            timeDialog.saveConsumer = Consumer {
                showLoadingDialog(R.string.text_setting_calibration, R.string.app_name)
                submitSchedulerTask(it, true)
            }
            timeDialog.show(supportFragmentManager, "timeDialog")

        }


        //设置定时任务的监听
        GizDeviceSchedulerCenter.setListener(schedulerListener)
        //获取同步device的定时任务
        GizDeviceSchedulerCenter.updateSchedulers(uid, token, device)
    }

    /**
     *校准时间
     *
     * @param showDialog 是否显示对话框
     * @param auto 是否自动校准
     */
    private fun adjustTime(showDialog: Boolean = false, auto: Boolean = true): Boolean {
        if (!deviceIsOnline(mDevice = device)) {
            ToastUtil.showToast(this, R.string.offline)
            return false
        }

        if (showDialog) {
            showLoadingDialog(R.string.text_set_calibration, R.string.app_name)
        }

        //是否是夏令时
        val tz = TimeZone.getTimeZone(TimeZoneTableUtils.getByTimeZone(currTimeZone).timeZoneId)
        val calendar = GregorianCalendar()
        calendar.timeZone = tz

        val commandMap = ConcurrentHashMap<String, Any>()
        //不显示对话框就是从配网页面过来的
        if (showDialog.not()) {
            commandMap[GizDataPointKey.KEY_DST] = tz.inDaylightTime(calendar.time)
        }
        commandMap[GizDataPointKey.KEY_TIME_CALIBRATION] = auto

        sendCommand(device, commandMap, MODIFY_ALARM_CALI)

        return true
    }

    /**
     * 提交定时任务，有的就修改，没有的就新建
     * 时间格式:HH:mm 如06:30
     */
    private fun submitSchedulerTask(time: String = "12:00", showDialog: Boolean = false) {

        if (showDialog) {
            showLoadingDialog(R.string.load_set_scheduler, R.string.app_name)
        }

        isCreateOrUpdate = true
        var isCreate = false
        if (currScheduler == null) {
            //创建定时任务
            currScheduler = GizDeviceScheduler()
            isCreate = true
        }

        //同步定时任务
        currScheduler!!.time = time
        currScheduler!!.remark = "AutoCalibrationTask"

        //执行的操作
        val attrs = ConcurrentHashMap<String, Any>()
        attrs.put(GizDataPointKey.KEY_TIME_CALIBRATION, true)

        currScheduler!!.attrs = attrs

        currScheduler!!.date = null
        currScheduler!!.enabled = true
        currScheduler!!.weekdays = listOf(
                GizScheduleWeekday.GizScheduleSunday,
                GizScheduleWeekday.GizScheduleMonday,
                GizScheduleWeekday.GizScheduleTuesday,
                GizScheduleWeekday.GizScheduleWednesday,
                GizScheduleWeekday.GizScheduleThursday,
                GizScheduleWeekday.GizScheduleFriday,
                GizScheduleWeekday.GizScheduleSaturday)

        if (isCreate) {
            // 创建设备的定时任务，mDevice是设备列表中要创建定时任务的设备对象
            GizDeviceSchedulerCenter.createScheduler(uid, token, device, currScheduler)
        } else {
            // 修改设备的定时任务，mDevice是设备列表中要创建定时任务的设备对象
            GizDeviceSchedulerCenter.editScheduler(uid, token, device, currScheduler)
        }

        //显示定时任务时间
//        showScheduler()
    }


    /**
     * 从当前群组移除设备并添加到默认群组
     */
    private fun removeDeviceFromCurrGroup(fromGroupId: String, toGroupId: String) {

        HttpRequest.removeDeviceFromGroup(fromGroupId, listOf(device!!.did))
                .filter {
                    val deleteSuccess = it.success.isNotEmpty()

                    if (!deleteSuccess) {//删除失败
                        toast(R.string.fail_remove_device_from_group)
                    }
                    deleteSuccess
                }
                .flatMap { HttpRequest.moveDevice2Group(toGroupId, it.success) }
                .subscribe({
                    closeLoadingDialog()
                    //直接修改设备的备注信息
                    device?.setCustomInfo(piv_group.leftBottomText, null)
                }, {
                    closeLoadingDialog()
                    it.printStackTrace()
                })
    }

    /**
     *  修改设备名称
     *  name和group可能修改，checkbox不需要修改，因为已经实时修改了
     */
    private fun modifyDeviceName(deviceName: String) {
        showLoadingDialog(R.string.modify_device_name, R.string.app_name)
        device?.let {
            tv_alarmClockName.text = deviceName
            modifyDeviceName(device!!, deviceName)
        }
    }

}