package com.xbdl.flalexaclock.ui.device

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import com.jakewharton.rxbinding2.widget.RxTextView
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.FRAGMENT_CHANGED
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.extensions.mContext
import com.xbdl.flalexaclock.ui.adapter.SearchAdapter
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search.*
import java.util.concurrent.TimeUnit

/**
 * 搜索界面
 * Created by xtagwgj on 2017/9/15.
 */
class SearchActivity() : GizBaseActivity() {

    var adapter: SearchAdapter? = null

    private val entityList = ClockUtils.getAllAlarmClock()

    companion object {
        fun doAction(mContext: Activity) {
            RxBus.getInstance().post(FRAGMENT_CHANGED, 1)
            val intent = Intent(mContext, SearchActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_search

    override fun initView(p0: Bundle?) {

        adapter = SearchAdapter {
            AlarmClockDetailsActivity.doAction(this@SearchActivity, it.device)
        }

        recyclerSearch.apply {
            layoutManager = LinearLayoutManager(this@SearchActivity)
            addItemDecoration(SimpleDividerDecoration(this@SearchActivity,
                    dividerColorInt = resources.getColor(R.color.colorBg), leftPadding = 32))
        }.adapter = adapter

    }

    override fun initEventListener() {

        clickEvent(tv_cancel) {
            destroyByAnim(this)
        }

        RxTextView.textChanges(et_inputSearch)
                .compose(bindToLifecycle())
                .debounce(150, TimeUnit.MILLISECONDS)
                .map { it.toString() }
                .map { input ->
                    entityList.filter { getDeviceName(it.device).contains(input) }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    refreshSearchResult(it)

                   LogUtils.w("", "--> $it")
                }, {
                    LogUtils.e(this@SearchActivity.javaClass.simpleName, "error: ${it.message}")
                })


//        /**
//         *  设备列表
//         */
//        RxBus.getInstance().register<DiscoverdResponse>("DiscoverdResponse")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    loadClockInfo(it.deviceList)
//                }, {
//                    LogUtils.d("SearchActivity", "收到设备列表的响应")
//                })
//
//
//        loadClockInfo(GizWifiSDK.sharedInstance().deviceList)
    }


    private fun refreshSearchResult(list: List<ClockEntity>?) {

        if (list == null || list.isEmpty()) {
            tv_resultSum.visibility = View.INVISIBLE
            adapter?.refreshData(emptyList())
            return
        }

        adapter?.refreshData(list)

        val showNumber = String.format(getString(R.string.text_search_result_sum), list.size)

        val spannableString = SpannableString(showNumber)
        val colorSpan = ForegroundColorSpan(mContext.resources.getColor(R.color.colorAccent))
        spannableString.setSpan(colorSpan, 0, showNumber.indexOf(" "), Spanned.SPAN_INCLUSIVE_EXCLUSIVE)

        tv_resultSum.text = spannableString
        tv_resultSum.visibility = View.VISIBLE
    }

//    /**
//     * 根据获取到的设备信息，去拉取设备最新的数据点信息进行解析 然后显示
//     */
//    private fun loadClockInfo(deviceList: List<GizWifiDevice>) {
//
//        Observable.fromIterable(deviceList)
//                .compose(bindToLifecycle())
//                .flatMap { HttpRequest.getLatestDevicePoint(it.did) }
//                .filter { it.error_code == 0 }//正确的获取了数据
//                .map { entity ->
//                    LogUtils.e("SearchActivity next:", entity)
//
//                    val clock = ClockEntity()
//                    clock.device = deviceList.first { it.did == entity.did }
//                    clock.timeZone = entity.attr.time_zone
//                    clock.isClockSwitch = entity.attr.alarm_state == 1
//
//                    //主闹钟
//                    clock.mainAlarm = entity.attr.getMainAlarm(null)
//                    val subList = (1..12).map {
//                        entity.attr.getSubAlarm(it, null)
//                    }
//                    //子闹钟 只取没有删除的闹钟
//                    clock.alarmList = subList.filter { it.state != 3 }
//                    clock
//                }//解析成了需要的闹钟的实体
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .toList()
//                .toObservable()
//                .subscribe({
//                    LogUtils.e("SearchActivity next:", it)
//                    entityList.clear()
//                    entityList.addAll(it)
//                }, {
//                    it.printStackTrace()
//                    LogUtils.e("SearchActivity error:", it.message)
//                    closeLoadingDialog()
//                }, {
//                    closeLoadingDialog()
//                })
//    }

}