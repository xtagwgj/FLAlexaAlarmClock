package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.jakewharton.rxbinding2.view.RxView
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.ui.adapter.WeekAdapter
import com.xtagwgj.baseproject.constant.BaseConstants
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find
import java.util.concurrent.TimeUnit

/**
 * 闹铃重复时间的对话框
 * Created by xtagwgj on 2017/7/20.
 */

class CustomChooseDialog : RxDialogFragment() {

    lateinit var inflate: View

    var selectedConsumer: Consumer<String>? = null

    var retryInfoMap = mutableMapOf<String, Boolean>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_custom, null)


        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {
        dialogWindow.setGravity(Gravity.BOTTOM)
        dialogWindow.decorView.setPadding(0, 0, 0, 0)
        val lp = dialogWindow.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }

    private fun initView() {

        val adapter = WeekAdapter(listOf(
                getString(R.string.text_monday),
                getString(R.string.text_tues),
                getString(R.string.text_wed),
                getString(R.string.text_thur),
                getString(R.string.text_fri),
                getString(R.string.text_sat),
                getString(R.string.text_sun)
        ))

        adapter.retryInfoMap = retryInfoMap

        inflate.find<RecyclerView>(R.id.recyclerWeek).apply {
            layoutManager = LinearLayoutManager(activity)
        }.adapter = adapter

        inflate.find<View>(R.id.bt_cancel).setOnClickListener({
            dismiss()
        })

        inflate.find<View>(R.id.bt_ok).setOnClickListener({
            selectedConsumer?.accept(
                    adapter.getRetryInfo()
            )
            dismiss()
        })
    }

    private fun clickEvent(view: View?, consumer: Consumer<Any>) {
        if (view == null)
            return

        RxView.clicks(view)
                .throttleFirst(BaseConstants.THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .compose(this.bindToLifecycle())
                .subscribe(consumer)
    }

}