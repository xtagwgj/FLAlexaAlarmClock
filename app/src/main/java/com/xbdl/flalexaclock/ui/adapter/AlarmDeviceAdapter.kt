package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import android.widget.RadioButton
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemAlarmDeviceBinding
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.widget.AlarmView
import org.jetbrains.anko.find

/**
 * 添加闹铃页面 显示设备的适配器
 * Created by xtagwgj on 2017/9/13.
 */
class AlarmDeviceAdapter(data: List<ClockEntity>, private val onClick: (ClockEntity) -> Unit)
    : DBAdapter<ClockEntity, ItemAlarmDeviceBinding>(data, R.layout.item_alarm_device) {

    private val selectedList = mutableListOf<ClockEntity>()

    /**
     * 选择的设备，单选的时候使用
     */
    private var selectedDid: String? = null

    fun setSelectedDid(did: String) {
        selectedDid = did
    }

    override fun onBindViewHolder(holder: DBViewHolder<ItemAlarmDeviceBinding>, position: Int, item: ClockEntity) {
        holder.binding.name =
                mContext.getDeviceName(item.device)

        holder.binding.area = TimeZoneTableUtils.getByTimeZone(item.timeZone).getShow()

        val radio = holder.binding.root.find<RadioButton>(R.id.rb_device).apply {
            isChecked = selectedDid == item.device.did
        }
        holder.binding.root.find<View>(R.id.rl_device).setOnClickListener {
            radio.isChecked = !radio.isChecked
            onClick(item)
        }
        holder.binding.root.find<View>(R.id.ll_content).setOnClickListener {
            radio.isChecked = !radio.isChecked
            onClick(item)
        }

        holder.binding.root.find<AlarmView>(R.id.tv_devicePic).apply {
            when (item.device.netStatus) {
                GizWifiDeviceNetStatus.GizDeviceControlled,
                GizWifiDeviceNetStatus.GizDeviceOnline -> {
                    val timeZone = TimeZoneTableUtils.getCurrTimeZone(item.timeZone)
                    setTime(timeZone.id)
                }

                else -> {
                    setTimeVisible(false)
                }
            }
        }

        radio.setOnCheckedChangeListener({ _, isChecked ->
            if (isChecked) {
                selectedList.add(item)
            } else
                selectedList.remove(item)

        })
    }

    fun getSelectedDevice(): List<ClockEntity> = selectedList.distinct()

}