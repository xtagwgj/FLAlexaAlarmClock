package com.xbdl.flalexaclock.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.text.format.DateFormat
import com.flyco.tablayout.listener.CustomTabEntity
import com.flyco.tablayout.listener.OnTabSelectListener
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.pgyersdk.update.PgyUpdateManager
import com.tbruyelle.rxpermissions2.RxPermissions
import com.trello.rxlifecycle2.android.ActivityEvent
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.entity.DeviceChangeEntity
import com.xbdl.flalexaclock.entity.GizBaseEntity
import com.xbdl.flalexaclock.entity.TabEntity
import com.xbdl.flalexaclock.entity.giz.BindOrUnBindResponse
import com.xbdl.flalexaclock.entity.giz.DiscoverdResponse
import com.xbdl.flalexaclock.entity.giz.ReceiveDataResponse
import com.xbdl.flalexaclock.entity.giz.UpdateNetStatusResponse
import com.xbdl.flalexaclock.extensions.Preference
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.config.ConfigurationWifiTipsActivity
import com.xbdl.flalexaclock.ui.dialog.NormalMessageDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.DbUtils
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : GizBaseActivity() {

    private val TAG: String = "self_MainActivity"

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")
    private var account: String by Preference(this, SP_ACCOUNT, "")
    private var photo: String by Preference(this, SP_USER_PHOTO, "")

    private val fragments: MutableList<Fragment> = mutableListOf()

    private var is24 = false

    //是否显示添加设备的对话框
    private var showAddDeviceDialog = true

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.action == Intent.ACTION_TIME_TICK) {//时间变化了
                RxBus.getInstance().post(Intent.ACTION_TIME_TICK, true)

            } else if (intent.action == Intent.ACTION_TIME_CHANGED) {//12小时和24小时转化了
                RxBus.getInstance().post(Intent.ACTION_TIME_CHANGED, true)
            }
        }
    }

    //导航栏标题
    private val titles = arrayOf(
            App.instance.getString(R.string.tab_alarm),
            App.instance.getString(R.string.tab_clock),
            App.instance.getString(R.string.tab_group),
            App.instance.getString(R.string.tab_account)
    )

    //未选择的导航栏图表
    private val iconUnSelectedList = arrayOf(
            R.mipmap.menu_01,
            R.mipmap.menu_02,
            R.mipmap.menu_03,
            R.mipmap.menu_04
    )

    //已选择的导航栏图表
    private val iconSelectedList = arrayOf(
            R.mipmap.menu_01_on,
            R.mipmap.menu_02_on,
            R.mipmap.menu_03_on,
            R.mipmap.menu_04_on
    )

    private val mTabEntities: ArrayList<CustomTabEntity> = ArrayList()

    companion object {

        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, MainActivity::class.java)
            startByAnim(mContext, intent, finishOld = true)
        }

    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun initView(p0: Bundle?) {
        is24 = DateFormat.is24HourFormat(this)

        ClockUtils.uid = uid
        ClockUtils.token = token
        initParse()

        //加token进入请求
        HttpRequest.addUserToken(token = token)

        //根据信息生成TabEntity
        (0 until titles.size).mapTo(mTabEntities)
        { TabEntity(titles[it], iconSelectedList[it], iconUnSelectedList[it]) }

        mTabEntities.let {
            tabLayout.setTabData(mTabEntities)
        }

        //添加fragment
        fragments.add(AlarmFragment.newInstance())
        fragments.add(ClockFragment.newInstance(uid, token))
        fragments.add(GroupFragment.newInstance())
        fragments.add(AccountFragment.newInstance(uid, token, account, photo))

        viewPager.adapter = MyPagerAdapter(supportFragmentManager)
    }

    override fun initEventListener() {

        tabLayout.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
//                LogUtils.e(TAG, "onTabSelect ${viewPager.currentItem } --> $position")

                RxBus.getInstance().post(FRAGMENT_CHANGED, viewPager.currentItem)
                viewPager.currentItem = position
            }

            override fun onTabReselect(position: Int) {
            }
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {

            }
        })

        //设置第一次进入显示的页面
        viewPager.currentItem = 0

        //监听系统时间变化
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_TIME_TICK)
        filter.addAction(Intent.ACTION_TIME_CHANGED)
        registerReceiver(receiver, filter)

        //绑定监听
        RxBus.getInstance()
                .register<BindOrUnBindResponse>("BindOrUnBindResponse")
                .filter {
                    LogUtils.e(TAG, "绑定回调 $it")
                    it.isBind
                }
                .subscribe({ response ->

                    if (response.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {

                        val simpleDateFormat = SimpleDateFormat(BIND_TIME_FORMAT, Locale.getDefault())

                        val remark = ClockUtils.tempMap["remark"] ?: ""

                        //设置分组为原来的 设置绑定的时间[名称_时间]
                        GizWifiSDK.sharedInstance()
                                .deviceList
                                .firstOrNull { response.did == it.did }
                                ?.run {

                                    val alias = if (this.alias.isNullOrEmpty() || this.alias.indexOf("_") <= 0) {
                                        simpleDateFormat.format(Date())
                                    } else {
                                        val index = this.alias.indexOf("_")
                                        this.alias.substring(0, index) + simpleDateFormat.format(Date())
                                    }

                                    setCustomInfo(remark, alias)
                                }
                    }
                }, {
                    it.printStackTrace()
                })

        //检查是否设备列表为空
        RxBus.getInstance().register<DiscoverdResponse>("DiscoverdResponse")
                .compose(bindUntilEvent(ActivityEvent.STOP))
                .filter { showAddDeviceDialog && it.deviceList.isEmpty() }
                .subscribe({
                    //切换到clock页面
                    tabLayout.currentTab = 1

                    showAddDeviceDialog = false

                    NormalMessageDialog().apply {
                        rightTextRes = R.string.text_add
                        textRes = R.string.text_add_device_content
                        okConsumer = Consumer {
                            dismiss()
                            ConfigurationWifiTipsActivity.doAction(activity)
                        }
                    }
                            .show(supportFragmentManager, "addDevice")

                }, {
                    it.printStackTrace()
                })

        RxBus.getInstance().register<UpdateNetStatusResponse>("UpdateNetStatusResponse")
                .compose(bindUntilEvent(ActivityEvent.STOP))
                .subscribe({

                    it?.let {
                        if (it.netStatus == GizWifiDeviceNetStatus.GizDeviceOffline) {
                            doAsync {
                                SystemClock.sleep(5000)

                                GizWifiSDK.sharedInstance().getBoundDevices(uid, token, listOf(
                                        BuildConfig.PRODUCT_KEY
                                ))
                            }
                        }
                    }

                }, {
                    it.printStackTrace()
                })

        ClockUtils.refreshAllGroup()

        RxBus.getInstance().register<Any>(CHECK_UPDATE)
                .compose(bindToLifecycle())
                .subscribe({
                    checkPgyUpdate()
                }, {
                    it.printStackTrace()
                })

        checkPgyUpdate()

        //3S还没有数据就主动请求列表
        doAsync {
            SystemClock.sleep(3000)
            if (ClockUtils.deviceList.isEmpty()) {
                GizWifiSDK.sharedInstance().getBoundDevices(uid, token, listOf(BuildConfig.PRODUCT_KEY))
            }
        }

    }

    private fun checkPgyUpdate() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            checkUpdate()
        else {
            RxPermissions(this)
                    .request(Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .subscribe({
                        if (it) {
                            checkUpdate()
                        }
                    }, {
                        it.printStackTrace()
                    })
        }
    }

    private fun checkUpdate() {
        //设置是否强制更新。true为强制更新；false为不强制更新（默认值）。
        PgyUpdateManager.setIsForced(false)
        PgyUpdateManager.register(this, "com.xbdl.flalexaclock.fileProvider")
    }

    override fun onResume() {
        super.onResume()
        //刷新设备列表
//        if (ClockUtils.deviceList.isEmpty()) {
//            LogUtils.e(TAG, "设备列表为空，请求设备列表")
//            GizWifiSDK.sharedInstance().getBoundDevices(uid, token, listOf(BuildConfig.PRODUCT_KEY))
//        }

        //时间格式改变
        if (is24 != DateFormat.is24HourFormat(this)) {
            is24 = !is24
            RxBus.getInstance().post(REFRESH_ALARM_LIST, "")
        }

//        showSetDialog()
    }


    /**
     * 检查是否有要进行引导设置的设备
     */
//    private fun showSetDialog() {
//        val showDialogDevice = ClockUtils.deviceList.firstOrNull { ClockUtils.showDialogDevicesDid.contains(it.did) }
//        showDialogDevice?.let {
//
//            ClockUtils.showDialogDevicesDid.remove(showDialogDevice.did)
//
//            val showSetting = NormalMessageDialog()
//            showSetting.leftTextRes = R.string.text_wait_2_update
//            showSetting.rightTextRes = R.string.text__update_now
//            showSetting.textRes = R.string.bind_2_set_content
//
//            showSetting.cancelConsumer = Consumer {
//                showSetting.dismiss()
//            }
//            showSetting.okConsumer = Consumer {
//                AlarmClockSettingActivity.doAction(MainActivity@ this, showDialogDevice)
//                showSetting.dismiss()
//            }
//            showSetting.show(supportFragmentManager, "showSetting")
//        }
//    }


    inner class MyPagerAdapter(fm: android.support.v4.app.FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = titles.size

        override fun getPageTitle(position: Int): CharSequence = titles[position]
    }


    @SuppressLint("RestrictedApi")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragmentManager = supportFragmentManager
        for (fragment in fragmentManager.fragments) {
            fragment?.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onStart() {
        super.onStart()
        DbUtils.onStartUser(uid, {
            //未注册或者头像不一致
            if (it == null || !StringUtils.equals(it.headUrl, photo)) {
                LogUtils.e(TAG, "原来的头像：${it?.headUrl}")
                LogUtils.e(TAG, "头像不一致，上传头像：$photo")
                DbUtils.updateUserIcon(uid, photo, it == null)
            }
        })
    }

    override fun onStop() {
        super.onStop()
        DbUtils.onStopUser(uid)
    }


    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
        PgyUpdateManager.unregister()
    }

    private fun initParse() {
        RxBus.getInstance()
                .register<ReceiveDataResponse>("ReceiveDataResponse")
                .filter { response ->
                    LogUtils.d(TAG, "接收到了数据 $response")
                    response.result == GizWifiErrorCode.GIZ_SDK_SUCCESS
                            && response.dataMap?.get("data") != null
                }
                //获取该设备的数据点
                .map { receive ->
                    var gizBaseEntity = ClockUtils.onlineDevicePoint[receive.device!!.did]
                    if (gizBaseEntity == null) {
                        gizBaseEntity = GizBaseEntity()
                        gizBaseEntity.did = receive.device.did
                        ClockUtils.onlineDevicePoint[receive.device.did] = gizBaseEntity
                    }

                    gizBaseEntity.getDataFromReceiveDataMap(receive.dataMap)
                    gizBaseEntity
                }
                //保存到对应的设备闹铃列表中
                .map { entity ->
                    ClockEntity().apply {

                        LogUtils.e(TAG, "parseDid = ${entity!!.did} , deviceList = ${ClockUtils.deviceList}")

                        device = ClockUtils.deviceList.first { it.did == entity.did }
                        timeZone = entity.data_time_zone
                        isClockSwitch = entity.data_alarm_state
                        //主闹钟
                        mainAlarm = entity.getMainAlarm(entity.did)

                        alarmList = (1..12)
                                .map {
                                    entity.getSubAlarm(it, entity.did)
                                }
                                .sortedBy {
                                    val hour = if (it.hour == 0)
                                        24 else it.hour

                                    hour * 60 + it.minute
                                }
                    }
                }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    ClockUtils.clockAlarmMap[it.device.did] = it
                    //设备可以刷新了
                    RxBus.getInstance().post(REFRESH_DEVICE, DeviceChangeEntity(it.device.did))
                    RxBus.getInstance().post(REFRESH_ALARM_SINGLE, it.device.did)

                    ClockUtils.findGroupEntityByDid(it.device.did)?.let { groupEntity ->
                        ClockUtils.updateClockEntityListByDidList(groupEntity, listOf(it.device.did))
                        RxBus.getInstance().post(REFRESH_GROUP, groupEntity.id)
                    }

                }, {
                    it.printStackTrace()
                    LogUtils.e(TAG, it.message)
                })

    }
}