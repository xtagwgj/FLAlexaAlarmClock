package com.xbdl.flalexaclock.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.giz.DiscoverdResponse
import com.xbdl.flalexaclock.entity.giz.SetCustomInfoResponse
import com.xbdl.flalexaclock.extensions.sendCommand
import com.xbdl.flalexaclock.extensions.textColor
import com.xbdl.flalexaclock.ui.adapter.AlarmListAdapter
import com.xbdl.flalexaclock.ui.config.ConfigurationWifiTipsActivity
import com.xbdl.flalexaclock.ui.dialog.NoDeviceDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.MeasureUtil
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_alarm.*

/**
 * 闹钟页面
 *
 * 都通过接口来获取数据
 * 当列表发生变化时，重新获取整个列表的数据
 * 当数据发生变化是，更新该设备下的数据
 */
class AlarmFragment : BaseFragment() {

    fun myTag(): String = "self_AlarmFragment"

    private val adapter: AlarmListAdapter by lazy {
        AlarmListAdapter({
            AddAlarmActivity.doAction(activity, it.currDevice, it)
        }, { currDevice, stateKey ->
            //删除提示
            showLoadingDialog(R.string.load_del_alarm, R.string.load_del_alarm_success)
            activity.sendCommand(currDevice, stateKey, 3, SN_DELETE_ALARM)
        })
    }

    private val dialog: NoDeviceDialog by lazy {
        NoDeviceDialog()
    }

    companion object {
        fun newInstance(): AlarmFragment {
            val fragment = AlarmFragment()
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_alarm

    override fun initView(savedInstanceState: Bundle?) {
        emptyView.initView(
                R.mipmap.icon_no_alram,
                R.string.text_empty_alarm_list,
                R.string.text_alarm_add, {

            AddAlarmActivity.doAction(AlarmFragment@ this.activity)
        })
        recyclerAlarm.setEmptyView(emptyView)

        if (recyclerAlarm.adapter == null) {
            recyclerAlarm.layoutManager = LinearLayoutManager(activity)
            recyclerAlarm.addItemDecoration(SimpleDividerDecoration(
                    activity, dividerColorInt = resources.getColor(R.color.colorDevider),
                    leftPadding = MeasureUtil.dp2px(activity, 16F)))
            recyclerAlarm.adapter = adapter
        }

    }

    override fun initEventListener() {

        titleLayout.setMoreClickListener {
            AddAlarmActivity.doAction(AlarmFragment@ this.activity)
        }

        //左上角文字的点击事件
        clickEvent(tv_titleLeft, Consumer {
            when (tv_titleLeft.text.toString().toUpperCase()) {
                "EDIT" -> {

                    tv_titleLeft.run {
                        text = "Done"
                        textColor = resources.getColor(R.color.colorAccent)
                    }

                    adapter.isEdit = true
                    titleLayout?.setMoreImageViewVisiable(false)
                }

                "DONE" -> {
                    tv_titleLeft.apply {
                        text = "Edit"
                        textColor = resources.getColor(R.color.colorEdit)
                    }

                    adapter.isEdit = false
                    titleLayout?.setMoreImageViewVisiable(true)
                }
            }
        })

        dialog.okConsumer = Consumer {
            ConfigurationWifiTipsActivity.doAction(activity)
            dialog.dismiss()
        }

        /**
         *  设备的基本数据变化（有可能是 remark 更改导致） 数据进行更新
         */
        RxBus.getInstance()
                .register<SetCustomInfoResponse>("SetCustomInfoResponse")
//                .compose(bindToLifecycle())
                .filter { it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    LogUtils.d(myTag(), "获取did为 ${response.device?.did} 的闹钟列表")
                    //刷新列表
                    val clockEntity = ClockUtils.clockAlarmMap.filter { it.key == response.device?.did ?: "" }.values.first()
                    val detailsAlarm = mutableListOf<AlarmDetailsEntity>()
                    detailsAlarm.add(clockEntity.mainAlarm)
                    detailsAlarm.addAll(clockEntity.alarmList.filter { it.state == 1 || it.state == 2 })
                    adapter.replaceAlarm(detailsAlarm)

                    checkIfShowEditText()
                    closeNoDeviceDialog()
                    closeLoadingDialog()
                }, {
                    it.printStackTrace()
//                    closeNoDeviceDialog()
                })

        RxBus.getInstance()
                .register<String>(REFRESH_ALARM_SINGLE)
                .filter { !StringUtils.isEmpty(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ did ->
                    LogUtils.d(myTag(), "获取did为 $did 的闹钟列表")
                    //刷新列表
                    val clockEntity = ClockUtils.clockAlarmMap.filter { it.key == did }.values.first()
                    val detailsAlarm = mutableListOf<AlarmDetailsEntity>()
                    detailsAlarm.add(clockEntity.mainAlarm)
                    detailsAlarm.addAll(clockEntity.alarmList.filter { it.state == 1 || it.state == 2 })
                    adapter.replaceAlarm(detailsAlarm)

                    checkIfShowEditText()
                    closeNoDeviceDialog()
                    closeLoadingDialog()
                }, {
                    it.printStackTrace()
                })

        /**
         * 刷新所有的闹铃列表
         */
        RxBus.getInstance()
                .register<String>(REFRESH_ALARM_LIST)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onFirstLoad()
                    closeLoadingDialog()
                }, {
                    closeLoadingDialog()
                    it.printStackTrace()
                })

        RxBus.getInstance()
                .register<String>(REFRESH_GROUP)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onFirstLoad()
                }, {
                    it.printStackTrace()
                })


        //时间变化
        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_TICK)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (adapter.itemCount > 0) {
                        adapter.notifyDataSetChanged()
                    }
                })

        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_CHANGED)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (adapter.itemCount > 0) {
                        adapter.notifyDataSetChanged()
                    }
                })

        //检查是否设备列表为空
        RxBus.getInstance().register<DiscoverdResponse>("DiscoverdResponse")
                .filter { it.deviceList.isEmpty() }
                .subscribe({
                    closeLoadingDialog()
                }, {
                    it.printStackTrace()
                })

        //页面切换时重置编辑状态
        RxBus.getInstance().register<Int>(FRAGMENT_CHANGED)
                .filter { it == 0 && tv_titleLeft?.text.toString().toUpperCase() == "DONE" }
                .subscribe({
                    tv_titleLeft.performClick()
                }, {
                    it.printStackTrace()
                })

        if (isFirstLoad) {
            if (userVisibleHint)
                showLoadingDialog(R.string.text_load_alarm)

            isFirstLoad = false
        }
    }

    override fun onFirstLoad() {
        adapter.refresh(ClockUtils.getAllAlarmDetails().filter { it.state == 1 || it.state == 2 })
        checkIfShowEditText()

    }

    private fun checkIfShowEditText() {
        if (adapter.mDatas.isEmpty()) {
            tv_titleLeft?.visibility = View.GONE
            titleLayout?.setMoreImageViewVisiable(false)
        } else {
            tv_titleLeft?.visibility = View.VISIBLE
            titleLayout?.setMoreImageViewVisiable(true)
        }
    }

    private fun closeNoDeviceDialog() {

        if (ClockUtils.deviceList.isEmpty()) {
            if (dialog.isVisible)
                dialog.show(activity.supportFragmentManager, "NoDevice")
            adapter.refresh(emptyList())
            closeLoadingDialog()
        }

//        adapter.refreshData(ClockUtils.getAllAlarmDetails())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ADD_ALARM or REQUEST_MODIFY_ALARM) {
            if (resultCode == Activity.RESULT_OK) {
//                refreshAlarmList()
            }
        }
    }
}