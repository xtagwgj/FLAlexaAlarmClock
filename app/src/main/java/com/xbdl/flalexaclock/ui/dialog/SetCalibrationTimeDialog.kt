package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.text.format.DateFormat
import android.view.*
import cn.qqtheme.framework.widget.WheelView
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xbdl.flalexaclock.R
import com.xtagwgj.baseproject.utils.MeasureUtil
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find

/**
 * 选择定时任务时间的对话框
 * Created by xtagwgj on 2017/9/13.
 */
class SetCalibrationTimeDialog : RxDialogFragment() {
    lateinit var inflate: View

    lateinit var wll_hour: WheelView
    lateinit var wll_minute: WheelView
    lateinit var wll_unit: WheelView

    private var hour = 0
    private var minute = 0
    private var isAm = false

    private var is24 = true

    var saveConsumer = Consumer<String> {


    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_set_calibration_time, null)

        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    fun initTime(timeString: String) {
        if (timeString.isNullOrEmpty() || !timeString.contains(":"))
            return


        try {
            isAm = hour in 1..12

            val timeArray = timeString.split(":")
            hour = timeArray[0].toInt()
            minute = timeArray[1].toInt()
        } catch (e: Exception) {
            hour = 0
            minute = 0
            isAm = false
        }

    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {
        dialogWindow.setGravity(Gravity.CENTER)
        dialogWindow.decorView.setPadding(16, 0, 16, 0)
        val lp = dialogWindow.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }


    fun initView() {

        wll_hour = inflate.find(R.id.wll_hour)
        wll_minute = inflate.find(R.id.wll_minute)
        wll_unit = inflate.find(R.id.wll_unit)

        inflate.find<View>(R.id.bt_cancel).setOnClickListener {
            dismiss()
        }

        inflate.find<View>(R.id.bt_confirm).setOnClickListener {

            if (!is24) {

                if (!isAm) {
                    hour += 12
                } else if (hour == 0) {
                    hour = 12
                }

                hour %= 24
            }

            saveConsumer.accept(String.format("%1$02d:%2$02d", hour, minute))
            dismiss()
        }

        initWheelTime()
    }

    private fun initWheelTime() {
        val hoursList = mutableListOf<String>()

        //24小时制
        is24 = DateFormat.is24HourFormat(activity)

        val defaultIndexHour =
                if (is24) {
                    hour
                } else {
                    when {
                        hour == 0 -> 11
                        hour <= 12 -> hour - 1
                        else -> hour - 12 - 1
                    }
                }

        if (is24) {
            //小时0到23
            (0 until 24).mapTo(hoursList) {
                String.format("%02d", it)
            }

        } else {
            //小时1到12
            (1 until 13).mapTo(hoursList) {
                String.format("%02d", it)
            }
        }

        initWheelView(hoursList, wll_hour, defaultIndex = defaultIndexHour,
                listener = WheelView.OnItemSelectListener {
                    hour = hoursList[it].toInt()

                }
        )

        val minuteList = mutableListOf<String>()
        //分钟0到59
        (0 until 60).mapTo(minuteList) { String.format("%02d", it) }
        initWheelView(minuteList, wll_minute, defaultIndex = minute,
                listener = WheelView.OnItemSelectListener {
                    minute = minuteList[it].toInt()
                }
        )

        if (is24) {
            wll_unit.visibility = View.GONE
        } else {

            initWheelView(
                    listOf(
                            "AM", "PM"
                    ),
                    wll_unit,
                    false,
                    defaultIndex = if (hour in 1..12) 0 else 1,
                    listener = WheelView.OnItemSelectListener {
                        isAm = it == 0
                    })
        }
    }

    /**
     * 初始化滚轮
     * @param strings 显示文字的集合
     * @param wheelView 滚轮控件
     * @param cycle 是否可循环滑动
     * @param defaultIndex 默认显示的位置
     * @param listener 监听
     */
    private fun initWheelView(strings: List<String>, wheelView: WheelView?, cycle: Boolean = true,
                              defaultIndex: Int = 0, listener: WheelView.OnItemSelectListener) {

        if (wheelView == null)
            return

        wheelView.setItems(strings, 2)
        wheelView.selectedIndex = defaultIndex
        //选项偏移量，可用来要设置显示的条目数，范围为1-5，1显示3行、2显示5行、3显示7行
        wheelView.setOffset(3)
        //设置是否禁用循环滚动
        wheelView.setCycleDisable(!cycle)
        wheelView.setTextColor(resources.getColor(R.color.colorAlarmCloseTime), resources.getColor(R.color.colorAccent))
        wheelView.setTextSize(MeasureUtil.sp2px(activity, 12F) * 1F)
        wheelView.setUseWeight(true)

        val config = WheelView.DividerConfig()
        config.setRatio((0.0 / 10.0).toFloat())//线比率
        config.setColor(resources.getColor(android.R.color.transparent))//线颜色
        config.setAlpha(1)//线透明度
        config.setThick(MeasureUtil.dp2px(activity, 2F) * 1F)//线粗
        wheelView.setDividerConfig(config)
        wheelView.setOnItemSelectListener(listener)
    }

}