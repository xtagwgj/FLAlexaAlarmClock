package com.xbdl.flalexaclock.ui.dialog

import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.text.InputType
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import com.xbdl.flalexaclock.extensions.inputCheck
import com.xbdl.flalexaclock.utils.ToastUtil
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_input.*
import org.jetbrains.anko.longToast

/**
 * 输入消息的对话框
 * Created by xtagwgj on 2017/9/11.
 */

class InputlMessageDialog : BaseRxDialog() {

    var leftTextRes: Int = R.string.text_step_cancel
        set(@StringRes value) {
            field = value
        }

    var leftColorRes: Int = R.color.colorAlarmCloseTime
        set(@ColorRes value) {
            field = value
        }

    var cancelConsumer: Consumer<Any> = Consumer {
        dismiss()
    }


    var rightTextRes: Int = R.string.text_dialog_confirm
        set(@StringRes value) {
            field = value
        }

    var rightColorRes: Int = R.color.black
        set(@ColorRes value) {
            field = value
        }

    var okConsumer: Consumer<String> = Consumer {
        dismiss()
    }

    var withSpaceBlank: Boolean = false


    var defaultText: String = ""

    var hintTextRes: Int = R.string.text_hint_group_name


    var titleTextRes: Int = R.string.text_dialog_set_group_name


    override fun getLayoutId(): Int {
        return R.layout.dialog_input
    }

    override fun initView() {

        tv_dialogTitle.text = getString(titleTextRes)
        et_input.hint = getString(hintTextRes)
        et_input.setText(defaultText)

        et_input.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS

        bt_cancel.apply {
            text = getString(leftTextRes)
            setTextColor(resources.getColor(leftColorRes))
        }

        bt_ok.apply {
            text = getString(rightTextRes)
            setTextColor(resources.getColor(rightColorRes))
        }

        clickEvent(bt_cancel, cancelConsumer)
        clickEvent(bt_ok, {
            if (et_input.text.toString().trim().isEmpty()) {
                ToastUtil.showToast(activity, R.string.text_input_empty)
                return@clickEvent
            }

            if (!et_input.inputCheck(withSpaceBlank)) {
                activity.longToast(R.string.text_name_error)
                return@clickEvent
            }

            okConsumer.accept(et_input.text.toString().trim())
//            dismiss()
        })
    }

    fun setInputInfoType(inputType: Int) {
        et_input?.inputType = inputType
    }

}