package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemGroupListBinding
import com.xbdl.flalexaclock.entity.GroupEntity
import org.jetbrains.anko.find

/**
 * 闹钟页面选择分组的适配器
 * Created by xtagwgj on 2017/9/30.
 */
class GroupNameAdapter(data: List<GroupEntity>, private val defaultCheckId: String, private val onItemClick: (GroupEntity) -> Unit)
    : DBAdapter<GroupEntity, ItemGroupListBinding>(data, R.layout.item_group_list) {
    override fun onBindViewHolder(holder: DBViewHolder<ItemGroupListBinding>, position: Int, item: GroupEntity) {
        holder.binding.checkId = defaultCheckId
        holder.binding.groupEntity = item

        holder.binding.root.find<View>(R.id.tv_groupName)
                .setOnClickListener {
                    onItemClick(item)
                }
    }
}