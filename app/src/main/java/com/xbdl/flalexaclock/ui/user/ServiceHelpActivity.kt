package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import kotlinx.android.synthetic.main.activity_service_help.*

/**
 * 服务与帮助
 * Created by xtagwgj on 2017/9/12.
 */
class ServiceHelpActivity() : GizBaseActivity(), View.OnClickListener {


    companion object {
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, ServiceHelpActivity::class.java)
            GizBaseActivity.startByAnim(mContext, intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_service_help

    override fun initView(p0: Bundle?) {

    }

    override fun initEventListener() {
        piv_ques1.setOnClickListener(this)
        piv_ques2.setOnClickListener(this)
    }

    override fun onClick(v: View) {

        when (v.id) {
            piv_ques1.id -> {
                ServiceHelpDetailsActivity.doAction(this, 1)
            }

            piv_ques2.id -> {
                ServiceHelpDetailsActivity.doAction(this, 2)
            }

            else -> {

            }
        }

    }
}