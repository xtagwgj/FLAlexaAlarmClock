package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import android.widget.CheckBox
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemClockListBinding
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.extensions.deviceIsOnline
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.ui.device.AlarmClockDetailsActivity
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.widget.AlarmView
import com.xtagwgj.baseproject.base.AppManager
import org.jetbrains.anko.find

/**
 * 分组设置页面的适配器
 * Created by xtagwgj on 2017/9/13.
 */
class ClockListAdapter(data: List<ClockEntity>, private val onClick: (ClockEntity, Boolean) -> Unit)
    : DBAdapter<ClockEntity, ItemClockListBinding>(data, R.layout.item_clock_list) {

    override fun onBindViewHolder(holder: DBViewHolder<ItemClockListBinding>, position: Int, item: ClockEntity) {
        holder.binding.name = mContext.getDeviceName(item.device)
        holder.binding.isOn = item.isClockSwitch
        holder.binding.area =
                TimeZoneTableUtils.getByTimeZone(item.timeZone).getShow()

        holder.binding.root.find<View>(R.id.ibt_del).setOnClickListener {
            onClick(item, false)
        }

        holder.binding.root.find<CheckBox>(R.id.rb_deviceAlarm).apply {
            setOnClickListener {
                isChecked = item.isClockSwitch

//                if (mContext.deviceIsController(item.device)) {
                onClick(item, true)
//                } else {
//                    mContext.toast(R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
//                }
            }
        }

        holder.binding.root.find<View>(R.id.rl_device).setOnClickListener {
            if (item.device == null)
                return@setOnClickListener

            AlarmClockDetailsActivity.doAction(
                    AppManager.getAppManager().currentActivity(), item.device)
        }

        if (mContext.deviceIsOnline(item.device)) {
            holder.binding.root.find<AlarmView>(R.id.tv_devicePic)
                    .setTime(TimeZoneTableUtils.getIdByTimeZoneWithCity(item.timeZone))
        } else {
            holder.binding.root.find<AlarmView>(R.id.tv_devicePic).setTimeVisible(false)
        }
    }


    fun getAllDidList(): List<String> = mDatas.map { it.device.did }

    //查找符合条件的设备是否存在
    fun findDeviceExits(did: String): Boolean {
        return mDatas.any { it.device.did == did }
    }

    fun updateTzAndStatus(did: String, timeZone: Int, isSwitchStatus: Boolean) {
        val clockEntity = mDatas.firstOrNull { it.device.did == did }
        if (clockEntity != null) {
            clockEntity.isClockSwitch = isSwitchStatus
            clockEntity.timeZone = timeZone
            notifyItemChanged(mDatas.indexOf(clockEntity))
        }

    }
}