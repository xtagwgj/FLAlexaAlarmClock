package com.xbdl.flalexaclock.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.PROVICE_URL
import kotlinx.android.synthetic.main.activity_webview.*

/**
 * 网页
 * Created by xtagwgj on 2017/9/18.
 */
class WebViewActivity() : GizBaseActivity() {


    companion object {

        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, WebViewActivity::class.java)
            startByAnim(mContext, intent)
        }

    }

    private val client = object : WebChromeClient() {

        override fun onProgressChanged(view: WebView, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            progressBar.progress = newProgress
            if (progressBar.progress >= 100) {
                progressBar.visibility = View.GONE
            }
        }
    }

    override fun initEventListener() {

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initView(p0: Bundle?) {
//        val webView = findViewById(R.id.webView)

        /**
         * 为webview设置进度条
         */
        webView.webChromeClient = client
        /**
         * 这个设置是为了让一个网页跳转到其他网页时也能不再浏览器里打开
         */
        webView.webViewClient = NoAdWebViewClient(this, PROVICE_URL)


        val settings = webView.settings
        settings.javaScriptCanOpenWindowsAutomatically = true
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.displayZoomControls = false
        settings.javaScriptEnabled = true
        settings.allowFileAccess = true
        settings.builtInZoomControls = true
        settings.setSupportZoom(true)
        settings.domStorageEnabled = true
        settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS

        webView.loadUrl(PROVICE_URL)

    }

    override fun getLayoutId(): Int = R.layout.activity_webview
}