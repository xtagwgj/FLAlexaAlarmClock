package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.view.View
import com.xtagwgj.baseproject.utils.StringUtils
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import kotlinx.android.synthetic.main.activity_about_us.*

/**
 * 关于我们
 * Created by xtagwgj on 2017/9/18.
 */
class AboutUsActivity() : GizBaseActivity(), View.OnClickListener {
    override fun onClick(v: View) {

        val url = when (v.id) {
            bt_faceBook.id -> "https://www.facebook.com/homtime.homtime"
            bt_instagram.id -> "https://www.instagram.com/homtimecn/"
            bt_twitter.id -> "https://twitter.com/homtimecn"
            else -> null
        }

        if (!StringUtils.isEmpty(url)) {
            val intent = Intent()//创建Intent对象
            intent.action = Intent.ACTION_VIEW//为Intent设置动作
            intent.data = Uri.parse(url)//为Intent设置数据
            startActivity(intent)//将Intent传递给Activity
        }

    }

    companion object {
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, AboutUsActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun initEventListener() {

        bt_faceBook.setOnClickListener(this)
        bt_instagram.setOnClickListener(this)
        bt_twitter.setOnClickListener(this)

    }

    override fun initView(p0: Bundle?) {

        val spannableString = SpannableString(tv_webSite.text.toString())
        val urlSpan = URLSpan("http://en.homtime.com")
        spannableString.setSpan(urlSpan, tv_webSite.text.toString().indexOf(":") + 2, spannableString.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        tv_webSite.movementMethod = LinkMovementMethod.getInstance()
        tv_webSite.highlightColor = resources.getColor(R.color.colorAccent)
        tv_webSite.text = spannableString
    }

    override fun getLayoutId(): Int = R.layout.activity_about_us
}