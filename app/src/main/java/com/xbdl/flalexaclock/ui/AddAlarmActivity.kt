package com.xbdl.flalexaclock.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.view.View
import cn.qqtheme.framework.widget.WheelView
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.trello.rxlifecycle2.android.ActivityEvent
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.DeviceChangeEntity
import com.xbdl.flalexaclock.entity.giz.GizCommandResponse
import com.xbdl.flalexaclock.entity.giz.UpdateNetStatusResponse
import com.xbdl.flalexaclock.extensions.*
import com.xbdl.flalexaclock.ui.dialog.AlarmDeviceDialog
import com.xbdl.flalexaclock.ui.dialog.CustomChooseDialog
import com.xbdl.flalexaclock.ui.dialog.RepeatChooseDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.HexStrUtils
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.MeasureUtil
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_alarm.*
import org.jetbrains.anko.toast
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * 添加或是修改闹钟
 * Created by xtagwgj on 2017/9/12.
 */
class AddAlarmActivity : GizBaseActivity() {

    private val TAG: String = "self_AddAlarmActivity"

    private var device: GizWifiDevice? = null
    private var alarm: AlarmDetailsEntity? = null

    private var isModify: Boolean = false

    companion object {

        /**
         *@param alarm 不为空 则是修改指定的闹铃
         * @param device 不为空这是操作这个设备，否则就是选择设备
         */
        fun doAction(mContext: Activity, device: GizWifiDevice? = null, alarm: AlarmDetailsEntity? = null) {

            //判断是否有闹铃
            if (ClockUtils.deviceList.size == 0) {
                ToastUtil.showToast(mContext,R.string.toast_add_clock)
                return
            }

            val intent = Intent(mContext, AddAlarmActivity::class.java)
            intent.putExtra("device", device)
            intent.putExtra("alarm", alarm)


            if (alarm == null)
                startByAnim(mContext, intent, REQUEST_ADD_ALARM)
            else
                startByAnim(mContext, intent, REQUEST_MODIFY_ALARM)
        }

    }

    override fun getLayoutId(): Int = R.layout.activity_add_alarm

    override fun initView(p0: Bundle?) {

        device = intent.getParcelableExtra("device")
        alarm = intent.getParcelableExtra("alarm")

        LogUtils.d(TAG, "选择的设备=$device 选择的闹钟=$alarm")

        if (device == null) {//没有设备的 只能是添加 选择设备后判断是否可以添加
            initAddAlarm()
        } else {
            //不可以选择设备了
            arrow2.visibility = View.INVISIBLE
            rl_clockName.isEnabled = false
            iv_clock.visibility = View.VISIBLE

            refreshDeviceImage()

            if (alarm == null) {//新增闹钟
                initAddAlarm()
            } else {//修改闹钟

                isModify = true
                ll_repeat.isEnabled = !alarm!!.isMainAlarm

                if (alarm!!.isMainAlarm)
                    iv_repeatArrow?.visibility = View.INVISIBLE
            }
        }

        //界面初始化
        //重复次数
        when {
            alarm!!.isSingle -> tv_repeat.text = getString(R.string.text_once)
            alarm!!.isEveryDay -> {
                tv_repeat.text = getString(R.string.text_everyday)
            }
            else -> tv_repeat.text = getString(R.string.text_custom)
        }

        //修改页面的数据
        tv_clockname.text =
                if (StringUtils.isEmpty(getDeviceName(device)))
                    getString(R.string.text_no_choose_device)
                else
                    getDeviceName(device)

//        tv_time.text = alarm?.showTime
//        tv_timeUnit.text = alarm?.timeUnit
//
//        if (alarm?.timeUnit.isNullOrEmpty()) {
//            wll_unit.visibility = View.GONE
//        } else {
//            wll_unit.visibility = View.VISIBLE
//            wll_unit.selectedIndex = if (alarm?.timeUnit == "AM") 0 else 1
//        }

        //发送命令的回调
        RxBus.getInstance()
                .register<GizCommandResponse>("GizCommandResponse")
                .compose(bindToLifecycle())
                .filter { it.sn == SN_ADD_MODIFY_ALARM }//只接收添加修改闹钟的命令回调
                .subscribe({
                    LogUtils.d(TAG, "发送命令的回调=$it")
                    if (it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {//成功
                        device?.getDeviceStatus(null)

                        closeLoadingDialog()
                        setResult(Activity.RESULT_OK)
                        AppManager.getAppManager().finishActivity()
                    } else {//失败
                        setFailLoadingText(GizErrorToast.parseExceptionCode(it.result))
                        failLoading()
                    }
                }, {
                    it.printStackTrace()
                })

        if (!isModify)
        //获取数据并且解析显示
            RxBus.getInstance()
                    .register<DeviceChangeEntity>(REFRESH_DEVICE)
                    .compose(bindUntilEvent(ActivityEvent.DESTROY))
                    .filter { it.did == device?.did }
//                    .subscribeOn(Schedulers.io())
//                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        findAlarmLocation()
                    }, {
                        it.printStackTrace()
                    })

        //监听设备的网络变化
        RxBus.getInstance()
                .register<UpdateNetStatusResponse>("UpdateNetStatusResponse")
                .compose(bindToLifecycle())
                .filter { it.device?.did == device?.did }
                .subscribe({

                    if (it.netStatus == GizWifiDeviceNetStatus.GizDeviceControlled) {
                        if (alarm?.state == -2) {
                            LogUtils.d(TAG, "未找到当前设备的可空位置，重新查找")
                            registerDevice()
                        }
                    } else if (it?.netStatus == GizWifiDeviceNetStatus.GizDeviceOffline
                            || it?.netStatus == GizWifiDeviceNetStatus.GizDeviceUnavailable) {
                        toast(R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
                    }

                }, {
                    it.printStackTrace()
                })

        //时间制发生变化
        RxBus.getInstance()
                .register<Any>(Intent.ACTION_TIME_CHANGED)
                .compose(bindToLifecycle())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    initWheelTime()
                })

        initWheelTime()
    }


    /**
     * 初始化新增的闹铃
     */
    private fun initAddAlarm() {
        LogUtils.d(TAG, "初始化新增的闹铃")

        alarm = AlarmDetailsEntity()
        alarm?.isMainAlarm = false
        alarm?.hour = 7
        alarm?.minute = 0
        alarm?.retryInfo = "1111111"
        alarm?.isSingle = false
        alarm?.pos = -2//未找到设置的位置

        findAlarmLocation()
    }

    /**
     * 查找闹钟要设置的位置
     */
    private fun findAlarmLocation() {
        if (device != null) {
            Observable.just(device)
                    .map { ClockUtils.onlineDevicePoint[it!!.did] }
                    .filter { it != null }
                    .map { entity ->
                        (1..12).map {
                            entity!!.getSubAlarm(it, entity.did)
                        }.filter { it.state == 3 || it.state == 0 }
                    }//找到可以放置数据的地方
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        refreshDeviceImage()

                        val canUpdateAlarm = it.firstOrNull()
                        if (canUpdateAlarm == null) {
                            toast(getString(R.string.text_alarm_above_12))
                            alarm?.pos = -2
                        } else {
                            alarm?.pos = canUpdateAlarm.pos
                            alarm?.state = canUpdateAlarm.state
                            alarm?.isGroupAlarm = canUpdateAlarm.isGroupAlarm
                        }
                    }, {
                        it.printStackTrace()
                    })
        }
    }

    override fun initEventListener() {

        clickEvent(tv_cancel, { destroyByAnim(this) })

        //选择重复次数
        clickEvent(ll_repeat, {
            toChooseRepeatMode()
        })

        //闹钟设备
        clickEvent(rl_clockName, {
            toChooseDevice()
        })

        //确定，添加或修改闹钟
        clickEvent(tv_ok, {
            if (device == null) {
                toast(R.string.no_choose_device)
            } else {
                modifyAlarm()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        registerDevice()
    }

    private fun registerDevice() {
        LogUtils.d(TAG, "注册获取设备信息")
        if (device != null) {//订阅设备

            if (device?.listener == null)
                device?.listener = ParseDataService.deviceListener

            if (!device!!.isSubscribed)
                device?.setSubscribe(BuildConfig.PRODUCT_SECRET, true)
        }

        device?.getDeviceStatus(null)
    }

    /**
     * 选择重复的模式
     */
    private fun toChooseRepeatMode() {
        val repeatChooseDialog = RepeatChooseDialog()
        repeatChooseDialog.defaultSelectedRepeat =
                when (tv_repeat.text.toString().toUpperCase()) {
                    getString(R.string.text_once).toUpperCase() -> RepeatChooseDialog.ONCE
                    getString(R.string.text_daily).toUpperCase(),
                    getString(R.string.text_everyday).toUpperCase() -> RepeatChooseDialog.DAILY

                    else -> RepeatChooseDialog.CUSTOM
                }
        repeatChooseDialog.selectedConsumer = Consumer {

            when (it) {
                RepeatChooseDialog.ONCE -> {
                    tv_repeat.text = getString(R.string.text_once)
                    //fixme:要判断是今日还是明日 按照设备的时区来设定
                    alarm?.isSingle = true
                }
                RepeatChooseDialog.DAILY -> {
                    tv_repeat.text = getString(R.string.text_everyday)
                    alarm?.retryInfo = "1111111"
                    alarm?.isSingle = false
                }
                else -> {
                    tv_repeat.text = getString(R.string.text_custom)

                    val reverseInfo = ("0000000" + alarm?.retryInfo).reversed()

                    val retryInfoMap = mutableMapOf(
                            "0" to false,
                            "1" to false,
                            "2" to false,
                            "3" to false,
                            "4" to false,
                            "5" to false,
                            "6" to false
                    )

                    (0..7)
                            .filter { reverseInfo[it].toString() == "1" }
                            .forEach { retryInfoMap.put("$it", true) }

                    val customDialog = CustomChooseDialog()
                    //上次选择的信息
                    customDialog.retryInfoMap = retryInfoMap

                    customDialog.selectedConsumer = Consumer {
                        alarm?.retryInfo = it
                        alarm?.isSingle = false

                        LogUtils.d(TAG, "重复时间=$it")
                    }
                    customDialog.show(AddAlarmActivity@ this.supportFragmentManager, "customDialog")
                }
            }

            repeatChooseDialog.dismiss()
        }
        repeatChooseDialog.show(AddAlarmActivity@ this.supportFragmentManager, "repeatChooseDialog")
    }

    /**
     * 更新设备图像的显示
     */
    private fun refreshDeviceImage() {

        if (deviceIsOnline(device)) {
            val timeZoneWithCity = ClockUtils.clockAlarmMap[device?.did]?.timeZone ?: 256
            iv_clock?.setTime(TimeZoneTableUtils.getIdByTimeZoneWithCity(timeZoneWithCity))
        } else {
            iv_clock.setTimeVisible(false)
        }
    }

    /**
     * 去选择设备
     */
    private fun toChooseDevice() {
        val deviceDialog = AlarmDeviceDialog()

        deviceDialog.selectedDeviceDid = device?.did

        deviceDialog.singleChooseConsumer = Consumer {

            if (!deviceIsController(it.device)) {
                toast(R.string.select_device_online)
                return@Consumer
            }

            //更新显示的设备
            tv_clockname.text = getDeviceName(it.device)
            device = it.device
            iv_clock.visibility = View.VISIBLE

            //查找位置
            findAlarmLocation()

            refreshDeviceImage()

            registerDevice()
        }
        deviceDialog.show(supportFragmentManager, "deviceDialog")
    }

    /**
     * 是否是24小时制的
     */
    private fun is24() = DateFormat.is24HourFormat(this)

    private fun initWheelTime() {
        val is24Hour = is24()

        val hoursList = mutableListOf<String>()

        if (is24Hour) {
            //小时0到23
            (0 until 24).mapTo(hoursList) { String.format("%02d", it) }
        } else {
            //小时1到12
            (1 until 13).mapTo(hoursList) { String.format("%02d", it) }
        }

        initWheelView(hoursList, wll_hour,
                defaultIndex = if (is24Hour) alarm!!.hour else {
                    //12小时的
                    when (alarm!!.hour) {
                        0 -> 11
                        in 1..12 -> alarm!!.hour - 1
                        else -> {
                            alarm!!.hour - 13
                        }
                    }
                },
                listener = WheelView.OnItemSelectListener {
                    if (is24Hour) {//24小时制
                        alarm!!.hour = hoursList[it].toInt()
                    } else {
                        //12小时制的 0->11
                        if (it == 11) {
                            if (wll_unit.selectedIndex == 0) {
                                alarm!!.hour = 0
                            } else {
                                alarm!!.hour = 12
                            }
                        } else {
                            alarm!!.hour = (hoursList[it].toInt() + wll_unit.selectedIndex * 12) % 24
                        }


                    }

                    showTime()
                }
        )

        val minuteList = mutableListOf<String>()
        //分钟0到59
        (0 until 60).mapTo(minuteList) { String.format("%02d", it) }
        initWheelView(minuteList, wll_minute, defaultIndex = alarm!!.minute,
                listener = WheelView.OnItemSelectListener {
                    alarm!!.minute = it
                    showTime()
                }
        )
//        if (wll_minute.selectedIndex != alarm?.minute) {
//            wll_minute.selectedIndex = alarm?.minute ?: 0
//        }

        //12小时制的显示早上下午
        wll_unit.visibility = if (is24Hour) {
            View.GONE
        } else {
            View.VISIBLE
        }

        //12小时制才初始化显示早上下午
        if (!is24Hour)
            initWheelView(
                    listOf("AM", "PM"),
                    wll_unit,
                    false,
                    defaultIndex = if (alarm!!.hour in 0..11) 0 else 1,
                    listener = WheelView.OnItemSelectListener {

                        if (it == 0) {
                            //移到早上
//                            if (alarm!!.hour !in 0..11) {//但是当前时间不在早上，变化时间
//                                alarm!!.hour = ((alarm!!.hour + 24) - 12) % 24
//                            }


                            if (alarm!!.hour == 12) {
                                alarm!!.hour = 0
                            } else if (alarm!!.hour > 12) {
                                alarm!!.hour -= 12
                            }


                        } else {
                            //移到下午
//                            if (alarm!!.hour in 0..11) {//但是当前时间在早上，变化时间
//                                alarm!!.hour = (alarm!!.hour + 12) % 24
//                            }

                            if (alarm!!.hour == 0) {
                                alarm!!.hour = 12
                            } else if (alarm!!.hour < 12) {
                                alarm!!.hour += 12
                            }

                        }


                        showTime()
                    })

        showTime()
    }

    private fun showTime() {
        tv_time?.text =
                alarm!!.showTime

        tv_timeUnit.text = alarm!!.timeUnit
    }

    /**
     * 初始化滚轮
     * @param strings 显示文字的集合
     * @param wheelView 滚轮控件
     * @param cycle 是否可循环滑动
     * @param defaultIndex 默认显示的位置
     * @param listener 监听
     */
    private fun initWheelView(strings: List<String>, wheelView: WheelView, cycle: Boolean = true,
                              defaultIndex: Int = 0, listener: WheelView.OnItemSelectListener) {
        wheelView.setItems(strings, defaultIndex)
//        wheelView.selectedIndex = defaultIndex
        //选项偏移量，可用来要设置显示的条目数，范围为1-5，1显示3行、2显示5行、3显示7行
        wheelView.setOffset(3)
        //设置是否禁用循环滚动
        wheelView.setCycleDisable(!cycle)
        wheelView.setTextColor(resources.getColor(R.color.colorAlarmCloseTime), resources.getColor(R.color.colorAccent))
        wheelView.setTextSize(MeasureUtil.sp2px(this, 12F) * 1F)
        wheelView.setUseWeight(true)

        val config = WheelView.DividerConfig()
        config.setRatio((0.0 / 10.0).toFloat())//线比率
        config.setColor(resources.getColor(android.R.color.transparent))//线颜色
        config.setAlpha(1)//线透明度
        config.setThick(MeasureUtil.dp2px(this, 2F) * 1F)//线粗
        wheelView.setDividerConfig(config)
        wheelView.setOnItemSelectListener(listener)
    }

    /**
     * 计算单次闹铃情况下的重复信息
     */
    private fun dealSingleRetryInfo() {

        val timeZone = TimeZoneTableUtils.getCurrTimeZone(
                ClockUtils.onlineDevicePoint[device?.did]?.data_time_zone ?: 256
        )


        TimeZone.setDefault(timeZone)
        LogUtils.e(TAG, timeZone)
        val calendar = Calendar.getInstance()

        calendar.set(Calendar.HOUR_OF_DAY, alarm!!.hour)
        calendar.set(Calendar.MINUTE, alarm!!.minute)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        val currCalendar = Calendar.getInstance()
        //设置的时间在现在的时刻之前，天数加一天
        if (currCalendar.timeInMillis > calendar.timeInMillis) {
            calendar.add(Calendar.DAY_OF_YEAR, 1)
        }

        //该周的第pos天的闹钟  第一天是星期日 第二天是星期1 第三天是星期二
        val pos = calendar.get(Calendar.DAY_OF_WEEK)
        val sb = StringBuilder("0000000")
        if (pos == 1)
            alarm?.retryInfo = sb.replace(0, 1, "1").toString()
        else
            alarm?.retryInfo = sb.replace(8 - pos, 9 - pos, "1").toString()

        LogUtils.e(TAG, "该周的第 $pos 天, retryInfo是 ${alarm?.retryInfo}")
    }

    /**
     * 修改闹钟
     */
    private fun modifyAlarm() {
        if (device == null) {
            toast(R.string.no_choose_device)
            return
        }

        if (device?.netStatus == GizWifiDeviceNetStatus.GizDeviceOffline
                || device?.netStatus == GizWifiDeviceNetStatus.GizDeviceUnavailable) {
            toast(R.string.text_device_unAvailable)
            return
        }

        if (alarm?.pos == -2) {//没找到位置
            toast("Can not find the location where the alarm is placed")
            return
        }

        showLoadingDialog(R.string.text_loading_set_alarm, R.string.app_name)

        if (alarm!!.isSingle) {
            dealSingleRetryInfo()
        }

        val currRetryInfo: String =
                //重复情况
                if (alarm!!.isSingle) {
                    "0" + alarm!!.retryInfo
                } else {
                    "1" + alarm!!.retryInfo
                }

        LogUtils.e("notice", "重复周日到周一[$currRetryInfo]")

        //分 时  重复情况
        val alarmSet = StringBuffer()

        val hexMinute = if (Integer.toHexString(alarm!!.minute).length == 2) {
            Integer.toHexString(alarm!!.minute)
        } else {
            "0" + Integer.toHexString(alarm!!.minute)
        }

        val hexHour = if (Integer.toHexString(alarm!!.hour).length == 2) {
            Integer.toHexString(alarm!!.hour)
        } else {
            "0" + Integer.toHexString(alarm!!.hour)
        }

        if (PARSE_FROM_LEFT) {
            alarmSet.append(hexMinute)
                    .append(hexHour)
                    .append(HexStrUtils.binaryString2hexString(currRetryInfo))
                    .append(if (alarm!!.isSingle) "00" else HexStrUtils.binaryString2hexString(currRetryInfo))

        } else {
            alarmSet.append(if (alarm!!.isSingle) "00" else HexStrUtils.binaryString2hexString(currRetryInfo))
                    .append(HexStrUtils.binaryString2hexString(currRetryInfo))
                    .append(hexHour)
                    .append(hexMinute)
        }

        LogUtils.d(TAG, "hexString=${alarmSet.toString()}")
        LogUtils.d(TAG, "hexString=${HexStrUtils.hexStringToBytes(alarmSet.toString())}")

        //如果是新添加的，打开闹钟开关，否则保持原来的状态
//        val state =
//                if (alarm!!.state == 3) {
//                    1
//                } else alarm!!.state


        getSwitchSetKeyAndStateKey(alarm!!.pos, { setKey, stateKey ->
            val commandMap = ConcurrentHashMap<String, Any>()

            commandMap.put(setKey, HexStrUtils.hexStringToBytes(alarmSet.toString()))
            commandMap.put(stateKey, 1)

            sendCommand(device!!, commandMap, SN_ADD_MODIFY_ALARM)
        })

        showLoadingDialog(R.string.text_load_save_alarm, R.string.text_load_save_alarm_success)
    }

}