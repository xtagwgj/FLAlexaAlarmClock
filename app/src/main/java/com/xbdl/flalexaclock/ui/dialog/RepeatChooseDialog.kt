package com.xbdl.flalexaclock.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.RadioButton
import com.jakewharton.rxbinding2.view.RxView
import com.trello.rxlifecycle2.components.support.RxDialogFragment
import com.xtagwgj.baseproject.constant.BaseConstants.THROTTLE_TIME
import com.xbdl.flalexaclock.R
import io.reactivex.functions.Consumer
import org.jetbrains.anko.find
import java.util.concurrent.TimeUnit

/**
 * 重复方式的对话框
 * Created by xtagwgj on 2017/7/20.
 */

class RepeatChooseDialog : RxDialogFragment(), View.OnClickListener {

    //默认选择仅一次
    var defaultSelectedRepeat = ONCE

    lateinit var inflate: View

    var selectedConsumer: Consumer<Int>? = null

    private var rb_once: RadioButton? = null
    private var rb_custom: RadioButton? = null
    private var rb_daily: RadioButton? = null

    companion object {
        val ONCE = 0
        val DAILY = 1
        val CUSTOM = 2
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity, R.style.ActionSheetDialogStyle)
        inflate = LayoutInflater.from(activity).inflate(R.layout.dialog_repeat, null)

        rb_once = inflate.find(R.id.rb_once)
        rb_daily = inflate.find(R.id.rb_daily)
        rb_custom = inflate.find(R.id.rb_custom)

        dialog.setContentView(inflate)

        val dialogWindow = dialog.window
        if (dialogWindow != null) {
            val lp = setDialogSetting(dialogWindow)
            dialogWindow.attributes = lp
        }

        return dialog
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun setDialogSetting(dialogWindow: Window): WindowManager.LayoutParams {
        dialogWindow.setGravity(Gravity.BOTTOM)
        dialogWindow.decorView.setPadding(0, 0, 0, 0)
        val lp = dialogWindow.attributes
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        return lp
    }

    private fun initView() {

        when (defaultSelectedRepeat) {
            ONCE -> rb_once?.isChecked = true
            DAILY -> rb_daily?.isChecked = true
            CUSTOM -> rb_custom?.isChecked = true
        }

        rb_once?.setOnClickListener(this)
        rb_daily?.setOnClickListener(this)
        rb_custom?.setOnClickListener(this)
    }

    private fun clickEvent(view: View?, consumer: Consumer<Any>) {
        if (view == null)
            return

        RxView.clicks(view)
                .throttleFirst(THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .compose(this.bindToLifecycle())
                .subscribe(consumer)
    }

    override fun onClick(v: View) {
        if (selectedConsumer == null) {
            return
        }

        when (v.id) {
            rb_once?.id -> selectedConsumer?.accept(ONCE)
            rb_daily?.id -> selectedConsumer?.accept(DAILY)
            rb_custom?.id -> selectedConsumer?.accept(CUSTOM)
            else -> {
            }
        }
    }
}
