package com.xbdl.flalexaclock.ui.dialog

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import com.xbdl.flalexaclock.ui.config.ConfigurationWifiActivity
import com.xbdl.flalexaclock.utils.ClockUtils
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_normal_msg.*

/**
 * 重新连接设备对话框
 * Created by xtagwgj on 2017/9/11.
 */

class ReconnectDialog : BaseRxDialog() {

    var device: GizWifiDevice? = null


    var cancelConsumer: Consumer<Any> = Consumer {
        dismiss()
    }


    var okConsumer: Consumer<Any> = Consumer {
        //保存重新配网的设置信息
        ClockUtils.tempMap["alias"] = device?.alias ?: ""
        ClockUtils.tempMap["remark"] = device?.remark ?: ""

        ConfigurationWifiActivity.doAction(activity, device?.did)
        dismiss()
    }


    override fun getLayoutId(): Int = R.layout.dialog_reconnect

    override fun initView() {

        clickEvent(bt_cancel, cancelConsumer)
        clickEvent(bt_ok, okConsumer)
    }

}