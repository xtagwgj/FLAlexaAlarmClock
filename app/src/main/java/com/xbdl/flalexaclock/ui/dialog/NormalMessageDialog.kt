package com.xbdl.flalexaclock.ui.dialog

import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_normal_msg.*

/**
 * 普通消息提示的对话框
 * Created by xtagwgj on 2017/9/11.
 */

class NormalMessageDialog : BaseRxDialog() {

    var leftTextRes: Int = R.string.text_step_cancel
        set(@StringRes value) {
            field = value
        }

    var leftColorRes: Int = R.color.colorAlarmCloseTime
        set(@ColorRes value) {
            field = value
        }

    var cancelConsumer: Consumer<Any> = Consumer {
        dismiss()
    }


    var rightTextRes: Int = R.string.text_ok
        set(@StringRes value) {
            field = value
        }

    var rightColorRes: Int = R.color.colorAccent
        set(@ColorRes value) {
            field = value
        }

    var okConsumer: Consumer<Any> = Consumer {
        dismiss()
    }



    var textRes: Int = R.string.text_step_cancel



    override fun getLayoutId(): Int {
        return R.layout.dialog_normal_msg
    }

    override fun initView() {

        tv_content.text = getString(textRes)

        bt_cancel.apply {
            text = getString(leftTextRes)
            setTextColor(resources.getColor(leftColorRes))
        }

        bt_ok.apply {
            text = getString(rightTextRes)
            setTextColor(resources.getColor(rightColorRes))
        }

        clickEvent(bt_cancel, cancelConsumer)
        clickEvent(bt_ok, okConsumer)
    }

}