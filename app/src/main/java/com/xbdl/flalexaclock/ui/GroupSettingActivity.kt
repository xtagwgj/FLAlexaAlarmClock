package com.xbdl.flalexaclock.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.REFRESH_DEVICE
import com.xbdl.flalexaclock.base.REFRESH_GROUP
import com.xbdl.flalexaclock.base.SN_CLOSE_ALARM
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.entity.DeviceChangeEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.extensions.getStateList
import com.xbdl.flalexaclock.extensions.mContext
import com.xbdl.flalexaclock.extensions.sendCommand
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.adapter.ClockListAdapter
import com.xbdl.flalexaclock.ui.dialog.AlarmDeviceDialog
import com.xbdl.flalexaclock.ui.dialog.InputlMessageDialog
import com.xbdl.flalexaclock.ui.dialog.NormalMessageDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xbdl.flalexaclock.widget.MorePop
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_group_setting.*
import java.util.concurrent.ConcurrentHashMap

/**
 * 分组设置
 * Created by xtagwgj on 2017/9/14.
 */
class GroupSettingActivity() : GizBaseActivity() {

    var adapter: ClockListAdapter? = null

    private var morePop: MorePop? = null

    lateinit var groupId: String
    lateinit var groupName: String

    companion object {

        private val REQUEST_GROUP_CHANGE = 1011

        fun doAction(mContext: Activity, groupId: String, groupName: String) {
            val intent = Intent(mContext, GroupSettingActivity::class.java)
            intent.putExtra("groupId", groupId)
            intent.putExtra("groupName", groupName)
            startByAnim(mContext, intent, REQUEST_GROUP_CHANGE)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_group_setting


    override fun initView(p0: Bundle?) {

        groupId = intent.getStringExtra("groupId")
        groupName = intent.getStringExtra("groupName")

        tv_groupName.text = groupName

        adapter = ClockListAdapter(emptyList(), { clockEntity, isSwitchChanged ->
            if (isSwitchChanged) {
                val commandMap = ConcurrentHashMap<String, Any>()
                commandMap.put(GizDataPointKey.KEY_ALARM_STATE, !clockEntity.isClockSwitch)
                commandMap.put(GizDataPointKey.KEY_MAIN_ALARM_SET, if (clockEntity.isClockSwitch) 2 else 1)

                //当前显示的是打开状态，其实是要进行关闭的操作
                if (clockEntity.isClockSwitch) {
                    commandMap.put(GizDataPointKey.KEY_ALARM_ACTION, true)

                    //关闭所有打开的闹铃
                    mContext.getStateList(clockEntity.alarmList, 1).forEach {
                        commandMap.put(it, 2)
                    }
                } else {
                    //打开所有关闭的闹钟
                    mContext.getStateList(clockEntity.alarmList, 2).forEach {
                        commandMap.put(it, 1)
                    }
                }

                sendCommand(clockEntity.device, commandMap, SN_CLOSE_ALARM)

            } else
                displayRemoveAlarmDialog(clockEntity)
        })

        recyclerClockList.apply {
            layoutManager = LinearLayoutManager(this@GroupSettingActivity)
            addItemDecoration(SimpleDividerDecoration(this@GroupSettingActivity,
                    dividerColorInt = this@GroupSettingActivity.resources.getColor(R.color.colorBg)))
        }.adapter = adapter
    }


    override fun initEventListener() {

        iv_more.setOnClickListener {
            if (morePop == null) {
                morePop = MorePop(this, object : MorePop.PopClickListener {
                    override fun updateDeviceName() {

                        updateGroupName()
                    }

                    override fun exchangePic() {

                    }

                    override fun deleteDevice() {
                        val delDialog = NormalMessageDialog()
                        delDialog.textRes = R.string.text_dialog_del_group
                        delDialog.okConsumer = Consumer {
                            deleteGroup()
                            delDialog.dismiss()
                        }
                        delDialog.show(supportFragmentManager, "delDialog")
                    }

                })
            }

            if (morePop!!.isShowing)
                morePop!!.dismiss()
            else
                morePop!!.showAsDropDown(iv_more)
        }

        clickEvent(bt_addNew, {
            displayAddDeviceDialog()
        })

        //获取数据并且解析显示
        RxBus.getInstance()
                .register<DeviceChangeEntity>(REFRESH_DEVICE)
                .compose(bindToLifecycle())
                .filter { adapter?.findDeviceExits(it.did) == true }
                .map { ClockUtils.onlineDevicePoint[it.did] }
                .filter { it != null }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    adapter?.updateTzAndStatus(it!!.did, it.data_time_zone, it.data_alarm_state)
                }, {
                    it.printStackTrace()
                })

        //分组中的设备变化导致要刷新分组
        RxBus.getInstance()
                .register<String>(REFRESH_GROUP)
                .compose(bindToLifecycle())
                .filter { it == groupId }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //                    val groupEntity = ClockUtils.groupDeviceMap.keys.first { it.id == groupId }
//                    adapter?.refreshData(groupEntity.clockEntityList)
                    requestClockList()
                }, {
                    it.printStackTrace()
                })


    }

    override fun onResume() {
        super.onResume()
        requestClockList()
    }

    /**
     * 删除分组 在clockUtils中要记得删除
     */
    private fun deleteGroup() {

        showLoadingDialog(R.string.load_del_group, R.string.load_del_group_success)

        HttpRequest.deleteGroup(groupId, {
            if (it) {
                val adapterList = adapter?.getAllDidList() ?: emptyList()

                GizWifiSDK.sharedInstance().deviceList.filter { adapterList.contains(it.did) }.forEach {
                    it.setCustomInfo("", null)
                }

//                val groupEntity = ClockUtils.groupDeviceMap.keys.first { it.id == groupId }
                //在ClockUtil中删除
//                ClockUtils.groupDeviceMap.remove(groupEntity)

                ClockUtils.deleteGroupEntity(groupId)

            }

        }, getErrorConsumer())
    }

    private fun displayRemoveAlarmDialog(clockEntity: ClockEntity) {
        val removeDialog = NormalMessageDialog()
        removeDialog.textRes = R.string.text_dialog_remove_clock
        removeDialog.okConsumer = Consumer {
            removeDeviceFromCurrGroup(clockEntity)
            removeDialog.dismiss()
        }
        removeDialog.show(supportFragmentManager, "removeDialog")
    }

    /**
     * 加载分组下闹钟的列表
     */
    private fun requestClockList() {

        val groupEntity = ClockUtils.groupDeviceMap.keys.firstOrNull { it.id == groupId }

        if (groupEntity == null) {
            closeLoadingDialog()
            destroyByAnim(this)
        } else {
            adapter?.refreshData(groupEntity.clockEntityList)
        }
    }

    /**
     * 更新分组的名字
     */
    private fun updateGroupName() {
        val inputMessageDialog = InputlMessageDialog()
        inputMessageDialog.okConsumer = Consumer { name ->
            inputMessageDialog.dismiss()

            showLoadingDialog(R.string.text_update_group_name, R.string.text_update_group_name_success)

            HttpRequest.modifyGroupName(groupId, name, {
                if (it) {
                    groupName = name
                    tv_groupName.text = groupName
                    val groupEntity = ClockUtils.groupDeviceMap.keys.first { it.id == groupId }
                    groupEntity.group_name = name

                    //所有设备的remark也要修改

                    groupEntity.let {
                        //该分组下的设备did列表
                        val didList = ClockUtils.groupDeviceMap[groupEntity]

                        didList?.run {

                            ClockUtils.deviceList
                                    .filter { didList.contains(it.did) }
                                    .forEach {
                                        it.setCustomInfo(name, null)
                                    }
                        }

                    }

                    //提示其他页面更新
                    RxBus.getInstance().post(REFRESH_GROUP, groupEntity.id)
                    closeLoadingDialog()
                } else {
                    setFailLoadingText(getString(R.string.text_modify_group_name_fail))
                    failLoading()
                }

            }, getErrorConsumer())

        }
        inputMessageDialog.show(supportFragmentManager, "inputMessageDialog")
    }

    /**
     * 从当前群组移除设备并添加到默认群组
     */
    private fun removeDeviceFromCurrGroup(clockEntity: ClockEntity) {

        HttpRequest.removeDeviceFromGroup(groupId, listOf(clockEntity.device.did))
                .filter {
                    val deleteSuccess = it.success.isNotEmpty()

                    if (deleteSuccess) {//删除成功
                        adapter?.removeData(clockEntity)

                        //删除本地的设备
                        for ((groupEntity, didList) in ClockUtils.groupDeviceMap) {
                            if (groupEntity.id == groupId) {
//                                didList.remove(clockEntity.device.did)
//                                groupEntity.clockEntityList.removeAll { it.device.did == clockEntity.device.did }
//                                ClockUtils.groupDeviceMap.put(groupEntity,didList)
                                ClockUtils.refreshGroupDeviceByGroup(groupEntity)
                            }
                        }


                    } else {//删除失败
                        ToastUtil.showToast(this, R.string.fail_remove_device_from_group)
                    }
                    deleteSuccess
                }
//                .flatMap { HttpRequest.moveDevice2Group(defaultGroupId, it.success) }
                .subscribe({
                    clockEntity.device?.setCustomInfo("", null)
                    Log.e("从当前群组移除设备并添加到默认群组成功", it.toString())
                }, {
                    it.printStackTrace()
                })
    }

    /**
     * 添加设备到当前的群组
     */
    private fun addDevice2CurrGroup(didList: List<String>) {

        HttpRequest.moveDevice2Group(groupId, didList)
                .filter {
                    val addSuccess = it.success.isNotEmpty()

                    if (addSuccess) {

                        if (it.success.size == didList.size) {
                            ToastUtil.showToast(this, R.string.move_device_2_group_success)
                        } else {
                            ToastUtil.showToast(this, R.string.move_device_2_group_success_some)
                        }
                    } else {
                        ToastUtil.showToast(this, R.string.move_device_2_group_fail)
                    }

                    addSuccess
                }
                .subscribe({
                    //这里是移动设备到新添加的组，所以可以直接修改设备的remark
                    GizWifiSDK.sharedInstance()
                            .deviceList
                            .filter { didList.contains(it.did) }
                            .forEach {
                                it.setCustomInfo(groupName, null)
                            }

                    Log.e("添加设备到当前群组成功", it.toString())

                    for ((groupEntity, _) in ClockUtils.groupDeviceMap) {
                        if (groupEntity.id == groupId) {
                            ClockUtils.refreshGroupDeviceByGroup(groupEntity)
//                            requestClockList()
                            RxBus.getInstance().post(REFRESH_GROUP, groupId)
                        }
                    }
                }, {
                    it.printStackTrace()
                })
    }

    /**
     * 显示添加设备的对话框
     * @param groupId 设备要添加到的组id
     */
    private fun displayAddDeviceDialog() {

        val unGroupList = GizWifiSDK.sharedInstance().deviceList.filter { it.isBind && StringUtils.isEmpty(it.remark) }
        if (unGroupList.isEmpty()) {
            ToastUtil.showToast(this, R.string.device_ungrouped_no_found)
        } else
            Observable.just(unGroupList)//可以添加的设备
                    .map { it.map { it.did } }
                    .flatMap { Observable.fromIterable(it) }
                    .flatMap { HttpRequest.getLatestDevicePoint(it) }
                    .filter { it.error_code == 0 }//正确的获取了数据
                    .map { entity ->
                        val clock = ClockEntity()
                        clock.device = GizWifiSDK.sharedInstance().deviceList.first { it.did == entity.did }
                        clock.timeZone = entity.attr.time_zone
                        clock.isClockSwitch = entity.attr.alarm_state == 1

                        //主闹钟
                        clock.mainAlarm = entity.attr.getMainAlarm(null)
                        val subList = (1..12).map {
                            entity.attr.getSubAlarm(it, null)
                        }
                        //子闹钟 只取没有删除的闹钟
                        clock.alarmList = subList.filter { it.state != 3 && it.state != 0 }
                        clock
                    }//解析成了需要的闹钟的实体
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toList()//该组下闹钟实体的集合
                    .toObservable()
                    .subscribe(
                            {
                                //显示对话框，并且执行添加操作
                                val deviceDialog = AlarmDeviceDialog()
                                deviceDialog.clockEntityList = it
                                deviceDialog.multiChooseConsumer = Consumer {
                                    addDevice2CurrGroup(it.map { it.device.did })
                                }

                                deviceDialog.show(supportFragmentManager, "deviceDialog")
                            },
                            {
                                it.printStackTrace()
                            }
                    )


    }

}