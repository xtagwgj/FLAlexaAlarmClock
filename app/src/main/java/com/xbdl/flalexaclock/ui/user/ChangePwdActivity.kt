package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.GizErrorToast
import com.xbdl.flalexaclock.base.SP_TOKEN
import com.xbdl.flalexaclock.entity.giz.GizBaseResponse
import com.xbdl.flalexaclock.extensions.Preference
import com.xtagwgj.baseproject.base.RxBus
import kotlinx.android.synthetic.main.activity_change_pwd.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast

/**
 * 重置密码
 * Created by xtagwgj on 2017/9/12.
 */
class ChangePwdActivity() : GizBaseActivity() {

    private var token: String by Preference(this, SP_TOKEN, "")

    companion object {
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, ChangePwdActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_change_pwd
    }

    override fun initView(p0: Bundle?) {

        RxBus.getInstance()
                .register<GizBaseResponse>("GizBaseResponse")
                .compose(bindToLifecycle())
                .subscribe({
                    if (it.result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {
                        successLoading()
                        doAsync {
                            SystemClock.sleep(1000)
                            finish()
                        }
                    } else if (it.result == GizWifiErrorCode.GIZ_OTHER_ERROR) {
                        setFailLoadingText(getString(R.string.changed_password_error))
                        failLoading()
                    } else {
                        setFailLoadingText(GizErrorToast.parseExceptionCode(it.result))
                        failLoading()
                    }
                }, {

                }, {
                    RxBus.getInstance().unRegister("GizBaseResponse")
                })

    }

    override fun initEventListener() {

        bt_changePwd.setOnClickListener({

            val error = getErrorInfo()
            if (error == null) {

                showLoadingDialog(R.string.text_modify_password_in, R.string.text_modify_password_success)

                GizWifiSDK.sharedInstance().changeUserPassword(token,
                        et_currPwd.text.toString().trim(),
                        et_newPwd.text.toString().trim())
            } else {
                toast(error)
            }

        })

    }


    private fun getErrorInfo(): String? {

        if (et_currPwd.text.toString().trim().isEmpty()) {
            return getString(R.string.text_error_curr_pwd_empty)
        }

        if (et_newPwd.text.toString().trim().isEmpty()) {
            return getString(R.string.text_error_new_pwd_empty)
        }

        if (et_newPwd.text.toString().trim() != et_newPwd2.text.toString().trim()) {
            return getString(R.string.text_error_new_pwd_different)
        }

        if (et_currPwd.text.toString().trim() == et_newPwd.text.toString().trim()) {
            return getString(R.string.text_error_pwd_same)
        }

        return null
    }


}