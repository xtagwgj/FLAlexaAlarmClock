package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import com.amazon.identity.auth.device.AuthError
import com.amazon.identity.auth.device.api.Listener
import com.amazon.identity.auth.device.api.authorization.*
import com.amazon.identity.auth.device.api.workflow.RequestContext
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.firebase.auth.FirebaseAuth
import com.jakewharton.rxbinding2.widget.RxTextView
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.giz.UserLoginResponse
import com.xbdl.flalexaclock.extensions.Preference
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.loginterminator.FacebookHelper
import com.xbdl.flalexaclock.loginterminator.GooglePlusHelper
import com.xbdl.flalexaclock.ui.MainActivity
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.AppUtils
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.RegexUtil
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import java.util.*


/**
 * 登录页面
 */
class LoginActivity : GizBaseActivity() {

    private val TAG = "self_LoginActivity"

    private lateinit var mAuth: FirebaseAuth

    private var uid: String by Preference(this, SP_UID, "")
    private var token: String by Preference(this, SP_TOKEN, "")
    private var account: String by Preference(this, SP_ACCOUNT, "")
    private var expire_at: Long by Preference(this, SP_EXPIRE_AT, 0L)
    private var version: Int by Preference(this, SP_VERSION, 0)
    private var photo: String by Preference(this, SP_USER_PHOTO, "")
    private var loginType: String by Preference(this, SP_LOGIN_TYPE, TYPE_NORMAL)

    private var passwordIsShow = false

    private var facebookHelper: FacebookHelper? = null

    private var requestContext: RequestContext? = null

    private val startTokenTag = "Bearer "

    //亚马逊授权
    private val authorizeListener by lazy {
        object : AuthorizeListener() {
            override fun onSuccess(authorizeResult: AuthorizeResult?) {
                LogUtils.e(TAG, "亚马逊登录授权成功：authorizeResult = $authorizeResult")
                fetchUserProfile(authorizeResult?.accessToken ?: "")
            }

            override fun onCancel(authCancellation: AuthCancellation) {
                LogUtils.e(TAG, "亚马逊登录授权取消：authCancellation = $authCancellation")
                setFailLoadingText("Cancel authorization")
                failLoading()
            }

            override fun onError(authError: AuthError) {
                LogUtils.e(TAG, "亚马逊登录授权失败：authError = $authError")
                setFailLoadingText("Authorization failed")
                failLoading()
            }
        }
    }

    companion object {
        val REQUEST_CODE_GOOGLE_PLUS: Int = 1001

        val ACCOUNT = "account"
        val PASSWORD = "password"
        val REQUEST_SIGN_UP = 2

        private var expire_at: Long by Preference(App.instance, SP_EXPIRE_AT, 0L)
        private var default_group: String by Preference(App.instance, SP_DEFAULT_GROUP, "")
        private var photo: String by Preference(App.instance, SP_USER_PHOTO, "")

        fun doAction(mContext: Activity, clearData: Boolean = false) {
            if (clearData) {
                photo = ""
                expire_at = 0L
                default_group = ""
            }
            val intent = Intent(mContext, LoginActivity::class.java)
            startByAnim(mContext, intent, finishOld = true)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_login


    override fun initView(p0: Bundle?) {

        //自动跳转
        if (expire_at - System.currentTimeMillis() / 1000 > 0 && version == AppUtils.getAppVersionCode(this)) {
            LogUtils.e(TAG, "token有效")
            //跳转到首页
            MainActivity.doAction(LoginActivity@ this)
            return
        } else if (version != AppUtils.getAppVersionCode(this)) {
            //保存最新的版本号
            version = AppUtils.getAppVersionCode(this)
        }


        if (BuildConfig.DEBUG) {
            account = TEMP_PHONE
            passwordEditText.setText(TEMP_PWD)
            emailEditText.setText(TEMP_PHONE)
        }

        //设置
        hidePwdButton.setImageDrawable(resources.getDrawable(R.mipmap.ic_eye_close))

        RxTextView.textChanges(passwordEditText)
                .compose(bindToLifecycle())
                .map { it.isNotEmpty() }
                .subscribe({
                    clearPwdButton.visibility = if (it) View.VISIBLE else View.GONE
                })

        RxTextView.textChanges(emailEditText)
                .compose(bindToLifecycle())
                .map { it.isNotEmpty() }
                .subscribe({
                    clearEmailButton.visibility = if (it) View.VISIBLE else View.GONE
                })

        try {
            initGoogle()
            initAmazon()
            initFaceBook()
            mAuth = FirebaseAuth.getInstance()
            //Firebase匿名登录
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this@LoginActivity) == ConnectionResult.SUCCESS) {
                mAuth.signInAnonymously()
                        .addOnCompleteListener(this) { task ->
                            if (task.isSuccessful) {
                                Log.d(TAG, "signInAnonymously:success")

                            } else {
                                Log.w(TAG, "signInAnonymously:failure", task.exception)
                            }
                        }
            }
        } catch (e: Exception) {
            LogUtils.e("login", e.message)
        }

        emailEditText.setText(account)
        if (StringUtils.isEmpty(account)) {
            emailEditText.isFocusable = true
            emailEditText.requestFocus()
        } else {
            passwordEditText.isFocusable = true
            passwordEditText.requestFocus()
        }
    }

    override fun initEventListener() {
        clickEvent(signInButton, {

            if (!RegexUtil.checkEmail(emailEditText.text.toString().trim())) {
                ToastUtil.showToast(this, R.string.text_hint_error_phone)
                return@clickEvent
            }

            if (passwordEditText.text.toString().trim().length < MIN_LENGTH_LOGIN_PWD) {
                ToastUtil.showToast(this, R.string.text_hint_error_password)
                return@clickEvent
            }

            doSignIn()

        })

        clickEvent(outSizeView, { hideInput() })

        //密码的显示与隐藏
        clickEvent(hidePwdButton, {

            if (passwordIsShow) {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_close)
                //不可以看到密码
                passwordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
            } else {
                hidePwdButton.setImageResource(R.mipmap.ic_eye_open)
                //不可以看到密码
                passwordEditText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            }
            passwordIsShow = !passwordIsShow

            //移动光标到最后的位置
            passwordEditText.setSelection(passwordEditText.text.toString().trim().length)
        })


        //清除密码
        clickEvent(clearPwdButton, {
            passwordEditText.setText("")
            passwordEditText.requestFocus()
        })

        //清除账号
        clickEvent(clearEmailButton, {
            emailEditText.setText("")
            emailEditText.requestFocus()
        })

        clickEvent(signUpTextView, {
            SignUpActivity.doAction(this)
        })

        clickEvent(forgotPwdTextView, {
            ForgetPasswordActivity.doAction(this)
        })

        //amazon登录
        clickEvent(btn_amazon, {
            logInAmazon()
        })

        RxBus.getInstance()
                .register<UserLoginResponse>("UserLoginResponse")
                .compose(bindToLifecycle())
                .subscribe({

                    LogUtils.e("LoginActivity", "RxBus收到回调: $it")

                    displayUserLogin(it.result, it.uid, it.token)
                }, {

                }, {
                    RxBus.getInstance().unRegister("UserLoginResponse")
                })
    }

    override fun onResume() {
        super.onResume()
        requestContext?.onResume()
    }

    override fun onStart() {
        super.onStart()
        GooglePlusHelper.initGooglePlusOnstart()

        try {
            if (requestContext != null) {

                AuthorizationManager.getToken(
                        this,
                        arrayOf(ProfileScope.profile(), ProfileScope.postalCode()),
                        object : Listener<AuthorizeResult, AuthError> {
                            override fun onSuccess(result: AuthorizeResult) {
                                if (result.accessToken != null) {
                                    fetchUserProfile(result.accessToken)
                                } else {
                                }
                            }

                            override fun onError(authError: AuthError) {
                                toast(authError.message ?: authError.localizedMessage)
                                logOutAmazon()
                            }
                        }
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onStop() {
        super.onStop()
        GooglePlusHelper.initGooglePlusOnstop()
    }

    /**
     * 机智云登录操作
     */
    private fun doSignIn() {
        displayLoginDialog()

        GizWifiSDK.sharedInstance().userLogin(
                emailEditText.text.toString().trim(),
                passwordEditText.text.toString().trim()
        )
    }


    /**
     * 第三方登录
     */
    private fun doSignInByThirdParty(account: String, uid: String, token: String, src: String, photoUrl: String, gender: String?) {
        LogUtils.e(TAG, "请求第三方登录，account=$account ,uid =$uid ,token=$token ,src=$src ,photoUrl=$photoUrl")
        loginType = src

        HttpRequest.createThirdUser(src, uid, token)
                .subscribe(Consumer {
                    LogUtils.e(TAG, "机智云登录成功，返回信息:$it")

                    HttpRequest.modifyUserInfo(it.token, gender ?: "", "", src, { result, errorBody ->
                        if (result) {
                            //保存是用什么账号登录的
                            signInSuccess(account, it.uid, it.token, it.expire_at, photoUrl)
                        } else {
                            setFailLoadingText(errorBody)
                            failLoading()
                        }
                    })


                }, getErrorConsumer(false))
    }

    /**
     * 用户登录的回调
     */
    private fun displayUserLogin(result: GizWifiErrorCode, uid: String, token: String) {
        loginType = TYPE_NORMAL

        when (result) {
            GizWifiErrorCode.GIZ_SDK_SUCCESS -> {
                signInSuccess(
                        emailEditText.text.toString().trim(),
                        uid,
                        token,
                        Calendar.getInstance().run {
                            add(Calendar.DAY_OF_MONTH, 7)
                            timeInMillis / 1000
                        },
                        ""
                )
            }

            else -> {
                //登录失败的操作
                setFailLoadingText(GizErrorToast.parseExceptionCode(result))
                failLoading()
            }
        }
    }

    /**
     * 登录成功
     */
    private fun signInSuccess(account: String, uid: String, token: String, expire_at: Long, photoUrl: String) {

        //保存uid和token
        this.uid = uid
        this.token = token
        this.account = account
        this.photo = photoUrl

        //登录一周过期
        this.expire_at = expire_at
        LogUtils.e("登录成功", "登录时间：${System.currentTimeMillis() / 1000} 过期时间：$expire_at")

//        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this@LoginActivity) == ConnectionResult.SUCCESS)
//            mAuth.signInAnonymously()
//                    .addOnCompleteListener(this) { task ->
//                        if (task.isSuccessful) {
//                            closeLoadingDialog()
//                            Log.d(TAG, "signInAnonymously:success")
//
//                        } else {
//                            Log.w(TAG, "signInAnonymously:failure", task.exception)
////                            setFailLoadingText("Authentication failed.")
////                            failLoading()
//
//                            toast("Authentication failed.")
//                        }
//
//                        //跳转到首页
//                        MainActivity.doAction(LoginActivity@ this)
//                    }
//        else {
//            closeLoadingDialog()
//            //跳转到首页
//            MainActivity.doAction(LoginActivity@ this)
//        }

        closeLoadingDialog()
        //跳转到首页
        MainActivity.doAction(LoginActivity@ this)
    }

    /////////////////////////////////////  Google  ////////////////////////////////////////////////

    //https//developers.google.com/identity/sign-in/android/sign-in
    private fun initGoogle() {
        LogUtils.e(TAG, "initGoogle")
        try {
            when (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this)) {
                ConnectionResult.SUCCESS -> {
                    GooglePlusHelper.initGooglePlus(this@LoginActivity, btn_googleReal, btn_google)
                }

                else -> {
                    LogUtils.e(TAG, GooglePlayServicesUtil.isGooglePlayServicesAvailable(this).toString())
                    //fixme:不弹出不可用提示
//                    toast(R.string.google_service_disable)
                }
            }
        } catch (e: Exception) {
            failLoading()
        }
    }


    ////////////////////////////////////  FaceBook  ////////////////////////////////////////////////
    private fun initFaceBook() {
        LogUtils.e(TAG, "initFaceBook")

        if (facebookHelper == null)
            facebookHelper = FacebookHelper()

        facebookHelper?.initFBLoginButton(btn_facebook, btn_faceBookReal, null, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                if (result == null) return

                LogUtils.e("Login", "FaceBook 登录成功 token=${result.accessToken}")
                getFacebookUserBasicInfo()
            }

            override fun onCancel() {
                LogUtils.e("Login", "FaceBook 取消登录")
            }

            override fun onError(error: FacebookException) {
                LogUtils.e("Login", "FaceBook 登录失败 error=$error")
            }

        }, Runnable { })
    }

    /**
     * 获取facebook用户的基本信息
     */
    private fun getFacebookUserBasicInfo() {
        displayLoginDialog()

        FacebookHelper.getUserFacebookBasicInfo(object : FacebookHelper.FacebookUserInfoCallback {
            override fun onCompleted(userInfo: FacebookHelper.FaceBookUserInfo?) {
                if (userInfo == null) {
                    setFailLoadingText(getString(R.string.text_fail_get_facebook_user))
                    failLoading()
                    return
                }

                getFacebookUserImage(userInfo)
            }

            override fun onFailed(reason: String?) {
                LogUtils.e("Login", "获取facebook用户信息失败::" + reason)
                FacebookHelper.signOut()

                setFailLoadingText(getString(R.string.text_fail_get_facebook_user))
                failLoading()
            }
        })
    }

    /**
     * 获取facebook用户的头像
     */
    private fun getFacebookUserImage(facebookUser: FacebookHelper.FaceBookUserInfo) {
        FacebookHelper.getFacebookUserPictureAsync(facebookUser.id, object : FacebookHelper.FacebookUserImageCallback {
            override fun onCompleted(imageUrl: String) {
                LogUtils.e("Login", "用户是" + facebookUser)
                //成功获取到了头像之后
                LogUtils.e("Login", "用户高清头像的下载url是" + imageUrl)
                LogUtils.e("Login", "用户高清头像的下载url是" + imageUrl)

                val gender: String = when {
                    StringUtils.isEmpty(facebookUser.gender) -> ""
                    StringUtils.equalsIgnoreCase(facebookUser.gender, "male") -> "M"
                    else -> "F"
                }

                doSignInByThirdParty(facebookUser.email, facebookUser.id, facebookUser.token, TYPE_FACEBOOK, imageUrl, gender)
            }

            override fun onFailed(reason: String) {
                FacebookHelper.signOut()//如果获取失败了，别忘了将整个登录结果回滚
                LogUtils.e("Login", reason)
                setFailLoadingText(reason)
                failLoading()
            }
        })
    }

    //////////////////////////////////////  amazon  ////////////////////////////////////////////////
    //https://developer.amazon.com/public/apis/engage/login-with-amazon/content/android_docs

    /**
     * 初始化并且登录
     */
    private fun initAmazon() {
        LogUtils.e(TAG, "initAmazon")
        requestContext = RequestContext.create(this)
        requestContext?.registerListener(authorizeListener)
    }

    private fun logInAmazon() {
        displayLoginDialog()

        //登录
        try {
            AuthorizationManager.authorize(
                    AuthorizeRequest
                            .Builder(requestContext)
                            .addScopes(ProfileScope.profile(), ProfileScope.postalCode())
                            .build()
            )
        } catch (e: Exception) {
            closeLoadingDialog()
            toast(e.message ?: "登录错误")
        }
    }

    private fun displayLoginDialog() {
        showLoadingDialog(R.string.text_loading_login, R.string.text_loading_login_success)
    }

    /**
     * 退出登录
     */
    private fun logOutAmazon() {
        AuthorizationManager.signOut(this, object : Listener<Void, AuthError> {
            override fun onSuccess(response: Void) {

            }

            override fun onError(authError: AuthError) {


            }
        })
    }

    private fun fetchUserProfile(accessToken: String) {
        User.fetch(this, object : Listener<User, AuthError> {
            override fun onSuccess(user: User) {
                LogUtils.e(TAG, "亚马逊获取的用户信息：user = $user")
                doSignInByThirdParty(user.userEmail, user.userId, startTokenTag + accessToken, TYPE_AMAZON, "", "")
            }

            override fun onError(authError: AuthError) {
                LogUtils.e(TAG, "亚马逊获取的用户信息失败：authError = $authError")
                logOutAmazon()
                setFailLoadingText(authError.message ?: "")
                failLoading()
            }
        })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_SIGN_UP) {
            if (resultCode == Activity.RESULT_OK)
                data?.let {
                    passwordEditText.setText(data.getStringExtra(PASSWORD))
                    emailEditText.setText(data.getStringExtra(ACCOUNT))

                    doSignIn()
                }
        } else if (requestCode == REQUEST_CODE_GOOGLE_PLUS) {

            displayLoginDialog()
//            GooglePlusHelper.handleSignInResult(data)

            doAsync {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                if (result.isSuccess) {

                    val account = result.signInAccount

                    LogUtils.e(TAG, "Google login Success: 用户名是 = ${account?.displayName}")
                    LogUtils.e(TAG, "Google login Success: 用户email是 = ${account?.email}")
                    LogUtils.e(TAG, "Google login Success: 用户头像是 = ${account?.photoUrl}")
                    LogUtils.e(TAG, "Google login Success: 用户Id是 = ${account?.id}")
                    LogUtils.e(TAG, "Google login Success: 用户Token是 = ${account?.idToken}")
                    LogUtils.e(TAG, "Google login Success: 用户authCode是 = ${account?.serverAuthCode}")

                    try {
                        val response = GoogleAuthorizationCodeTokenRequest(
                                NetHttpTransport(),
                                JacksonFactory(),
                                getString(R.string.google_server_client_id),
                                getString(R.string.google_server_client_secret),
                                account?.serverAuthCode,
                                "http://localhost")
                                .execute()

                        LogUtils.e(TAG, "Google login accessToken ${response.accessToken}")

                        doSignInByThirdParty(account?.email ?: "", account?.id ?: "",
                                startTokenTag + (response.accessToken ?: ""), TYPE_GOOGLE, account?.photoUrl.toString(), "")
                    } catch (e: Exception) {
                        e.printStackTrace()

                        runOnUiThread {
                            setFailLoadingText(e.message)
                            failLoading()
                        }
                    }

                } else {
                    runOnUiThread {
                        setFailLoadingText(result.status.statusMessage ?: "")
                        failLoading()
                    }

                    LogUtils.e(TAG, "Google login Fail: status = ${result.status}")
                }
            }


        }

        if (facebookHelper?.facebookCallbackManager != null)
            facebookHelper?.facebookCallbackManager?.onActivityResult(requestCode, resultCode, data)

    }

}