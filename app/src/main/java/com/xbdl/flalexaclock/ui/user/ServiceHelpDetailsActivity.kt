package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.GizBaseActivity
import kotlinx.android.synthetic.main.activity_service_help_detials.*

/**
 * 服务与帮助详情
 * Created by xtagwgj on 2017/9/12.
 */
class ServiceHelpDetailsActivity() : GizBaseActivity() {

    companion object {
        fun doAction(mContext: Activity, position: Int = 1) {
            val intent = Intent(mContext, ServiceHelpDetailsActivity::class.java)
            intent.putExtra("position", position)
            startByAnim(mContext, intent)
        }
    }

    override fun initEventListener() {

    }

    override fun initView(p0: Bundle?) {


        val noteBuilder = StringBuffer("")

        when (intent.getIntExtra("position", 1)) {
            1 -> {
                val noteArray = resources.getStringArray(R.array.note)
                noteArray.forEachIndexed { index, item ->
                    noteBuilder.append(String.format("%-3d.", index + 1))
                            .append(item)
                            .append("\n\n")
                }
            }

            2 -> {
                tv_title.text = getString(R.string.text_note_2)
                noteBuilder.append(getString(R.string.text_note_2_content))
            }
        }



        tv_content.text = noteBuilder.toString()

    }

    override fun getLayoutId(): Int = R.layout.activity_service_help_detials
}