package com.xbdl.flalexaclock.ui.dialog

import android.text.InputFilter
import android.text.InputType
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseRxDialog
import com.xbdl.flalexaclock.extensions.inputCheck
import com.xbdl.flalexaclock.utils.ToastUtil
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.dialog_input.*
import org.jetbrains.anko.longToast
import java.util.regex.Pattern

/**
 * 输入设备名称的对话框
 * Created by xtagwgj on 2017/9/11.
 */

class InputlDeviceNameDialog : BaseRxDialog() {

    var cancelConsumer: Consumer<Any> = Consumer {
        dismiss()
    }


    var okConsumer: Consumer<String> = Consumer {
        dismiss()
    }


    var defaultText: String = ""

    /**
     * 表情过滤器
     */
    val emojiFilter = InputFilter { source, start, end, dest, dstart, dend ->
        val emoji = Pattern.compile(
                "[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",
                Pattern.UNICODE_CASE or Pattern.CASE_INSENSITIVE)
        val emojiMatcher = emoji.matcher(source)
        return@InputFilter if (emojiMatcher.find()) {
            ""
        } else null
    }

    /**
     * 特殊字符过滤器
     */
    val specialCharFilter = InputFilter { source, start, end, dest, dstart, dend ->
        val regexStr = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]"
        val pattern = Pattern.compile(regexStr)
        val matcher = pattern.matcher(source.toString())
        return@InputFilter if (matcher.matches()) {
            ""
        } else {
            null
        }
    }


    override fun getLayoutId(): Int = R.layout.dialog_input

    override fun initView() {

        tv_dialogTitle.text = getString(R.string.text_dialog_input_device_name)
        et_input.hint = getString(R.string.text_dialog_input_device_name)
        et_input.setText(defaultText)

        et_input.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        et_input?.filters = arrayOf(emojiFilter, specialCharFilter)

        clickEvent(bt_cancel, cancelConsumer)
        clickEvent(bt_ok, {
            if (et_input.text.toString().trim().isEmpty()) {
                ToastUtil.showToast(activity, R.string.text_input_empty)
                return@clickEvent
            }

            if (!et_input.inputCheck(true)) {
                activity.longToast(R.string.text_name_error)
                return@clickEvent
            }

            okConsumer.accept(et_input.text.toString().trim())
            dismiss()
        })
    }

}