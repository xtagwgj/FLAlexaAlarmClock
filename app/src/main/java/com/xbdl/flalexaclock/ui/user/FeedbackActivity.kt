package com.xbdl.flalexaclock.ui.user

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.FEEDBACK_EMAIL_DEBUG
import com.xbdl.flalexaclock.base.FEEDBACK_EMAIL_RELEASE
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.SP_ACCOUNT
import com.xbdl.flalexaclock.email.SendMailUtil
import com.xbdl.flalexaclock.extensions.Preference
import com.xtagwgj.baseproject.utils.StringUtils
import kotlinx.android.synthetic.main.activity_feedback.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.find
import org.jetbrains.anko.toast

/**
 * Feedback
 * Created by xtagwgj on 2017/9/12.
 */
class FeedbackActivity() : GizBaseActivity() {

    private var account: String by Preference(this, SP_ACCOUNT, "")

    companion object {
        fun doAction(mContext: Activity) {
            val intent = Intent(mContext, FeedbackActivity::class.java)
            startByAnim(mContext, intent)
        }
    }

    override fun initEventListener() {

        find<View>(R.id.tv_submit).setOnClickListener {

            if (StringUtils.isEmpty(et_info.text.toString().trim())) {
                toast(R.string.text_feedback_empty)
                return@setOnClickListener
            }

            showLoadingDialog(R.string.text_load_sendemail, R.string.text_email_success)

            SendMailUtil.send(
                    getString(R.string.text_email_subject),
                    getString(R.string.text_email_content, account, et_info.text.toString().trim()),
                    if (BuildConfig.DEBUG) FEEDBACK_EMAIL_DEBUG else FEEDBACK_EMAIL_RELEASE
            ) {
                if (it) {
                    successLoading()
                    doAsync {
                        SystemClock.sleep(1500)
                        runOnUiThread {
                            et_info.setText("")
                            finish()
                        }
                    }
                } else {
                    setFailLoadingText(getString(R.string.text_email_fail))
                    failLoading()
                }
            }


        }


    }

    override fun initView(p0: Bundle?) {

    }

    override fun getLayoutId(): Int = R.layout.activity_feedback


}