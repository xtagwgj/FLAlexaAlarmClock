package com.xbdl.flalexaclock.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.xbdl.flalexaclock.R;
import com.xbdl.flalexaclock.base.GizBaseActivity;
import com.xbdl.flalexaclock.entity.User;
import com.xbdl.flalexaclock.loginterminator.GooglePlusHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * 测试页面
 * Created by xtagwgj on 2017/10/16.
 */

public class TestActivity extends GizBaseActivity {

    private DatabaseReference userReferences;
    private ValueEventListener userValueEventListener;


    private SignInButton btn_googleReal;
    private TextView tv_info;
    private final int GOOGLE_PLUS = 1001;

    @Override
    public int getLayoutId() {
        return R.layout.activity_test;
    }

    @Override
    public void initView(Bundle bundle) {
        btn_googleReal = (SignInButton) findViewById(R.id.btn_googleReal);
        tv_info = (TextView) findViewById(R.id.tv_info);
        userReferences = FirebaseDatabase.getInstance().getReference("AlarmClock").child("users");

        initGoogle();
    }

    @Override
    public void initEventListener() {
        findViewById(R.id.bt_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = new User("234", "http");
                String key = userReferences.push().getKey();

                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("" + 123, user.toMap());

                userReferences.updateChildren(childUpdates);

            }
        });

        findViewById(R.id.bt_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userReferences.child("456").child("headUrl").setValue("https://");
                userReferences.child("456").child("uid").setValue("123");
            }
        });

        findViewById(R.id.bt_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userReferences.child("456").child("headUrl").removeValue();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        userValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TextView textView = (TextView) findViewById(R.id.tv_info);
                textView.setText("UserInfo: " + dataSnapshot.getValue(User.class));


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        userReferences.child("456").addValueEventListener(userValueEventListener);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (userValueEventListener != null) {
            userReferences.child("456").removeEventListener(userValueEventListener);
        }
    }


    private void initGoogle() {
        tv_info.append("\ninitGoogle()");
        int googlePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
            tv_info.append("\ninitGoogle success");
            GooglePlusHelper.initGooglePlus(this, btn_googleReal, null);
        } else {

            tv_info.append(" isGooglePlayServiceAvailable: " + googlePlayServicesAvailable);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_PLUS) {
            GooglePlusHelper.handleSignInResult(data);

            GoogleSignInResult signInResultFromIntent = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            tv_info.append("\n");
            tv_info.append("Google result= " + signInResultFromIntent);

            if (signInResultFromIntent.isSuccess()) {
                GoogleSignInAccount account = signInResultFromIntent.getSignInAccount();
                tv_info.append("\n");
                tv_info.append("Google login Success: account=" + account.toString());

                tv_info.append("用户名是:" + account.getDisplayName() + ",用户email是:" + account.getEmail() + ",用户头像是:" + account.getPhotoUrl() +
                        ",用户Id是:" + account.getId() + ",用户Token是" + account.getIdToken() + ",ServerAuthCode是" + account.getServerAuthCode());

            } else {
                tv_info.append("\n");
                tv_info.append("Google login Fail: status=${result.status}");
            }

        }
    }
}
