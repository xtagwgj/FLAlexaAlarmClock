package com.xbdl.flalexaclock.ui.adapter

import android.view.View
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemTimezoneBinding
import com.xbdl.flalexaclock.entity.CityCodeEntity
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils

/**
 * 时区选择的适配器
 * Created by xtagwgj on 2017/9/18.
 */
class TimeZoneAdapter(private val defaultTimeZone: Int = 0, private val onItemClick: (CityCodeEntity) -> Unit)
    : DBAdapter<CityCodeEntity, ItemTimezoneBinding>(TimeZoneTableUtils.codeTable.sortedBy { it.cityName }, R.layout.item_timezone) {

    fun getCheckPosition(): Int {
        return mDatas.indexOfFirst { it.timeZoneWithCity == defaultTimeZone }.run {
            if (this == -1) 0 else this
        }
    }

    override fun onBindViewHolder(holder: DBViewHolder<ItemTimezoneBinding>, position: Int, item: CityCodeEntity) {
        holder.binding.apply {
            top = item.cityName
            bottom = item.getShow()
            selected = item.timeZoneWithCity == defaultTimeZone
            clickListener = View.OnClickListener {
                onItemClick(item)
            }
        }
    }
}