package com.xbdl.flalexaclock.ui.adapter

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.View
import android.widget.RadioButton
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.databinding.ItemCustomWeekBinding
import org.jetbrains.anko.find

/**
 *
 * Created by xtagwgj on 2017/9/13.
 */
class WeekAdapter(data: List<String>) : DBAdapter<String, ItemCustomWeekBinding>(data, R.layout.item_custom_week) {

    //周一到周日
    var retryInfoMap = mutableMapOf(
            "0" to false,
            "1" to false,
            "2" to false,
            "3" to false,
            "4" to false,
            "5" to false,
            "6" to false
    )

    override fun onBindViewHolder(holder: DBViewHolder<ItemCustomWeekBinding>, position: Int, item: String) {

        val index = item.indexOf("(")
        if (index == -1) {
            holder.binding.day = SpannableString(item)
        } else {
            val spannableString = SpannableString(item)
            val colorSpan = ForegroundColorSpan(mContext.resources.getColor(R.color.colorAlarmCloseTime))
            spannableString.setSpan(colorSpan, index, item.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)

            val sizeSpan = RelativeSizeSpan(0.8F)
            spannableString.setSpan(sizeSpan, index, item.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)

            holder.binding.day = spannableString
        }
        val radioButton = holder.binding.root.find<RadioButton>(R.id.rb_day).apply {
            isChecked = retryInfoMap["$position"] ?: false
        }

        radioButton.setOnCheckedChangeListener { _, isChecked ->
            retryInfoMap.put("$position", isChecked)
        }

        holder.binding.root.find<View>(R.id.rl_day).setOnClickListener({
            radioButton.isChecked = !radioButton.isChecked
        })
    }

    fun getRetryInfo(): String {
        val retryBuilder = StringBuffer()

        //0是周一 6是周日
        for ((_, value) in retryInfoMap) {
            retryBuilder.append(if (value) "1" else "0")
        }

        return retryBuilder.reverse().toString()
    }

}