package com.xbdl.flalexaclock.ui

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.gizwits.gizwifisdk.api.GizWifiSDK
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.BaseFragment
import com.xbdl.flalexaclock.base.FRAGMENT_CHANGED
import com.xbdl.flalexaclock.base.GizBaseActivity
import com.xbdl.flalexaclock.base.REFRESH_GROUP_LIST
import com.xbdl.flalexaclock.entity.GroupEntity
import com.xbdl.flalexaclock.extensions.textColor
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.adapter.GroupListAdapter
import com.xbdl.flalexaclock.ui.dialog.AlarmDeviceDialog
import com.xbdl.flalexaclock.ui.dialog.InputlMessageDialog
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xbdl.flalexaclock.widget.SimpleDividerDecoration
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.StringUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_group.*

/**
 * 分组页面
 */
class GroupFragment : BaseFragment() {


    private val TAG = "self_GroupFragment"

    private val adapter: GroupListAdapter by lazy {
        GroupListAdapter(
                { item ->
                    //删除提示
                    showLoadingDialog(R.string.load_del_group, R.string.load_del_group_success)

                    //获取该组下的所有设备
                    HttpRequest.getGroupDevice(item.id)
                            .map { it.map { it.did } }
                            .subscribe({ didList ->
                                //在服务区上删除组信息
                                HttpRequest.deleteGroup(item.id, {
                                    if (it) {
                                        //删除成功后 修改remark名字
                                        if (didList.isNotEmpty()) {
                                            GizWifiSDK.sharedInstance().deviceList.filter { didList.contains(it.did) }.forEach {
                                                it.setCustomInfo("", null)
                                            }
                                        }

                                        adapter.removeData(item)
                                        if (adapter.itemCount == 0)
                                            tv_titleLeft.performClick()

                                        //删除
//                                        ClockUtils.groupDeviceMap.remove(item)
                                        ClockUtils.deleteGroupEntity(item.id)

                                        //刷新列表
                                        // ClockUtils.refreshAllGroup()
                                    } else
                                        ToastUtil.showToast(activity, R.string.text_del_group_error)

                                    closeLoadingDialog()
                                },
                                        (activity as GizBaseActivity).getErrorConsumer())

                            }, {
                                closeLoadingDialog()
                                it.printStackTrace()
                            })
                }
        )
    }

    companion object {
        fun newInstance(): GroupFragment {
            val fragment = GroupFragment()
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_group

    override fun initView(var1: Bundle?) {

        //为空时，显示添加群组的视图
        recycler.setEmptyView(emptyView.apply {
            initView(
                    R.mipmap.icon_no_group,
                    R.string.text_empty_group_list,
                    R.string.text_group_add, {
                toAddGroup()
            })
        })

        if (recycler.adapter == null) {
            recycler.layoutManager = LinearLayoutManager(activity)
            recycler.addItemDecoration(SimpleDividerDecoration(
                    activity, dividerColorInt = resources.getColor(R.color.colorSplit), leftPadding = 32))
            recycler.adapter = adapter
        }

    }

    override fun initEventListener() {

        //标题栏右边箭头的点击事件
        titleLayout.setMoreClickListener { toAddGroup() }

        //左上角文字的点击事件
        clickEvent(tv_titleLeft, Consumer {
            when (tv_titleLeft.text.toString().toUpperCase()) {
                "EDIT" -> {

                    tv_titleLeft.apply {
                        text = "Done"
                        textColor = resources.getColor(R.color.colorAccent)
                    }

                    adapter.isEdit = true
                    titleLayout?.setMoreImageViewVisiable(false)
                }

                "DONE" -> {
                    tv_titleLeft.apply {
                        text = "Edit"
                        textColor = resources.getColor(R.color.colorEdit)
                    }

                    adapter.isEdit = false
                    titleLayout?.setMoreImageViewVisiable(true)
                }
            }
        })

        if (isFirstLoad) {
            if (userVisibleHint)
                showLoadingDialog(R.string.text_load_group)

            isFirstLoad = false
        }

        //分组刷新了
        RxBus.getInstance().register<String>(REFRESH_GROUP_LIST)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onFirstLoad()
                }, {
                    it.printStackTrace()
                })

        //页面切换时重置编辑状态
        RxBus.getInstance().register<Int>(FRAGMENT_CHANGED)
                .filter { it == 2 && tv_titleLeft.text.toString().toUpperCase() == "DONE" }
                .subscribe({
                    tv_titleLeft.performClick()
                }, {
                    it.printStackTrace()
                })


    }

    override fun onFirstLoad() {
        requestGroup()
    }


    /**
     * 输入分组名称的对话框
     */
    private val inputMessageDialog by lazy {
        InputlMessageDialog().apply {
            okConsumer = Consumer { groupName ->

                showLoadingDialog(R.string.load_add_group, R.string.load_add_group_success)

                //判断groupName是否已经存在
                HttpRequest.findAllGroup()
                        .map {
                            it.map { it.group_name }
                        }
                        .subscribe({

                            if (it.contains(groupName)) {//groupName已经存在 提示用户
                                closeLoadingDialog()
                                ToastUtil.showToast(activity, R.string.text_group_exist)
                            } else {//groupName不存在 提交请求
                                dismiss()
                                HttpRequest.createGroup("$groupName",
                                        Consumer { itemId ->
                                            //group创建成功了
                                            val newGroup = GroupEntity()
                                            newGroup.id = itemId
                                            newGroup.group_name = groupName

                                            //先添加一个
                                            adapter.addData(newGroup)

                                            closeLoadingDialog()
                                            displayAddDeviceDialog(groupName, itemId)
                                        },
                                        (activity as GizBaseActivity).getErrorConsumer())
                            }

                        }, {
                            closeLoadingDialog()
                            it.printStackTrace()
                        })
            }
        }
    }

    /**
     * 添加分组
     */
    private fun toAddGroup() {
        inputMessageDialog.show(activity.supportFragmentManager, "inputMessageDialog")
    }

    /**
     * 请求分组信息
     */
    private fun requestGroup() {
        val groupList = ClockUtils.groupDeviceMap.keys.toMutableList()
        adapter.refreshDataByTime(groupList)
        tv_titleLeft?.visibility = if (groupList.isNotEmpty()) View.VISIBLE else View.GONE
        closeLoadingDialog()

    }

    override fun onResume() {
        super.onResume()
        requestGroup()
    }


    /**
     * 显示添加设备的对话框
     * @param groupId 设备要添加到的组id
     */
    private fun displayAddDeviceDialog(groupName: String, groupId: String) {

        //查找未分组的设备
        val unGroupList = GizWifiSDK.sharedInstance().deviceList.filter { it.isBind && StringUtils.isEmpty(it.remark) }.distinct()

        if (unGroupList.isEmpty()) {
            //没有可以添加的设备
            ToastUtil.showToast(activity, R.string.device_ungrouped_no_found)
            ClockUtils.refreshAllGroup()
        } else {

            Observable.just(unGroupList)
                    //遍历每一个设备
                    .flatMap { Observable.fromIterable(it) }
                    .flatMap { device ->

                        //本地保存的 clockEntity 数据
                        val clockEntity = ClockUtils.clockAlarmMap[device.did]
                        if (clockEntity == null) {
                            //本地不存在，则去网络获取
                            ClockUtils.findOfflineAlarmByDevice(device)
                        } else {
                            //本地存在则使用本地的
                            Observable.just(clockEntity)
                        }
                    }
                    .subscribeOn(Schedulers.io())
                    .toList()//该组下闹钟实体的集合
                    .toObservable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                //显示对话框，并且执行添加操作
                                AlarmDeviceDialog().apply {
                                    clockEntityList = it
                                    isCancelable = false
                                    multiChooseConsumer = Consumer {
                                        addDevice2CurrGroup(it.map { it.device.did }, groupId, groupName)
                                    }
                                    cancelConsumer = Consumer {
                                        //请求所有的分组
                                        ClockUtils.refreshAllGroup()
                                    }
                                }.show(activity.supportFragmentManager, "deviceDialog")
                            },
                            {
                                it.printStackTrace()
                                //请求所有的分组
                                ClockUtils.refreshAllGroup()
                            }
                    )
        }
    }

    /**
     * 添加设备到当前的群组
     */
    private fun addDevice2CurrGroup(didList: List<String>, groupId: String, groupName: String) {

        HttpRequest.moveDevice2Group(groupId, didList)//将默认组中的设备移到创建的组
                .filter {
                    val addSuccess = it.success.isNotEmpty()

                    if (addSuccess) {

                        if (it.success.size == didList.size) {
                            ToastUtil.showToast(activity, R.string.move_device_2_group_success)
                        } else {
                            ToastUtil.showToast(activity, R.string.move_device_2_group_success_some)
                        }
                    } else {
                        ToastUtil.showToast(activity, R.string.move_device_2_group_fail)
                    }

                    addSuccess
                }
                .subscribe({ successInfo ->
                    //这里是移动设备到新添加的组，所以可以直接修改设备的remark
                    GizWifiSDK.sharedInstance()
                            .deviceList
                            .filter { successInfo.success.contains(it.did) }
                            .toList()
                            .forEach {
                                it.setCustomInfo(groupName, null)
                            }
                    Log.e(TAG, successInfo.toString())

                }, {
                    it.printStackTrace()
                    //请求所有的分组
                    ClockUtils.refreshAllGroup()
                }, {
                    //请求所有的分组
                    ClockUtils.refreshAllGroup()
                })
    }
}