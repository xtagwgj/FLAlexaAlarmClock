package com.xbdl.flalexaclock.extensions

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import java.util.regex.Pattern

/**
 * Created by xtagwgj on 2017/6/27.
 */
val View.mContext: Context
    get() = context

val AppCompatActivity.mContext: Context
    get() = applicationContext

var TextView.textColor: Int
    get() = currentTextColor
    set(value) = setTextColor(value)

/**
 * 检查输入是否合法
 */
fun EditText.inputCheck(withSpaceBlank: Boolean = true): Boolean {
    val inputInfo = text.toString().trim()

    val regex = if (withSpaceBlank) {
        //有空格的
        "^[_a-zA-Z0-9\u4E00-\u9FA5\\s+]+$"
    } else {
        //无空格的
        "^[_a-zA-Z0-9\u4E00-\u9FA5]+$"
    }


    val pattern = Pattern.compile(regex)
    val match = pattern.matcher(inputInfo)

    //中文，英文，数字，下划线
    return match.matches()
}