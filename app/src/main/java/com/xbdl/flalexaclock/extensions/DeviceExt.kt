package com.xbdl.flalexaclock.extensions

import android.content.Context
import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.R
import com.xbdl.flalexaclock.base.App
import com.xbdl.flalexaclock.base.BIND_TIME_FORMAT
import com.xbdl.flalexaclock.base.PRODUCT_1
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.GizDataPointKey
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.utils.LogUtils
import com.xtagwgj.baseproject.utils.StringUtils
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * 显示toast消息
 * Created by xtagwgj on 2017/6/19.
 */
fun android.support.v7.app.AppCompatActivity.showToast(toastMsg: String? = null, toastRes: Int = -1) {

    ToastUtil.showToast(App.Companion.instance,
            if (toastRes == -1)
                com.xtagwgj.baseproject.utils.StringUtils.null2Length0(toastMsg)
            else
                App.Companion.instance.getString(toastRes)
    )
}

/**
 * 获取设备名称
 */
fun Context.getDeviceName(device: GizWifiDevice?): String {

    device ?: return ""

    return if (StringUtils.isEmpty(device.alias))
        PRODUCT_1
    else {
        if (device.alias.contains("_")) {
            val index = device.alias.lastIndexOf("_")
            if (index == 0) {
                PRODUCT_1
            } else {
                device.alias.substring(0, index)
            }

        } else {
            device.alias
        }
    }
}

/**
 * 修改设备名称
 */
fun Context.modifyDeviceName(device: GizWifiDevice, name: String) {
    //获取绑定的时间，没有的话使用当前时间
    val realName = device.alias ?: SimpleDateFormat(BIND_TIME_FORMAT, Locale.getDefault()).format(Date())

    val time = if (realName.contains("_")) {
        val index = realName.lastIndexOf("_")

        if (index != -1) {
            realName.substring(index)
        } else {
            SimpleDateFormat(BIND_TIME_FORMAT, Locale.getDefault()).format(Date())
        }

    } else {
        SimpleDateFormat(BIND_TIME_FORMAT, Locale.getDefault()).format(Date())
    }

    device.setCustomInfo(null, name + time)
}

/**
 * 获取设备的绑定日期，如果没有的话，显示今天
 */
fun Context.getDeviceBindTime(device: GizWifiDevice?): String {

    if (device == null) {
        return SimpleDateFormat(BIND_TIME_FORMAT.replace("_", ""), Locale.getDefault()).format(Date())
    }

    //yyyy-MM-dd
    val dateFormat = SimpleDateFormat(BIND_TIME_FORMAT.replace("_", ""), Locale.getDefault())

    //xx_yyyy-MM-dd
    val realName = device.alias ?: "_" + dateFormat.format(Date())

    return if (realName.contains("_")) {

        val index = realName.lastIndexOf("_")
        val mayDate = realName.substring(index + 1)

        try {
            dateFormat.parse(mayDate)
        } catch (e: Exception) {
            dateFormat.format(Date())
        }

        mayDate
    } else {
        dateFormat.format(Date())
    }
}

/**
 * 发送命令
 */
fun Context.sendCommand(mDevice: com.gizwits.gizwifisdk.api.GizWifiDevice?, commandMap: java.util.concurrent.ConcurrentHashMap<String, Any>?, sn: Int = 1) {
    commandMap?.let {
        if (deviceIsController(mDevice))
            mDevice?.write(commandMap, sn)
        else
            ToastUtil.showToast(this, R.string.GIZ_OPENAPI_DEVICE_OFFLINE)
    }

    LogUtils.w("sendCommand", commandMap.toString())
}

/**
 * 发送命令
 */
fun Context.sendCommand(mDevice: com.gizwits.gizwifisdk.api.GizWifiDevice?, key: String, value: Any, sn: Int = 1) {
    val commandmap = ConcurrentHashMap<String, Any>()
    commandmap.put(key, value)
    sendCommand(mDevice, commandmap, sn)
}

fun Context.deviceIsController(mDevice: GizWifiDevice?): Boolean {
    return if (mDevice == null)
        false
    else
        mDevice.netStatus == GizWifiDeviceNetStatus.GizDeviceControlled
}

fun Context.deviceIsOnline(mDevice: GizWifiDevice?): Boolean {
    return if (mDevice == null)
        false
    else
        mDevice.netStatus == GizWifiDeviceNetStatus.GizDeviceControlled
                || mDevice.netStatus == GizWifiDeviceNetStatus.GizDeviceOnline
}

fun Context.getSwitchSetKeyAndStateKey(pos: Int, result: (String, String) -> Unit) {
    var setKey: String = ""
    var stateKey: String = ""

    //查找要设置的表示
    when (pos) {

        0 -> {
            setKey = GizDataPointKey.KEY_MAIN_ALARM
            stateKey = GizDataPointKey.KEY_MAIN_ALARM_SET
        }

        1 -> {
            setKey = GizDataPointKey.KEY_ALARM1_SET
            stateKey = GizDataPointKey.KEY_ALARM1_STATE
        }

        2 -> {
            setKey = GizDataPointKey.KEY_ALARM2_SET
            stateKey = GizDataPointKey.KEY_ALARM2_STATE
        }

        3 -> {
            setKey = GizDataPointKey.KEY_ALARM3_SET
            stateKey = GizDataPointKey.KEY_ALARM3_STATE
        }

        4 -> {
            setKey = GizDataPointKey.KEY_ALARM4_SET
            stateKey = GizDataPointKey.KEY_ALARM4_STATE
        }

        5 -> {
            setKey = GizDataPointKey.KEY_ALARM5_SET
            stateKey = GizDataPointKey.KEY_ALARM5_STATE
        }

        6 -> {
            setKey = GizDataPointKey.KEY_ALARM6_SET
            stateKey = GizDataPointKey.KEY_ALARM6_STATE
        }

        7 -> {
            setKey = GizDataPointKey.KEY_ALARM7_SET
            stateKey = GizDataPointKey.KEY_ALARM7_STATE
        }

        8 -> {
            setKey = GizDataPointKey.KEY_ALARM8_SET
            stateKey = GizDataPointKey.KEY_ALARM8_STATE
        }

        9 -> {
            setKey = GizDataPointKey.KEY_ALARM9_SET
            stateKey = GizDataPointKey.KEY_ALARM9_STATE
        }

        10 -> {
            setKey = GizDataPointKey.KEY_ALARM10_SET
            stateKey = GizDataPointKey.KEY_ALARM10_STATE
        }

        11 -> {
            setKey = GizDataPointKey.KEY_ALARM11_SET
            stateKey = GizDataPointKey.KEY_ALARM11_STATE
        }

        12 -> {
            setKey = GizDataPointKey.KEY_ALARM12_SET
            stateKey = GizDataPointKey.KEY_ALARM12_STATE
        }
    }

    LogUtils.e("sendCommand", "setKey=$setKey ,stateKey=$stateKey")
    result(setKey, stateKey)
}

fun Context.getStateList(mDatas: List<AlarmDetailsEntity>, state: Int): List<String> {
    return mDatas.filter { it.state == state }.map {
        when (it.pos) {
            0 -> GizDataPointKey.KEY_MAIN_ALARM_SET
            1 -> GizDataPointKey.KEY_ALARM1_STATE
            2 -> GizDataPointKey.KEY_ALARM2_STATE
            3 -> GizDataPointKey.KEY_ALARM3_STATE
            4 -> GizDataPointKey.KEY_ALARM4_STATE
            5 -> GizDataPointKey.KEY_ALARM5_STATE
            6 -> GizDataPointKey.KEY_ALARM6_STATE
            7 -> GizDataPointKey.KEY_ALARM7_STATE
            8 -> GizDataPointKey.KEY_ALARM8_STATE
            9 -> GizDataPointKey.KEY_ALARM9_STATE
            10 -> GizDataPointKey.KEY_ALARM10_STATE
            11 -> GizDataPointKey.KEY_ALARM11_STATE
            12 -> GizDataPointKey.KEY_ALARM12_STATE
            else -> {
                ""
            }
        }
    }
}