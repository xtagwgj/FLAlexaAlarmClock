package com.xbdl.flalexaclock.email;

/**
 * 发送邮件的监听
 * Created by xtagwgj on 2017/11/18.
 */

public interface SendEmailListener {
    void onSendEmailResult(boolean result);
}
