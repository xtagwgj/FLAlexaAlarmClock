package com.xbdl.flalexaclock.email;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * 认证类MyAuthenticator
 * Created by xtagwgj on 2017/11/18.
 */

public class MyAuthenticator extends Authenticator {
    String userName = null;
    String password = null;
    public MyAuthenticator() {
    }
    public MyAuthenticator(String username, String password) {
        this.userName = username;
        this.password = password;
    }
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(userName, password);
    }
}
