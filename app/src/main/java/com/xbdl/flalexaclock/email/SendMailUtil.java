package com.xbdl.flalexaclock.email;

import android.support.annotation.NonNull;

import com.xtagwgj.baseproject.utils.StringUtils;

import java.io.File;

/**
 * 发送邮件的工具类
 * Created by xtagwgj on 2017/11/18.
 */

public class SendMailUtil {

    /**
     * sina
     */
    private static final String HOST = "smtp.sina.com";
    private static final String PORT = "587";
    private static final String FROM_ADD = "sendhomtime@sina.com";
    /**
     * sina密码
     */
    private static final String FROM_PSW = "qH6-FBL-zKr-qbg";

    public static void send(String subject, String content, final File file, String toAdd, final SendEmailListener sendEmailListener) {
        final MailInfo mailInfo = creatMail(subject, content, toAdd);
        final MailSender sms = new MailSender();
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean flag = sms.sendFileMail(mailInfo, file);
                if (sendEmailListener != null) {
                    sendEmailListener.onSendEmailResult(flag);
                }
            }
        }).start();
    }

    public static void send(String subject, String content, String toAdd, final SendEmailListener sendEmailListener) {
        final MailInfo mailInfo = creatMail(subject, content, toAdd);
        final MailSender sms = new MailSender();
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean flag = sms.sendTextMail(mailInfo);
                if (sendEmailListener != null) {
                    sendEmailListener.onSendEmailResult(flag);
                }
            }
        }).start();
    }

    @NonNull
    private static MailInfo creatMail(String subject, String content, String toAdd) {
        final MailInfo mailInfo = new MailInfo();
        mailInfo.setMailServerHost(HOST);
        mailInfo.setMailServerPort(PORT);
        mailInfo.setValidate(true);
        // 你的邮箱地址
        mailInfo.setUserName(FROM_ADD);
        // 您的邮箱密码
        mailInfo.setPassword(FROM_PSW);
        // 发送的邮箱
        mailInfo.setFromAddress(FROM_ADD);
        // 发到哪个邮件去
        mailInfo.setToAddress(toAdd);
        // 邮件主题
        mailInfo.setSubject(StringUtils.isEmpty(subject) ? "Hello" : subject);
        // 邮件文本
        mailInfo.setContent(StringUtils.isEmpty(content) ? "Android Test Send Email" : content);
        return mailInfo;
    }

}
