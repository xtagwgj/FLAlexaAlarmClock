package com.xbdl.flalexaclock

import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.os.IBinder
import com.gizwits.gizwifisdk.api.*
import com.gizwits.gizwifisdk.enumration.GizEventType
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import com.gizwits.gizwifisdk.listener.GizDeviceSharingListener
import com.gizwits.gizwifisdk.listener.GizWifiDeviceListener
import com.gizwits.gizwifisdk.listener.GizWifiSDKListener
import com.xbdl.flalexaclock.base.App
import com.xbdl.flalexaclock.base.SERVER_CN
import com.xbdl.flalexaclock.entity.giz.*
import com.xbdl.flalexaclock.http.HttpRequest
import com.xbdl.flalexaclock.ui.user.LoginActivity
import com.xbdl.flalexaclock.utils.ClockUtils
import com.xbdl.flalexaclock.utils.ToastUtil
import com.xtagwgj.baseproject.base.AppManager
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ConcurrentHashMap

/**
 * 解析机智云的服务
 * Created by xtagwgj on 2017/9/8.
 */
class ParseDataService : Service() {


    companion object {

        private val TAG = "self_ParseDataService"

        val deviceListener = object : GizWifiDeviceListener() {

            override fun didUpdateNetStatus(device: GizWifiDevice?, netStatus: GizWifiDeviceNetStatus?) {
                postResponse("UpdateNetStatusResponse", UpdateNetStatusResponse(device, netStatus))
            }

            override fun didSetSubscribe(result: GizWifiErrorCode, device: GizWifiDevice?, isSubscribed: Boolean) {
                postResponse("SetSubscribeResposne", SetSubscribeResposne(result, device, isSubscribed))
            }

            override fun didSetCustomInfo(result: GizWifiErrorCode, device: GizWifiDevice?) {
                postResponse("SetCustomInfoResponse", SetCustomInfoResponse(result, device))
            }

            override fun didReceiveData(result: GizWifiErrorCode?, device: GizWifiDevice?, dataMap: ConcurrentHashMap<String, Any>?, sn: Int) {
                Flowable.just(dataMap)
                        .filter { result != null && device != null }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (sn == 0) {
                                postResponse("ReceiveDataResponse", ReceiveDataResponse(result!!, device, dataMap, sn))
                            } else {
                                postResponse("GizCommandResponse", GizCommandResponse(result!!, device!!, sn))
                            }

                        }, {
                            LogUtils.e(TAG, "$result _ $device _ $dataMap _ $sn")
                            it.printStackTrace()
                        })


            }

            override fun didGetHardwareInfo(result: GizWifiErrorCode, device: GizWifiDevice?, hardwareInfo: ConcurrentHashMap<String, String>?) {
                postResponse("GetHardwareInfoResponse", GetHardwareInfoResponse(result, device, hardwareInfo))
            }
        }

        private fun postResponse(tag: String, obj: Any) {
            LogUtils.e(TAG, obj)
            RxBus.getInstance().post(tag, obj)
        }


    }

    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        throw UnsupportedOperationException("Not yet implemented")
    }


    override fun onCreate() {
        super.onCreate()
        LogUtils.e(TAG, "service启动")
//        initGiz()

        GizWifiSDK.sharedInstance().setListener(sdkListener)
//        GizDeviceSharing.setListener(sharingListener)
    }

    private fun initGiz() {
        val map = ConcurrentHashMap<String, String>()
        map.put("openAPIInfo", "usapi.gizwits.com")
        map.put("siteInfo", "ussite.gizwits.com")
        map.put("pushInfo", "us.push.gizwitsapi.com")

        GizWifiSDK.sharedInstance().startWithAppID(
                App.instance,
                BuildConfig.APP_ID,
                BuildConfig.APP_SECRET,
                arrayListOf(BuildConfig.PRODUCT_KEY),
                map,
                true)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        LogUtils.e(TAG, "service StartCommand  $intent - $flags - $startId")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        LogUtils.e(TAG, "service销毁")
        super.onDestroy()
    }


    private val sdkListener = object : GizWifiSDKListener() {
        override fun didUserLogin(result: GizWifiErrorCode, uid: String?, token: String?) {
            LogUtils.e("didUserLogin", "$result - $uid - $token")
            postResponse("UserLoginResponse", UserLoginResponse(result, uid ?: "", token ?: ""))
        }

        override fun didRequestSendPhoneSMSCode(result: GizWifiErrorCode, token: String?) {
            postResponse("SendPhoneSMSResponse", SendPhoneSMSResponse(result, token ?: ""))
        }

        override fun didRegisterUser(result: GizWifiErrorCode, uid: String?, token: String?) {
            postResponse("RegisterUserResponse", RegisterUserResponse(result, uid ?: "", token ?: ""))
        }

        override fun didChangeUserPassword(result: GizWifiErrorCode) {
            postResponse("GizBaseResponse", GizBaseResponse(result))
        }

        override fun didDiscovered(result: GizWifiErrorCode, deviceList: MutableList<GizWifiDevice>?) {

            if (result == GizWifiErrorCode.GIZ_SDK_SUCCESS) {

                //刷新设备
                ClockUtils.refreshDeviceList(deviceList ?: mutableListOf())

                if (deviceList == null || deviceList.isEmpty()) {
                    postResponse("DiscoverdResponse", DiscoverdResponse(result, mutableListOf()))
                }
//
// else {
////                postResponse("DiscoverdResponse", DiscoverdResponse(result,
////                        deviceList.filter { it.productKey == BuildConfig.PRODUCT_KEY }.toMutableList()))
//
//                    //刷新设备
//                    ClockUtils.refreshDeviceList(deviceList?: mutableListOf())
//
//                }

            } else if (result == GizWifiErrorCode.GIZ_SDK_CLIENT_NOT_AUTHEN) {
                initGiz()
            } else if (result == GizWifiErrorCode.GIZ_OPENAPI_TOKEN_EXPIRED
                    || result == GizWifiErrorCode.GIZ_SDK_TOKEN_INVALID
                    || result == GizWifiErrorCode.GIZ_OPENAPI_TOKEN_INVALID) {
                LogUtils.e(TAG, "发现设备 未授权: 重新初始化应用 ")

                if (AppManager.getAppManager().currentActivity() != null) {
                    LoginActivity.doAction(AppManager.getAppManager().currentActivity(), true)
                }

            }
        }


        override fun didBindDevice(result: GizWifiErrorCode, did: String?) {
            postResponse("BindOrUnBindResponse", BindOrUnBindResponse(result, true, did ?: ""))
        }

        override fun didUnbindDevice(result: GizWifiErrorCode, did: String?) {
            postResponse("BindOrUnBindResponse", BindOrUnBindResponse(result, false, did ?: ""))
        }

        override fun didGetUserInfo(result: GizWifiErrorCode, userInfo: GizUserInfo?) {
            postResponse("UserInfoResponse", UserInfoResponse(result, userInfo))
        }

        override fun didChangeUserInfo(result: GizWifiErrorCode) {
            postResponse("changeUserInfo", GizBaseResponse(result))
        }

        override fun didSetDeviceOnboarding(result: GizWifiErrorCode, mac: String?, did: String?, productKey: String?) {
            postResponse("DeviceOnBoardingResposne", DeviceOnBoardingResposne(result, mac ?: "", did ?: "", productKey ?: ""))
        }

        override fun didGetCurrentCloudService(result: GizWifiErrorCode, cloudServiceInfo: ConcurrentHashMap<String, String>?) {
            LogUtils.e("ParseDataService", "result = $result ; cloudServiceInfo  = $cloudServiceInfo")
            if (result == GizWifiErrorCode.GIZ_SDK_SUCCESS && cloudServiceInfo != null) {
                val openApiDomain = cloudServiceInfo["openAPIDomain"] ?: "api.gizwits.com"

                val realDomain = if (!openApiDomain.startsWith("https://")) {
                    "https://" + openApiDomain
                } else {
                    openApiDomain
                }

                HttpRequest.isInChina = realDomain.contains(SERVER_CN)
//                HttpRequest.initBaseUrl = realDomain
            }
        }

        override fun didNotifyEvent(eventType: GizEventType?, eventSource: Any?, eventID: GizWifiErrorCode?, eventMessage: String?) {
            if (eventType === GizEventType.GizEventSDK) {
                LogUtils.i(TAG, "GizWifiSDK", "SDK发生异常的通知: $eventID, $eventMessage")

                if (eventID == GizWifiErrorCode.GIZ_SDK_START_SUCCESS) {
                    //获取服务器信息
//                    GizWifiSDK.sharedInstance().getCurrentCloudService()
                }

            } else if (eventType === GizEventType.GizEventDevice) {
                val mDevice = eventSource as com.gizwits.gizwifisdk.api.GizWifiDevice
                LogUtils.i(TAG, "GizWifiSDK", "设备连接断开时可能产生的通知 device mac: ${mDevice.macAddress}  disconnect caused by eventID: $eventID, eventMessage: $eventMessage")
            } else if (eventType === GizEventType.GizEventM2MService) {
                LogUtils.i(TAG, "GizWifiSDK", "M2M服务返回的异常通知 M2M domain ${eventSource as String} exception happened, eventID: $eventID, eventMessage: $eventMessage")
            } else if (eventType === GizEventType.GizEventToken) {
                LogUtils.i(TAG, "GizWifiSDK", "token失效通知 ${eventSource as String}  expired: $eventMessage")

                //fixme://待验证
                val currAty = AppManager.getAppManager().currentActivity()
                if (currAty != null && currAty::class.java.simpleName != LoginActivity::class.java.simpleName) {
                    ToastUtil.showToast(currAty, "登录过期，请重新登录")
                    LoginActivity.doAction(currAty)
                }
            }
        }
    }

    /**
     * 分享的监听
     */
    private val sharingListener = object : GizDeviceSharingListener() {
        override fun didGetBindingUsers(result: GizWifiErrorCode, deviceID: String?, bindUsers: MutableList<GizUserInfo>?) {
            postResponse("GetBindingUsersResponse", GetBindingUsersResponse(result, deviceID ?: "", bindUsers ?: mutableListOf()))
        }

        override fun didUnbindUser(result: GizWifiErrorCode, deviceID: String?, guestUID: String?) {
            postResponse("UnBindUserResponse", UnBindUserResponse(result, deviceID ?: "", guestUID ?: ""))
        }

        override fun didGetDeviceSharingInfos(result: GizWifiErrorCode, deviceID: String?, deviceSharingInfos: MutableList<GizDeviceSharingInfo>?) {
            postResponse("GetDeviceSharingInfoResposne", GetDeviceSharingInfoResposne(result, deviceID ?: "",
                    deviceSharingInfos ?: mutableListOf()))
        }

        override fun didSharingDevice(result: GizWifiErrorCode, deviceID: String?, sharingID: Int, QRCodeImage: Bitmap?) {
            postResponse("SharingDeviceResponse", SharingDeviceResponse(result, deviceID ?: "", sharingID, QRCodeImage))
        }

        override fun didRevokeDeviceSharing(result: GizWifiErrorCode, sharingID: Int) {
        }

        override fun didAcceptDeviceSharing(result: GizWifiErrorCode, sharingID: Int) {
            postResponse("AcceptDeviceSharingResponse", AcceptDeviceSharingResponse(result, sharingID))
        }

        override fun didModifySharingInfo(result: GizWifiErrorCode, sharingID: Int) {
        }

        override fun didQueryMessageList(result: GizWifiErrorCode, messageList: MutableList<GizMessage>?) {
        }
    }
}