package com.xbdl.flalexaclock.http

import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import com.androidnetworking.interfaces.ParsedRequestListener
import com.google.gson.Gson
import com.rx2androidnetworking.Rx2AndroidNetworking
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.base.SERVER_CN
import com.xbdl.flalexaclock.base.SERVER_US
import com.xbdl.flalexaclock.entity.*
import com.xbdl.flalexaclock.utils.MD5Util
import com.xtagwgj.baseproject.utils.LogUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import okhttp3.Response
import org.json.JSONObject

/**
 * Http请求
 * Created by xtagwgj on 2017/9/19.
 */
object HttpRequest {

    var isInChina = false

    private fun initBaseUrl() = if (isInChina) SERVER_CN else SERVER_US

    private fun getBaseUrl(): String = initBaseUrl()

    //进入MainActivity把userToken加进来
    private val headerMap by lazy {
        mutableMapOf(
                "Content-Type" to "application/json",
                "Accept" to "application/json",
                "X-Gizwits-Application-Id" to BuildConfig.APP_ID
        )
    }

    val authToken by lazy {
        MD5Util.MD5(BuildConfig.APP_ID + BuildConfig.APP_SECRET).toLowerCase()
    }

    fun addUserToken(key: String = "X-Gizwits-User-token", token: String) {
        headerMap.put(key, token)
    }

    /**
     * 创建第三方登录用户
     */
    fun createThirdUser(src: String, uid: String, token: String): Observable<ThirdPartyResponseEntity> {
        return Rx2AndroidNetworking.post(getBaseUrl() + "/app/users")
                .addHeaders(headerMap)
                .addApplicationJsonBody("{\"authData\": {\"src\": \"$src\", \"uid\": \"$uid\", \"token\": \"$token\"}}")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectObservable(ThirdPartyResponseEntity::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * 修改用户信息
     * @param gender  M男 F女
     * @param birthday yyyy-MM-dd
     */
    fun modifyUserInfo(userToken: String, gender: String, birthday: String, remark: String?, resultConsumer: (Boolean, String) -> Unit) {

        val jsonBody =
                if (remark == null) {
                    "{\"gender\": \"$gender\", \"birthday\": \"$birthday\"}"
                } else {
                    "{\"gender\": \"$gender\", \"birthday\": \"$birthday\", \"remark\": \"$remark\"}"
                }

        Rx2AndroidNetworking.put(getBaseUrl() + "/app/users")
                .addHeaders(headerMap)
                .addHeaders("X-Gizwits-User-token", userToken)
                .addApplicationJsonBody(jsonBody)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        resultConsumer(true, "")
                    }

                    override fun onError(anError: ANError) {
                        resultConsumer(true, anError.errorDetail)
                    }
                })
    }

    /**
     * 获取用户信息
     */
    fun getUserInfo(successResult: (user: GizUser) -> Unit, errorResult: (anError: ErrorResult) -> Unit) {
        Rx2AndroidNetworking.get(getBaseUrl() + "/app/users")
                .addHeaders(headerMap)
                .setTag("user")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsObject(GizUser::class.java, object : ParsedRequestListener<GizUser> {
                    override fun onError(anError: ANError) {
                        val fromJson = Gson().fromJson(anError.errorBody, ErrorResult::class.java)
                        errorResult(fromJson)
                    }

                    override fun onResponse(response: GizUser) {
                        successResult(response)
                    }

                })
    }

    /**
     * 获取设备最新的数据点
     * @did 设备的did
     */
    fun getLatestDevicePoint(did: String): Observable<GizLatestDeviceData> {

        return Rx2AndroidNetworking.get(getBaseUrl() + "/app/devdata/$did/latest")
                .addHeaders(headerMap)
//                .addHeaders("X-Gizwits-Application-Auth", authToken)
//                .setTag("")
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectObservable(GizLatestDeviceData::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())


    }

    /**
     * 控制设备
     * @param did 设备的did
     * @param userToken 用户的token
     * @param body 控制内容
     */
    fun controlDevice(did: String, userToken: String, body: MutableMap<String, Any>, resultResponse: (Boolean, String?) -> Unit) {
        LogUtils.e("sendCommand", body.toString())

        return Rx2AndroidNetworking.post(getBaseUrl() + "/app/control/{did}")
                .addPathParameter("did", did)
                .addHeaders(headerMap)
                .addApplicationJsonBody(ControlEntity(body))
                .addHeaders("X-Gizwits-User-token", userToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsObject(GizBaseResponse::class.java, object : ParsedRequestListener<GizBaseResponse> {
                    override fun onResponse(response: GizBaseResponse?) {
                        if (response == null || response.error_code == 0) {
                            resultResponse(true, null)
                        } else {
                            resultResponse(false, response.error_message)
                        }
                    }

                    override fun onError(anError: ANError) {
                        resultResponse(false, anError.errorDetail)
                    }
                })

    }

    /**
     * 解绑设备
     * @param did 设备did
     * @param userToken  用户token
     * 可扩展为多个设备的同时绑定，暂时只是单个设备的绑定
     */
    fun unBindDevice(did: String, userToken: String, requestResult: (Boolean, String?) -> Unit) {
        Rx2AndroidNetworking.delete(getBaseUrl() + "/app/bindings")
                .addHeaders(headerMap)
                .addApplicationJsonBody(UnBindDeviceRequest(listOf(didEntity(did))))
                .addHeaders("X-Gizwits-User-token", userToken)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsOkHttpResponse(object : OkHttpResponseListener {
                    override fun onResponse(response: Response) {
//                        LogUtils.e("okhttp", response)

                        val responseEntity = Gson().fromJson(response.body().string(), UnBindDeviceResponse::class.java)
                        LogUtils.e("okhttp", responseEntity)
                        if (responseEntity.failed.isEmpty()) {
                            requestResult(true, null)
                        } else {
                            requestResult(false, "UnBind device fail")
                        }

                    }

                    override fun onError(anError: ANError) {
                        requestResult(false, anError.errorDetail ?: "UnBind device fail")
                    }
                })
    }

    /**
     * 查询所有的分组列表
     */
    fun findAllGroup(): Observable<MutableList<GroupEntity>> {

        return Rx2AndroidNetworking.get(getBaseUrl() + "/app/group")
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectListObservable(GroupEntity::class.java)
                .map { it.toMutableList() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * 创建分组
     */
    fun createGroup(groupName: String,
                    successConsumer: Consumer<String>,
                    errorConsumer: Consumer<Throwable>) {

        Rx2AndroidNetworking.post(getBaseUrl() + "/app/group")
                .addHeaders(headerMap)
                .addApplicationJsonBody(CreateGroupRequest(BuildConfig.PRODUCT_KEY, groupName))
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        successConsumer.accept(response.getString("id"))
                    }

                    override fun onError(anError: ANError) {
                        errorConsumer.accept(anError)
                    }
                })
    }

    /**
     * 删除分组
     */
    fun deleteGroup(id: String, result: (Boolean) -> Unit, errorConsumer: Consumer<Throwable>) {
        Rx2AndroidNetworking.delete(getBaseUrl() + "/app/group/{id}")
                .addPathParameter("id", id)
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsOkHttpResponse(object : OkHttpResponseListener {
                    override fun onResponse(response: Response) {
                        result(response.code() == 200)
                    }

                    override fun onError(anError: ANError) {
                        result(false)
                        errorConsumer.accept(anError)
                    }
                })
    }

    /**
     * 修改分组名称
     */
    fun modifyGroupName(id: String, groupName: String,
                        result: (Boolean) -> Unit,
                        errorConsumer: Consumer<Throwable>) {

        Rx2AndroidNetworking.put(getBaseUrl() + "/app/group/{id}")
                .addPathParameter("id", id)
                .addApplicationJsonBody("{\"group_name\":\"$groupName\"}")
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getAsOkHttpResponse(object : OkHttpResponseListener {
                    override fun onResponse(response: Response) {
                        result(response.code() == 200)
                    }

                    override fun onError(anError: ANError) {
                        result(false)
                        errorConsumer.accept(anError)
                    }
                })
    }

    /**
     * 查询分组的设备信息
     * @param id 分组id
     */
    fun getGroupDevice(id: String): Observable<MutableList<GroupDeviceEntity>> {

        return Rx2AndroidNetworking.get(getBaseUrl() + "/app/group/{id}/devices")
                .addPathParameter("id", id)
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectListObservable(GroupDeviceEntity::class.java)
                .map { it.toMutableList() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * 将设备列表添加到分组
     */
    fun moveDevice2Group(groupId: String, didList: List<String>): Observable<MoveDeviceResponse> {

        return Rx2AndroidNetworking.post(getBaseUrl() + "/app/group/{id}/devices")
                .addPathParameter("id", groupId)
                .addQueryParameter("show_detail", "1")
                .addApplicationJsonBody(MoveDeviceRequest(didList))
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectObservable(MoveDeviceResponse::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    /**
     * 将设备列表移除分组
     * fixme:没有did参数
     */
    fun removeDeviceFromGroup(groupId: String, didList: List<String>): Observable<MoveDeviceResponse> {

        return Rx2AndroidNetworking.delete(getBaseUrl() + "/app/group/{id}/devices")
                .addPathParameter("id", groupId)
                .addApplicationJsonBody(MoveDeviceRequest(didList))
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectObservable(MoveDeviceResponse::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * 对设备分组内的设备统一控制
     */
    fun controlGroupDevice(groupId: String, body: MutableMap<String, Any>):
            Observable<MutableList<ControlDeviceResponse>> {

        return Rx2AndroidNetworking.post(getBaseUrl() + "/app/group/{id}/control")
                .addPathParameter("id", groupId)
                .addApplicationJsonBody(ControlEntity(body))
                .addHeaders(headerMap)
                .setPriority(Priority.IMMEDIATE)
                .build()
                .getObjectListObservable(ControlDeviceResponse::class.java)
                .map { it.toMutableList() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

}