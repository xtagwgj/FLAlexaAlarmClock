package com.xbdl.flalexaclock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.xbdl.flalexaclock.BR;
import com.xbdl.flalexaclock.R;
import com.xbdl.flalexaclock.base.App;

import java.util.List;
import java.util.Locale;

/**
 * 分组信息
 * Created by xtagwgj on 2017/9/28.
 */
/*
created_at	string	设备分组创建时间（UTC 时间）
updated_at	string	设备分组更新时间（UTC 时间）
product_key	string	产品 product_key，单PK的分组才会有值
group_name	string	设备分组名称
verbose_name	string	产品名称，单PK的分组才会有值
id	string	设备分组 id
 */
public class GroupEntity extends BaseObservable implements Parcelable {

    private String created_at;
    private String updated_at;
    private String group_name;
    private String product_key;
    private String verbose_name;
    private String id;

    private transient List<ClockEntity> clockEntityList;


    public GroupEntity() {
    }

    protected GroupEntity(Parcel in) {
        created_at = in.readString();
        updated_at = in.readString();
        group_name = in.readString();
        product_key = in.readString();
        verbose_name = in.readString();
        id = in.readString();
        clockEntityList = in.createTypedArrayList(ClockEntity.CREATOR);
    }

    public static final Creator<GroupEntity> CREATOR = new Creator<GroupEntity>() {
        @Override
        public GroupEntity createFromParcel(Parcel in) {
            return new GroupEntity(in);
        }

        @Override
        public GroupEntity[] newArray(int size) {
            return new GroupEntity[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(group_name);
        dest.writeString(product_key);
        dest.writeString(verbose_name);
        dest.writeString(id);
        dest.writeTypedList(clockEntityList);
    }

    @Bindable
    public String getCreated_at() {
        return created_at;
    }

    @Bindable
    public String getUpdated_at() {
        return updated_at;
    }

    @Bindable
    public String getGroup_name() {
        return group_name;
    }

    @Bindable
    public String getProduct_key() {
        return product_key;
    }

    @Bindable
    public String getVerbose_name() {
        return verbose_name;
    }

    @Bindable
    public String getId() {
        return id;
    }

    public List<ClockEntity> getClockEntityList() {
        return clockEntityList;
    }

    public void setClockEntityList(List<ClockEntity> clockEntityList) {
        this.clockEntityList = clockEntityList;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
        notifyPropertyChanged(BR.created_at);
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
        notifyPropertyChanged(BR.updated_at);
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
        notifyPropertyChanged(BR.group_name);
    }

    public void setProduct_key(String product_key) {
        this.product_key = product_key;
        notifyPropertyChanged(BR.product_key);
    }

    public void setVerbose_name(String verbose_name) {
        this.verbose_name = verbose_name;
        notifyPropertyChanged(BR.verbose_name);
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    /**
     * 显示闹钟的信息
     *
     * @return 3 alarm open, total 7
     */
    public String getShowAlarmInfo() {
        int open = 0;
        int total = 0;
        if (clockEntityList == null || clockEntityList.size() == 0) {
            open = 0;
            total = 0;
        } else {

            for (ClockEntity clockEntity : clockEntityList) {
                open += clockEntity.getOpenAlarmNumber();
                total += clockEntity.getTotalAlarmNumber();
            }

        }

        return String.format(Locale.ENGLISH,
                App.instance.getResources().getString(R.string.text_alarm_number_info), open, total);
    }

    @Override
    public String toString() {
        return "GroupEntity{" +
                "created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", group_name='" + group_name + '\'' +
                ", product_key='" + product_key + '\'' +
                ", verbose_name='" + verbose_name + '\'' +
                ", id='" + id + '\'' +
                ", clockEntityList=" + clockEntityList +
                '}';
    }
}
