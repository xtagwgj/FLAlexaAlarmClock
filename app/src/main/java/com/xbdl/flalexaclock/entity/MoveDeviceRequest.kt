package com.xbdl.flalexaclock.entity

/**
 * 设备列表添加到分组 的body参数
 * Created by xtagwgj on 2017/9/28.
 */
data class MoveDeviceRequest(val dids: List<String>)