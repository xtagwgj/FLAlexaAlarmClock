package com.xbdl.flalexaclock.entity

/**
 * 将设备列表添加到分组的响应
 * Created by xtagwgj on 2017/9/28.
 */
data class MoveDeviceResponse(
        val failed: List<String>,
        val detail: List<FailMessage>? = null,
        val success: List<String>)

data class FailMessage(
        val did: String,
        val error_message: String,
        val error_code: Int,
        val detail_msg: String?)