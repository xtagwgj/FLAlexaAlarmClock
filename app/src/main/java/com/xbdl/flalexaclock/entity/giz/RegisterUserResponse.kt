package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 注册用户的回调
 * Created by xtagwgj on 2017/9/4.
 */
data class RegisterUserResponse(val result: GizWifiErrorCode, val uid: String, val token: String)