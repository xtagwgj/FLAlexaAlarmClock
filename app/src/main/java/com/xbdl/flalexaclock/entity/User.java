package com.xbdl.flalexaclock.entity;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * 用户
 * Created by xtagwgj on 2017/10/16.
 */

public class User {
    public String uid;
    public String headUrl;

    public User() {
    }

    public User(String uid, String headUrl) {
        this.uid = uid;
        this.headUrl = headUrl;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", headUrl='" + headUrl + '\'' +
                '}';
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("headUrl", headUrl);
        return result;
    }
}
