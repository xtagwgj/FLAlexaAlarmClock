package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 用户登录的响应
 * Created by xtagwgj on 2017/9/4.
 */
data class UserLoginResponse(val result: GizWifiErrorCode, val uid: String, val token: String)