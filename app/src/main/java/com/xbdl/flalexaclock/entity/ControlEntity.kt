package com.xbdl.flalexaclock.entity

/**
 * 远程控制设备 参数的实体类
 * Created by xtagwgj on 2017/9/21.
 */
data class ControlEntity(val attrs: MutableMap<String, Any>)