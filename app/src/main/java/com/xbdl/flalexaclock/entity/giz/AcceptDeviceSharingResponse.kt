package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 接受了设备的分享
 * Created by xtagwgj on 2017/9/4.
 */
data class AcceptDeviceSharingResponse(val result: GizWifiErrorCode, val sharingID: Int)