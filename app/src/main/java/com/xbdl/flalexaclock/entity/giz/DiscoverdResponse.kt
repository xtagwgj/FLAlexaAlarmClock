package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 设备变化的响应
 * Created by xtagwgj on 2017/9/4.
 */
data class DiscoverdResponse(val result: GizWifiErrorCode, val deviceList: MutableList<GizWifiDevice>)