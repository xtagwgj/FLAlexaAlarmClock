package com.xbdl.flalexaclock.entity;

/**
 * 基本的机智云回复类
 * Created by xtagwgj on 2017/9/20.
 */

public class GizBaseResponse {
    private int error_code;
    private String error_message;
    private String detail_message;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getDetail_message() {
        return detail_message;
    }

    public void setDetail_message(String detail_message) {
        this.detail_message = detail_message;
    }
}
