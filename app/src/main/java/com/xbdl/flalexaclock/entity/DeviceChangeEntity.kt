package com.xbdl.flalexaclock.entity

/**
 * Created by xtagwgj on 2017/11/7.
 */
data class DeviceChangeEntity(var did: String)