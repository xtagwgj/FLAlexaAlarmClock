package com.xbdl.flalexaclock.entity;

/**
 * 设备最新的数据点
 * Created by xtagwgj on 2017/9/20.
 */

public class GizLatestDeviceData extends GizBaseResponse {
    private String did;
    private long updated_at;
    private GizAttr attr;

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public GizAttr getAttr() {
        return attr;
    }

    public void setAttr(GizAttr attr) {
        this.attr = attr;
    }

    @Override
    public String toString() {
        return "GizLatestDeviceData{" +
                "did='" + did + '\'' +
                ", updated_at=" + updated_at +
                ", attr=" + attr +
                '}';
    }
}
