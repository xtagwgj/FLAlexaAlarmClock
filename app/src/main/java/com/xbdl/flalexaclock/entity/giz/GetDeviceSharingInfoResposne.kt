package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizDeviceSharingInfo
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 设备分享信息
 * Created by xtagwgj on 2017/9/4.
 */
data class GetDeviceSharingInfoResposne(val result: GizWifiErrorCode, val deviceID: String,
                                        val deviceSharingInfos: MutableList<GizDeviceSharingInfo>)