package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 发送命令的响应
 * Created by xtagwgj on 2017/9/27.
 */
data class GizCommandResponse(val result: GizWifiErrorCode, val device: GizWifiDevice, val sn: Int)