package com.xbdl.flalexaclock.entity.giz

/**
 * 闹钟重复的类型
 * Created by xtagwgj on 2017/11/14.
 */
enum class AlarmRepeatType {
    ONCE,
    EVERYDAY,
    CUSTOM
}