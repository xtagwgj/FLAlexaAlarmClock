package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizUserInfo
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 获取绑定用户的列表
 * Created by xtagwgj on 2017/9/4.
 */
data class GetBindingUsersResponse(val result: GizWifiErrorCode, val deviceID: String, val bindUsers: MutableList<GizUserInfo>)