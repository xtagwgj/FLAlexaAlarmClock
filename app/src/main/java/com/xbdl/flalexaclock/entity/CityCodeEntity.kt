package com.xbdl.flalexaclock.entity

import com.xbdl.flalexaclock.utils.TimeZoneTableUtils

/**
 * 时区城市代码
 * Created by xtagwgj on 2017/9/21.
 */
data class CityCodeEntity(
        val cityCode: String,
        val cityName: String,
        val timeZone: Int,
        val dstOffset: Float,
        val timeZoneId: String = "",
        var timeZoneWithCity: Int = 100) {

    fun getShow(): String {
        val header = StringBuffer("")
        header.append("$cityName (GMT")
        if (dstOffset == 0F) {
            if (TimeZoneTableUtils.getRealTimeZone(timeZone) > 0)
                header.append(" + ")
            else
                header.append(" - ")

            header.append("${Math.abs(TimeZoneTableUtils.getRealTimeZone(timeZone))}")
        } else {

            if (dstOffset > 0)
                header.append(" + ")
            else
                header.append(" - ")

            header.append("${Math.abs(dstOffset)}")
        }

        header.append(")")
//        header.append("$cityName")
        return header.toString()
    }

    fun getShowBySys(): String {

//        return when(timeZone){
//            30-> "Rio De Janeiro"
//
//            else -> timeZoneId
//        }
        return cityName
    }

}