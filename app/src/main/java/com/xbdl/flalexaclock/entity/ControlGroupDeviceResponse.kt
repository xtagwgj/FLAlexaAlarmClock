package com.xbdl.flalexaclock.entity

/**
 * 控制分组设备的返回结果
 * Created by xtagwgj on 2017/9/28.
 */
data class ControlDeviceResponse(val did: String, val result: Boolean)