package com.xbdl.flalexaclock.entity.giz

import android.graphics.Bitmap
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 分享设备的响应
 * Created by xtagwgj on 2017/9/4.
 */
data class SharingDeviceResponse(val result: GizWifiErrorCode, val deviceID: String, val sharingID: Int, val QRCodeImage: Bitmap?)