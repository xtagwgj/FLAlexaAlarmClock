package com.xbdl.flalexaclock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.gizwits.gizwifisdk.api.GizWifiDevice;
import com.xbdl.flalexaclock.BR;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 闹钟的实体类
 * Created by xtagwgj on 2017/9/20.
 */

public class ClockEntity extends BaseObservable implements Parcelable {
    //机智云设备
    private GizWifiDevice device;

    //分组的名称
    private String groupName;

    //时区
    private int timeZone;

    //闹钟的总开关
    private boolean clockSwitch;

    //主闹铃
    private AlarmDetailsEntity mainAlarm;

    //12个子闹铃
    private List<AlarmDetailsEntity> alarmList = new ArrayList<>(12);

    public ClockEntity() {
    }


    protected ClockEntity(Parcel in) {
        device = in.readParcelable(GizWifiDevice.class.getClassLoader());
        groupName = in.readString();
        timeZone = in.readInt();
        clockSwitch = in.readByte() != 0;
        mainAlarm = in.readParcelable(AlarmDetailsEntity.class.getClassLoader());
        alarmList = in.createTypedArrayList(AlarmDetailsEntity.CREATOR);
    }

    public static final Creator<ClockEntity> CREATOR = new Creator<ClockEntity>() {
        @Override
        public ClockEntity createFromParcel(Parcel in) {
            return new ClockEntity(in);
        }

        @Override
        public ClockEntity[] newArray(int size) {
            return new ClockEntity[size];
        }
    };

    @Bindable
    public GizWifiDevice getDevice() {
        return device;
    }

    public void setDevice(GizWifiDevice device) {
        this.device = device;
        notifyPropertyChanged(BR.device);
    }

    @Bindable
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
        notifyPropertyChanged(BR.groupName);
    }

    @Bindable
    public int getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(int timeZone) {
        this.timeZone = timeZone;
        notifyPropertyChanged(BR.timeZone);
    }

    @Bindable
    public boolean isClockSwitch() {
        return clockSwitch;
    }

    public void setClockSwitch(boolean clockSwitch) {
        this.clockSwitch = clockSwitch;
        notifyPropertyChanged(BR.clockSwitch);
    }

    @Bindable
    public AlarmDetailsEntity getMainAlarm() {
        return mainAlarm;
    }

    public void setMainAlarm(AlarmDetailsEntity mainAlarm) {
        this.mainAlarm = mainAlarm;
        notifyPropertyChanged(BR.mainAlarm);
    }

    @Bindable
    public List<AlarmDetailsEntity> getAlarmList() {
        return alarmList;
    }

    public void setAlarmList(List<AlarmDetailsEntity> alarmList) {
        this.alarmList = alarmList;
        notifyPropertyChanged(BR.alarmList);
    }

    @Override
    public String toString() {
        return "ClockEntity{" +
                "device=" + device +
                ", groupName='" + groupName + '\'' +
                ", timeZone=" + timeZone +
                ", clockSwitch=" + clockSwitch +
                ", mainAlarm=" + mainAlarm +
                ", alarmList=" + alarmList +
                '}';
    }

    public int getOpenAlarmNumber() {
        int open = 0;

        if (!clockSwitch) {
            return open;
        }

        if (mainAlarm.getState() == 1) {
            open++;
        }
        for (AlarmDetailsEntity alarmDetailsEntity : alarmList) {
            if (alarmDetailsEntity.getState() == 1) {
                open++;
            }
        }
        return open;
    }

    public int getTotalAlarmNumber() {
        int total = 1;

        for (AlarmDetailsEntity alarmDetailsEntity : alarmList) {
            if (alarmDetailsEntity.getState() == 1 || alarmDetailsEntity.getState() == 2) {
                total++;
            }
        }
        return total;
    }

    //几个闹钟开启，一共几个
    public String getAlarmInfo() {
        int open = 0, total = 1;

        if (mainAlarm.getState() == 1 && clockSwitch) {
            open++;
        }

        for (AlarmDetailsEntity alarmDetailsEntity : alarmList) {
            if (alarmDetailsEntity.getState() == 1 || alarmDetailsEntity.getState() == 2) {
                total++;
            }

            if (alarmDetailsEntity.getState() == 1 && clockSwitch) {
                open++;
            }
        }

        return String.format(Locale.ENGLISH, "%1$d alarm open, total %2$d", open, total);
    }

    //找一个可以设置闹铃的子闹铃 state=3即可以设置
    public AlarmDetailsEntity getNewAlarm() {
        for (AlarmDetailsEntity alarmDetailsEntity : alarmList) {
            if (alarmDetailsEntity.getState() == 3 || alarmDetailsEntity.getState() == 0) {
                return alarmDetailsEntity;
            }
        }

        return null;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(device, flags);
        dest.writeString(groupName);
        dest.writeInt(timeZone);
        dest.writeByte((byte) (clockSwitch ? 1 : 0));
        dest.writeParcelable(mainAlarm, flags);
        dest.writeTypedList(alarmList);
    }
}
