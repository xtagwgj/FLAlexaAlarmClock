package com.xbdl.flalexaclock.entity

/**
 * 错误的响应
 * Created by xtagwgj on 2017/8/6.
 */
data class ErrorResult(val error_message: String, val error_code: Int, val detail_message: String?)