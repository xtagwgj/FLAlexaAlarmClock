package com.xbdl.flalexaclock.entity

/**
 *登录成功的回调
 * Created by xtagwgj on 2017/10/11.
 */
data class ThirdPartyResponseEntity(var expire_at: Long, var uid: String, var token: String)