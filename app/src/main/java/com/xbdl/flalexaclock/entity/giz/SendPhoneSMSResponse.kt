package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 发送短信验证码的回调
 * Created by xtagwgj on 2017/9/4.
 */
data class SendPhoneSMSResponse(val result: GizWifiErrorCode,  val token: String)