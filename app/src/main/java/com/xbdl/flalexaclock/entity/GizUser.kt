package com.xbdl.flalexaclock.entity

/**
 * 用户信息
 * Created by xtagwgj on 2017/8/7.
 */
data class GizUser(var username: String?, val lang: String = "cn", var remark: String?, val uid: String,
                   val is_anonymous: Boolean = false, var gender: String? = "M", val phone: String,
                   var birthday: String?, var address: String?, var email: String?, var name: String?)