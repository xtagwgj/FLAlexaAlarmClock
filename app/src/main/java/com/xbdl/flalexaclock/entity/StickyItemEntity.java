package com.xbdl.flalexaclock.entity;

/**
 * 粘性布局下具体的设置
 * Created by xtagwgj on 2017/9/19.
 */

public class StickyItemEntity extends StickyItemBaseEntity {
    private ClockEntity details;

    public StickyItemEntity(String title) {
        super(title);
    }

    public StickyItemEntity(String title, ClockEntity details) {
        super(title);
        this.details = details;
    }

    public ClockEntity getDetails() {
        return details;
    }

    public void setDetails(ClockEntity details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "StickyItemEntity{" +
                "details='" + details + '\'' +
                '}';
    }
}
