package com.xbdl.flalexaclock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.xbdl.flalexaclock.BR;

/**
 * 用户的分组信息
 * Created by xtagwgj on 2017/9/18.
 * <p>
 * {
 * "created_at": "2017-09-18T09:47:13Z",
 * "updated_at": "2017-09-18T09:47:13Z",
 * "product_key": "5adb74f926ac4ea0b03d719d5d4002c5",
 * "group_name": "测试分组",
 * "verbose_name": "智能闹钟",
 * "id": "59bf9621dc348f001a7d23ed"
 * }
 */
public class GroupInfoEntity extends BaseObservable {
    private String created_at;
    private String updated_at;
    private String product_key;
    private String group_name;
    private String verbose_name;
    private String id;

    @Bindable
    public String getCreated_at() {
        return created_at;
    }

    @Bindable
    public String getUpdated_at() {
        return updated_at;
    }

    @Bindable
    public String getProduct_key() {
        return product_key;
    }

    @Bindable
    public String getGroup_name() {
        return group_name;
    }

    @Bindable
    public String getVerbose_name() {
        return verbose_name;
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
        notifyPropertyChanged(BR.created_at);
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
        notifyPropertyChanged(BR.updated_at);
    }

    public void setProduct_key(String product_key) {
        this.product_key = product_key;
        notifyPropertyChanged(BR.product_key);
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
        notifyPropertyChanged(BR.group_name);
    }

    public void setVerbose_name(String verbose_name) {
        this.verbose_name = verbose_name;
        notifyPropertyChanged(BR.verbose_name);
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }
}
