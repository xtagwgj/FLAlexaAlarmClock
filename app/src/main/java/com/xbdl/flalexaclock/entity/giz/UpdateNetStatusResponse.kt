package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus

/**
 * 更新网络状态
 * Created by xtagwgj on 2017/9/4.
 */
data class UpdateNetStatusResponse(val device: GizWifiDevice?, val netStatus: GizWifiDeviceNetStatus?)