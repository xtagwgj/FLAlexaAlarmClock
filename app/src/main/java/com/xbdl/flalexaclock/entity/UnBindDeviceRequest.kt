package com.xbdl.flalexaclock.entity

/**
 * 解绑设备列表的请求
 * Created by xtagwgj on 2017/9/21.
 */

/*
{
  "devices": [
    {
      "did": "h7WGYYJdMwZasWvbGFMYaf1"
    }
  ]
}
 */

open class didEntity(val did: String)

data class UnBindDeviceRequest(val devices: List<didEntity>)

data class UnBindDeviceResponse(val failed: List<String>, val success: List<String>)