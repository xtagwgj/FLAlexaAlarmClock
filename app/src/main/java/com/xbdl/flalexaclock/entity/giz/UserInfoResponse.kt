package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizUserInfo
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 用户信息的响应
 * Created by xtagwgj on 2017/9/4.
 */
data class UserInfoResponse(val result: GizWifiErrorCode, val userInfo: GizUserInfo?)