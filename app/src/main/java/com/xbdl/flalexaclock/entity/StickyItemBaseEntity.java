package com.xbdl.flalexaclock.entity;

/**
 * 粘性布局的父类，只包含标题
 * Created by xtagwgj on 2017/9/19.
 */

public class StickyItemBaseEntity {
    private String title;

    public StickyItemBaseEntity(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "StickyItemBaseEntity{" +
                "title='" + title + '\'' +
                '}';
    }
}

