package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 设置设备的新
 * Created by xtagwgj on 2017/9/4.
 */
data class SetCustomInfoResponse(val result: GizWifiErrorCode, val device: GizWifiDevice?)