package com.xbdl.flalexaclock.entity;

/**
 * Created by xtagwgj on 2017/9/18.
 */

public class GizDataPointKey {

    /*
     * ===========================================================
	 * 以下key值对应开发者在云端定义的数据点标识名
	 * ===========================================================
	 */
    // 数据点"alarm_state"对应的标识名
    public static final String KEY_ALARM_STATE = "alarm_state";
    // 数据点"alarm_action"对应的标识名
    public static final String KEY_ALARM_ACTION = "alarm_action";
    // 数据点"alarm1_state"对应的标识名
    public static final String KEY_ALARM1_STATE = "alarm1_state";
    // 数据点"alarm2_state"对应的标识名
    public static final String KEY_ALARM2_STATE = "alarm2_state";
    // 数据点"alarm3_state"对应的标识名
    public static final String KEY_ALARM3_STATE = "alarm3_state";
    // 数据点"alarm4_state"对应的标识名
    public static final String KEY_ALARM4_STATE = "alarm4_state";
    // 数据点"alarm5_state"对应的标识名
    public static final String KEY_ALARM5_STATE = "alarm5_state";
    // 数据点"alarm6_state"对应的标识名
    public static final String KEY_ALARM6_STATE = "alarm6_state";
    // 数据点"alarm7_state"对应的标识名
    public static final String KEY_ALARM7_STATE = "alarm7_state";
    // 数据点"alarm8_state"对应的标识名
    public static final String KEY_ALARM8_STATE = "alarm8_state";
    // 数据点"alarm9_state"对应的标识名
    public static final String KEY_ALARM9_STATE = "alarm9_state";
    // 数据点"alarm10_state"对应的标识名
    public static final String KEY_ALARM10_STATE = "alarm10_state";
    // 数据点"alarm11_state"对应的标识名
    public static final String KEY_ALARM11_STATE = "alarm11_state";
    // 数据点"alarm12_state"对应的标识名
    public static final String KEY_ALARM12_STATE = "alarm12_state";
    // 数据点"time_zone"对应的标识名
    public static final String KEY_TIME_ZONE = "time_zone";
    // 数据点"main_alarm"对应的标识名
    public static final String KEY_MAIN_ALARM = "main_alarm";
    // 数据点"alarm1_set"对应的标识名
    public static final String KEY_ALARM1_SET = "alarm1_set";
    // 数据点"alarm2_set"对应的标识名
    public static final String KEY_ALARM2_SET = "alarm2_set";
    // 数据点"alarm3_set"对应的标识名
    public static final String KEY_ALARM3_SET = "alarm3_set";
    // 数据点"alarm4_set"对应的标识名
    public static final String KEY_ALARM4_SET = "alarm4_set";
    // 数据点"alarm5_set"对应的标识名
    public static final String KEY_ALARM5_SET = "alarm5_set";
    // 数据点"alarm6_set"对应的标识名
    public static final String KEY_ALARM6_SET = "alarm6_set";
    // 数据点"alarm7_set"对应的标识名
    public static final String KEY_ALARM7_SET = "alarm7_set";

    public static final String KEY_ALARM8_SET = "alarm8_set";
    // 数据点"alarm9_set"对应的标识名
    public static final String KEY_ALARM9_SET = "alarm9_set";
    // 数据点"alarm10_set"对应的标识名
    public static final String KEY_ALARM10_SET = "alarm10_set";
    // 数据点"alarm11_set"对应的标识名
    public static final String KEY_ALARM11_SET = "alarm11_set";
    // 数据点"alarm12_set"对应的标识名
    public static final String KEY_ALARM12_SET = "alarm12_set";
    // 数据点"main_alarm_report"对应的标识名
    public static final String KEY_MAIN_ALARM_REPORT = "main_alarm_report";
    // 数据点"alarm_state_report"对应的标识名
    public static final String KEY_ALARM_STATE_REPORT = "alarm_state_report";

    public static final String KEY_MAIN_ALARM_SET = "main_alarm_set";

    public static final String KEY_TIME_CALIBRATION = "time_calibration";

    public static final String KEY_DST = "DST";

    public static final String KEY_DEV_ID = "devID";
}
