package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 解绑分享的用户
 * Created by xtagwgj on 2017/9/4.
 */
data class UnBindUserResponse(val result: GizWifiErrorCode,val deviceID: String, val guestUID: String)