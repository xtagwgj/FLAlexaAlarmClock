package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 绑定或者解绑设备的回调
 * Created by xtagwgj on 2017/9/4.
 */
data class BindOrUnBindResponse(val result: GizWifiErrorCode, val isBind:Boolean,val did: String)