package com.xbdl.flalexaclock.entity;

import android.support.annotation.IntRange;

import com.xbdl.flalexaclock.utils.GizParseUtils;
import com.xbdl.flalexaclock.utils.HexStrUtils;
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 获取到的数据点
 * Created by xtagwgj on 2017/9/18.
 */

public class GizBaseEntity extends GizDataPointKey {


    public String did;

    /*
     * ===========================================================
     * 以下变量对应设备上报类型为布尔、数值、扩展数据点的数据存储
     * ===========================================================
     */
    //整体开启和关闭闹钟功能（由app或者语音控制），true表示打开，false表示关闭
    public boolean data_alarm_state;

    // 闹钟响铃是由语音引发的动作，true表示关闭闹钟，false表示小睡，如当前没有激活状态的闹钟，则忽略
    public boolean data_alarm_action;

    //TRUE:打开时间自动校准功能 FALSE：关闭时间自动校准功能
    public boolean data_time_calibration;

    //  true表示开启闹钟，false表示关闭闹钟
    public int data_alarm1_state;
    // 数据点"alarm2_state"对应的存储数据
    public int data_alarm2_state;
    // 数据点"alarm3_state"对应的存储数据
    public int data_alarm3_state;
    // 数据点"alarm4_state"对应的存储数据
    public int data_alarm4_state;
    // 数据点"alarm5_state"对应的存储数据
    public int data_alarm5_state;
    // 数据点"alarm6_state"对应的存储数据
    public int data_alarm6_state;
    // 数据点"alarm7_state"对应的存储数据
    public int data_alarm7_state;
    // 数据点"alarm8_state"对应的存储数据
    public int data_alarm8_state;
    // 数据点"alarm9_state"对应的存储数据
    public int data_alarm9_state;
    // 数据点"alarm10_state"对应的存储数据
    public int data_alarm10_state;
    // 数据点"alarm11_state"对应的存储数据
    public int data_alarm11_state;
    // 数据点"alarm12_state"对应的存储数据
    public int data_alarm12_state;
    // 数据点"main_alarm_state"对应的存储数据
    public int data_main_alarm_set;

    // 以GMT为准，0.5小时步长，东时区为：0-24，西时区为24-48.如东1区对应的值为：1/0.5=2，西1区对应的值为：24+1/0.5=26
    public int data_time_zone;

    // 用以APP调整设备闹钟，第0个字节代表分，第1个字节代表时。第2和第3字节为255。
    public byte[] data_main_alarm;
    // 用以APP调整设备闹钟，第1个字节（字节[0]）代表分，第2个字节（字节[1]）代表时，
    // 第3个字节（字节[2]）表示一周内的重复情况（周一：1，周二：2，周三：4，周四：8，周五：16，周六：32，周日：64，重复：128），多选求和
    public byte[] data_alarm1_set;
    // 数据点"alarm2_set"对应的存储数据
    public byte[] data_alarm2_set;
    // 数据点"alarm3_set"对应的存储数据
    public byte[] data_alarm3_set;
    // 数据点"alarm4_set"对应的存储数据
    public byte[] data_alarm4_set;
    // 数据点"alarm5_set"对应的存储数据
    public byte[] data_alarm5_set;
    // 数据点"alarm6_set"对应的存储数据
    public byte[] data_alarm6_set;
    // 数据点"alarm7_set"对应的存储数据
    public byte[] data_alarm7_set;
    // 数据点"alarm8_set"对应的存储数据
    public byte[] data_alarm8_set;
    // 数据点"alarm9_set"对应的存储数据
    public byte[] data_alarm9_set;
    // 数据点"alarm10_set"对应的存储数据
    public byte[] data_alarm10_set;
    // 数据点"alarm11_set"对应的存储数据
    public byte[] data_alarm11_set;
    // 数据点"alarm12_set"对应的存储数据
    public byte[] data_alarm12_set;

    public boolean dst;

    public int devID;


    @SuppressWarnings("unchecked")
    public GizBaseEntity getDataFromReceiveDataMap(ConcurrentHashMap<String, Object> dataMap) {
        // 已定义的设备数据点，有布尔、数值和枚举型数据

        if (dataMap.get("data") != null) {
            ConcurrentHashMap<String, Object> map = (ConcurrentHashMap<String, Object>) dataMap.get("data");
            getDataFromDataMap(map);
        }

        return this;
    }

    private void getDataFromDataMap(ConcurrentHashMap<String, Object> map) {
        // 已定义的设备数据点，有布尔、数值和枚举型数据
        for (String dataKey : map.keySet()) {
            if (dataKey.equals(KEY_ALARM_STATE)) {
                data_alarm_state = (Boolean) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM_ACTION)) {
                data_alarm_action = (Boolean) map.get(dataKey);
            }
            if (dataKey.equals(KEY_TIME_CALIBRATION)) {
                data_time_calibration = (Boolean) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM1_STATE)) {

                data_alarm1_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM2_STATE)) {

                data_alarm2_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM3_STATE)) {

                data_alarm3_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM4_STATE)) {

                data_alarm4_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM5_STATE)) {

                data_alarm5_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM6_STATE)) {

                data_alarm6_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM7_STATE)) {

                data_alarm7_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM8_STATE)) {

                data_alarm8_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM9_STATE)) {

                data_alarm9_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM10_STATE)) {

                data_alarm10_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM11_STATE)) {

                data_alarm11_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM12_STATE)) {

                data_alarm12_state = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_MAIN_ALARM_SET)) {

                data_main_alarm_set = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_TIME_ZONE)) {

                data_time_zone = (Integer) map.get(dataKey);
            }
            if (dataKey.equals(KEY_MAIN_ALARM)) {

                data_main_alarm = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM1_SET)) {

                data_alarm1_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM2_SET)) {

                data_alarm2_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM3_SET)) {

                data_alarm3_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM4_SET)) {

                data_alarm4_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM5_SET)) {

                data_alarm5_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM6_SET)) {

                data_alarm6_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM7_SET)) {

                data_alarm7_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM9_SET)) {

                data_alarm9_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM10_SET)) {

                data_alarm10_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM11_SET)) {

                data_alarm11_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM12_SET)) {

                data_alarm12_set = (byte[]) map.get(dataKey);
            }
            if (dataKey.equals(KEY_ALARM8_SET)) {

                data_alarm8_set = (byte[]) map.get(dataKey);
            }

            if (dataKey.equals(KEY_DST)) {
                dst = (Boolean) map.get(dataKey);
            }

            if (dataKey.equals(KEY_DEV_ID)) {
                devID = (Integer) map.get(dataKey);
            }


        }
    }

    public AlarmDetailsEntity getMainAlarm(String did) {

        AlarmDetailsEntity alarm = GizParseUtils.INSTANCE.getAlarm(HexStrUtils.bytesToHexString(data_main_alarm),
                data_main_alarm_set, true, 0, did);

        alarm.setTimeZone(TimeZoneTableUtils.INSTANCE.getCurrTimeZone(data_time_zone));
        alarm.setGroupAlarm(data_alarm_state);
        alarm.setDevID(devID);
        return alarm;
    }

    public AlarmDetailsEntity getSubAlarm(@IntRange(from = 1, to = 12) int index, String did) {

        byte[] hexString = null;
        int state = 1;
        switch (index) {
            case 1:
                hexString = data_alarm1_set;
                state = data_alarm1_state;
                break;

            case 2:
                hexString = data_alarm2_set;
                state = data_alarm2_state;
                break;

            case 3:
                hexString = data_alarm3_set;
                state = data_alarm3_state;
                break;

            case 4:
                hexString = data_alarm4_set;
                state = data_alarm4_state;
                break;

            case 5:
                hexString = data_alarm5_set;
                state = data_alarm5_state;
                break;

            case 6:
                hexString = data_alarm6_set;
                state = data_alarm6_state;
                break;

            case 7:
                hexString = data_alarm7_set;
                state = data_alarm7_state;
                break;

            case 8:
                hexString = data_alarm8_set;
                state = data_alarm8_state;
                break;

            case 9:
                hexString = data_alarm9_set;
                state = data_alarm9_state;
                break;

            case 10:
                hexString = data_alarm10_set;
                state = data_alarm10_state;
                break;

            case 11:
                hexString = data_alarm11_set;
                state = data_alarm11_state;
                break;

            case 12:
                hexString = data_alarm12_set;
                state = data_alarm12_state;
                break;
        }

        AlarmDetailsEntity alarm = GizParseUtils.INSTANCE.getAlarm(HexStrUtils.bytesToHexString(hexString),
                state, false, index, did);
        alarm.setTimeZone(TimeZoneTableUtils.INSTANCE.getCurrTimeZone(data_time_zone));
        alarm.setGroupAlarm(data_alarm_state);
        alarm.setDevID(devID);
        return alarm;
    }

    @Override
    public String toString() {
        return "GizBaseEntity{" +
                "did='" + did + '\'' +
                ", data_alarm_state=" + data_alarm_state +
                ", data_time_calibration=" + data_time_calibration +
                ", data_alarm_action=" + data_alarm_action +
                ", data_alarm1_state=" + data_alarm1_state +
                ", data_alarm2_state=" + data_alarm2_state +
                ", data_alarm3_state=" + data_alarm3_state +
                ", data_alarm4_state=" + data_alarm4_state +
                ", data_alarm5_state=" + data_alarm5_state +
                ", data_alarm6_state=" + data_alarm6_state +
                ", data_alarm7_state=" + data_alarm7_state +
                ", data_alarm8_state=" + data_alarm8_state +
                ", data_alarm9_state=" + data_alarm9_state +
                ", data_alarm10_state=" + data_alarm10_state +
                ", data_alarm11_state=" + data_alarm11_state +
                ", data_alarm12_state=" + data_alarm12_state +
                ", data_main_alarm_set=" + data_main_alarm_set +
                ", data_time_zone=" + data_time_zone +
                ", data_main_alarm=" + Arrays.toString(data_main_alarm) +
                ", data_alarm1_set=" + Arrays.toString(data_alarm1_set) +
                ", data_alarm2_set=" + Arrays.toString(data_alarm2_set) +
                ", data_alarm3_set=" + Arrays.toString(data_alarm3_set) +
                ", data_alarm4_set=" + Arrays.toString(data_alarm4_set) +
                ", data_alarm5_set=" + Arrays.toString(data_alarm5_set) +
                ", data_alarm6_set=" + Arrays.toString(data_alarm6_set) +
                ", data_alarm7_set=" + Arrays.toString(data_alarm7_set) +
                ", data_alarm8_set=" + Arrays.toString(data_alarm8_set) +
                ", data_alarm9_set=" + Arrays.toString(data_alarm9_set) +
                ", data_alarm10_set=" + Arrays.toString(data_alarm10_set) +
                ", data_alarm11_set=" + Arrays.toString(data_alarm11_set) +
                ", data_alarm12_set=" + Arrays.toString(data_alarm12_set) +
                ", dst=" + dst +
                ", devID=" + devID +
                '}';
    }
}