package com.xbdl.flalexaclock.entity

/**
 * 分组的设备信息
 * Created by xtagwgj on 2017/9/28.
 */
/*
did	string	该分组下的设备ID
type	string	设备类型：普通设备：noramal；中控设备：center_control；中控子设备：sub_dev
verbose_name	string	产品名称，单PK的分组才会有值
dev_alias	string	设备别名
product_key	string	产品 product_key，单PK的分组才会有值
 */
data class GroupDeviceEntity(
        val did: String,
        val type: String,
        val verbose_name: String,
        val dev_alias: String,
        val product_key: String)