package com.xbdl.flalexaclock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.xbdl.flalexaclock.BR;

/**
 * 闹铃的实体类
 * Created by xtagwgj on 2017/9/11.
 */

public class AlarmEntity extends BaseObservable {

    private int id;
    private String time;
    private boolean am;
    private String day;
    private String alarmName;
    private String groupName;
    private boolean close;

    public AlarmEntity(String time, boolean am, String day, String alarmName, String groupName, boolean close) {
        this.time = time;
        this.am = am;
        this.day = day;
        this.alarmName = alarmName;
        this.groupName = groupName;
        this.close = close;
    }

    @Bindable
    public int getId() {
        return id;
    }

    @Bindable
    public String getTime() {
        return time;
    }


    @Bindable
    public String getDay() {
        return day;
    }

    @Bindable
    public String getAlarmName() {
        return alarmName;
    }

    @Bindable
    public String getGroupName() {
        return groupName;
    }

    @Bindable
    public boolean isAm() {
        return am;
    }

    @Bindable
    public boolean isClose() {
        return close;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public void setTime(String time) {
        this.time = time;
        notifyPropertyChanged(BR.time);
    }


    public void setDay(String day) {
        this.day = day;
        notifyPropertyChanged(BR.day);
    }

    public void setAlarmName(String alarmName) {
        this.alarmName = alarmName;
        notifyPropertyChanged(BR.alarmName);
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
        notifyPropertyChanged(BR.groupName);
    }


    public void setAm(boolean am) {
        this.am = am;
        notifyPropertyChanged(BR.am);
    }

    public void setClose(boolean close) {
        this.close = close;
        notifyPropertyChanged(BR.close);
    }
}
