package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import java.util.concurrent.ConcurrentHashMap

/**
 * 接收的数据
 * Created by xtagwgj on 2017/9/4.
 */
data class ReceiveDataResponse(val result: GizWifiErrorCode, val device: GizWifiDevice?, val dataMap: ConcurrentHashMap<String, Any>?, val sn: Int)