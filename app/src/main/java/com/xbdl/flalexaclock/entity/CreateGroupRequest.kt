package com.xbdl.flalexaclock.entity

/**
 * 创建分组
 * Created by xtagwgj on 2017/9/28.
 */
data class CreateGroupRequest(val product_key: String, val group_name: String)