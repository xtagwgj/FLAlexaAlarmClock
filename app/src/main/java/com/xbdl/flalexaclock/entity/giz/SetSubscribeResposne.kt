package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 订阅设备
 * Created by xtagwgj on 2017/9/4.
 */
data class SetSubscribeResposne(val result: GizWifiErrorCode, val device: GizWifiDevice?, val isSubscribed: Boolean)