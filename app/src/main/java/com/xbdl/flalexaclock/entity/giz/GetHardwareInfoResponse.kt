package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode
import java.util.concurrent.ConcurrentHashMap

/**
 * 获取硬件信息
 * Created by xtagwgj on 2017/9/4.
 */
data class GetHardwareInfoResponse (val result: GizWifiErrorCode,val device: GizWifiDevice?,val hardwareInfo: ConcurrentHashMap<String, String>?)