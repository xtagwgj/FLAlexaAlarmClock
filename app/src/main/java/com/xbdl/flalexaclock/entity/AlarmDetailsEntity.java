package com.xbdl.flalexaclock.entity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;

import com.gizwits.gizwifisdk.api.GizWifiDevice;
import com.xbdl.flalexaclock.BR;
import com.xbdl.flalexaclock.base.App;
import com.xbdl.flalexaclock.utils.ClockUtils;
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils;
import com.xtagwgj.baseproject.utils.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 闹铃的实体
 *
 * @author xtagwgj
 *         on 2017/9/20.
 */

public class AlarmDetailsEntity extends BaseObservable implements Parcelable, Comparator<AlarmDetailsEntity> {


    private boolean mainAlarm = false;

    //alarmFragment 判别设备使用
    private String did;

    private int hour;
    private int minute;
    private int second;

    //1：开启 2：关闭 3：删除  主闹钟只有1和2
    private int state;

    //是否是单次
    private boolean single;

    //重复的是星期几 共7位
    private String retryInfo;

    //表示是第几个闹钟 -1是闹钟的开启关闭 0是主闹钟的开启关闭  -2为为选择闹铃
    private int pos = 0;

    //所属的时区
    private TimeZone timeZone = TimeZone.getDefault();

    //设备类型
    private int devID = 1;

    /**
     * 主闹钟的开关
     * 开时：
     * 关时：每个闹铃的状态不变，但是闹铃都变为灰色的状态,点击的时候提醒用户
     */
    private boolean groupAlarm;

    protected AlarmDetailsEntity(Parcel in) {
        mainAlarm = in.readByte() != 0;
        did = in.readString();
        hour = in.readInt();
        minute = in.readInt();
        second = in.readInt();
        state = in.readInt();
        single = in.readByte() != 0;
        retryInfo = in.readString();
        pos = in.readInt();
        groupAlarm = in.readByte() != 0;
        devID = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (mainAlarm ? 1 : 0));
        dest.writeString(did);
        dest.writeInt(hour);
        dest.writeInt(minute);
        dest.writeInt(second);
        dest.writeInt(state);
        dest.writeByte((byte) (single ? 1 : 0));
        dest.writeString(retryInfo);
        dest.writeInt(pos);
        dest.writeByte((byte) (groupAlarm ? 1 : 0));
        dest.writeInt(devID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AlarmDetailsEntity> CREATOR = new Creator<AlarmDetailsEntity>() {
        @Override
        public AlarmDetailsEntity createFromParcel(Parcel in) {
            return new AlarmDetailsEntity(in);
        }

        @Override
        public AlarmDetailsEntity[] newArray(int size) {
            return new AlarmDetailsEntity[size];
        }
    };

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    //07:45
    public String getShowTime() {
        if (is24()) {
            return String.format(Locale.ENGLISH, "%1$02d:%2$02d", hour, minute);
        } else if (hour == 0) {
            return String.format(Locale.ENGLISH, "%1$02d:%2$02d", 12, minute);
        } else {
            return String.format(Locale.ENGLISH, "%1$02d:%2$02d", (hour > 12) ? hour - 12 : hour, minute);
        }
    }

    public String getTimeUnit() {
        if (is24()) {
            return "";
        } else if (hour >= 12) {
            return "PM";
        } else {
            return "AM";
        }
    }

    private boolean is24() {
        return DateFormat.is24HourFormat(App.instance);
    }

    public String getRetryWeek() {
        if (StringUtils.isEmpty(retryInfo)) {
            return "";
        }

        StringBuilder sb = new StringBuilder("");

        //        Mon Tue Wed Thu Fri Sat
        for (int i = 0; i < retryInfo.length(); i++) {

            if (!"1".equalsIgnoreCase(retryInfo.charAt(i) + "")) {
                continue;
            }
            switch (i) {
                case 0:
                    sb.append("Sun ");
                    break;

                case 1:
                    sb.append("Sat ");
                    break;

                case 2:
                    sb.append("Fri ");
                    break;

                case 3:
                    sb.append("Thu ");
                    break;
                case 4:
                    sb.append("Wed ");
                    break;

                case 5:
                    sb.append("Tue ");
                    break;

                case 6:
                    sb.append("Mon ");
                    break;
                default:
                    break;
            }
        }

        return sb.toString();
    }

    public AlarmDetailsEntity() {
    }


    @Bindable
    public boolean isMainAlarm() {
        return mainAlarm;
    }

    @Bindable
    public int getHour() {
        return hour;
    }

    @Bindable
    public int getMinute() {
        return minute;
    }

    @Bindable
    public int getSecond() {
        return second;
    }

    @Bindable
    public int getState() {
        return state;
    }

    public boolean showButton() {
        if (isMainAlarm()) {
            return state == 1;
        } else {
            return groupAlarm && state == 1;
        }
    }

    @Bindable
    public boolean isSingle() {
        return single;
    }

    @Bindable
    public boolean isEveryDay() {
        return !retryInfo.contains("0");
    }

    @Bindable
    public String getRetryInfo() {
        return retryInfo;
    }

    public void setMainAlarm(boolean mainAlarm) {
        this.mainAlarm = mainAlarm;
        notifyPropertyChanged(BR.mainAlarm);
    }

    public void setHour(int hour) {
        this.hour = hour;
        notifyPropertyChanged(BR.hour);
    }

    public void setMinute(int minute) {
        this.minute = minute;
        notifyPropertyChanged(BR.minute);
    }

    public void setSecond(int second) {
        this.second = second;
        notifyPropertyChanged(BR.second);
    }

    public void setState(int state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    public void setSingle(boolean single) {
        this.single = single;
        notifyPropertyChanged(BR.single);

    }

    public void setRetryInfo(String retryInfo) {
        this.retryInfo = retryInfo;
        notifyPropertyChanged(BR.retryInfo);
    }

    @Bindable
    public int getDevID() {
        return devID;
    }

    public void setDevID(int devID) {
        this.devID = devID;
        notifyPropertyChanged(BR.devID);
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public boolean isGroupAlarm() {
        return groupAlarm;
    }

    public AlarmDetailsEntity setGroupAlarm(boolean groupAlarm) {
        this.groupAlarm = groupAlarm;
        return this;
    }

    /**
     * 获取当前闹钟的设备信息
     *
     * @return 可为null
     */
    public GizWifiDevice getCurrDevice() {
        if (StringUtils.isEmpty(did)) {
            return null;
        }

        List<GizWifiDevice> deviceList = ClockUtils.INSTANCE.getDeviceList();

        for (GizWifiDevice gizWifiDevice : deviceList) {
            if (gizWifiDevice.getDid().equals(did)) {
                return gizWifiDevice;
            }
        }

        return null;
    }

    public String getGroupName() {
        return getGroupName(false);
    }

    public String getGroupName(boolean withDeviceName) {
        if (!withDeviceName) {
            return ClockUtils.INSTANCE.getGroupName(did);
        } else {

            String name = ClockUtils.INSTANCE.getDeviceName(did);

            if (!StringUtils.isEmpty(name)) {
                name = name + "  |  ";
            }

            return name + ClockUtils.INSTANCE.getGroupName(did);
        }
    }

    @Override
    public String toString() {
        return "AlarmDetailsEntity{" +
                "mainAlarm=" + mainAlarm +
                ", did='" + did + '\'' +
                ", hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                ", state=" + state +
                ", single=" + single +
                ", retryInfo='" + retryInfo + '\'' +
                ", pos=" + pos +
                ", groupAlarm=" + groupAlarm +
                ", devID=" + devID +
                '}';
    }


    @Override
    public int compare(AlarmDetailsEntity o1, AlarmDetailsEntity o2) {
        if (o1.getHour() * 60 + o1.getMinute() > o2.getHour() * 60 + o2.getMinute()) {
            return o1.getHour() * 60 + o1.getMinute() - o2.getHour() * 60 + o2.getMinute();
        } else if (StringUtils.equals(o1.getDid(), o2.getDid())) {
            return -o1.pos + o2.pos;
        } else {
            return o1.getDid().compareTo(o2.getDid());
        }
    }

    /**
     * 获取下一次响铃的时间，闹钟是打开状态才获取
     *
     * @return
     */
    public String getNextRingTime() {

        if (state != 1) {
            return "Alarm is closed";
        }

        if (!retryInfo.contains("1")) {
            return "Not set the alarm time";
        }

        //设定为闹钟当前的时区
        TimeZone.setDefault(timeZone);
        //获取该时区的日历
        Calendar calendar = Calendar.getInstance();

        List<Long> timeMills = getRetryTimeInMilles();

        long currTimeInMillis = calendar.getTimeInMillis();
        long minTime = 0L;
        for (Long timeMill : timeMills) {
            if (timeMill >= currTimeInMillis) {
                //第一个匹配的时间
                if (minTime == 0L) {
                    minTime = timeMill;
                } else if (timeMill < minTime) {
                    //找到比当前最小时间还小的
                    minTime = timeMill;
                }
            }
        }

        return TimeZoneTableUtils.INSTANCE.getTimeDifferent(currTimeInMillis, minTime);
    }


    /**
     * @return
     */
    private List<Long> getRetryTimeInMilles() {
        //设定为闹钟当前的时区
        TimeZone.setDefault(timeZone);

        //获取该时区的日历
        Calendar calendar = Calendar.getInstance();
//        LogUtils.e("self_calendar", calendar.getTime());
        //获取今天是星期几
        boolean isFirstSunday = (calendar.getFirstDayOfWeek() == Calendar.SUNDAY);
        //获取周几
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        //若一周第一天为星期天，则-1
        if (isFirstSunday) {
            weekDay = weekDay - 1;
            if (weekDay == 0) {
                weekDay = 7;
            }
        }

        //这是今天所处的位置，小于等于dayposition的是本周，大于dayPosition的是下周
        int dayPosition = 7 - weekDay;

        List<Long> timeList = new ArrayList<>();
        for (int i = 0; i < retryInfo.length(); i++) {
            //当前选中
            if ("1".equals(retryInfo.charAt(i) + "")) {
                //设定为闹钟当前的时区
                TimeZone.setDefault(timeZone);
                calendar = Calendar.getInstance();

                if (i > dayPosition) {
                    calendar.add(Calendar.WEEK_OF_MONTH, 1);
                }

                if (isFirstSunday) {
                    calendar.set(Calendar.DAY_OF_WEEK, (7 - i) % 7);
                } else {
                    calendar.set(Calendar.DAY_OF_WEEK, 7 - i);
                }

                calendar.add(Calendar.DATE, 1);
                calendar.set(Calendar.MILLISECOND, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MINUTE, minute);
                calendar.set(Calendar.HOUR_OF_DAY, hour);

//                LogUtils.e("self_calendar_cal", "curr = " + calendar.getTime() +
//                        " ,long = " + calendar.getTimeInMillis() + " ,now = " + System.currentTimeMillis());
                timeList.add(calendar.getTimeInMillis());
            }
        }

        return timeList;
    }
}