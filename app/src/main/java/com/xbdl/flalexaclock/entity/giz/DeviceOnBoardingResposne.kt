package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 配网的响应
 * Created by xtagwgj on 2017/9/4.
 */
data class DeviceOnBoardingResposne (val result: GizWifiErrorCode, val mac: String, val did: String, val productKey: String)