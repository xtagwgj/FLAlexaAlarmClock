package com.xbdl.flalexaclock.entity;

import android.support.annotation.IntRange;

import com.xbdl.flalexaclock.utils.GizParseUtils;
import com.xbdl.flalexaclock.utils.TimeZoneTableUtils;

/**
 * 数据点
 * Created by xtagwgj on 2017/9/20.
 */

public class GizAttr {
    private int alarm_state = 0;
    private int alarm_action;
    //校准功能
    private int time_calibration = 0;

    private int main_alarm_set = 2;
    private String main_alarm = "00000000";
    private int time_zone;

    private int alarm1_state = 3;
    private int alarm2_state = 3;
    private int alarm3_state = 3;
    private int alarm4_state = 3;
    private int alarm5_state = 3;
    private int alarm6_state = 3;
    private int alarm7_state = 3;
    private int alarm8_state = 3;
    private int alarm9_state = 3;
    private int alarm10_state = 3;
    private int alarm11_state = 3;
    private int alarm12_state = 3;

    private String alarm1_set = "00000000";
    private String alarm2_set = "00000000";
    private String alarm3_set = "00000000";
    private String alarm4_set = "00000000";
    private String alarm5_set = "00000000";
    private String alarm6_set = "00000000";
    private String alarm7_set = "00000000";
    private String alarm8_set = "00000000";
    private String alarm9_set = "00000000";
    private String alarm10_set = "00000000";
    private String alarm11_set = "00000000";
    private String alarm12_set = "00000000";

    /**
     * 是否进入夏令时
     */
    private boolean dst = false;

    /**
     * 设备ID，用于区分不同的闹钟品类
     */
    private int devID = 1;

    public void setAlarm7_state(int alarm7_state) {
        this.alarm7_state = alarm7_state;
    }

    public int getAlarm7_state() {
        return alarm7_state;
    }

    public void setAlarm9_state(int alarm9_state) {
        this.alarm9_state = alarm9_state;
    }

    public int getAlarm9_state() {
        return alarm9_state;
    }

    public void setAlarm11_state(int alarm11_state) {
        this.alarm11_state = alarm11_state;
    }

    public int getAlarm11_state() {
        return alarm11_state;
    }

    public void setAlarm5_set(String alarm5_set) {
        this.alarm5_set = alarm5_set;
    }

    public String getAlarm5_set() {
        return alarm5_set;
    }

    public void setAlarm3_state(int alarm3_state) {
        this.alarm3_state = alarm3_state;
    }

    public int getAlarm3_state() {
        return alarm3_state;
    }

    public void setAlarm10_set(String alarm10_set) {
        this.alarm10_set = alarm10_set;
    }

    public String getAlarm10_set() {
        return alarm10_set;
    }

    public void setAlarm10_state(int alarm10_state) {
        this.alarm10_state = alarm10_state;
    }

    public int getAlarm10_state() {
        return alarm10_state;
    }

    public void setAlarm6_set(String alarm6_set) {
        this.alarm6_set = alarm6_set;
    }

    public String getAlarm6_set() {
        return alarm6_set;
    }

    public void setAlarm4_state(int alarm4_state) {
        this.alarm4_state = alarm4_state;
    }

    public int getAlarm4_state() {
        return alarm4_state;
    }

    public void setAlarm5_state(int alarm5_state) {
        this.alarm5_state = alarm5_state;
    }

    public int getAlarm5_state() {
        return alarm5_state;
    }

    public void setAlarm9_set(String alarm9_set) {
        this.alarm9_set = alarm9_set;
    }

    public String getAlarm9_set() {
        return alarm9_set;
    }

    public void setAlarm_state(int alarm_state) {
        this.alarm_state = alarm_state;
    }

    public int getAlarm_state() {
        return alarm_state;
    }

    public void setMain_alarm(String main_alarm) {
        this.main_alarm = main_alarm;
    }

    public String getMain_alarm() {
        return main_alarm;
    }

    public void setAlarm1_set(String alarm1_set) {
        this.alarm1_set = alarm1_set;
    }

    public String getAlarm1_set() {
        return alarm1_set;
    }

    public void setAlarm6_state(int alarm6_state) {
        this.alarm6_state = alarm6_state;
    }

    public int getAlarm6_state() {
        return alarm6_state;
    }

    public void setAlarm3_set(String alarm3_set) {
        this.alarm3_set = alarm3_set;
    }

    public String getAlarm3_set() {
        return alarm3_set;
    }

    public void setAlarm2_set(String alarm2_set) {
        this.alarm2_set = alarm2_set;
    }

    public String getAlarm2_set() {
        return alarm2_set;
    }

    public void setAlarm12_state(int alarm12_state) {
        this.alarm12_state = alarm12_state;
    }

    public int getAlarm12_state() {
        return alarm12_state;
    }

    public void setAlarm_action(int alarm_action) {
        this.alarm_action = alarm_action;
    }

    public int getAlarm_action() {
        return alarm_action;
    }

    public void setAlarm8_state(int alarm8_state) {
        this.alarm8_state = alarm8_state;
    }

    public int getAlarm8_state() {
        return alarm8_state;
    }

    public void setAlarm8_set(String alarm8_set) {
        this.alarm8_set = alarm8_set;
    }

    public String getAlarm8_set() {
        return alarm8_set;
    }

    public void setAlarm2_state(int alarm2_state) {
        this.alarm2_state = alarm2_state;
    }

    public int getAlarm2_state() {
        return alarm2_state;
    }

    public void setAlarm4_set(String alarm4_set) {
        this.alarm4_set = alarm4_set;
    }

    public String getAlarm4_set() {
        return alarm4_set;
    }

    public void setAlarm1_state(int alarm1_state) {
        this.alarm1_state = alarm1_state;
    }

    public int getAlarm1_state() {
        return alarm1_state;
    }

    public void setMain_alarm_set(int main_alarm_set) {
        this.main_alarm_set = main_alarm_set;
    }

    public int getMain_alarm_set() {
        return main_alarm_set;
    }

    public void setAlarm11_set(String alarm11_set) {
        this.alarm11_set = alarm11_set;
    }

    public String getAlarm11_set() {
        return alarm11_set;
    }

    public void setTime_zone(@IntRange(from = 0, to = 48) int time_zone) {
        this.time_zone = time_zone;
    }

    public int getTime_zone() {
        return time_zone;
    }

    public void setAlarm7_set(String alarm7_set) {
        this.alarm7_set = alarm7_set;
    }

    public String getAlarm7_set() {
        return alarm7_set;
    }

    public void setAlarm12_set(String alarm12_set) {
        this.alarm12_set = alarm12_set;
    }

    public String getAlarm12_set() {
        return alarm12_set;
    }

    public int getTime_calibration() {
        return time_calibration;
    }

    public void setTime_calibration(int time_calibration) {
        this.time_calibration = time_calibration;
    }

    public boolean isDst() {
        return dst;
    }

    public void setDst(boolean dst) {
        this.dst = dst;
    }

    public int getDevID() {
        return devID;
    }

    public void setDevID(int devID) {
        this.devID = devID;
    }

    @Override
    public String toString() {
        return "GizAttr{" +
                "alarm_state=" + alarm_state +
                ", alarm_action=" + alarm_action +
                ", main_alarm_set=" + main_alarm_set +
                ", main_alarm='" + main_alarm + '\'' +
                ", time_zone=" + time_zone +
                ", alarm1_state=" + alarm1_state +
                ", alarm2_state=" + alarm2_state +
                ", alarm3_state=" + alarm3_state +
                ", alarm4_state=" + alarm4_state +
                ", alarm5_state=" + alarm5_state +
                ", alarm6_state=" + alarm6_state +
                ", alarm7_state=" + alarm7_state +
                ", alarm8_state=" + alarm8_state +
                ", alarm9_state=" + alarm9_state +
                ", alarm10_state=" + alarm10_state +
                ", alarm11_state=" + alarm11_state +
                ", alarm12_state=" + alarm12_state +
                ", alarm1_set='" + alarm1_set + '\'' +
                ", alarm2_set='" + alarm2_set + '\'' +
                ", alarm3_set='" + alarm3_set + '\'' +
                ", alarm4_set='" + alarm4_set + '\'' +
                ", alarm5_set='" + alarm5_set + '\'' +
                ", alarm6_set='" + alarm6_set + '\'' +
                ", alarm7_set='" + alarm7_set + '\'' +
                ", alarm8_set='" + alarm8_set + '\'' +
                ", alarm9_set='" + alarm9_set + '\'' +
                ", alarm10_set='" + alarm10_set + '\'' +
                ", alarm11_set='" + alarm11_set + '\'' +
                ", alarm12_set='" + alarm12_set + '\'' +
                ", time_calibration='" + time_calibration + '\'' +
                ", dst='" + dst + '\'' +
                ", devID='" + devID + '\'' +
                '}';
    }

    public AlarmDetailsEntity getMainAlarm(String did) {
        String hexString = main_alarm;
        if (hexString.length() != 8) {
            hexString = hexString + "00000000";
            hexString = hexString.substring(0, 8);
        }

        AlarmDetailsEntity alarm = GizParseUtils.INSTANCE.getAlarm(hexString,
                main_alarm_set, true, 0, did);
        alarm.setGroupAlarm(alarm_state == 1);
        alarm.setTimeZone(TimeZoneTableUtils.INSTANCE.getCurrTimeZone(time_zone));
        alarm.setDevID(devID);
        return alarm;
    }

    public AlarmDetailsEntity getSubAlarm(@IntRange(from = 1, to = 12) int index, String did) {

        String hexString = null;
        int state = 1;
        switch (index) {
            case 1:
                hexString = alarm1_set;
                state = alarm1_state;
                break;

            case 2:
                hexString = alarm2_set;
                state = alarm2_state;
                break;

            case 3:
                hexString = alarm3_set;
                state = alarm3_state;
                break;

            case 4:
                hexString = alarm4_set;
                state = alarm4_state;
                break;

            case 5:
                hexString = alarm5_set;
                state = alarm5_state;
                break;

            case 6:
                hexString = alarm6_set;
                state = alarm6_state;
                break;

            case 7:
                hexString = alarm7_set;
                state = alarm7_state;
                break;

            case 8:
                hexString = alarm8_set;
                state = alarm8_state;
                break;

            case 9:
                hexString = alarm9_set;
                state = alarm9_state;
                break;

            case 10:
                hexString = alarm10_set;
                state = alarm10_state;
                break;

            case 11:
                hexString = alarm11_set;
                state = alarm11_state;
                break;

            case 12:
                hexString = alarm12_set;
                state = alarm12_state;
                break;

            default:
                break;
        }

        if (hexString.length() != 8) {
            hexString = hexString + "00000000";
            hexString = hexString.substring(0, 8);
        }

        AlarmDetailsEntity alarm = GizParseUtils.INSTANCE.getAlarm(hexString, state, false, index, did);
        alarm.setTimeZone(TimeZoneTableUtils.INSTANCE.getCurrTimeZone(time_zone));
        alarm.setGroupAlarm(alarm_state == 1);
        alarm.setDevID(devID);
        return alarm;
    }
}