package com.xbdl.flalexaclock.entity.giz

import com.gizwits.gizwifisdk.enumration.GizWifiErrorCode

/**
 * 单一结果的回调
 * Created by xtagwgj on 2017/9/4.
 */
data class GizBaseResponse(val result: GizWifiErrorCode)