package com.xbdl.flalexaclock.utils

import android.net.Uri
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.xbdl.flalexaclock.base.App
import com.xbdl.flalexaclock.entity.User
import com.xtagwgj.baseproject.utils.StringUtils
import java.io.File

/**
 * 数据库 保存时区信息  用户的头像
 * Created by xtagwgj on 2017/10/16.
 */
object DbUtils {

    private var userValueEventListener: ValueEventListener? = null
    private var deviceValueEventListener: ValueEventListener? = null

    private val mDatabase: DatabaseReference by lazy {
        FirebaseDatabase.getInstance().getReference("AlarmClock")
    }

    private val mStorageRef: StorageReference by lazy {
        FirebaseStorage.getInstance().getReference("AlarmClock").child("avaters")
    }

    private fun getUserRef(uid: String): DatabaseReference = mDatabase.child("users").child(uid)

    /**
     * 获取具体设备的数据库映射
     */
    private fun getClockRef(did: String): DatabaseReference = mDatabase.child("device").child(did)


    private fun checkGoogleAvailable(): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = googleApiAvailability.isGooglePlayServicesAvailable(App.instance)
        return resultCode == ConnectionResult.SUCCESS
    }


    fun onStartUser(uid: String, userChange: (User?) -> Unit) {
        if (StringUtils.isEmpty(uid) || checkGoogleAvailable().not())
            return

        if (userValueEventListener == null) {
            userValueEventListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    userChange(dataSnapshot.getValue(User::class.java))
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            }
        }

        getUserRef(uid).addValueEventListener(userValueEventListener)
    }


    fun onStopUser(uid: String) {
        if (userValueEventListener != null && checkGoogleAvailable())
            getUserRef(uid).removeEventListener(userValueEventListener)
    }

    /**
     * 更新用户的id和头像 注册的是否也调用一下
     */
    fun updateUserIcon(uid: String, headUrl: String, isRegister: Boolean = false) {

        if (isRegister)
            getUserRef(uid).child("uid").setValue(uid)

        if (StringUtils.isEmpty(headUrl))
            getUserRef(uid).child("headUrl").removeValue()
        else
            getUserRef(uid).child("headUrl").setValue(headUrl)
    }

    /**
     * 上传用户头像
     */
    fun uploadUserAvatar(uid: String, file: File, uploadResult: (Boolean, String) -> Unit) {
        val avatarRef = mStorageRef.child(uid + ".jpg")

        avatarRef.putFile(Uri.fromFile(file))
                .addOnSuccessListener {
                    val downloadUrl = it.downloadUrl
                    uploadResult(true, downloadUrl.toString())
                    updateUserIcon(uid, downloadUrl.toString())
                }
                .addOnFailureListener {
                    uploadResult(false, it.message ?: "upload error")
                }

    }

}