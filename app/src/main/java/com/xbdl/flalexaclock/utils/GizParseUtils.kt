package com.xbdl.flalexaclock.utils

import com.xbdl.flalexaclock.base.PARSE_FROM_LEFT
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.http.HttpRequest
import com.xtagwgj.baseproject.utils.LogUtils
import io.reactivex.Observable

/**
 * 解析工具类
 * Created by xtagwgj on 2017/9/20.
 */
object GizParseUtils {
    /**
     * 设备did对应的分组id
     */
    private val deviceGroupInfo = mutableMapOf<String, String>()

    /**
     * 分组id对应的分组名
     */
    private val groupNameMap = mutableMapOf<String, String>()

    /**
     * 解析获取到闹钟
     * @param alarmHexString 16进制的
     * 第1个字节（字节[0]）代表分，第2个字节（字节[1]）代表时，
     * 第3个字节（字节[2]）表示一周内的重复情况（周一：1，周二：2，周三：4，周四：8，周五：16，周六：32，周日：64，重复：128），多选求和。
     *
     * 主闹钟：第2和第3字节为255。
     */
    fun getAlarm(alarmHexString: String, alarmState: Int, isMainAlarm: Boolean = false, alarmPos: Int = 0, did: String?): AlarmDetailsEntity {

        val alarmClock = AlarmDetailsEntity()
        alarmClock.did = did
        alarmClock.isMainAlarm = isMainAlarm
        alarmClock.state = alarmState

        var hexString = "${alarmHexString}00000000".substring(0,8)

        val minute = if (PARSE_FROM_LEFT)
            Integer.valueOf(hexString.substring(0, 2), 16)
        else
            Integer.valueOf(hexString.substring(6, 8), 16)

        val hour = if (PARSE_FROM_LEFT)
            Integer.valueOf(hexString.substring(2, 4), 16)
        else
            Integer.valueOf(hexString.substring(4, 6), 16)


        alarmClock.hour = hour
        alarmClock.minute = minute

        if (isMainAlarm) {
            alarmClock.isSingle = false
            alarmClock.retryInfo = "1111111"
        } else {
            //重复的时间
            val temp = "00000000" + (
                    if (PARSE_FROM_LEFT)
                        Integer.toBinaryString(Integer.valueOf(hexString.substring(4, 6), 16))
                    else
                        Integer.toBinaryString(Integer.valueOf(hexString.substring(2, 4), 16))
                    )
            val retryInfo = temp.substring(temp.length - 8)

            alarmClock.isSingle = retryInfo.indexOf("1") == retryInfo.lastIndexOf("1") && retryInfo.startsWith("0")
            alarmClock.retryInfo = retryInfo.substring(1)

        }

        alarmClock.pos = alarmPos

        return alarmClock
    }

    /**
     * 用来处理 设备 分组 和 分组名
     */
    fun initGroupInfo() {
        HttpRequest.findAllGroup()
                .flatMap {
                    groupNameMap.clear()
                    deviceGroupInfo.clear()

                    for (groupEntity in it) {
                        groupNameMap.put(groupEntity.id, groupEntity.group_name)
                    }
                    Observable.fromIterable(it)
                }
                .subscribe({ groupEntity ->
                    HttpRequest
                            .getGroupDevice(groupEntity.id)
                            .subscribe(
                                    {
                                        for ((did) in it) {
                                            deviceGroupInfo.put(did, groupEntity.id)
                                        }
                                    },
                                    {

                                    }
                            )
                }, {
                    it.printStackTrace()
                }, {
                    LogUtils.e("parse group", groupNameMap)
                    LogUtils.e("parse device", deviceGroupInfo)

                })


    }

    fun getGroupId(did: String) = deviceGroupInfo[did] ?: "0"

    fun getGroupName(did: String) = groupNameMap[getGroupId(did)] ?: "UnGrouped"

}