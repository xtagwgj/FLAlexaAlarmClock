package com.xbdl.flalexaclock.utils

import com.xbdl.flalexaclock.entity.CityCodeEntity
import java.lang.StringBuilder
import java.util.*

/**
 * 时区表格
 * Created by xtagwgj on 2017/9/21.
 */
object TimeZoneTableUtils {
    val codeTable = listOf(
            CityCodeEntity("LIS", "Lisbon", 0, 1F, "Europe/Lisbon", 256),
            CityCodeEntity("LON", "London", 0, 1F, "Europe/London", 512),

            CityCodeEntity("MAD", "Madrid", 2, 2F, "Europe/Madrid", 258),
            CityCodeEntity("PAR", "Paris", 2, 2F, "Europe/Paris", 514),
            CityCodeEntity("ROM", "Rome", 2, 2F, "Europe/Rome", 770),
            CityCodeEntity("BER", "Berlin", 2, 2F, "Europe/Berlin", 1026),
            CityCodeEntity("STO", "Stockholm", 2, 2F, "Europe/Stockholm", 1282),

            CityCodeEntity("ATH", "Athens", 4, 3F, "Europe/Athens", 260),
            CityCodeEntity("CAI", "Cairo", 4, 3F, "Africa/Cairo", 516),
            CityCodeEntity("JRS", "Jerusalem", 4, 3F, "Asia/Jerusalem", 772),

            CityCodeEntity("MOW", "Moscow", 6, 4F, "Europe/Moscow", 262),
            CityCodeEntity("JED", "Jeddah", 6, 0F, "Europe/Moscow", 518), //不实行夏令时，原来dstOffset为4

            CityCodeEntity("THR", "Tehran", 7, 0F, "Asia/Tehran", timeZoneWithCity = 263),
            CityCodeEntity("DXB", "Dubai", 8, 0F, "Asia/Dubai", timeZoneWithCity = 264),
            CityCodeEntity("KBL", "kabul", 9, 0F, "Asia/Kabul", timeZoneWithCity = 265),
            CityCodeEntity("KHI", "Karachi", 10, 0F, "Asia/Karachi", timeZoneWithCity = 266),
            CityCodeEntity("DEL", "Delhi", 11, 0F, "Asia/Kolkata", timeZoneWithCity = 267),
            CityCodeEntity("DAC", "Dhaka", 12, 0F, "Asia/Dhaka", timeZoneWithCity = 268),
            CityCodeEntity("RGN", "Yangon", 13, 0F, "Asia/Rangoon", timeZoneWithCity = 269),
            CityCodeEntity("BKK", "Bangkok", 14, 0F, "Asia/Bangkok", timeZoneWithCity = 270),

            CityCodeEntity("SIN", "Singapore", 16, 0F, "Asia/Singapore", timeZoneWithCity = 272),
            CityCodeEntity("HKG", "Hong Kong", 16, 0F, "Asia/Hong_Kong", timeZoneWithCity = 528),
            CityCodeEntity("BJS", "Beijing", 16, 0F, "Asia/Hong_Kong", timeZoneWithCity = 784),
            CityCodeEntity("TPE", "Taipei", 16, 0F, "Asia/Taipei", timeZoneWithCity = 1040),

            CityCodeEntity("SEL", "Seoul", 18, 0F, "Asia/Seoul", timeZoneWithCity = 274),
            CityCodeEntity("TYO", "Tokyo", 18, 0F, "Asia/Tokyo", timeZoneWithCity = 530),

            CityCodeEntity("ADL", "Adelaide", 19, 0F, "Australia/Adelaide", timeZoneWithCity = 275),

            CityCodeEntity("GUM", "Guam", 20, 0F, "Pacific/Guam", timeZoneWithCity = 276),
            CityCodeEntity("SYD", "Sydney", 20, 0F, "Australia/Sydney", timeZoneWithCity = 532),

            CityCodeEntity("NOU", "Noumea", 22, 0F, "Pacific/Noumea", timeZoneWithCity = 278),
            CityCodeEntity("WLG", "Wellington", 24, 0F, "Pacific/Auckland", timeZoneWithCity = 280),

            CityCodeEntity("PPG", "Pago Pago", 46, 0F, "Pacific/Pago_Pago", timeZoneWithCity = 302),
            CityCodeEntity("HNL", "Honolulu", 44, 0F, "Pacific/Honolulu", timeZoneWithCity = 300),

            CityCodeEntity("ANC", "Anchorage", 42, 8F, "America/Anchorage", timeZoneWithCity = 298),

            CityCodeEntity("YVR", "Vancouver", 40, -7F, "America/Vancouver", timeZoneWithCity = 296),
            CityCodeEntity("LAX", "Los Angeles", 40, -7F, "America/Los_Angeles", timeZoneWithCity = 552),

            CityCodeEntity("YEA", "Edmonton", 38, -6F, "America/Edmonton", timeZoneWithCity = 294),
            CityCodeEntity("DEN", "Denver", 38, -6F, "America/Denver", timeZoneWithCity = 550),

            CityCodeEntity("MEX", "Mexico City", 36, -5F, "America/Mexico_City", timeZoneWithCity = 292),
            CityCodeEntity("CHI", "Chicago", 36, -5F, "America/Chicago", timeZoneWithCity = 548),

            CityCodeEntity("MIA", "Miami", 34, -4F, "America/New_York", timeZoneWithCity = 290),
            CityCodeEntity("YTO", "Toronto", 34, -4F, "America/Toronto", timeZoneWithCity = 546),
            CityCodeEntity("NYC", "New York", 34, -4F, "America/New_York", timeZoneWithCity = 802),

            CityCodeEntity("SCL", "Santiago", 32, -3F, "America/Santiago", timeZoneWithCity = 288),
            CityCodeEntity("YHZ", "Halifax", 32, -3F, "America/Halifax", timeZoneWithCity = 544),

            CityCodeEntity("YYT", "St.Johns", 31, -2.5F, "America/St_Johns", timeZoneWithCity = 287),
            CityCodeEntity("RIO", "Rio De Janeiro", 30, -2F, "America/Sao_Paulo", timeZoneWithCity = 286),
            CityCodeEntity("RAI", "Praia", 26, 0F, "Atlantic/Cape_Verde", timeZoneWithCity = 282)
    )

    /**
     * 根据时区获取城市时区的列表
     * @param timeZone 时区代码
     * @param is48 是否是48制的
     * @return 时区表的集合 keweinull
     */
    fun getByTimeZone(timeZone: Int, is48: Boolean): List<CityCodeEntity>? {
        val realZone = when {
            is48 -> timeZone
            timeZone >= 0 -> timeZone * 2
            else -> 24 - timeZone * 2
        }
        return codeTable.filter { it.timeZone == realZone }
    }

    fun getByTimeZone(timeZoneCity: Int): CityCodeEntity {
        return codeTable.firstOrNull { it.timeZoneWithCity == timeZoneCity } ?: codeTable.first()
    }

    fun getIdByTimeZoneWithCity(timeZoneCity: Int): String {
        return codeTable.firstOrNull { it.timeZoneWithCity == timeZoneCity }?.timeZoneId ?: codeTable.first().timeZoneId
    }

    /**
     * 根据数据点中的时区，切换为正常的时区东为正，西为负
     */
    fun getRealTimeZone(timeZone48: Int): Float {
        return if (timeZone48 <= 24)
            return timeZone48 / 2F
        else
            (timeZone48 - 24) / -2F
    }

    /**
     * 转换成时区要显示的格式GMT+0800 或 GMT-0800
     */
    fun getRealTimeZoneStr(timeZone48: Int): String {

        val zone = if (timeZone48 <= 24)
            timeZone48 / 2F
        else (timeZone48 - 24) / 2F

        val sBuilder = StringBuilder()

        if (zone.toInt() < 10) {
            sBuilder.append("0${zone.toInt()}")
        } else
            sBuilder.append("${zone.toInt()}")

        if (zone.toInt() * 1F == zone) {
            sBuilder.append("00")
        } else {
            sBuilder.append((zone - zone.toInt()) * 60)
        }

//        return sBuilder.toString()

        return if (timeZone48 <= 24)
            "GMT+" + sBuilder.toString()
        else
            "GMT-" + sBuilder.toString()
    }

    fun getRealTimeZoneStrByCity(timeZoneCity: Int): String {

        val timeZone48 = codeTable.firstOrNull { it.timeZoneWithCity == timeZoneCity }?.timeZone

        return if (timeZone48 != null)
            getRealTimeZoneStr(timeZone48)
        else {
//            TimeZone.getDefault().id

            codeTable.first().timeZoneId
        }
    }

    /**
     * 获取指定城市的时区id
     */
    fun getCurrTimeZoneId(cityCodeEntity: CityCodeEntity): TimeZone {

        var timeZoneId = getRealTimeZoneStr(cityCodeEntity.timeZone)
        if (cityCodeEntity.dstOffset != 0F)
            timeZoneId = cityCodeEntity.timeZoneId

        return TimeZone.getTimeZone(timeZoneId)
    }

    fun getCurrTimeZone(timeZoneCity: Int): TimeZone {
//        val cityCodeEntity = getByTimeZone(timeZone, is48)?.firstOrNull() ?: return TimeZone.getDefault()

        val cityCodeEntity = codeTable.firstOrNull { it.timeZoneWithCity == timeZoneCity }

        return if (cityCodeEntity == null) {
//            TimeZone.getDefault()
            TimeZone.getTimeZone(codeTable.first().timeZoneId)
        } else {
            var timeZoneId = getRealTimeZoneStr(cityCodeEntity.timeZone)
            if (cityCodeEntity.dstOffset != 0F)
                timeZoneId = cityCodeEntity.timeZoneId

            TimeZone.getTimeZone(timeZoneId)
        }
    }


    /**
     * 计算到当前时间段的
     */
    fun getTimeDifferent(from: Long, to: Long): String {

        val timeBuilder = StringBuilder()
        if (from > to) {//已经结束了
            timeBuilder.append("Alarm is over")
        } else {
            val aboveSevenDays = to - from > 86400000 * 7

            val day = ((to - from) / 86400000).toInt()
            val hour = ((to - from - day * 86400000) / 3600000).toInt()
            val minute = ((to - from - day * 86400000 - hour * 3600000) / 60000).toInt()

            //Alarm in 15 hours 40 minutes
            timeBuilder.append("Alarm in ")
            if (day > 0 && aboveSevenDays.not()) {
                timeBuilder.append(day)
                if (day == 1)
                    timeBuilder.append(" day ")
                else
                    timeBuilder.append(" days ")
            }

            if (hour > 0) {
                timeBuilder.append(hour)
                timeBuilder.append(" hours ")
            }

            timeBuilder.append(minute)
            timeBuilder.append(" minutes ")
        }

        return timeBuilder.toString()
    }
}