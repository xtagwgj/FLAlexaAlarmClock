package com.xbdl.flalexaclock.utils

import com.gizwits.gizwifisdk.api.GizWifiDevice
import com.gizwits.gizwifisdk.enumration.GizWifiDeviceNetStatus
import com.xbdl.flalexaclock.BuildConfig
import com.xbdl.flalexaclock.ParseDataService
import com.xbdl.flalexaclock.base.*
import com.xbdl.flalexaclock.entity.AlarmDetailsEntity
import com.xbdl.flalexaclock.entity.ClockEntity
import com.xbdl.flalexaclock.entity.GizBaseEntity
import com.xbdl.flalexaclock.entity.GroupEntity
import com.xbdl.flalexaclock.extensions.getDeviceName
import com.xbdl.flalexaclock.http.HttpRequest
import com.xtagwgj.baseproject.base.RxBus
import com.xtagwgj.baseproject.utils.LogUtils
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

/**
 * 闹钟的工具类
 * @author xtagwgj
 *      on 2017/11/6.
 */
object ClockUtils {

    private val TAG = "self_ClockUtils"

    var uid: String = ""
    var token: String = ""

    /**
     * 设备列表
     */
    val deviceList = mutableListOf<GizWifiDevice>()

    /**
     * 在线设备 did 对应的数据点
     */
    val onlineDevicePoint = mutableMapOf<String, GizBaseEntity>()

    /**
     * 待绑定的设备 mac 列表
     */
    val bindDeviceMacList = mutableListOf<String>()

    /**
     * 闹钟 did 对应的闹铃数据
     */
    val clockAlarmMap = mutableMapOf<String, ClockEntity>()


    /**
     *分组设备 did 列表
     */
    val groupDeviceMap = mutableMapOf<GroupEntity, MutableList<String>>()


    /**
     * 临时存放数据的map
     */
    val tempMap = mutableMapOf<String, String>()

    /**
     * 重置 用户注销的时候使用
     */
    fun doReset() {
        deviceList.clear()
        onlineDevicePoint.clear()
        bindDeviceMacList.clear()
        clockAlarmMap.clear()
        groupDeviceMap.clear()
    }


    fun getDeviceName(did: String): String =
            App.instance.getDeviceName(deviceList.firstOrNull { it.did == did })

    /**
     * 解绑设备，删除跟该设备相关的数据
     */
    fun unbindDevice(did: String) {
        deviceList.removeAll { it.did == did }

        onlineDevicePoint.remove(did)
        clockAlarmMap.remove(did)

        for ((groupEntity, didList) in groupDeviceMap) {
            if (didList.contains(did)) {
                groupEntity.clockEntityList.removeAll { it.device.did == did }
            }
        }

        RxBus.getInstance().post(REFRESH_DEVICE_LIST, "")
        RxBus.getInstance().post(REFRESH_GROUP_LIST, "")
        RxBus.getInstance().post(REFRESH_ALARM_LIST, "")
    }

    /**
     * 刷新设备列表
     * 如果列表中有待绑定的设备就去执行绑定操作
     */
    fun refreshDeviceList(list: MutableList<GizWifiDevice>) {

        //绑定 和 待绑定的设备列表
        val bindList = list.filter { it.isBind }

        deviceList.clear()
        deviceList.addAll(bindList)

        RxBus.getInstance().post(REFRESH_DEVICE_LIST, "")
        LogUtils.d(TAG, "设备列表发生变化，刷新 $bindList")

        //订阅 未曾订阅过的设备 或是待绑定的设备
        val unSubscribeList = bindList.filter { !it.isSubscribed }
        for (gizWifiDevice in unSubscribeList) {
            LogUtils.d(TAG, "订阅设备 $gizWifiDevice")

            gizWifiDevice.listener = ParseDataService.deviceListener
            gizWifiDevice.setSubscribe(BuildConfig.PRODUCT_SECRET, true)
        }

        if (bindList.isEmpty()) {
            LogUtils.d(TAG, "列表为空，清除数据点列表和闹铃列表")
            //绑定设备的列表为空
            onlineDevicePoint.clear()
            clockAlarmMap.clear()

            RxBus.getInstance().post(REFRESH_ALARM_LIST, "")
        } else {
            requestOfflineDeviceAlarm()
        }
    }


    /**
     * 查找所有离线设备的闹钟
     */
    private fun requestOfflineDeviceAlarm() {
        LogUtils.d(TAG, "查找所有离线设备的闹钟")

        val offlineDeviceList = deviceList
                //离线设备
                .filter {
                    it.netStatus == GizWifiDeviceNetStatus.GizDeviceOffline ||
                            it.netStatus == GizWifiDeviceNetStatus.GizDeviceUnavailable
                }
                //并且没有获取过
                .filter {
                    clockAlarmMap[it.did] == null
                }

        //没有未获取的离线设备
        if (offlineDeviceList.isEmpty()) {
            LogUtils.d(TAG, "没有未获取的离线设备，返回")
            return
        } else {
            LogUtils.d(TAG, "存在未获取的离线设备:$offlineDeviceList")
        }

        findClockByDidList(offlineDeviceList)
    }

    private fun findClockByDidList(didList: List<GizWifiDevice>) {
        Observable.fromIterable(didList)
                .flatMap { findOfflineAlarmByDevice(it) }
                .subscribeOn(Schedulers.io())
                .subscribe({
                    LogUtils.d(TAG, "离线设备 ${it.device.did} 的闹钟数据： $it")

                    clockAlarmMap[it.device.did] = it

                    findGroupEntityByDid(it.device.did)?.let { groupEntity ->
                        updateClockEntityListByDidList(groupEntity, listOf(it.device.did))
                        RxBus.getInstance().post(REFRESH_GROUP, groupEntity.id)
                    }
                    RxBus.getInstance().post(REFRESH_ALARM_SINGLE, it.device.did)
                }, {
                    LogUtils.e(TAG, "获取离线设备的闹钟数据出错")
                    it.printStackTrace()
                })
    }


    /**
     *  通过离线设备查找闹钟
     */
    fun findOfflineAlarmByDevice(device: GizWifiDevice): Observable<ClockEntity> {
        return HttpRequest.getLatestDevicePoint(device.did)
                .filter { it.error_code == 0 }
                .map { entity ->

                    val clock = ClockEntity()
                    clock.device = device
                    clock.timeZone = entity.attr.time_zone
                    clock.isClockSwitch = entity.attr.alarm_state == 1

                    //主闹钟
                    clock.mainAlarm = entity.attr.getMainAlarm(device.did)

                    //只取开启和关闭状态的子闹铃
                    clock.alarmList = (1..12)
                            .map { entity.attr.getSubAlarm(it, device.did) }
                            .filter { it.state == 1 || it.state == 2 }
                            .sortedBy {
                                val hour = if (it.hour == 0)
                                    24 else it.hour

                                hour * 60 + it.minute
                            }

                    clock
                }
    }


    /**
     * 刷新分组列表
     */
    fun refreshAllGroup() {
        LogUtils.e(TAG, "刷新分组列表")

        HttpRequest.findAllGroup()
                .flatMap { groupList ->
                    //删除本地存在但是远程不存在的分组
                    val remoteGroupIdList = groupList.map { it.id }

                    groupDeviceMap.keys
                            .filter {
                                //远端不存在这个id
                                !remoteGroupIdList.contains(it.id)
                            }
                            .forEach {
                                groupDeviceMap.remove(it)
                            }

                    groupTotal = groupList.size

                    currGroup = 0

                    //遍历获取每一个 group
                    Observable.fromIterable(groupList)
                }
                .subscribe({ groupEntity ->
                    refreshGroupDeviceByGroup(groupEntity)
                }, {
                    LogUtils.e(TAG, "获取全部分组信息出错")
                    it.printStackTrace()
                })
    }

    /**
     *分组的总数
     */
    private var groupTotal = 0
    private var currGroup = 0


    /**
     * 刷新分组下的设备
     */
    fun refreshGroupDeviceByGroup(groupEntity: GroupEntity) {
        LogUtils.e(TAG, "刷新分组下的设备列表 $groupEntity")
        HttpRequest
                .getGroupDevice(groupEntity.id)
                .subscribe(
                        {
                            //分组的设备did列表
                            val didList = it.map { it.did }.toMutableList()

                            //原来的分组
                            val oriGroupEntity = groupDeviceMap.keys.firstOrNull { it.id == groupEntity.id }
                                    ?: groupEntity

                            //没有的话，说明是新建的群主
                            if (oriGroupEntity.product_key.isNullOrEmpty()) {
                                oriGroupEntity.created_at = groupEntity.created_at
                                oriGroupEntity.product_key = groupEntity.product_key
                                oriGroupEntity.updated_at = groupEntity.updated_at
                                oriGroupEntity.verbose_name = groupEntity.verbose_name
                            }

                            //在groupEntity中删除没在did列表里面的数据
                            oriGroupEntity.clockEntityList?.removeAll {
                                !didList.contains(it.device.did)
                            }

                            //更新分组下面的闹钟实体
                            updateClockEntityListByDidList(oriGroupEntity, didList)

                            groupDeviceMap.put(oriGroupEntity, didList)
                            LogUtils.e(TAG, "groupEntity = $oriGroupEntity ; didList = $didList")
                            RxBus.getInstance().post(REFRESH_GROUP, oriGroupEntity.id)

                            currGroup++

                            //所以的数据都请求完了，刷新分组的列表
                            if (groupTotal == currGroup) {
                                RxBus.getInstance().post(REFRESH_GROUP_LIST, "")
                            }
                        },
                        {
                            LogUtils.e(TAG, "获取某个分组的设备信息出错")
                            it.printStackTrace()
                        }
                )
    }

    fun getGroupId(did: String): String = getGroup(did)?.id ?: "0"

    fun getGroupName(did: String) = getGroup(did)?.group_name ?: "UnGrouped"

    /**
     * 通过设备的 did 获取分组信息
     */
    private fun getGroup(did: String): GroupEntity? {
        for ((groupEntity, didList) in groupDeviceMap) {
            if (didList.contains(did)) {
                return groupEntity
            }
        }

        return null
    }

    fun getAllAlarmClock() = clockAlarmMap.values.toMutableList()

    fun getAllAlarmDetails(): MutableList<AlarmDetailsEntity> {
        val detailsAlarm = mutableListOf<AlarmDetailsEntity>()

        for (clockEntity in getAllAlarmClock()) {
            detailsAlarm.add(clockEntity.mainAlarm)
            detailsAlarm.addAll(clockEntity.alarmList.filter { it.state != 3 && it.state!=0 })
        }
        LogUtils.e(TAG, "所有的闹铃 $detailsAlarm")

        detailsAlarm.sortedBy {
            val hour = if (it.hour == 0)
                24 else it.hour

            hour * 60 + it.minute
        }

        return detailsAlarm
    }

    /**
     * 通过did查找groupEntity
     */
    fun findGroupEntityByDid(did: String): GroupEntity? {

        for ((groupEntity, valueList) in groupDeviceMap) {
            if (valueList.contains(did)) {
                return groupEntity
            }
        }

        return null
    }

    /**
     *更新分组下面的闹钟实体
     */
    fun updateClockEntityListByDidList(groupEntity: GroupEntity, didList: List<String>) {
        var clockEntityList = groupEntity.clockEntityList
        if (clockEntityList == null) {
            clockEntityList = mutableListOf()
            groupEntity.clockEntityList = clockEntityList
        } else {
            //删除did对应的闹钟实体
            clockEntityList.removeAll {
                didList.contains(it.device.did)
            }
        }

        didList.forEach { deviceDid ->
            clockAlarmMap[deviceDid]?.let {
                clockEntityList.add(it)
            }
        }
    }

    fun findAllGroup(): MutableList<GroupEntity> = groupDeviceMap.keys.toMutableList()

    fun deleteGroupEntity(groupId: String?) {
        groupId?.let {
            groupDeviceMap
                    .filter { it.key.id == groupId }
                    .forEach {
                        groupDeviceMap.remove(it.key)
                        RxBus.getInstance().post(REFRESH_GROUP, groupId)
                    }
        }
    }

}